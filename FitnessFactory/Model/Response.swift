//
//  Response.swift
//  SportCoach
//
//  Created by iroid on 01/01/21.
//

import Foundation
import ObjectMapper

class Response: Mappable{
    var success: String?
    var message: String?
    var isLinkRequest : Bool?
    var loginResponse: LoginResponse?
    var myAccountResponse: MyAccountResponse?
    var nearByGymResponse:[NearByGymResponse]?
    var gymCategory:[GymCategoryResponse]?
    var metaResponse: Meta?
    var metaDataResponse: MetaData?
    var gymLinkStatusResponse:GymLinkStatusResponse?
    var eventListResponse:[EventListResponse]?
    var nutritionListResponse:[NutritionListResponse]?
    var workoutListResponse:[WorkoutListResponse]?
    var workoutDayResponse:[WorkoutDayResponse]?
    var recipeDetailResponse:NutritionListResponse?
    var nutritionRecipeListResponse:NutritionRecipeListResponse?
    var workoutSetResponse:[WorkoutSetResponse]?
    var pickWorkoutResponse:[pickWorkoutResponse]?
    var pickNutritionResponse:[PickNutrtionResponse]?
    var yourMealListResponse :[MealListResponse]?
    var selectmeal:MealListResponse?
    var gymOfferResponse:[GymOfferResponse]?
    var workoutResponse:[WorkoutResponse]?
    var addWorkoutResponse:WorkoutResponse?
    var workoutDetailsResponse:WorkoutDetailsResponse?
    var pickWorkoutDetailsResponse:PickWorkoutDetailsResponse?
    var workoutExerciseResponse:[WorkoutExerciseResponse]?
    var workoutExerciseEetListResponse:[WorkoutExerciseEetListResponse]?
    var gymResponse:GymResponse?
    var selectWorkoutDayresponse:[SelectWorkoutDayResponse]?
    var gifListResponse:[GifListResponse]?
    var workoutMultipalExerciseResponse:[WorkoutExerciseMultipleSetListResponse]?
    var pickWorkoutExerciseListResponse:[PickWorkoutExerciseListResponse]?
    var mealItemSearchResponse:[MealItemSearchResponse]?
    var storeMealItemListResponse:[StoreMealItemListResponse]?
    var mealRecipeListResponse:[MealRecipeListResponse]?
    var selectrecipe: MealRecipeListResponse?
    var mealItemListResponse:[MealItemListResponse]?
    var itemListResponse:[ItemListResponse]?
    var searchExerciseResponse:[SearchExerciseResponse]?
    var storeRecipeItemListResponse:[StoreRecipeItemListResponse]?
    var recipeDetailListResponse:[RecipeDetailResponse]?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        success <- map["success"]
        message <- map["message"]
        loginResponse <- map["data"]
        myAccountResponse <- map["data"]
        nearByGymResponse <- map["data"]
        gymCategory <- map["data"]
        metaResponse <- map["meta"]
        gymLinkStatusResponse <- map["data"]
        eventListResponse <- map["data"]
        nutritionListResponse <- map["data"]
        workoutListResponse  <- map["data"]
        isLinkRequest <- map["is_link_request"]
        workoutDayResponse  <- map["data"]
        recipeDetailResponse  <- map["data"]
        workoutSetResponse <- map["data"]
        pickWorkoutResponse <- map["data"]
        pickWorkoutDetailsResponse  <- map["data"]
        pickNutritionResponse  <- map["data"]
        yourMealListResponse  <- map["data"]
        gymOfferResponse <- map["data"]
        workoutResponse <- map["data"]
        workoutDetailsResponse <- map["data"]
        workoutExerciseResponse <- map["data"]
        workoutExerciseEetListResponse <- map["data"]
        gymResponse <- map["gym_data"]
        selectWorkoutDayresponse  <- map["data"]
        gifListResponse  <- map["data"]
        workoutMultipalExerciseResponse  <- map["data"]
        pickWorkoutExerciseListResponse  <- map["data"]
        mealItemSearchResponse  <- map["data"]
        metaDataResponse   <- map["meta"]
        storeMealItemListResponse  <- map["data"]
        mealRecipeListResponse  <- map["data"]
        selectmeal  <- map["data"]
        mealItemListResponse  <- map["data"]
        itemListResponse <- map["data"]
        searchExerciseResponse  <- map["data"]
        nutritionRecipeListResponse  <- map["data"]
        storeRecipeItemListResponse <- map["data"]
        addWorkoutResponse <- map["data"]
        recipeDetailResponse <- map["data"]
        selectrecipe <- map["data"]
        recipeDetailListResponse <- map["data"]
    }
}
