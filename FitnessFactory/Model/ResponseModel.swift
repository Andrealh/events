//
//  ResponseModel.swift
//  SportCoach
//
//  Created by iroid on 01/01/21.
//

import Foundation
import ObjectMapper

class LoginResponse: Mappable{
    var id: Int?
    var email: String?
    var userName: String?
    var firstName: String?
    var lastName: String?
    var country: String?
    var city: String?
    var postalCode: String?
    var gender:Int?
    var dob:String?
    var latitude:String?
    var longtitude:String?
    var currentWeight:String?
    var currentWeightType:String?
    var goalWeight:String?
    var goalWeightType:String?
    var timeFramInDays:String?
    var workoutWeek:String?
    var routine:String?
    var fitnessGoal:String?
    var otherGoal:String?
    var auth : Auth?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        email <- map["email"]
        userName <- map["username"]
        firstName <- map["firstname"]
        lastName <- map["lastname"]
        country <- map["country"]
        city <- map["city"]
        postalCode <- map["postalcode"]
        gender <- map["gender"]
        dob <- map["dob"]
        latitude <- map["latitude"]
        longtitude <- map["longtitude"]
        currentWeight <- map["current_weight"]
        currentWeightType <- map["current_weight_type"]
        goalWeight <- map["goal_weight"]
        goalWeightType <- map["goal_weight_type"]
        timeFramInDays <- map["time_fram_in_days"]
        workoutWeek <- map["workout_week"]
        routine <- map["routine"]
        fitnessGoal <- map["fitness_goal"]
        otherGoal <- map["other_goal"]
        auth <- map["auth"]
    }    
}

class Auth: Mappable{
    public var tokenType: String?
    public var accessToken: String?
    public var refreshToken: String?
    public var expiresIn: Double?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        tokenType <- map["token_type"]
        accessToken <- map["access_token"]
        refreshToken <- map["refresh_token"]
        expiresIn <- map["expires_in"]
    }
}

class MyAccountResponse: Mappable{
    var id: Int?
    var email: String?
    var userName: String?
    var firstName: String?
    var lastName: String?
    var country: String?
    var city: String?
    var postalCode: String?
    var gender:Int?
    var dob:String?
    var latitude:String?
    var longtitude:String?
    var currentWeight:Int?
    var currentWeightType:String?
    var goalWeight:Int?
    var goalWeightType:String?
    var workoutWeek:Int?
    var routine:String?
    var fitnessGoal:String?
    var whyGoal:String?
    var supplementryGoal:String?
    var expYears:Int?
    var gym_select: Bool?
    var start_date: String?
    var end_date: String?
    var tier:String?
    var gym_data:GymResponse?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        email <- map["email"]
        userName <- map["username"]
        firstName <- map["firstname"]
        lastName <- map["lastname"]
        country <- map["country"]
        city <- map["city"]
        postalCode <- map["postalcode"]
        gender <- map["gender"]
        dob <- map["dob"]
        latitude <- map["latitude"]
        longtitude <- map["longtitude"]
        currentWeight <- map["current_weight"]
        currentWeightType <- map["current_weight_type"]
        goalWeight <- map["goal_weight"]
        goalWeightType <- map["goal_weight_type"]
        workoutWeek <- map["workout_week"]
        routine <- map["routine"]
        fitnessGoal <- map["fitness_goal"]
        whyGoal <- map["why_goal"]
        supplementryGoal <- map["supplementary_goal"]
        expYears <- map["exp_years"]
        gym_select <- map["gym_select"]
        gym_data <- map["gym_data"]
        start_date <- map["start_date"]
        end_date <- map["end_date"]
        tier <- map["tier"]
    }
}

class GymResponse: Mappable{
    var gymId: Int?
    var category: String?
    var name: String?
    var profile:String?
    var member_count: String?
    var owner_name: String?
    var owner_phone: String?
    var owner_email: String?
    var address: String?
    var latitude: String?
    var longtitude: String?
    var message: String?
    var website: String?
    var member_since: Double?
    var is_status: String?

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        gymId <- map["gym_id"]
        category <- map["category"]
        name <- map["name"]
        member_count <- map["member_count"]
        profile <- map["profile"]
        owner_name <- map["owner_name"]
        owner_phone <- map["owner_phone"]
        owner_email <- map["owner_email"]
        address <- map["address"]
        latitude <- map["latitude"]
        longtitude <- map["longtitude"]
        message <- map["message"]
        website <- map["website"]
        member_since <- map["member_since"]
        is_status <- map["is_status"]
    }
}


class NearByGymResponse: Mappable{
    var gymId: Int?
    var name: String?
    var profile:String?
    var member_count: String?
    var owner_name: String?
    var owner_phone: String?
    var owner_email: String?
    var address: String?
    var latitude: String?
    var longtitude: String?
    var message: String?
    var website : String?
    var is_status: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        gymId <- map["gym_id"]
        name <- map["name"]
        member_count <- map["member_count"]
        profile <- map["profile"]
        owner_name <- map["owner_name"]
        owner_phone <- map["owner_phone"]
        owner_email <- map["owner_email"]
        address <- map["address"]
        latitude <- map["latitude"]
        longtitude <- map["longtitude"]
        message <- map["message"]
        website <- map["website"]
        is_status <- map["is_status"]
    }
}

class GymCategoryResponse: Mappable{
    var gym_category_id: Int?
    var name: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        gym_category_id <- map["gym_category_id"]
        name <- map["name"]
    }
}

class Meta: Mappable{
    var total: Int?
    var perPage: Int?
    var currentPage: Int?
    var lastPage: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        total <- map["total"]
        perPage <- map["per_page"]
        currentPage <- map["current_page"]
        lastPage <- map["last_page"]
    }
}

class MetaData: Mappable{
    var max_results: String?
    var page_number: String?
    var total_results: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        max_results <- map["max_results"]
        page_number <- map["page_number"]
        total_results <- map["total_results"]
    }
}

class GifListResponse: Mappable{
    var type: String?
    var category: String?
    var title: String?
    var gif: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        type <- map["type"]
        category <- map["category"]
        title <- map["title"]
        gif <- map["gif"]
    }
}


class GymLinkStatusResponse: Mappable{
    
    var link_gym_id: Int?
    var gym_id: Int?
    var category: String?
    var name: String?
    var profile: String?
    var member_count: String?
    var email: String?
    var address: String?
    var latitude: String?
    var longtitude: String?
    var member_since: Double?
    var tier: String?
    var is_status: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        link_gym_id <- map["link_gym_id"]
        gym_id <- map["gym_id"]
        category <- map["category"]
        name <- map["name"]
        profile <- map["profile"]
        member_count <- map["member_count"]
        email <- map["email"]
        address <- map["address"]
        latitude <- map["latitude"]
        longtitude <- map["longtitude"]
        member_since <- map["member_since"]
        tier <- map["tier"]
        is_status <- map["is_status"]
    }
}

class EventListResponse: Mappable{
    var event_id: Int?
    var gym_id: Int?
    var title: String?
    var desc: String?
    var image: String?
    var date: String?

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        event_id <- map["event_id"]
        gym_id <- map["gym_id"]
        title <- map["title"]
        desc <- map["desc"]
        image <- map["image"]
        date <- map["date"]
    }
}

class NutritionListResponse: Mappable{
    var nutrition_recipe__id: Int?
    var meal_id: Int?
    var title: String?
    var desc: String?
    var recipe:String?
    var cal: Int?
    var image: String?
    var meal_item: [RecipeItemListResponse]?
   
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        nutrition_recipe__id <- map["nutrition_recipe__id"]
        meal_id  <- map["meal_id"]
        title <- map["title"]
        image <- map["image"]
        cal  <- map["cal"]
        recipe  <- map["recipe"]
        desc  <- map["desc"]
        meal_item  <- map["meal_item"]
    }
    }

class NutritionRecipeListResponse: Mappable{
    var nutrition_recipe__id: Int?
    var meal_id: Int?
    var title: String?
    var desc: String?
    var recipe:String?
    var cal: Int?
    var image: String?
    var nutrition_item: [MyMealItemListResponse]?
   
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        nutrition_recipe__id <- map["nutrition_recipe__id"]
        meal_id  <- map["meal_id"]
        title <- map["title"]
        image <- map["image"]
        cal  <- map["cal"]
        recipe  <- map["recipe"]
        desc  <- map["desc"]
        nutrition_item  <- map["nutrition_item"]
    }
}

class RecipeItemListResponse: Mappable{
    var meal_item_id: Int?
    var food_id: Int?
    var title: String?
    var desc: String?
   
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        meal_item_id <- map["meal_item_id"]
        food_id <- map["food_id"]
        title <- map["title"]
        desc  <- map["desc"]
    }
}

class MyMealItemListResponse: Mappable{
    var nutrition_item_id: Int?
    var nutrition_id: Int?
    var title: String?
    var desc: String?
   
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        nutrition_item_id <- map["nutrition_item_id"]
        nutrition_id <- map["nutrition_id"]
        title <- map["title"]
        desc  <- map["desc"]
    }
}


class MealRecipeListResponse: Mappable{
    var meal_recipe_id: Int?
    var meal_id: Int?
    var title: String?
    var image: String?
    var desc: String?
    var recipe:String?
    var cal: Int?
    var meal_recipe_item : StoreRecipeItemListResponse?
   
   
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        meal_recipe_id <- map["meal_recipe_id"]
        meal_id <- map["meal_id"]
        title <- map["title"]
        image <- map["image"]
        cal  <- map["cal"]
        recipe  <- map["recipe"]
        desc  <- map["desc"]
        meal_recipe_item <- map["meal_recipe_item"]
    }
}


class WorkoutListResponse: Mappable{
    var admin_workout_subcategory_id: Int?
    var admin_workout_id: Int?
    var title: String?
    var image: String?
    var sets: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        admin_workout_subcategory_id  <- map["admin_workout_subcategory_id"]
        admin_workout_id <- map["admin_workout_id"]
        title <- map["title"]
        image <- map["image"]
        sets <- map["sets"]
    }
}

class WorkoutDayResponse: Mappable{
    var calendar_id: Int?
    var date: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        calendar_id <- map["calendar_id"]
        date <- map["date"]
    }
}

class SelectWorkoutDayResponse: Mappable{
    var date: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        date <- map["date"]
    }
}



class WorkoutSetResponse: Mappable{
    var exercise: ExerciseSetResponse?
    var number: String?
    var number_type: String?
    var total_weight: String?
    var total_weight_type:  String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        exercise <- map["exercise"]
        number <- map["number"]
        number_type <- map["number_type"]
        total_weight <- map["total_weight"]
        total_weight_type <- map["total_weight_type"]
    }
}

class ExerciseSetResponse: Mappable{
    
    var workout_exercise_id: Int?
    var workout_id: String?
    var title: String?
    var desc: String?
    var image: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        workout_exercise_id <- map["workout_exercise_id"]
        workout_id <- map["workout_id"]
        title <- map["title"]
        desc <- map["desc"]
        image <- map["image"]
    }
}

class pickWorkoutResponse: Mappable{
    
    var admin_workout_id: Int?
    var category: Int?
    var title: String?
    var desc: String?
    var image: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        admin_workout_id <- map["admin_workout_id"]
        category <- map["category"]
        title <- map["title"]
        desc <- map["desc"]
        image <- map["image"]
    }
}

class PickNutrtionResponse: Mappable{
    var nutrition_id: Int?
    var category: Int?
    var title: String?
    var image: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        nutrition_id <- map["nutrition_id"]
        category <- map["category"]
        title <- map["title"]
        image <- map["image"]
    }
}

class MealListResponse: Mappable{
    var calendar_id:Int?
    var meal_id: Int?
    var title: String?
    var image: String?
    var is_admin_meal: String?
    var desc: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        calendar_id <- map["calendar_id"]
        meal_id <- map["meal_id"]
        title <- map["title"]
        image <- map["image"]
        is_admin_meal <- map["is_admin_meal"]
        desc <- map["desc"]
    }
}

class GymOfferResponse: Mappable{
    var offer_id: Int?
    var title: String?
    var image: String?
    var desc: String?
    var website: String?
    var startDate: String?
    var endDate: String?
    var is_expire: Bool?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        offer_id <- map["offer_id"]
        title <- map["title"]
        image <- map["image"]
        desc <- map["desc"]
        website <- map["website"]
        startDate <- map["startDate"]
        endDate <- map["endDate"]
        is_expire <- map["is_expire"]
    }
}

struct BreakFastItem {
    var itemName: String?
    var cal: String?
}

struct ExerciseList {
    var title: String?
    var img: String?
}

class WorkoutResponse: Mappable{
    var calendar_id:Int?
    var workout_id: Int?
    var title: String?
    var desc: String?
    var image: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        calendar_id <- map["calendar_id"]
        workout_id <- map["workout_id"]
        title <- map["title"]
        desc <- map["desc"]
        image <- map["image"]
    }
}

class WorkoutDetailsResponse: Mappable{
    
    var workout_id: Int?
    var title: String?
    var desc: String?
    var image: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        workout_id <- map["workout_id"]
        title <- map["title"]
        desc <- map["desc"]
        image <- map["image"]
    }
}

class PickWorkoutDetailsResponse: Mappable{
    var nutrition_recipe__id: Int?
    var title: String?
    var desc: String?
    var image: String?
    var recipe: String?
    var cal: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        nutrition_recipe__id <- map["nutrition_recipe__id"]
        title <- map["title"]
        desc <- map["desc"]
        image <- map["image"]
        recipe <- map["recipe"]
        image <- map["image"]
    }
}


class WorkoutExerciseResponse: Mappable{
    
    var workout_exercise_id: Int?
    var workout_id: Int?
    var title: String?
    var desc: String?
    var image: String?
    var color: String?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        workout_exercise_id <- map["workout_exercise_id"]
        workout_id <- map["workout_id"]
        title <- map["title"]
        desc <- map["desc"]
        image <- map["image"]
        color <- map["color"]
    }
}

class WorkoutExerciseEetListResponse: Mappable{
    
    var workout_exercise_set_id: Int?
    var workout_exercise_id: Int?
    var number: String?
    var number_type: String?
    var total_weight: String?
    var total_weight_type: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        workout_exercise_set_id <- map["workout_exercise_set_id"]
        workout_exercise_id <- map["workout_exercise_id"]
        number <- map["number"]
        number_type <- map["number_type"]
        total_weight <- map["total_weight"]
        total_weight_type <- map["total_weight_type"]
    }
}

class WorkoutExerciseMultipleSetListResponse: Mappable, Identifiable{
    
    var workout_exercise_id: Int?
    var workout_id: Int?
    var title: String?
    var desc: String?
    var image:String?
    var setCount: Int?
    var workoutExerciseData: WorkoutExerciseEetListResponse?
    var color : String?
    var order : Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        workout_exercise_id <- map["workout_exercise_id"]
        workout_id <- map["workout_id"]
        title <- map["title"]
        desc <- map["desc"]
        image <- map["image"]
        setCount <- map["setCount"]
        workoutExerciseData <- map["workoutExerciseData"]
        color <- map["color"]
        order <- map["order"]
    }
}


class PickWorkoutExerciseListResponse: Mappable{
    
    var admin_workout_subcategory_id: Int?
    var admin_workout_id: Int?
    var title: String?
    var desc: String?
    var sets: Int?
    var image: String?
    var workoutExerciseData: WorkoutExerciseEetListResponse?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        admin_workout_subcategory_id <- map["admin_workout_subcategory_id"]
        admin_workout_id <- map["admin_workout_id"]
        title <- map["title"]
        desc <- map["desc"]
        sets <- map["sets"]
        image <- map["image"]
        workoutExerciseData <- map["workoutExerciseData"]
    }
}

class MealItemSearchResponse: Mappable{
    
    var food_description: String?
    var food_id: String?
    var food_name: String?
    var food_type: String?
    var food_url: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        food_description <- map["food_description"]
        food_id <- map["food_id"]
        food_name <- map["food_name"]
        food_type <- map["food_type"]
        food_url <- map["food_url"]
    }
}

class StoreMealItemListResponse: Mappable{
    
    var meal_item_id: Int?
    var food_id: Int?
    var title: String?
    var desc: String?
    
    init(meal_item_id: Int?,food_id: Int?,title: String?, desc: String?) {
        self.meal_item_id = meal_item_id
        self.food_id = food_id
        self.title = title
        self.desc = desc
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        meal_item_id <- map["meal_item_id"]
        food_id <- map["food_id"]
        title <- map["title"]
        desc <- map["desc"]
    }
}

class ItemListResponse: Mappable{
    
    var myitem_id: Int?
    var food_id: Int?
    var title: String?
    var desc: String?
    
    init(myitem_id: Int?,food_id: Int?,title: String?, desc: String?) {
        self.myitem_id = myitem_id
        self.food_id = food_id
        self.title = title
        self.desc = desc
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        myitem_id <- map["myitem_id"]
        food_id <- map["food_id"]
        title <- map["title"]
        desc <- map["desc"]
    }
}

class MealItemListResponse: Mappable{
    var nutrition_item_id: Int?
    var nutrition_id: Int?
    var title: String?
    var cal: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        nutrition_item_id <- map["nutrition_item_id"]
        nutrition_id <- map["nutrition_id"]
        title <- map["title"]
        cal <- map["cal"]
    }
}

class StoreRecipeItemListResponse: Mappable{
    var meal_recipe_item_id: Int?
    var food_id: Int?
    var title: String?
    var desc: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        meal_recipe_item_id <- map["meal_recipe_item_id"]
        food_id <- map["food_id"]
        title <- map["title"]
        desc <- map["desc"]
    }
}


class SearchExerciseResponse: Mappable {

    var workoutExerciseId: Int?
    var workoutId: Int?
    var title: String?
    var desc: String?
    var image: String?

    required init?(map: Map){
    }

    func mapping(map: Map) {
        workoutExerciseId <- map["workout_exercise_id"]
        workoutId <- map["workout_id"]
        title <- map["title"]
        desc <- map["desc"]
        image <- map["image"]
    }
}

class RecipeDetailResponse: Mappable {
    
    var recipe_description: String?
    var recipe_id: String?
    var recipe_name: String?
    var recipe_url: String?
    var recipe_image: String?
    var recipe_nutrition: RecipeNutrtionInfoResponse?

    required init?(map: Map){
    }

    func mapping(map: Map) {
        recipe_description <- map["recipe_description"]
        recipe_id <- map["recipe_id"]
        recipe_name <- map["recipe_name"]
        recipe_url <- map["recipe_url"]
        recipe_nutrition <- map["recipe_nutrition"]
        recipe_image <- map["recipe_image"]
    }
}

class RecipeNutrtionInfoResponse: Mappable {
    var calories : String?
    var carbohydrate: String?
    var fat: String?
    var protein: String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        calories <- map["calories"]
        carbohydrate <- map["carbohydrate"]
        fat <- map["fat"]
        protein <- map["protein"]
    }
}

