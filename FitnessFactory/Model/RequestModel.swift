//
//  RequestModel.swift
//  SportCoach
//
//  Created by iroid on 01/01/21.
//

import Foundation
import ObjectMapper

class LoginRequest: Mappable{
    var email: String?
    var password: String?
    
    init(email: String?,password: String?) {
        self.email = email
        self.password = password
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        email <- map["email"]
        password <- map["password"]
    }
}

class CreateAccountRequest : Mappable{
    
    var firstName: String?
    var lastName: String?
    var userName: String?
    var email: String?
    var password: String?
    var passwordConfirmation: String?
    var country: String?
    var city: String?
    var postalCode: String?
    var gender: Int?
    var dob: String?
    var latitude: String?
    var longtitude: String?
    
    init(firstName: String?,lastName: String?,userName: String?,email: String?,password: String?,passwordConfirmation: String?,country: String?,city: String?,postalCode: String?,gender: Int?,dob: String?,latitude: String?,longtitude: String?) {
        self.firstName = firstName
        self.lastName = lastName
        self.userName = userName
        self.email = email
        self.password = password
        self.passwordConfirmation = passwordConfirmation
        self.country = country
        self.city = city
        self.postalCode = postalCode
        self.gender = gender
        self.dob = dob
        self.latitude = latitude
        self.longtitude = longtitude
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        firstName <- map["firstname"]
        lastName <- map["lastname"]
        userName <- map["username"]
        email <- map["email"]
        password <- map["password"]
        passwordConfirmation <- map["password_confirmation"]
        country <- map["country"]
        city <- map["city"]
        postalCode <- map["postalcode"]
        gender <- map["gender"]
        dob <- map["dob"]
        latitude <- map["latitude"]
        longtitude <- map["longtitude"]
    }
}

class EditProfileRequest : Mappable{
    
    var firstName: String?
    var lastName: String?
    var userName: String?
    var country: String?
    var city: String?
    var postalCode: String?
    var gender: Int?
    var dob: String?
    var latitude: String?
    var longtitude: String?
    
    init(firstName: String?,lastName: String?,userName: String?,country: String?,city: String?,postalCode: String?,gender: Int?,dob: String?,latitude: String?,longtitude: String?) {
        self.firstName = firstName
        self.lastName = lastName
        self.userName = userName
        self.country = country
        self.city = city
        self.postalCode = postalCode
        self.gender = gender
        self.dob = dob
        self.latitude = latitude
        self.longtitude = longtitude
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        firstName <- map["firstname"]
        lastName <- map["lastname"]
        userName <- map["username"]
        country <- map["country"]
        city <- map["city"]
        postalCode <- map["postalcode"]
        gender <- map["gender"]
        dob <- map["dob"]
        latitude <- map["latitude"]
        longtitude <- map["longtitude"]
    }
}
class ForgetPasswordRequest : Mappable{
    var email: String?
    
    init(email: String?) {
        self.email = email
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        email <- map["email"]
    }
}
class resendEmailRequest : Mappable{
    var email: String?
    
    init(email: String?) {
        self.email = email
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        email <- map["email"]
    }
}
class YourMealListRequest : Mappable{
    var search: String?
    
    init(search: String?) {
        self.search = search
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        search <- map["search"]
    }
}

class HomeAddGymRequest : Mappable{
    var latitude: String?
    var longitude:String?
    var category:String?
    var search:String?
    init(latitude: String?,longitude:String?,category:String?,search:String) {
        self.latitude = latitude
        self.longitude = longitude
        self.category = category
        self.search = search
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        latitude <- map["latitude"]
        longitude <- map["longtitude"]
        category <- map["category"]
        search <- map["search"]
    }
}

class ChangePasswordRequest : Mappable{
    var oldPassword: String?
    var password:String?
    var passwordConfirmation:String?
    
    init(oldPassword: String?,password:String?,passwordConfirmation:String?) {
        self.oldPassword = oldPassword
        self.password = password
        self.passwordConfirmation = passwordConfirmation
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        oldPassword <- map["old_password"]
        password <- map["password"]
        passwordConfirmation <- map["password_confirmation"]
    }
}

class OtherGoalRequest : Mappable{
    var whyGoal: String?
    var supplementaryGoal:String?
    
    init(whyGoal: String?,supplementaryGoal:String?) {
        self.whyGoal = whyGoal
        self.supplementaryGoal = supplementaryGoal
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        whyGoal <- map["why_goal"]
        supplementaryGoal <- map["supplementary_goal"]
    }
}

class WorkoutListRequest : Mappable{
    var date: String?
    
    init(date: String?) {
        self.date = date
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        date <- map["date"]
    }
}

class addToMyMealRequest : Mappable{
    var nutrition_id: Int?
    
    init(nutrition_id: Int?) {
        self.nutrition_id = nutrition_id
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        nutrition_id <- map["nutrition_id"]
    }
}

class workoutScheduleDateRequest : Mappable{
    var id: Int?
    
    init(id: Int?) {
        self.id = id
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
    }
}

class MealScheduleRequest : Mappable{
    var date: String?
    var meal_id:Int?
    
    init(date: String?,meal_id:Int?) {
        self.date = date
        self.meal_id = meal_id
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        date <- map["date"]
        meal_id  <- map["meal_id"]
    }
}

class workoutScheduleRequest : Mappable{
    var date: String?
    var workout_id:Int?
    
    init(date: String?,workout_id:Int?) {
        self.date = date
        self.workout_id = workout_id
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        date <- map["date"]
        workout_id  <- map["workout_id"]
    }
}

class removeWorkoutRequest : Mappable{
    var type: String?
    var calendar_id:Int?
    
    init(type: String?,calendar_id:Int?) {
        self.type = type
        self.calendar_id = calendar_id
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        type <- map["type"]
        calendar_id  <- map["calendar_id"]
    }
}

class WeightGoalRequest : Mappable{
    var currentWeight: String?
    var currentWeightType:String?
    var goalWeight: String?
    var goalWeightType:String?
    var expYears: String?
    var workout_week:String?
    var start_date:String?
    var end_date: String?
    var routine: String?
    
    init(currentWeight: String?,currentWeightType:String?,goalWeight: String?,goalWeightType:String?,expYears: String?,workout_week:String?,start_date:String?,end_date: String?,routine: String?) {
        self.currentWeight = currentWeight
        self.currentWeightType = currentWeightType
        self.goalWeight = goalWeight
        self.goalWeightType = goalWeightType
        self.expYears = expYears
        self.workout_week = workout_week
        self.start_date = start_date
        self.end_date = end_date
        self.routine = routine
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        currentWeight <- map["current_weight"]
        currentWeightType <- map["current_weight_type"]
        goalWeight <- map["goal_weight"]
        goalWeightType <- map["goal_weight_type"]
        expYears <- map["exp_years"]
        workout_week <- map["workout_week"]
        start_date <- map["start_date"]
        end_date <- map["end_date"]
        routine <- map["routine"]
    }
}

class AddWorkoutRequest : Mappable{
    var title: String?
   // var desc: String?
    
    init(title: String?) {
        self.title = title
        //self.desc = desc
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        title <- map["title"]
     //   desc <- map["desc"]
    }
}

class EditWorkoutRequest : Mappable{
    var id: String?
    var title: String?
    var desc: String?
    
    init(id: String?, title: String?, desc: String?) {
        self.id = id
        self.title = title
        self.desc = desc
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        desc <- map["desc"]
    }
}

class AddWorkoutExerciseRequest : Mappable{
    var workout_id: String?
    var title: String?
    var desc: String?
    var color: String?
    init(workout_id: String? ,title: String?, desc: String?, color: String?) {
        self.workout_id = workout_id
        self.title = title
        self.desc = desc
        self.color = color
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        workout_id <- map["workout_id"]
        title <- map["title"]
        desc <- map["desc"]
        color <- map["color"]
    }
}

class AddMealRequest : Mappable{
    var title: String?
    var desc: String?
    
    
    init(title: String?,desc: String?) {
        self.title = title
        self.desc = desc
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        title <- map["title"]
        desc <- map["desc"]
    }
}

class AddRecipeRequest : Mappable{
    var title: String?
    var desc: String?
    var meal_id: String?
    
    
    init(title: String?,desc: String?,meal_id: String?) {
        self.title = title
        self.desc = desc
        self.meal_id = meal_id
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        title <- map["title"]
        desc <- map["desc"]
        meal_id <- map["meal_id"]
    }
}

class EditMealRequest : Mappable{
    var title: String?
    var id: String?
    var desc: String?
    
    init(title: String?,id:String,desc:String?) {
        self.title = title
        self.id = id
        self.desc = desc
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        title <- map["title"]
        id <- map["id"]
        desc <- map["desc"]
    }
}

class EditRecipeRequest : Mappable{
    var title: String?
    var id: String?
    var desc: String?
    var recipe: String?
    
    init(title: String?,id:String,desc: String?,recipe:String) {
        self.title = title
        self.id = id
        self.desc = desc
        self.recipe = recipe
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        title <- map["title"]
        id <- map["id"]
        desc <- map["desc"]
        recipe <- map["recipe"]
    }
}

class MealItemSearchListRequest : Mappable{
    var search: String?
    var max_results: Int?
    
    init(search: String?,max_results:Int) {
        self.search = search
        self.max_results = max_results
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        search <- map["search"]
        max_results <- map["max_results"]
    }
}

class orderRequest : Mappable, Encodable{
    var workout_id: Int?
    var workout_exercises_new_order: [workout_exercises_new_order]?

   
    init(workout_exercises_new_order: [workout_exercises_new_order]?,workout_id: Int?){
        self.workout_id = workout_id
        self.workout_exercises_new_order = workout_exercises_new_order
    }
    required init?(map: Map) {
        
    }
    func mapping(map: Map){
        workout_id <- map["workout_id"]
        workout_exercises_new_order <- map["workout_exercises_new_order"]
       
    }
}

struct workout_exercises_new_order: Codable, Mappable{
    var id: Int?
    var order: Int?
    init(id: Int?, order : Int?) {
        self.id = id
        self.order = order
    }
    
   init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        order <- map["order"]
    }
}


class AddWorkoutExerciseSetRequest : Mappable{
    var workout_id: String?
    var name: String?
    var number: String?
    var number_type: String?
    var total_weight: String?
    var total_weight_type: String?
    var no_sets: String?
    var desc: String?
    var color: String?
    var order: String?
    init(workout_id: String?, name: String?,number: String?, number_type: String?,total_weight: String?, total_weight_type: String?,no_sets: String?, desc: String?, color: String?, order : String?) {
        self.workout_id = workout_id
        self.name = name
        self.number = number
        self.number_type = number_type
        self.total_weight = total_weight
        self.total_weight_type = total_weight_type
        self.no_sets = no_sets
        self.desc = desc
        self.color = color
        self.order = order
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        workout_id <- map["workout_id"]
        name <- map["name"]
        number <- map["number"]
        number_type <- map["number_type"]
        total_weight <- map["total_weight"]
        total_weight_type <- map["total_weight_type"]
        no_sets <- map["no_sets"]
        desc <- map["desc"]
        color <- map["color"]
        order <- map["order"]
    }
}


class EditWorkoutExerciseSetRequest : Mappable{
    
    var workout_exercise_id: String?
    var name: String?
    var number: String?
    var number_type: String?
    var total_weight: String?
    var total_weight_type: String?
    var no_sets: String?
    var desc: String?
    var color : String?
    init(workout_exercise_id: String?, name: String?,number: String?, number_type: String?,total_weight: String?, total_weight_type: String?,no_sets: String?, desc: String?,color:String?) {
        self.workout_exercise_id = workout_exercise_id
        self.name = name
        self.number = number
        self.number_type = number_type
        self.total_weight = total_weight
        self.total_weight_type = total_weight_type
        self.no_sets = no_sets
        self.desc = desc
        self.color = color
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        workout_exercise_id <- map["workout_exercise_id"]
        name <- map["name"]
        number <- map["number"]
        number_type <- map["number_type"]
        total_weight <- map["total_weight"]
        total_weight_type <- map["total_weight_type"]
        no_sets <- map["no_sets"]
        desc <- map["desc"]
        color <- map["color"]
    }
}


class StoreMealItemRequest : Mappable{
    var meal_id: Int?
    var food_id: Int?
    var title: String?
    var desc: String?

    
    init(meal_id: Int?, food_id: Int?,title: String?, desc: String?) {
        self.meal_id = meal_id
        self.food_id = food_id
        self.title = title
        self.desc = desc
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        meal_id <- map["meal_id"]
        food_id <- map["food_id"]
        title <- map["title"]
        desc <- map["desc"]
    }
}

class StoreRecipeItemRequest : Mappable{
    var meal_recipe_id: Int?
    var food_id: Int?
    var title: String?
    var desc: String?

    
    init(meal_recipe_id: Int?, food_id: Int?,title: String?, desc: String?) {
        self.meal_recipe_id = meal_recipe_id
        self.food_id = food_id
        self.title = title
        self.desc = desc
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        meal_recipe_id <- map["meal_recipe_id"]
        food_id <- map["food_id"]
        title <- map["title"]
        desc <- map["desc"]
    }
}

class StoreRecipeRequest : Mappable{
    var meal_id: Int?
    var title: String?
    var image: String?
    var desc: String?
    var recipe:String?
    var cal:String?

    
    init(meal_id: Int?, image: String?,title: String?, desc: String?, recipe:String?, cal:String?) {
        self.meal_id = meal_id
        self.image = image
        self.title = title
        self.desc = desc
        self.recipe = recipe
        self.cal = cal
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        meal_id <- map["meal_id"]
        image <- map["image"]
        title <- map["title"]
        desc <- map["desc"]
        recipe <- map["recipe"]
        cal <- map["cal"]
    }
}


class StoreItemRequest : Mappable{
    var food_id: Int?
    var title: String?
    var desc: String?

    
    init(food_id: Int?,title: String?, desc: String?) {
        self.food_id = food_id
        self.title = title
        self.desc = desc
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        food_id <- map["food_id"]
        title <- map["title"]
        desc <- map["desc"]
    }
}

class SearchWorkoutRequest : Mappable{
    var id: Int?
    var search: String?
    
    init(id: Int?,search: String?) {
        self.id = id
        self.search = search
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        search <- map["search"]
    }
}
