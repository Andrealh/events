//
//  OfferCell.swift
//  FitnessFactory
//
//  Created by iroid on 16/07/21.
//

import UIKit

class OfferCell: UITableViewCell {

    @IBOutlet weak var offerView: dateSportView!
    @IBOutlet weak var offerImageView: dateSportImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var disableView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var item : GymOfferResponse? {
        didSet {
            titleLabel.text = item?.title
            subTitleLabel.text = item?.desc
            Utility.setImage(item?.image, imageView: offerImageView)
            if item!.is_expire ?? true {
                disableView.isHidden = false
            } else {
                disableView.isHidden = true
            }
            offerView.layer.borderColor = UIColor.primaryColour.cgColor
        }
    }
}
