//
//  EventsTableViewCell.swift
//  FitnessFactory
//
//  Created by iroid on 18/06/21.
//

import UIKit

class EventsTableViewCell: UITableViewCell {

    @IBOutlet weak var eventImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var item : EventListResponse? {
        didSet {
            Utility.setImage(item?.image, imageView:  eventImageView)
            titleLabel.text = item?.title
            eventImageView.isHidden = false
        }
    }
}
