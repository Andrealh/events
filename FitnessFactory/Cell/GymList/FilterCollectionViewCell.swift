//
//  FilterCollectionViewCell.swift
//  FitnessFactory
//
//  Created by iroid on 21/06/21.
//

import UIKit

class FilterCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var categoryView: UIView!
    @IBOutlet weak var filterNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    var item: GymCategoryResponse?{
        didSet{
            filterNameLabel.text = item?.name
        }
    }
}
