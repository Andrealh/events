//
//  genderOptionCell.swift
//  FitnessFactory
//
//  Created by iroid on 07/06/21.
//

import UIKit

class genderOptionCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var sepratorLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var item : SearchExerciseResponse? {
        didSet {
            titleLabel.text = item?.title
        }
    }
    
}
