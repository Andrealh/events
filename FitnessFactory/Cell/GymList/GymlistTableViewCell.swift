//
//  GymlistTableViewCell.swift
//  FitnessFactory
//
//  Created by iroid on 17/06/21.
//

import UIKit

class GymlistTableViewCell: UITableViewCell {

    @IBOutlet weak var gymImageView: UIImageView!
    @IBOutlet weak var gymNameLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var mailLabel: UILabel!
    @IBOutlet weak var websiteLabel: UILabel!
    
    @IBOutlet weak var gymCloseAndOpenImageView: UIImageView!
    var onCall : (() -> Void)?
    var onMail : (() -> Void)?
    var onWebsite : (() -> Void)?
    var onLink:(()->Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func onCall(_ sender: UIButton) {
        self.onCall!()
    }
    
    @IBAction func onMail(_ sender: UIButton) {
        self.onMail!()
    }
    
    @IBAction func onWebsite(_ sender: UIButton) {
        self.onWebsite!()
    }
    
    @IBAction func onLink(_ sender: UIButton) {
        self.onLink!()
    }
    
    var item: NearByGymResponse?{
        didSet{
            if item?.is_status == "Close"{
                gymCloseAndOpenImageView.image = UIImage(named: "close")
            }else{
                gymCloseAndOpenImageView.image = UIImage(named: "open")
            }
            Utility.setImage(item?.profile, imageView:  gymImageView)
            self.gymNameLabel.text = item?.name
            self.phoneNumberLabel.text = item?.owner_phone
            self.websiteLabel.text = item?.website
            self.mailLabel.text = item?.owner_email
        }
    }
}
