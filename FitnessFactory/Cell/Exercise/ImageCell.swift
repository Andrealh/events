//
//  ImageCell.swift
//  FitnessFactory
//
//  Created by iroid on 30/07/21.
//

import UIKit

class ImageCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    var item : GifListResponse? {
        didSet {
            Utility.setImage(item?.gif, imageView: imageView)
        }
    }
}
