//
//  ExerciseCell.swift
//  fitness_factory
//
//  Created by iroid on 23/06/21.
//

import UIKit

class ExerciseCell: UITableViewCell {

    @IBOutlet weak var exerciseImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var item : ExerciseList? {
        didSet {
            titleLabel.text = item?.title
            exerciseImage.image = UIImage(named: item?.img ?? "")
        }
    }
}
