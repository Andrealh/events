//
//  ExcercisesCell.swift
//  FitnessFactory
//
//  Created by iroid on 18/06/21.
//

import UIKit

class ExcercisesCell: UITableViewCell {

    @IBOutlet weak var cellView: dateSportView!
    @IBOutlet weak var workoutImageView: UIImageView!
    @IBOutlet weak var workoutNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var item : WorkoutListResponse?{
        didSet {
            workoutNameLabel.text = item?.title
            Utility.setImage(item?.image, imageView: workoutImageView )
            cellView.borderColor = UIColor.primaryColour
            workoutImageView.backgroundColor = UIColor.primaryColour
        }
    }
    
    var mealRecipe : MealRecipeListResponse? {
        didSet {
            workoutNameLabel.text = mealRecipe?.title
            Utility.setImage(mealRecipe?.image, imageView: workoutImageView )
            cellView.borderColor = UIColor.primaryColour
            workoutImageView.backgroundColor = UIColor.primaryColour
        }
    }
    
    var itemWorkoutResponse : WorkoutResponse?{
        didSet {
            workoutNameLabel.text = itemWorkoutResponse?.title
            Utility.setImage(itemWorkoutResponse?.image, imageView: workoutImageView )
            cellView.borderColor = UIColor.primaryColour
            workoutImageView.backgroundColor = UIColor.primaryColour
        }
    }
    
    var nutritionList : NutritionListResponse?{
        didSet {
            workoutNameLabel.text = nutritionList?.title
            Utility.setImage(nutritionList?.image, imageView: workoutImageView )
            cellView.borderColor = UIColor.primaryColour
            workoutImageView.backgroundColor = UIColor.primaryColour
        }
    }
    
    var pickWorkout : pickWorkoutResponse?{
        didSet{
            workoutNameLabel.text = pickWorkout?.title
            Utility.setImage(pickWorkout?.image, imageView: workoutImageView)
            cellView.borderColor = UIColor.primaryColour
            workoutImageView.backgroundColor =  UIColor.primaryColour
        }
    }
    
    var Workout : WorkoutResponse?{
        didSet{
            workoutNameLabel.text = Workout?.title
            Utility.setImage(Workout?.image, imageView: workoutImageView)
            cellView.borderColor = UIColor.primaryColour
            workoutImageView.backgroundColor =  UIColor.primaryColour
        }
    }
    
    var pickNutrition : PickNutrtionResponse?{
        didSet{
            workoutNameLabel.text = pickNutrition?.title
            Utility.setImage(pickNutrition?.image, imageView: workoutImageView)
            cellView.borderColor = UIColor.primaryColour
            workoutImageView.backgroundColor =  UIColor.primaryColour
        }
    }
    
    var yourMealList : MealListResponse?{
        didSet {
            workoutNameLabel.text = yourMealList?.title
            Utility.setImage(yourMealList?.image, imageView: workoutImageView)
            cellView.borderColor = UIColor.primaryColour
            workoutImageView.backgroundColor =  UIColor.primaryColour
        }
    }
    
    var exerciseList : WorkoutExerciseResponse?{
        didSet {
            workoutNameLabel.text = exerciseList?.title
            Utility.setImage(exerciseList?.image, imageView: workoutImageView)
            cellView.borderColor = UIColor.primaryColour
            workoutImageView.backgroundColor =  UIColor.primaryColour
        }
    }
}
