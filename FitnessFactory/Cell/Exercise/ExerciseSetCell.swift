//
//  ExerciseSetCell.swift
//  fitness_factory
//
//  Created by iroid on 23/06/21.
//

import UIKit

class ExerciseSetCell: UITableViewCell {

    @IBOutlet weak var setNumberLabel: UILabel!
    @IBOutlet weak var repsNumberLabel: UILabel!
    @IBOutlet weak var weightNumberLabel: UILabel!
    @IBOutlet weak var repsLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var EditButtonView: UIView!
    @IBOutlet weak var EditButton: UIButton!
    @IBOutlet weak var multiplyImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var item : WorkoutSetResponse? {
        didSet {
            repsNumberLabel.text = item?.number
            repsLabel.text = item?.number_type
            weightNumberLabel.text = item?.total_weight
            weightLabel.text = item?.total_weight_type
        }
    }
    
    var setItem : WorkoutExerciseEetListResponse? {
        didSet {
            repsNumberLabel.text = setItem?.number
            repsLabel.text = setItem?.number_type
            weightNumberLabel.text = setItem?.total_weight
            weightLabel.text = setItem?.total_weight_type
            if setItem?.number == "0" || setItem?.total_weight == "0"{
                if setItem?.number == "0"{
                    repsLabel.text = ""
                    repsNumberLabel.text = ""
                    multiplyImageView.image = UIImage.init(named: "")
                }
                else{
                    multiplyImageView.image = UIImage.init(named: "")
                    weightNumberLabel.text = ""
                    weightLabel?.text = ""
                }
            }
            else{
                repsLabel.text = "Reps"
                multiplyImageView.image = UIImage.init(named: "multiply")
                weightLabel?.text = "kgs"
            }
        }
    }
}
