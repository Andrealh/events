//
//  workoutDetailCell.swift
//  FitnessFactory
//
//  Created by iroid on 17/08/21.
//

import UIKit

class workoutDetailCell: UITableViewCell {

    @IBOutlet weak var multiplyImageView: UIImageView!
    @IBOutlet weak var execiseLabel: UILabel!
    @IBOutlet weak var numberOfSetLabel: UILabel!
    @IBOutlet weak var numberOfRepsLabel: UILabel!
    @IBOutlet weak var repLabel: UILabel!
    @IBOutlet weak var numberOfWeightLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var exerciseDescriptionLabel: UILabel!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var exImage: UIImageView!
    @IBOutlet weak var backgroundBBB : UIView!
    var onEdit: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        exerciseDescriptionLabel.sizeToFit()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var item : WorkoutExerciseMultipleSetListResponse? {
        didSet {
            let underlineAttribute = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.thick.rawValue]
            let underlineAttributedString = NSAttributedString(string: (item?.title)!, attributes: underlineAttribute)
            execiseLabel.attributedText = underlineAttributedString
            numberOfSetLabel.text = "\(item?.setCount ?? 0) set"
            numberOfRepsLabel.text = item?.workoutExerciseData?.number
            repLabel.text = item?.workoutExerciseData?.number_type
            numberOfWeightLabel.text = item?.workoutExerciseData?.total_weight
            weightLabel.text = item?.workoutExerciseData?.total_weight_type
            exerciseDescriptionLabel.text = item?.desc
            if(item?.color != nil){
                backgroundBBB.layer.borderWidth = 5.0
                backgroundBBB.layer.borderColor = UIColor(hex: (item?.color!)!)?.cgColor
                
            }else{
                backgroundBBB.layer.borderWidth = 0
            }
            if(item?.desc == "" || item?.desc == nil){
                exerciseDescriptionLabel.text = "No descritpion provided."
            }
           if let image = item?.image {
                Utility.setImage(image, imageView: exImage)
           }else{
               
           }
//            exImage.image = Utility.setImage(item?.image, imageView: exImage)
            if item?.setCount == 0{
                numberOfSetLabel.text = "1 set"
            }
            if item?.workoutExerciseData?.number == "0" || item?.workoutExerciseData?.number == nil || item?.workoutExerciseData?.total_weight_type == "0" || item?.workoutExerciseData?.total_weight_type == nil{
                if item?.workoutExerciseData?.number == "0"{
                    repLabel.text = ""
                    numberOfRepsLabel.text = ""
                    multiplyImageView.image = UIImage.init(named: "")
                }
                else{
                    multiplyImageView.image = UIImage.init(named: "")
                    numberOfWeightLabel.text = ""
                    weightLabel?.text = ""
                }
            } else {
                repLabel.text = item?.workoutExerciseData?.number_type
                multiplyImageView.image = UIImage.init(named: "multiply")
                weightLabel?.text = item?.workoutExerciseData?.total_weight_type
            }
        }
    }
    
    var pickitem : PickWorkoutExerciseListResponse? {
        didSet {
            if let image = pickitem?.image {
                 Utility.setImage(image, imageView: exImage)
            }else{
                
            }
            execiseLabel.text = pickitem?.title
            numberOfSetLabel.text = "\(pickitem?.sets ?? 0) set"
            numberOfRepsLabel.text = pickitem?.workoutExerciseData?.number
            repLabel.text = pickitem?.workoutExerciseData?.number_type
            numberOfWeightLabel.text = pickitem?.workoutExerciseData?.total_weight
            weightLabel.text = pickitem?.workoutExerciseData?.total_weight_type
            exerciseDescriptionLabel.text = pickitem?.desc
            if(item?.desc == "" || item?.desc == nil){
                exerciseDescriptionLabel.text = "No descritpion provided."
            }
            if pickitem?.sets == 0{
                numberOfSetLabel.text = ""
            }
            if pickitem?.workoutExerciseData?.number == "0" || pickitem?.workoutExerciseData?.number == nil || pickitem?.workoutExerciseData?.total_weight_type == "0" || pickitem?.workoutExerciseData?.total_weight_type == nil{
                if pickitem?.workoutExerciseData?.number == "0"{
                    repLabel.text = ""
                    numberOfRepsLabel.text = ""
                    multiplyImageView.image = UIImage.init(named: "")
                }
                else{
                    multiplyImageView.image = UIImage.init(named: "")
                    numberOfWeightLabel.text = ""
                    weightLabel?.text = ""
                }
            } else {
                repLabel.text = pickitem?.workoutExerciseData?.number_type
                multiplyImageView.image = UIImage.init(named: "multiply")
                weightLabel?.text = pickitem?.workoutExerciseData?.total_weight_type
            }
        }
    }
    
    @IBAction func onEdit(_ sender: UIButton) {
        self.onEdit!()
    }
}
extension UIColor {

        // MARK: - Initialization

        convenience init?(hex: String) {
            var hexSanitized = hex.trimmingCharacters(in: .whitespacesAndNewlines)
            hexSanitized = hexSanitized.replacingOccurrences(of: "#", with: "")

            var rgb: UInt32 = 0

            var r: CGFloat = 0.0
            var g: CGFloat = 0.0
            var b: CGFloat = 0.0
            var a: CGFloat = 1.0

            let length = hexSanitized.count

            guard Scanner(string: hexSanitized).scanHexInt32(&rgb) else { return nil }

            if length == 6 {
                r = CGFloat((rgb & 0xFF0000) >> 16) / 255.0
                g = CGFloat((rgb & 0x00FF00) >> 8) / 255.0
                b = CGFloat(rgb & 0x0000FF) / 255.0

            } else if length == 8 {
                r = CGFloat((rgb & 0xFF000000) >> 24) / 255.0
                g = CGFloat((rgb & 0x00FF0000) >> 16) / 255.0
                b = CGFloat((rgb & 0x0000FF00) >> 8) / 255.0
                a = CGFloat(rgb & 0x000000FF) / 255.0

            } else {
                return nil
            }

            self.init(red: r, green: g, blue: b, alpha: a)
        }

        // MARK: - Computed Properties

        var toHex: String? {
            return toHex()
        }

        // MARK: - From UIColor to String

        func toHex(alpha: Bool = false) -> String? {
            guard let components = cgColor.components, components.count >= 3 else {
                return nil
            }

            let r = Float(components[0])
            let g = Float(components[1])
            let b = Float(components[2])
            var a = Float(1.0)

            if components.count >= 4 {
                a = Float(components[3])
            }

            if alpha {
                return String(format: "%02lX%02lX%02lX%02lX", lroundf(r * 255), lroundf(g * 255), lroundf(b * 255), lroundf(a * 255))
            } else {
                return String(format: "%02lX%02lX%02lX", lroundf(r * 255), lroundf(g * 255), lroundf(b * 255))
            }
        }

    }
