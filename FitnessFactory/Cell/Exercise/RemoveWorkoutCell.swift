//
//  RemoveWorkoutCell.swift
//  FitnessFactory
//
//  Created by iroid on 28/08/21.
//

import UIKit

class RemoveWorkoutCell: UITableViewCell {
    @IBOutlet weak var workoutNameLabel: UILabel!
    @IBOutlet weak var workoutImageView: dateSportImageView!
    
    var onRemove : (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    var item : WorkoutResponse?{
        didSet {
            workoutNameLabel.text = item?.title
            Utility.setImage(item?.image, imageView: workoutImageView )
        }
    }
    
    var yourMealList : MealListResponse?{
        didSet {
            workoutNameLabel.text = yourMealList?.title
            Utility.setImage(yourMealList?.image, imageView: workoutImageView)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onRemove(_ sender: UIButton) {
        self.onRemove!()
    }
}
