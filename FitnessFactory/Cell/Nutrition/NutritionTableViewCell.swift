//
//  NutritionTableViewCell.swift
//  FitnessFactory
//
//  Created by iroid on 18/06/21.
//

import UIKit

class NutritionTableViewCell: UITableViewCell {

    @IBOutlet weak var backGroundView: UIView!
    @IBOutlet weak var caloryLabel: UILabel!
    @IBOutlet weak var itemNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var item: NutritionListResponse?{
        didSet{
            itemNameLabel.text = item?.title
            caloryLabel.text = "\(item?.cal ?? 0)" + " Cal"
            backGroundView.layer.borderColor = UIColor.primaryColour.cgColor
        }
    }
    
    var mealItem : MealRecipeListResponse?{
        didSet{
            itemNameLabel.text = mealItem?.title
            caloryLabel.text = "\(mealItem?.cal ?? 0)" + " Cal"
            backGroundView.layer.borderColor = UIColor.primaryColour.cgColor
        }
    }
}
