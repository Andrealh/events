//
//  RecipeListCell.swift
//  FitnessFactory
//
//  Created by iroid on 02/07/21.
//

import UIKit

class RecipeListCell: UITableViewCell {

    @IBOutlet weak var recipeNameLabel: UILabel!
    @IBOutlet weak var recipeImage: UIImageView!
    @IBOutlet weak var calLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var item : WorkoutListResponse? {
        didSet {
            Utility.setGridImage(item?.image, imageView: recipeImage)
            calLabel.text = "\(item?.sets ?? 0)" + " sets"
            recipeNameLabel.text = item?.title
        }
    }
    
}
