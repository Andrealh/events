//
//  RecipeItemListCell.swift
//  FitnessFactory
//
//  Created by iroid on 08/09/21.
//

import UIKit

class RecipeItemListCell: UITableViewCell {

    var onAdditem : (() -> Void)?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onAddItem(_ sender: UIButton) {
        self.onAdditem!()
    }
    
}
