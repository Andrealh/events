//
//  itemListCell.swift
//  FitnessFactory
//
//  Created by iroid on 20/08/21.
//

import UIKit

class itemListCell: UITableViewCell {

    var onAdditem : (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func onAddItems(_ sender: UIButton) {
        self.onAdditem!()
    }
    
}
