//
//  MealItemSearchListCell.swift
//  FitnessFactory
//
//  Created by iroid on 19/08/21.
//

import UIKit

class MealItemSearchListCell: UITableViewCell {
    @IBOutlet weak var mealItemNameLabel: UILabel!
    @IBOutlet weak var mealDescritpionLabel: UILabel!
    @IBOutlet weak var removeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var mealItem : MealItemSearchResponse? {
        didSet {
            mealItemNameLabel.text = mealItem?.food_name
            mealDescritpionLabel.text = mealItem?.food_description
        }
    }
    
    var item : ItemListResponse? {
        didSet {
            mealItemNameLabel.text = item?.title
            mealDescritpionLabel.text = item?.desc
        }
    }
    
    var recipeItem : RecipeDetailResponse? {
        didSet {
            mealItemNameLabel.text = recipeItem?.recipe_name
            mealDescritpionLabel.text = recipeItem?.recipe_description
        }
    }
}
