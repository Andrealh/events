//
//  RecipeItemCell.swift
//  FitnessFactory
//
//  Created by iroid on 02/09/21.
//

import UIKit

class RecipeItemCell: UITableViewCell {
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var itemDescriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var item : StoreRecipeItemListResponse?{
        didSet{
            itemNameLabel.text = item?.title
            itemDescriptionLabel.text = item?.desc
        }
    }
    
    var myItem : MyMealItemListResponse?{
        didSet{
            itemNameLabel.text = myItem?.title
            itemDescriptionLabel.text = myItem?.desc
        }
    }
}
