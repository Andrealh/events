//
//  SelectMealitemCell.swift
//  FitnessFactory
//
//  Created by iroid on 19/08/21.
//

import UIKit

class SelectMealitemCell: UITableViewCell {

    @IBOutlet weak var foodnameLabel: UILabel!
    @IBOutlet weak var foodDescriptionLabel: UILabel!
    
    var onRemove : (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var item : MealItemSearchResponse? {
        didSet {
            foodnameLabel.text = item?.food_name
            foodDescriptionLabel.text = item?.food_description
        }
    }
    
    var storeMealItem : StoreMealItemListResponse? {
        didSet {
            foodnameLabel.text = storeMealItem?.title
            foodDescriptionLabel.text = storeMealItem?.desc
        }
    }
    
    var myItem : ItemListResponse? {
        didSet {
            foodnameLabel.text = myItem?.title
            foodDescriptionLabel.text = myItem?.desc
        }
    }
    
    @IBAction func onRemove(_ sender: UIButton) {
        self.onRemove!()
    }
}
