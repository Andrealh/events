//
//  FitnessFactoryUIColor.swift
//  Fitness Factory
//
//  Created by Jueping Wang on 2022-05-01.
//

import Foundation
import UIKit

extension UIColor {
    
    static var calendarSwipeMenuHighEmphasisText : UIColor{
        return UIColor(named: "calendarSwipeMenuHighEmphasisText")!
    }
    
    static var calendarSwipeMenuMediumEmphasisText : UIColor{
        return UIColor(named: "calendarSwipeMenuMediumEmphasisText")!
    }
    
    static var calendarButtonTextStyle1 : UIColor{
        return UIColor(named: "calendarButtonTextStyle1")!
    }
    
    static var textfieldBackground : UIColor{
        return UIColor(named: "textfieldBackground")!
    }
    
    static var textfieldBorder : UIColor{
        return UIColor(named: "textfieldBorder")!
    }
    
    static var textfieldPlaceholder : UIColor{
        return UIColor(named: "textfieldPlaceholder")!
    }
    
    static var textfieldFloatPlaceholder : UIColor{
        return UIColor(named: "textfieldFloatPlaceholder")!
    }
    
    static var textfieldFloatPlaceholderActive : UIColor{
        return UIColor(named: "textfieldFloatPlaceholderActive")!
    }
    
    static var textfieldText : UIColor{
        return UIColor(named: "textfieldText")!
    }
    
    static var textfieldBorderActive : UIColor{
        return UIColor(named: "textfieldBorderActive")!
    }
    
    static var signUpTextStyle1 : UIColor{
        return UIColor(named: "signUpTextStyle1")!
    }
    
    static var signUpTextStyle2 : UIColor{
        return UIColor(named: "signUpTextStyle2")!
    }
    
    static var signUpButtonTextStyle2 : UIColor{
        return UIColor(named: "signUpButtonTextStyle2")!
    }
    
    static var signUpDropDownBorder : UIColor{
        return UIColor(named: "signUpDropDownBorder")!
    }
    
    static var signUpDropDownSeprator: UIColor{
        return UIColor(named: "signUpDropDownSeprator")!
    }
    
    static var profileTextStyle1: UIColor{
        return UIColor(named: "profileTextStyle1")!
    }
    
    static var profileTextStyle2: UIColor{
        return UIColor(named: "profileTextStyle2")!
    }
    
    static var profileBackground: UIColor{
        return UIColor(named: "profileBackground")!
    }
    
    static var profileButtonBackgroundStyle1: UIColor{
        return UIColor(named: "profileButtonBackgroundStyle1")!
    }
    
    static var homeActionBackground: UIColor{
        return UIColor(named: "homeActionBackground")!
    }

    static var homeButtonBackgroundStyle1: UIColor{
        return UIColor(named: "homeButtonBackgroundStyle1")!
    }

    static var homeBackground: UIColor{
        return UIColor(named: "homeBackground")!
    }

    static var homeTextStyle1: UIColor{
        return UIColor(named: "homeTextStyle1")!
    }

    static var homeTextStyle2: UIColor{
        return UIColor(named: "homeTextStyle2")!
    }
}
