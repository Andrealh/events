//
//  ViewController.swift
//  FitnessFactory
//
//  Created by iroid on 04/06/21.
//

import UIKit

class ViewController: UIViewController {
    
    //MARK:- Variables
    var splashTimer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        initializeSplashTimers()
    }
    
    //MARK:- Splash Screen
    func initializeSplashTimers(){
        splashTimer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(showNextScreen), userInfo: nil, repeats: false)
//        navigationController?.isNavigationBarHidden = true
    }
    
    @objc func showNextScreen(){
        if Utility.getUserData() == nil{
            let controller = STORYBOARD.onBoarding.instantiateViewController(withIdentifier:"OnBoardingScreen") as! OnBoardingScreen
            navigationController?.pushViewController(controller, animated: true)
//            let controller = STORYBOARD.myWorkout.instantiateViewController(withIdentifier:"ListOfImageScreen") as! ListOfImageScreen
//            navigationController?.pushViewController(controller, animated: true)
        }
        else{
            gymLinkStatusAPI()
            //            let controller = STORYBOARD.home.instantiateViewController(withIdentifier:"LinkYourGymScreen") as! LinkYourGymScreen
            //            navigationController?.pushViewController(controller, animated: true)
        }
    }
    //
    //    let controller = STORYBOARD.home.instantiateViewController(withIdentifier:"FilterScreen") as! FilterScreen
    //    navigationController?.pushViewController(controller, animated: true)
    // }
}

extension  ViewController {
    //MARK:- Gym Category API
    func gymLinkStatusAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
           // Utility.showIndecator()
            HomeAddGymService.shared.gymLinkStatus() { (statusCode, response) in
                Utility.hideIndicator()
               // if let res = response.gymLinkStatusResponse{
                    if response.gymLinkStatusResponse == nil {
                        let controller = STORYBOARD.home.instantiateViewController(withIdentifier:"LinkYourGymScreen") as! LinkYourGymScreen
                        self.navigationController?.pushViewController(controller, animated: true)
                    } else {
                        if response.isLinkRequest == true {
                            let controller = STORYBOARD.tabBar.instantiateViewController(withIdentifier:"TabBarScreen") as! TabBarScreen
                            let navVC = UINavigationController(rootViewController: controller)

                            controller.isFromLink = true
                            controller.gymDetail = response.gymLinkStatusResponse
//                            self.navigationController?.pushViewController(controller, animated: true)
                            navVC.interactivePopGestureRecognizer?.isEnabled = true
                            navVC.navigationBar.isHidden = true
                            appDelegate.window?.rootViewController = navVC
                            appDelegate.window?.makeKeyAndVisible()
                        } else {
                            let controller = STORYBOARD.tabBar.instantiateViewController(withIdentifier:"TabBarScreen") as! TabBarScreen
                            let navVC = UINavigationController(rootViewController: controller)
                            controller.isFromLink = false
                            controller.gymDetail = response.gymLinkStatusResponse
//                            self.navigationController?.pushViewController(controller, animated: true)
                            navVC.interactivePopGestureRecognizer?.isEnabled = true
                            navVC.navigationBar.isHidden = true
                            appDelegate.window?.rootViewController = navVC
                            appDelegate.window?.makeKeyAndVisible()
                        }
                    }
                //}
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}
