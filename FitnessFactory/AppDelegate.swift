//
//  AppDelegate.swift
//  FitnessFactory
//
//  Created by iroid on 04/06/21.
//

import UIKit
import IQKeyboardManagerSwift
import MapKit
import CoreLocation
import SDWebImageWebPCoder

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate {
    
    var window: UIWindow?
    let locationManager = CLLocationManager()
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
      
        self.window = UIWindow(frame: UIScreen.main.bounds)
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
       // IQKeyboardManager.shared.enableAutoToolbar = false
        locationPermistion()
        let WebPCoder = SDImageWebPCoder.shared
        SDImageCodersManager.shared.addCoder(WebPCoder)
        let vc = STORYBOARD.main.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        let navVC = UINavigationController(rootViewController: vc)
        navVC.interactivePopGestureRecognizer?.isEnabled = false
        navVC.navigationBar.isHidden = true
        self.window?.rootViewController = navVC
        self.window?.makeKeyAndVisible()
        return true
    }
    
    func locationPermistion(){
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self;
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            self.locationManager.requestAlwaysAuthorization()
            self.locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
            
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        currentLatitude = "0";
        currentLongitude = "0";
        currentLatitude = String(locValue.latitude)
        currentLongitude = String(locValue.longitude)
        print(currentLatitude)
    }
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error){
        print("error::: \(error)")
        locationManager.stopUpdatingLocation()
    }
}

