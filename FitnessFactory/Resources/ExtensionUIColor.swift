//
//  ExtensionUIColor.swift
//  Fitness Factory
//
//  Created by Jueping Wang on 2022-04-06.
//

import Foundation
import UIKit

extension UIColor {
    static var primaryColour : UIColor{
        return UIColor(named: "primaryColour")!
    }
}
