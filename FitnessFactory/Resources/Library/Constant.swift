//
//  Constant.swift
//  SportCoach
//
//  Created by iroid on 15/12/20.
//

import Foundation
import UIKit

struct STORYBOARD {
    static let main = UIStoryboard(name: "Main", bundle: Bundle.main)
    static let login = UIStoryboard(name: "LogIn", bundle: Bundle.main)
    static let signUp = UIStoryboard(name: "SignUp", bundle: Bundle.main)
    static let onBoarding = UIStoryboard(name: "OnBoarding", bundle: Bundle.main)
    static let home = UIStoryboard(name: "Home", bundle: Bundle.main)
    static let profile = UIStoryboard(name: "Profile", bundle: Bundle.main)
    static let exercise = UIStoryboard(name: "Exercise", bundle: Bundle.main)
    static let myWorkout = UIStoryboard(name: "MyWorkout", bundle: Bundle.main)
    static let tabBar = UIStoryboard(name: "TabBar", bundle: Bundle.main)
    static let calender = UIStoryboard(name: "Calender", bundle: Bundle.main)
    static let offer = UIStoryboard(name: "Offer", bundle: Bundle.main)
    static let nutrition = UIStoryboard(name: "Nutrition", bundle: Bundle.main)
    static let calIntake = UIStoryboard(name: "CaloriesIntake", bundle: Bundle.main)
    static let search = UIStoryboard(name: "Search", bundle: Bundle.main)
    
}
let textColor = "#FF5F38"
let appThemeColor = "#C7FF39"
let appUIThemeColor = UIColor(hex:textColor)
var tableViewCellHeightExercise: Int = 100
var tableViewCellHeightNutrition: Int = 100
var tableViewCellHeightEvent: Int = 100
let screenWidth = UIScreen.main.bounds.width
let screenHeight = UIScreen.main.bounds.height
let isiPad: Bool = UIDevice.current.userInterfaceIdiom == .pad
let DEVICE_UNIQUE_IDETIFICATION : String = UIDevice.current.identifierForVendor!.uuidString

var currentLatitude = ""
var currentLongitude = ""

var defaultWorkoutImage = "https://fitnessfac.ca/storage/app/public/workout/0yORGAlwvErpNHgUwcuwS2vvUnttTkZy1zLjqEQj.jpg"
var defaultMealImage = "https://fitnessfac.ca/assets/nutri.png"
var defaultRecipeImage = "https://fitnessfac.ca/assets/recipe.png"
//MARK:- API URL


//Mark:-Old
//let server_url = "https://demo.iroidsolutions.com/fitness-factory-backend/api/v1/"
//Mark:-New
 let server_url = "https://fitnessfac.ca/api/v1/"

//let server_url = "http://192.168.1.65:8100/api/v1/"
let loginURL = server_url+"login"
let nearByGymURL = server_url+"near-by-gym?page="
let mealItemListURl = server_url+"meal-item/search?page="
let searchRecipeListURl = server_url+"meal-recipe/search?page="
let updateProfileURL = server_url+"user/update-profile"
let logOutURL = server_url+"logout"
let signUpURL = server_url+"register"
let forgotPasswordURL = server_url+"forgot-password"
let ResendURL = server_url+"verification_resend"


//MARK:- User API
let myAccountURL = server_url+"my-account"
let changePasswordURL = server_url+"change-password"
let editProfileURL = server_url+"edit-profile"
let deleteProfileURL = server_url+"deactivate"
let linkGymURL = server_url+"link-gym/"
//MARK:- Goal API
let otherGoalURl = server_url+"other-goal"
let weightGoalURl = server_url+"weight-goal"
//MARK:- Filter
let gymCategoryURL = server_url+"gym-category/list"
let gymLinkStatusURL = server_url+"user/link-gym/status"
//MARK:- Home
let workoutListURl = server_url+"home/workout/list"
let exerciseListURL = server_url+"exercise/list?page="
let eventListURL = server_url+"event/list?page="
let nutritionListURL = server_url+"nutrition/list?page="
let cancelGymLinkURL = server_url+"link-gym/cancel/"
let unLinkGymURL = server_url+"gym-unlink/"
//MARK:- Calender
let calenderListURL = server_url+"calender-date/list"
let swipeGymDayURL = server_url+"its-gym-day"
let workoutListURL = server_url+"calender/workout?page="
let calenderNutritionListURL = server_url+"calender/nutrition?page="
let calenderEventListURL = server_url+"calender/event?page="
//MARK:- Nutrition
let recipeListURL = server_url+"nutrition/recipe/list/"
let myRecipeListURL = server_url+"meal-recipe/"
let storeMealItemListURL = server_url+"meal-item/list/"
let itemListURL = server_url+"my-item-list?page="
let nutritionItemListURL = server_url+"nutrition-item/list/"
let recipeItemListURL = server_url+"meal-recipe-item/list/"
let recipeDetailURL = server_url+"nutrition/recipe/detail/"
let myRecipeDetailURL = server_url+"meal-recipe/detail/"
let nutrtionTypeURL = server_url+"nutrition/list/"
let recipeDetailListURL = server_url+"meal-recipe/list/"
let yourMealListURL = server_url+"meal/list?page="
let createMealURL = server_url+"meal/store"
let createRecipeURL = server_url+"store-meal-recipe"
let mealScheduleURL = server_url+"meal-schedule/store"
let addToMyMealURL = server_url+"nutrition/my-meal/store"
let mealDeleteURL = server_url+"meal/delete/"
let recipeDeleteURL = server_url+"meal-recipe/delete/"
let editMealURL = server_url+"meal/edit"
let editRecipeURL = server_url+"meal-recipe/edit"

let mealScheduleDateURL = server_url+"meal/schedule/date/list/"
let removeMealItemURL = server_url+"meal-item/"
let removeItemFromRecipeURL = server_url+"meal-recipe-item/"
let removeItemURL = server_url+"my-item/"
let storeMealItemURL = server_url+"meal-item"
let storeItemURL = server_url+"my-item"
let storeRecipeItemURL = server_url+"meal-recipe-item"
let storeRecipeURL = server_url+"meal-recipe/store"

//MARK:- Pick Exercise
let workoutSetListURL = server_url+"workout/exercise/set/list/"
let pickWorkoutLitsURL = server_url+"workout/list/"
let pickExerciseListURL = server_url+"workout/exercise/list/"
//MARK:- Offer List
let offerListURL = server_url+"offer/list?page="

//MARK:- My workout
let myWorkoutLitsURL = server_url+"workout/list"
let addWorkoutURL = server_url+"workout/store"
let editWorkoutURL = server_url+"workout/edit"
let workoutDetailsURL = server_url+"workout/"
let pickWorkoutDetailsURL = server_url+"pick-workout/"
let workoutDeleteURL = server_url+"workout/delete/"
let removeWorkoutURL = server_url+"calender/delete"
let addToMyWorkoutURL = server_url+"exercise/store/my-workout/"
let addWorkoutExerciseURL = server_url+"workout-exercise/store"
let workoutExerciseDetailsURL = server_url+"workout-exercise/"
let workoutExerciseSearchURL = server_url + "workout-exercise-list-search"
let editWorkoutExerciseURL = server_url+"workout-exercise/edit"
let workoutExerciseDeleteURL = server_url+"workout-exercise/delete/"
let AddworkoutExercisetListURL = server_url+"workout-exercise-set/store"
let AddworkoutExerciseURL = server_url+"workout-exercise-set/multiple"
let EditworkoutExercisetListURL = server_url+"workout-exercise-set/edit/"
let EditWorkoutExerciseURL = server_url+"workout-exercise-set/multiple/edit"
let workoutExerciListURL = server_url+"workout-exercise-list/"
let workoutExercisetListURL = server_url+"workout-exercise-set-list/"
let workoutExerciMultipalSetListURL = server_url+"workout-exercise-set/multiple/"
let workoutExerciseSetDeleteURL = server_url+"workout-exercise-set/delete/"
let removeWorkoutExerciseSetDeleteURL = server_url+"workout-exercise/delete/"
let workoutScheduleURL = server_url+"workout-schedule/store"
let workoutScheduleDateURL = server_url+"workout/schedule/date/list/"
let gifListURL = server_url+"gif/list/"
let imageListURL = server_url+"image/list/"
let updateExcerciseOrder = server_url+"workout-exercise-list/update-order"

//MARK:- Type of Roles
enum BreakfastType: Int{
    case Excercises = 1
    case Nutrition = 2
    case Events = 3
}
//MARK:- Social Key
let EMAIL = "email"
let USER_IDD = "userid"
let USERNAME = "username"
let SOCIAL_provider = "socail_provider"
let APPLICATION_NAME = "Fitness Factory"
let AWS_ERROR_MESSAGE = "Something went wrong please try again"
//let USER_DATA = "user_data"

//MARK:- Post Key
let IMAGE = "image"
let POST_ID = "post_id"

//MARK:- Session Key
let USER_DATA = "user_data"
let USER_ROLE = "user_role"
let DEVICE_ID = "device_id"
//let USER_PROFILE = "user_profile"

let dayArray = ["S","M","T","W","T","F","S"]


let appDelegate = UIApplication.shared.delegate as! AppDelegate

func getFileName() -> String{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyyMMddHHmmssZ"//+UUID().uuidString//NSTimeIntervalSince1970.description
    return dateFormatter.string(from: Date())
}


enum IdentityType: String{
    case heHimhis = "1"
    case sheHerHers = "2"
    case theyThemTheirs = "3"
    case other = "4"
}

extension UIView {

    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.frame = bounds
        mask.path = path.cgPath
        self.layer.mask = mask
    }

}

