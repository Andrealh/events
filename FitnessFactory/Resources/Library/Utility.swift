//
//  Utility.swift
//  Iroid
//
//  Created by iroid on 30/03/18.
//  Copyright © 2018 iroidiroid. All rights reserved.
//

import UIKit
import AVFoundation
import CoreLocation
import SDWebImage
import AVKit
import NVActivityIndicatorView

class Utility: NSObject {
    class func showLocationAlert(vc: UIViewController, message:String) {
        let alertController = UIAlertController (title: APPLICATION_NAME, message: message, preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "OK", style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(cancelAction)
        
        vc.present(alertController, animated: true, completion: nil)
    }
    class func addShadow(view: UIView, shadowOpacity: CGFloat, shadowRadius: CGFloat, width: Int, height: Int){
        view.layer.shadowOpacity = Float(shadowOpacity)
        view.layer.shadowColor = #colorLiteral(red: 0, green: 0.8666666667, blue: 0.6, alpha: 0.5)
        view.layer.shadowRadius = shadowRadius
        view.layer.shadowOffset = .init(width: width, height: height)
    }
    
    class func showAlert(vc: UIViewController, message: String) {
        let alertController = UIAlertController(title: APPLICATION_NAME, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title:  "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        vc.present(alertController, animated: true, completion: nil)
    }
    
    class func showActionAlert(vc: UIViewController,message: String)
    {
        let alertController = UIAlertController(title: APPLICATION_NAME, message: message, preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            NSLog("OK Pressed")
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        vc.present(alertController, animated: true, completion: nil)
    }
    
    
    
    class func showNoInternetConnectionAlertDialog(vc: UIViewController) {
        
        let alertController = UIAlertController(title: APPLICATION_NAME, message: "It seems you are not connected to the internet. Kindly connect and try again", preferredStyle: .alert)
        let OKAction = UIAlertAction(title: /*Utility.getLocalizdString(value: */"OK"/*)*/, style: .default, handler: nil)
        alertController.addAction(OKAction)
        vc.present(alertController, animated: true, completion: nil)
    }
    
    class func showAlertSomethingWentWrong(vc: UIViewController) {
        let alertController = UIAlertController(title: APPLICATION_NAME, message: "Oops! Something went wrong. Kindly try again later", preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        vc.present(alertController, animated: true, completion: nil)
    }
    
    class func formattedDateFromString(dateString: String, withFormat format: String) -> String? {
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
        if let date = inputFormatter.date(from: dateString) {
            let outputFormatter = DateFormatter()
            outputFormatter.dateFormat = format
            return outputFormatter.string(from: date)
        }
        return nil
    }
    
    class func encode(_ s: String) -> String {
        let data = s.data(using: .nonLossyASCII, allowLossyConversion: true)!
        return String(data: data, encoding: .utf8)!
    }
    
    class func decode(_ s: String) -> String? {
        let data = s.data(using: .utf8)!
        return String(data: data, encoding: .nonLossyASCII)
    }
    
    
    class func getTime(date:String) -> String{
        let currentDate = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        let postDate = formatter.date(from: date)!
        let timeFromNow = currentDate.offset(from: postDate)
        return timeFromNow
    }
    
    class func getDateTimeFromTimeInterVel(from dateIntervel: Int) -> Date{
        let date = Date(timeIntervalSince1970: TimeInterval(dateIntervel))
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        return date
    }
    
    class func getLabelHeight(label:UILabel) -> (Int,Int){
        var lineCount = 0
        let textSize = CGSize(width: label.frame.size.width, height: CGFloat(MAXFLOAT))
        let rHeight = lroundf(Float(label.sizeThatFits(textSize).height))
        let charSize = lroundf(Float(label.font.lineHeight))
        lineCount = rHeight / charSize
        return (lineCount,rHeight)
    }
    
    class func checkForEmptyString(valueString: String) -> Bool {
        if valueString.isEmpty {
            return true
        }else{
            return false
        }
    }
    
    //    class func getCurrentUserId() -> String {
    //        let userDictionary = (UserDefaults.standard.object(forKey: USER_DETAILS) as? NSDictionary)!
    //        return userDictionary.object(forKey: USER_ID) as! String
    //        //        return "1"
    //    }
    
    //    class func getCurrentUserProfilePicture() -> String {
    //        let userDictionary = (UserDefaults.standard.object(forKey: USER_DETAILS) as? NSMutableDictionary)!
    //        return userDictionary.object(forKey: PROFILE_PIC) as! String
    //    }
    
//    class func showIndecator() {
//        //         AppDelegate().sharedDelegate().window?.isUserInteractionEnabled = false
//        UIApplication.shared.isNetworkActivityIndicatorVisible = true
//        //        if  UserDefaults.standard.string(forKey: LANGUAGE) == ARABIC_LANG_CODE{
//        //            SwiftLoader.show(title: "جار التحميل...", animated: true)
//        //        }else{
//        SwiftLoader.show(title: "Loading...", animated: true)
//        //        }
//
//    }
//
//    class func hideIndicator() {
//        //        AppDelegate().sharedDelegate().window?.isUserInteractionEnabled = true
//        UIApplication.shared.isNetworkActivityIndicatorVisible = false
//        SwiftLoader.hide()
//    }
    class func gradientColor(bounds: CGRect, gradientLayer :CAGradientLayer) -> UIColor? {
            UIGraphicsBeginImageContext(gradientLayer.bounds.size)
              //create UIImage by rendering gradient layer.
            gradientLayer.render(in: UIGraphicsGetCurrentContext()!)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
              //get gradient UIcolor from gradient UIImage
            return UIColor(patternImage: image!)
        }
        
        class func getGradientLayer(bounds : CGRect) -> CAGradientLayer{
            let gradient = CAGradientLayer()
            gradient.frame = bounds
            //order of gradient colors
            gradient.colors = [Utility.getUIcolorfromHex(hex: "#FF6A6A").cgColor,Utility.getUIcolorfromHex(hex: "#8A61FF").cgColor]
            // start and end points
            gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
            gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
            return gradient
        }
    
    class func showIndecator() {
        let MAIN_SCREEN_FRAME = UIScreen.main.bounds
            //         AppDelegate().sharedDelegate().window?.isUserInteractionEnabled = false
            //        UIApplication.shared.isNetworkActivityIndicatorVisible = true
            let frame = CGRect(x: (MAIN_SCREEN_FRAME.width/2 ), y: (MAIN_SCREEN_FRAME.height/2 - 30), width: 36, height: 36)

            let activityIndicator = NVActivityIndicatorView(frame: frame)
            activityIndicator.type = .circleStrokeSpin // add your type
            activityIndicator.color = #colorLiteral(red: 0.8039215686, green: 0.9490196078, blue: 0.4392156863, alpha: 1)
            activityIndicator.tag = 012
            UIApplication.shared.beginIgnoringInteractionEvents()
            UIWindow.key.addSubview(activityIndicator) // or use  webView.addSubview(activityIndicator)
            activityIndicator.startAnimating()
            //        SwiftLoader.show(title: "Loading...", animated: true)
        }
        
        class func hideIndicator() {
            //        AppDelegate().sharedDelegate().window?.isUserInteractionEnabled = true
            //        UIApplication.shared.isNetworkActivityIndicatorVisible = false
            //        SwiftLoader.hide()
            if let view = UIWindow.key.subviews.first(where: {$0.tag == 012}){
                view.removeFromSuperview()
                UIApplication.shared.endIgnoringInteractionEvents()
            }
        }
        
    
    //MARK: reachability
    class func isInternetAvailable() -> Bool
    {
        var  isAvailable : Bool
        isAvailable = true
        let reachability = Reachability()!
        if(reachability.connection == .none)
        {
            isAvailable = false
        }
        else
        {
            isAvailable = true
        }
        
        return isAvailable
        
    }
    
    class func getCurrentLanguage() -> String{
        let userdef = UserDefaults.standard
        let langArray = userdef.object(forKey: "AppleLanguages") as! NSArray
        let current = langArray.firstObject as! String
        return current
    }
    class func setLanguage(langStr:String){
        let defaults = UserDefaults.standard
        defaults.set([langStr], forKey: "AppleLanguages")
        defaults.synchronize()
        Bundle.setLanguage(langStr)
    }
    
    class func setLocalizedValuesforView(parentview: UIView ,isSubViews : Bool)
    {
        if parentview is UILabel {
            let label = parentview as! UILabel
            let titleLabel = label.text
            if titleLabel != nil {
                label.text = self.getLocalizdString(value: titleLabel!)
            }
        }
        else if parentview is UIButton {
            let button = parentview as! UIButton
            let titleLabel = button.title(for:.normal)
            if titleLabel != nil {
                button.setTitle(self.getLocalizdString(value: titleLabel!), for: .normal)
            }
        }
        else if parentview is UITextField {
            let textfield = parentview as! UITextField
            let titleLabel = textfield.text!
            if(titleLabel == "")
            {
                let placeholdetText = textfield.placeholder
                if(placeholdetText != nil)
                {
                    textfield.placeholder = self.getLocalizdString(value:placeholdetText!)
                }
                
                return
            }
            textfield.text = self.getLocalizdString(value:titleLabel)
        }
        else if parentview is UITextView {
            let textview = parentview as! UITextView
            let titleLabel = textview.text!
            textview.text = self.getLocalizdString(value:titleLabel)
        }
        if(isSubViews)
        {
            for view in parentview.subviews {
                self.setLocalizedValuesforView(parentview:view, isSubViews: true)
            }
        }
    }
    
    class func getLocalizdString(value: String) -> String
    {
        var str = ""
        let language = self.getCurrentLanguage()
        let path = Bundle.main.path(forResource: language, ofType: "lproj")
        if(path != nil)
        {
            let languageBundle = Bundle(path:path!)
            str = NSLocalizedString(value, tableName: nil, bundle: languageBundle!, value: value, comment: "")
        }
        return str
    }
    
    
    class func getUIcolorfromHex(hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    
    class  func UTCToLocal(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd H:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "HH:mm:ss"
        
        return dateFormatter.string(from: dt!)
    }
    
    class func UTCToLocal(date:String, fromFormat: String, toFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone?
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = toFormat
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        return dateFormatter.string(from: dt!)
    }
    
    class func getTimeYesterDayToday(dateValue:Date) -> String
    {
        var timeAgo = ""
        if(Calendar.current.isDateInToday(dateValue))
        {
            timeAgo = "Today"
        }
        else if(Calendar.current.isDateInYesterday(dateValue))
        {
            timeAgo = "Yesterday"
        }
        else
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"//"yyyy/dd/MM"
            let strDate = dateFormatter.string(from: dateValue)
            //print(strDate)
            timeAgo = strDate
        }
        return timeAgo
        
    }
    
    class func getTimeYesterDayTodayNew(dateValue:Date) -> String
    {
        var timeAgo = ""
        if(Calendar.current.isDateInToday(dateValue))
        {
            timeAgo = ""
        }
        else if(Calendar.current.isDateInYesterday(dateValue))
        {
            timeAgo = "Yesterday"
        }
        else
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"//"yyyy/dd/MM"
            let strDate = dateFormatter.string(from: dateValue)
            //print(strDate)
            timeAgo = strDate
        }
        return timeAgo
        
    }
    class func getFormattedDate(dateString:String) -> String{
        let date = dateString.toDate(dateFormat: "yyyy-MM-dd HH:mm:ss")
        print("Date is \(date)")
        print("\(Calendar.current.isDateInToday(date))")
        print("\(Calendar.current.isDateInYesterday(date))")
        var timeAgo :String = ""
        timeAgo = Utility.getTimeYesterDayTodayNew(dateValue: date)
        let time = Utility.UTCToLocal(date: dateString, fromFormat: "yyyy-MM-dd HH:mm:ss", toFormat: "h:mma")
        return "\(timeAgo) \(time)"
    }
    
    
    class func setImage(_ imageUrl: String!, imageView: UIImageView!) {
        if imageUrl != nil && !(imageUrl == "") {
            imageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
            //            imageView.sd_setShowActivityIndicatorView(true)
            //            imageView.sd_setIndicatorStyle(.gray)
            imageView!.sd_setImage(with: URL(string: imageUrl! ), placeholderImage: UIImage(named: "postImage"))
        }
        else
        {
            imageView?.image = UIImage(named: "default_profile_icon")
        }
    }
    
    class func setImageForExerice(_ imageUrl: String!, imageView: UIImageView!) {
        if imageUrl != nil && !(imageUrl == "") {
            imageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
            //            imageView.sd_setShowActivityIndicatorView(true)
            //            imageView.sd_setIndicatorStyle(.gray)
            imageView!.sd_setImage(with: URL(string: imageUrl! ), placeholderImage: UIImage(named: "postImage"))
        }
        else
        {
            imageView?.image = UIImage(named: "default_profile_icon")
        }
    }
    
    class func setGridImage(_ imageUrl: String!, imageView: UIImageView!) {
        if imageUrl != nil && !(imageUrl == "") {
            imageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
            //            imageView.sd_setShowActivityIndicatorView(true)
            //            imageView.sd_setIndicatorStyle(.gray)
            imageView!.sd_setImage(with: URL(string: imageUrl! ), placeholderImage: UIImage(named: "default_profile"))
        }
        else
        {
            imageView?.image = UIImage(named: "default_profile")
        }
    }
    
    class func silentModeSoundOn(){
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
        } catch {
        }
    }
    class func setMonth(strMonth: String) -> String
    {
        let strMonth=strMonth.lowercased();
        var strSpanishMonth = "" ;
        if strMonth == ("january") || strMonth == "enero" {
            strSpanishMonth="Enero"
        }
        else if strMonth == "february" || strMonth == "febrero" {
            strSpanishMonth="Febrero"
        }
        else if strMonth == "march" || strMonth == "marzo" {
            strSpanishMonth="Marzo"
        }
        else if strMonth == "april" || strMonth == "abril" {
            strSpanishMonth="Abril"
        }
        else if strMonth == "may" || strMonth == "mayo" {
            strSpanishMonth="Mayo"
        }
        else if strMonth == "june" || strMonth == "junio" {
            strSpanishMonth="Junio"
        }
        else if strMonth == "july" || strMonth == "julio" {
            strSpanishMonth="Junio"
        }
        else if strMonth == "august" || strMonth == "agosto" {
            strSpanishMonth="Agosto"
        }
        else if strMonth == "september" || strMonth == "septiembre" {
            strSpanishMonth="Septiembre"
        }
        else if strMonth == "october" || strMonth == "octubre" {
            strSpanishMonth="Octubre"
        }
        else if strMonth == "november" || strMonth == "noviembre" {
            strSpanishMonth="Noviembre"
        }
        else {
            strSpanishMonth = "Diciembre";
        }
        return strSpanishMonth;
    }
    
    class func setDay(strDay: String) -> String
    {
        let strDay=strDay.lowercased();
        var strEnglishDay = "" ;
        if strDay == "monday" {
            strEnglishDay="Monday"
        }
        else if strDay == "tuesday" {
            strEnglishDay="Tuesday"
        }
        else if strDay == "wednesday" {
            strEnglishDay="Wednesday"
        }
        else if strDay == "thursday" {
            strEnglishDay="Thursday"
        }
        else if strDay == "friday" {
            strEnglishDay="Friday"
        }
        else if strDay == "saturday" {
            strEnglishDay="Saturday"
        }
        else {
            strEnglishDay="Sunday"
        }
        return strEnglishDay;
    }
    
    
    class func addSuperScriptToLabel(label: UILabel, superScriptCount count: Int, fontSize size: CGFloat) {
        let font:UIFont? = UIFont(name: label.font!.fontName, size:size)
        let fontSuper:UIFont? = UIFont(name: label.font!.fontName, size:size/1.5)
        let attString:NSMutableAttributedString = NSMutableAttributedString(string: label.text!, attributes: [.font:font!])
        attString.setAttributes([.font:fontSuper!,.baselineOffset:10], range: NSRange(location:((label.text?.count)!-count),length:count))
        label.attributedText = attString
    }
    
    class func getCurrentLocalTime(format: String) -> String {
        let now = Date()
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = format
        return formatter.string(from: now)
    }
    
    class func makeBlueBorderToTextField(textField: UITextField) {
        let border = CALayer()
        let width = CGFloat(1)
        border.borderColor = #colorLiteral(red: 0, green: 0.6509803922, blue: 1, alpha: 1)
        border.frame = CGRect(x: 0, y: textField.frame.size.height - width, width: textField.frame.size.width , height: 1)
        border.borderWidth = width
        textField.layer.addSublayer(border)
        textField.layer.masksToBounds = true
    }
    
    class func makeGrayBorderToTextField(textField: UITextField) {
        let border = CALayer()
        let width = CGFloat(1)
        border.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        border.frame = CGRect(x: 0, y: textField.frame.size.height - width, width: textField.frame.size.width, height: 1)
        border.borderWidth = textField.frame.size.width
        textField.layer.addSublayer(border)
        textField.layer.masksToBounds = true
    }
    class func makeClearBorderToTextField(textField: UITextField) {
        let border = CALayer()
        let width = CGFloat(1)
        border.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        border.frame = CGRect(x: 0, y: textField.frame.size.height - width, width: textField.frame.size.width, height: 1)
        border.borderWidth = width
        textField.layer.addSublayer(border)
        textField.layer.masksToBounds = true
    }
    
    class func addBorderToView(view: UIView, color: UIColor,value: CGFloat){
        view.layer.borderWidth = value
        view.layer.borderColor = color.cgColor
    }
    
    class func getTimeInterval(fromDate timeImterval: Int) -> String? {
        var timeIntervalString = ""
        var timeDifference: Int = timeImterval / 86400
        timeIntervalString = "\(timeDifference)d"
        print("timeIntervalString \(timeIntervalString)")
        if timeDifference < 1 {
            timeDifference = timeImterval / 3600
            timeIntervalString = "\(timeDifference)h"
            print("timeIntervalString \(timeIntervalString)")
            if timeDifference < 1 {
                timeDifference = timeImterval / 60
                timeIntervalString = "\(timeDifference)m"
                print("timeIntervalString \(timeIntervalString)")
                if timeDifference < 1 {
                    timeIntervalString = "\(timeImterval)s"
                    print("timeIntervalString \(timeIntervalString)")
                }
            }
        }
        print("timeIntervalString \(timeIntervalString)")
        return timeIntervalString
    }
    class func getSameDate(as itemString: String?, havingDateFormatter dateFormatter: DateFormatter?) -> Date? {
        let twentyFour = NSLocale(localeIdentifier: "en_GB")
        dateFormatter?.locale = twentyFour as Locale
        let dateString = getGlobalTimeAsDate(fromDate: itemString, andWithFormat: dateFormatter)
        return dateString
    }
    class func getGlobalTimeAsDate(fromDate date: String?, andWithFormat dateFormatter: DateFormatter?) -> Date? {
        dateFormatter?.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone
        return dateFormatter?.date(from: date ?? "")
    }
    class func videoSnapshot(url: NSString) -> UIImage? {
        
        //let vidURL = NSURL(fileURLWithPath:filePathLocal as String)
        let vidURL = NSURL(string: url as String)
        let asset = AVURLAsset(url: vidURL! as URL)
        let generator = AVAssetImageGenerator(asset: asset)
        generator.appliesPreferredTrackTransform = true
        
        let timestamp = CMTime(seconds: 1, preferredTimescale: 60)
        
        do {
            let imageRef = try generator.copyCGImage(at: timestamp, actualTime: nil)
            return UIImage(cgImage: imageRef)
        }
        catch let error as NSError
        {
            print("Image generation failed with error \(error)")
            return nil
        }
    }
    
    class func getEncodeString(strig: String) -> String {
        let utf8str = strig.data(using: String.Encoding.utf8)
        let base64Encoded = utf8str?.base64EncodedData()
        
        let base64Decoded = NSString(data: base64Encoded!, encoding: String.Encoding.utf8.rawValue)
        
        return base64Decoded! as String
    }
    
    class func getDecodedString(encodedString: String) -> String {
        if(Data(base64Encoded: encodedString) != nil)
        {
            let decodedData = Data(base64Encoded: encodedString)!
            let decodedString = String(data:decodedData, encoding: .utf8)!
            
            return decodedString
        }
        return encodedString
    }
    
    class func format(phoneNumber: String, shouldRemoveLastDigit: Bool = false) -> String {
        guard !phoneNumber.isEmpty else { return "" }
        guard let regex = try? NSRegularExpression(pattern: "[\\s-\\(\\)]", options: .caseInsensitive) else { return "" }
        let r = NSString(string: phoneNumber).range(of: phoneNumber)
        var number = regex.stringByReplacingMatches(in: phoneNumber, options: .init(rawValue: 0), range: r, withTemplate: "")
        
        if number.count > 10 {
            let tenthDigitIndex = number.index(number.startIndex, offsetBy: 10)
            number = String(number[number.startIndex..<tenthDigitIndex])
        }
        
        if shouldRemoveLastDigit {
            let end = number.index(number.startIndex, offsetBy: number.count-1)
            number = String(number[number.startIndex..<end])
        }
        
        if number.count < 7 {
            let end = number.index(number.startIndex, offsetBy: number.count)
            let range = number.startIndex..<end
            number = number.replacingOccurrences(of: "(\\d{3})(\\d+)", with: "($1) $2", options: .regularExpression, range: range)
            
        } else {
            let end = number.index(number.startIndex, offsetBy: number.count)
            let range = number.startIndex..<end
            number = number.replacingOccurrences(of: "(\\d{3})(\\d{3})(\\d{4})", with: "($1)$2-$3", options: .regularExpression, range: range)
        }
        
        return number
    }
    
    class func isValidZipCode(zipCode:String) -> Bool
    {
        //        let zipCodeRegExArray = ["^\\d{5}([\\-]?\\d{4})?$","^(GIR|[A-Z]\\d[A-Z\\d]??|[A-Z]{2}\\d[A-Z\\d]??)[ ]??(\\d[A-Z]{2})$","\\b((?:0[1-46-9]\\d{3})|(?:[1-357-9]\\d{4})|(?:[4][0-24-9]\\d{3})|(?:[6][013-9]\\d{3}))\\b","^([ABCEGHJKLMNPRSTVXY]\\d[ABCEGHJKLMNPRSTVWXYZ])\\{0,1}(\\d[ABCEGHJKLMNPRSTVWXYZ]\\d)$","^(F-)?((2[A|B])|[0-9]{2})[0-9]{3}$","^(V-|I-)?[0-9]{5}$","^(0[289][0-9]{2})|([1345689][0-9]{3})|(2[0-8][0-9]{2})|(290[0-9])|(291[0-4])|(7[0-4][0-9]{2})|(7[8-9][0-9]{2})$","^[1-9][0-9]{3}\\s?([a-zA-Z]{2})?$","^([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{3}$","^([D|d][K|k]( |-))?[1-9]{1}[0-9]{3}$","^(s-|S-){0,1}[0-9]{3}\\s?[0-9]{2}$","^[1-9]{1}[0-9]{3}$","^\\d{6}$","^2899$"]
        let zipCodeRegExArray = ["^GIR[ ]?0AA|((AB|AL|B|BA|BB|BD|BH|BL|BN|BR|BS|BT|CA|CB|CF|CH|CM|CO|CR|CT|CV|CW|DA|DD|DE|DG|DH|DL|DN|DT|DY|E|EC|EH|EN|EX|FK|FY|G|GL|GY|GU|HA|HD|HG|HP|HR|HS|HU|HX|IG|IM|IP|IV|JE|KA|KT|KW|KY|L|LA|LD|LE|LL|LN|LS|LU|M|ME|MK|ML|N|NE|NG|NN|NP|NR|NW|OL|OX|PA|PE|PH|PL|PO|PR|RG|RH|RM|S|SA|SE|SG|SK|SL|SM|SN|SO|SP|SR|SS|ST|SW|SY|TA|TD|TF|TN|TQ|TR|TS|TW|UB|W|WA|WC|WD|WF|WN|WR|WS|WV|YO|ZE)(\\d[\\dA-Z]?[ ]?\\d[ABD-HJLN-UW-Z]{2}))|BFPO[ ]?\\d{1,4}$","^JE\\d[\\dA-Z]?[ ]?\\d[ABD-HJLN-UW-Z]{2}$","^GY\\d[\\dA-Z]?[ ]?\\d[ABD-HJLN-UW-Z]{2}$","^IM\\d[\\dA-Z]?[ ]?\\d[ABD-HJLN-UW-Z]{2}$","^\\d{5}([ \\-]\\d{4})?$","^[ABCEGHJKLMNPRSTVXY]\\d[ABCEGHJ-NPRSTV-Z][ ]?\\d[ABCEGHJ-NPRSTV-Z]\\d$","^\\d{5}$","^\\d{3}-\\d{4}$","^\\d{2}[ ]?\\d{3}$","^\\d{4}$","^\\d{5}$","^\\d{4}$","^\\d{4}[ ]?[A-Z]{2}$","^\\d{3}[ ]?\\d{2}$","^\\d{5}[\\-]?\\d{3}$","^\\d{4}([\\-]\\d{3})?$","^22\\d{3}$","^\\d{3}[\\-]\\d{3}$","^\\d{6}$","^\\d{3}(\\d{2})?$","^\\d{7}$","^\\d{4,5}|\\d{3}-\\d{4}$","^\\d{3}[ ]?\\d{2}$","^([A-Z]\\d{4}[A-Z]|(?:[A-Z]{2})?\\d{6})?$","^\\d{3}$","^\\d{3}[ ]?\\d{2}$","^39\\d{2}$","^(?:\\d{5})?$","^(\\d{4}([ ]?\\d{4})?)?$","^(948[5-9])|(949[0-7])$","^[A-Z]{3}[ ]?\\d{2,4}$","^(\\d{3}[A-Z]{2}\\d{3})?$","^980\\d{2}$","^((\\d{4}-)?\\d{3}-\\d{3}(-\\d{1})?)?$","^(\\d{6})?$","^(PC )?\\d{3}$","^\\d{2}-\\d{3}$","^00[679]\\d{2}([ \\-]\\d{4})?$","^4789\\d$","^\\d{3}[ ]?\\d{2}$","^00120$","^96799$","^6799$","^8\\d{4}$","^6798$","FIQQ 1ZZ","2899","(9694[1-4])([ \\-]\\d{4})?","9[78]3\\d{2}","9[78][01]\\d{2}","SIQQ 1ZZ","969[123]\\d([ \\-]\\d{4})?","969[67]\\d([ \\-]\\d{4})?","9695[012]([ \\-]\\d{4})?","9[78]2\\d{2}","988\\d{2}","008(([0-4]\\d)|(5[01]))([ \\-]\\d{4})?","987\\d{2}","9[78]5\\d{2}","PCRN 1ZZ","96940","9[78]4\\d{2}","(ASCN|STHL) 1ZZ","[HLMS]\\d{3}","TKCA 1ZZ","986\\d{2}","976\\d{2}"]
        var pinPredicate = NSPredicate()
        var result = Bool()
        for index in 0..<zipCodeRegExArray.count
        {
            pinPredicate = NSPredicate(format: "SELF MATCHES %@", zipCodeRegExArray[index])
            if(pinPredicate.evaluate(with: zipCode) == true)
            {
                result = true
                break
            }
            else
            {
                result = false
            }
        }
        //        result = pinPredicate.evaluate(with: zipCode) as Bool
        return result
        
    }
    
    class func dialNumber(number : String) {
        
        if let url = URL(string: "tel://\(number)"),
           UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler:nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        } else {
            // add error message here
        }
    }
    class func removeUserData(){
        UserDefaults.standard.removeObject(forKey: USER_DATA)
        UserDefaults.standard.removeObject(forKey: USER_ROLE)
        //        UserDefaults.standard.removeObject(forKey: USER_PROFILE)
    }
    
    class func saveUserData(data: [String: Any]){
        UserDefaults.standard.setValue(data, forKey: USER_DATA)
    }
    
    class func getUserData() -> LoginResponse?{
        if let data = UserDefaults.standard.value(forKey: USER_DATA) as? [String: Any]{
            let dic = LoginResponse(JSON: data)
            return dic
        }
        return nil
    }
    
    class func getAccessToken() -> String?{
        if let token = getUserData()?.auth?.accessToken{
            return token
        }
        return nil
    }
    
    class func setLoginRoot(){
            let vc = STORYBOARD.login.instantiateViewController(withIdentifier: "LogInScreen") as! LogInScreen
            let navVC = UINavigationController(rootViewController: vc)
            navVC.interactivePopGestureRecognizer?.isEnabled = false
            navVC.navigationBar.isHidden = true
            appDelegate.window?.rootViewController = navVC
            appDelegate.window?.makeKeyAndVisible()
        }
    
    
    class func saveImageAndGetPath(image: UIImage) -> String{
        let fileManager = FileManager.default
        let imageName = self.getImageName(fileExtension: "jpg")
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(imageName)
        let image = image
        let imageData = image.jpegData(compressionQuality: 0.3)
        fileManager.createFile(atPath: paths as String, contents: imageData, attributes: nil)
        return paths
    }
    
    class func getImageName(fileExtension : String) -> String{
        let uniqueString: String = ProcessInfo.processInfo.globallyUniqueString
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMddhhmmsss"
        let dateString: String = formatter.string(from: Date())
        let uniqueName: String = "\(uniqueString)_\(dateString)"
        if fileExtension.count > 0 {
            let fileName: String = "\(uniqueName).\(fileExtension)"
            return fileName
        }
        return uniqueName
    }
    
    class func checkMicrophonePermission(){
        switch AVAudioSession.sharedInstance().recordPermission {
        case AVAudioSession.RecordPermission.granted:
            print("Permission granted")
        case AVAudioSession.RecordPermission.denied:
            print("Pemission denied")
            AVAudioSession.sharedInstance().requestRecordPermission({ (granted) in
                // Handle granted
            })
        case AVAudioSession.RecordPermission.undetermined:
            print("Request permission here")
            AVAudioSession.sharedInstance().requestRecordPermission({ (granted) in
                // Handle granted
            })
        @unknown default:
            AVAudioSession.sharedInstance().requestRecordPermission({ (granted) in
                // Handle granted
            })
        }
    }
    
    
    class func timeAgoSinceDate(_ date:Date, numericDates:Bool = false) -> String {
        let calendar = NSCalendar.current
        let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
        let now = Date()
        let earliest = now < date ? now : date
        let latest = (earliest == now) ? date : now
        let components = calendar.dateComponents(unitFlags, from: earliest,  to: latest)
        
        if (components.year! >= 2) {
            return "\(components.year!) y "//ears ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 y"//ear ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) m"//onths ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 m"//onth ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) w"//eeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 w"//eek ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) d"//ays ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 d"//ay ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) h"//ours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 h"//our ago"
            } else {
                return "An hour "//ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) min"//utes ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 min"//ute ago"
            } else {
                return "A min"//ute ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) sec"//onds ago"
        } else {
            return "Just now"
        }
        
    }
    
    class func isValidPassword(Input:String) -> Bool {
        let RegExpression = "^(?=.*[#$@!%&*?])[A-Za-z[0-9]#$@!%&*?]{9,}+$"
        let result = NSPredicate(format:"SELF MATCHES %@", RegExpression)
        return result.evaluate(with: Input)
    }
    
    class func isSpecialCharacter(str: String) -> Bool{
        let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
        if str.rangeOfCharacter(from: characterset.inverted) != nil {
            return true
        }
        return false
    }
    
    class func labelWidth(height: CGFloat, font: UIFont,text: String) -> CGFloat {
        let label =  UILabel(frame: CGRect(x: 0, y: 0, width: .greatestFiniteMagnitude, height: height))
        label.numberOfLines = 0
        label.text = text
        label.font = font
        label.sizeToFit()
        return label.frame.width
    }
    
    class func addUnderLine(label: UILabel){
        let underlineAttribute = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue,NSAttributedString.Key.underlineColor : UIColor.white] as [NSAttributedString.Key : Any]
        let underlineAttributedString = NSAttributedString(string: label.text ?? "", attributes: underlineAttribute)
        label.attributedText = underlineAttributedString
    }
    
    class func removeUnderLine(label: UILabel){
        let underlineAttribute = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue,NSAttributedString.Key.underlineColor :UIColor.clear] as [NSAttributedString.Key : Any]
        let underlineAttributedString = NSAttributedString(string: label.text ?? "", attributes: underlineAttribute)
        label.attributedText = underlineAttributedString
    }
    
    class func compressVideo(inputURL: URL, outputURL: URL, handler:@escaping (_ exportSession: AVAssetExportSession?)-> Void) {
        
        let urlAsset = AVURLAsset(url: inputURL, options: nil)
        guard let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPresetMediumQuality) else {
            handler(nil)
            return
        }
        
        exportSession.outputURL = outputURL
        exportSession.outputFileType = AVFileType.mp4
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.exportAsynchronously { () -> Void in
            handler(exportSession)
        }
    }
    
    
    //MARK:- Create Thumbnail From Video
    class func createThumbnailFromVideoURL(videoURL: String) -> UIImage?{
        let asset = AVAsset(url: URL(string: videoURL)!)
        let assetImgGenerate = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        let time = CMTimeMakeWithSeconds(Float64(0), preferredTimescale: asset.duration.timescale)
        do {
            let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            let thumbnail = UIImage(cgImage: img)
            return thumbnail
        } catch let err{
            print(err.localizedDescription)
        }
        return nil
    }
    
    //MARK:- Return in MB
    class func getVideoSize(url: URL?) -> Double {
        guard let filePath = url?.path else {
            return 0.0
        }
        do {
            let attribute = try FileManager.default.attributesOfItem(atPath: filePath)
            if let size = attribute[FileAttributeKey.size] as? NSNumber {
                return size.doubleValue / 1000000.0
            }
        } catch {
            print("Error: \(error)")
        }
        return 0.0
    }
    
    
    class func heightOfLabel(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        
        label.sizeToFit()
        return label.frame.height
    }
    
    
    class func goToAuth(){
        //        let controller = STORYBOARD.main.instantiateViewController(withIdentifier: "AuthenticationOptionScreen") as! AuthenticationOptionScreen
        //        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    class func addBottomShadow(view:UIView,shadowColor:UIColor, shadowOpacity: CGFloat, shadowRadius: CGFloat){
        view.layer.shadowColor = shadowColor.cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 4)
        view.layer.shadowRadius = shadowRadius
        view.layer.shadowOpacity = Float(shadowOpacity)
    }
    
}


extension UIImage
{
    var highestQualityJPEGNSData: NSData { return self.jpegData(compressionQuality: 1.0)! as NSData }
    var highQualityJPEGNSData: NSData    { return self.jpegData(compressionQuality: 0.75)! as NSData}
    var mediumQualityJPEGNSData: NSData  { return self.jpegData(compressionQuality: 0.5)! as NSData }
    var lowQualityJPEGNSData: NSData     { return self.jpegData(compressionQuality: 0.5)! as NSData}
    var lowestQualityJPEGNSData: NSData
    {
        return self.jpegData(compressionQuality: 0.0)! as NSData
    }
}
var bundleKey: UInt8 = 0
class AnyLanguageBundle: Bundle {
    
    override func localizedString(forKey key: String,
                                  value: String?,
                                  table tableName: String?) -> String {
        
        guard let path = objc_getAssociatedObject(self, &bundleKey) as? String,
              let bundle = Bundle(path: path) else {
            
            return super.localizedString(forKey: key, value: value, table: tableName)
        }
        
        return bundle.localizedString(forKey: key, value: value, table: tableName)
    }
}

extension Bundle {
    
    class func setLanguage(_ language: String) {
        
        defer {
            
            object_setClass(Bundle.main, AnyLanguageBundle.self)
        }
        
        objc_setAssociatedObject(Bundle.main, &bundleKey,    Bundle.main.path(forResource: language, ofType: "lproj"), .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }
}
extension Optional where Wrapped == String {
    func isEmailValid() -> Bool{
        guard let email = self else { return false }
        let emailPattern = "[A-Za-z-0-9.-_+]+@[A-Za-z0-9]+\\.[A-Za-z]{2,3}"
        do{
            let regex = try NSRegularExpression(pattern: emailPattern, options: .caseInsensitive)
            let foundPatters = regex.numberOfMatches(in: email, options: .anchored, range: NSRange(location: 0, length: email.count))
            if foundPatters > 0 {
                return true
            }
        }catch{
            //error
        }
        return false
    }
}


extension Date {
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date))y ago"   }
        if months(from: date)  > 0 { return "\(months(from: date))mon ago"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w ago"   }
        if days(from: date)    > 0 { return "\(days(from: date))d ago"    }
        if hours(from: date)   > 0 { return "\(hours(from: date))hrs ago"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))min ago" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))sec ago" }
        return ""
    }
}
class UnderlinedLabel: UILabel {
    
    //override var text: String? {
    //    didSet {
    //        guard let text = text else { return }
    //        let textRange = NSMakeRange(0, text.count)
    //        let attributedText = NSMutableAttributedString(string: text)
    //        attributedText.addAttribute(NSAttributedString.Key.underlineStyle , value: NSUnderlineStyle.single.rawValue, range: textRange)
    //        // Add other attributes if needed
    //        self.attributedText = attributedText
    //        }
    //    }
}
class CustomSlider : UISlider {
    @IBInspectable open var trackWidth:CGFloat = 2 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    override open func trackRect(forBounds bounds: CGRect) -> CGRect {
        let defaultBounds = super.trackRect(forBounds: bounds)
        return CGRect(
            x: defaultBounds.origin.x,
            y: defaultBounds.origin.y + defaultBounds.size.height/2 - trackWidth/2,
            width: defaultBounds.size.width,
            height: trackWidth
        )
    }
}
extension UISlider {
    var thumbCenterX: CGFloat {
        let trackRect = self.trackRect(forBounds: frame)
        let thumbRect = self.thumbRect(forBounds: bounds, trackRect: trackRect, value: value)
        return thumbRect.midX
    }
}
extension String
{
    func toDate( dateFormat format  : String) -> Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = .current
        return dateFormatter.date(from: self)!
    }
}

extension UITabBarController {
    
    func addSubviewToLastTabItem(_ imageName: String) {
        if let lastTabBarButton = self.tabBar.subviews.last, let tabItemImageView = lastTabBarButton.subviews.first {
            if let accountTabBarItem = self.tabBar.items?.last {
                accountTabBarItem.selectedImage = nil
                accountTabBarItem.image = nil
            }
            let imgView = UIImageView()
            imgView.frame = tabItemImageView.frame
            imgView.layer.cornerRadius = tabItemImageView.frame.height/2
            imgView.layer.masksToBounds = true
            imgView.contentMode = .scaleAspectFill
            imgView.clipsToBounds = true
            imgView.image = UIImage(named: imageName)
            self.tabBar.subviews.last?.addSubview(imgView)
        }
    }
}

extension UIScrollView {
    func scrollToTop() {
        let desiredOffset = CGPoint(x: 0, y: -contentInset.top)
        setContentOffset(desiredOffset, animated: true)
    }
}
extension UIView {
    
    func roundCorners(corners:CACornerMask, radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.maskedCorners = corners
    }
}

extension UILabel {
    func underline() {
        if let textString = self.text {
            let attributedString = NSMutableAttributedString(string: textString)
            attributedString.addAttribute(NSAttributedString.Key.underlineStyle,
                                          value: NSUnderlineStyle.single.rawValue,
                                          range: NSRange(location: 0, length: attributedString.length))
            attributedText = attributedString
        }
    }
}

extension UIButton {
    @IBInspectable
    var letterSpacing: CGFloat {
        set {
            let attributedString: NSMutableAttributedString
            if let currentAttrString = attributedTitle(for: .normal) {
                attributedString = NSMutableAttributedString(attributedString: currentAttrString)
            }
            else {
                attributedString = NSMutableAttributedString(string: self.title(for: .normal) ?? "")
                setTitle(.none, for: .normal)
            }
            
            attributedString.addAttribute(NSAttributedString.Key.kern, value: newValue, range: NSRange(location: 0, length: attributedString.length))
            setAttributedTitle(attributedString, for: .normal)
        }
        get {
            if let currentLetterSpace = attributedTitle(for: .normal)?.attribute(NSAttributedString.Key.kern, at: 0, effectiveRange: .none) as? CGFloat {
                return currentLetterSpace
            }
            else {
                return 0
            }
        }
    }
}

extension UIView {
    @discardableResult
    func addLineDashedStroke(pattern: [NSNumber]?, radius: CGFloat, color: CGColor) -> CALayer {
        let borderLayer = CAShapeLayer()
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
        borderLayer.strokeColor = color
        borderLayer.lineDashPattern = pattern
        borderLayer.frame = shapeRect
        borderLayer.fillColor = nil
        borderLayer.path = UIBezierPath(roundedRect: shapeRect, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: radius, height: radius)).cgPath
        layer.addSublayer(borderLayer)
        return borderLayer
    }
}
extension UIAlertAction {
    var titleTextColor: UIColor? {
        get { return self.value(forKey: "titleTextColor") as? UIColor }
        set { self.setValue(newValue, forKey: "titleTextColor") }
    }
}

extension UIWindow {
    static var key: UIWindow! {
        if #available(iOS 13, *) {
            return UIApplication.shared.windows.first { $0.isKeyWindow }
        } else {
            return UIApplication.shared.keyWindow
        }
    }
}


extension UIImage {
    func toPngString() -> String? {
        let data = self.pngData()
        return data?.base64EncodedString(options: .endLineWithLineFeed)
    }
  
    func toJpegString(compressionQuality cq: CGFloat) -> String? {
        let data = self.jpegData(compressionQuality: cq)
        return data?.base64EncodedString(options: .endLineWithLineFeed)
    }
}

extension UIApplication {
    class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {

        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }

        if let tab = base as? UITabBarController {
            let moreNavigationController = tab.moreNavigationController

            if let top = moreNavigationController.topViewController, top.view.window != nil {
                return topViewController(base: top)
            } else if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }

        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }

        return base
    }}
