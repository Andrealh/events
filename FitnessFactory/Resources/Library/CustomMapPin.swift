//
//  CustomMapPin.swift
//  Pace
//
//  Created by iroid on 03/04/21.
//

import Foundation
import MapKit

class CustomPin: NSObject, MKAnnotation{
    var coordinate:CLLocationCoordinate2D
    var title:String?
    var storeImage:UIImage
    var phoneNumber:String?
    var name:String?
    var profileImage:String
    var gymId:Int?
    var website:String?
    var mail:String?
    
    init(pinTitle:String,Location:CLLocationCoordinate2D,storeImage:UIImage,phoneNumber:String,name:String,profileImage:String,gymId:Int,website:String,mail:String) {
        self.coordinate = Location
        self.title = pinTitle
        self.storeImage = storeImage
        self.phoneNumber = phoneNumber
        self.name = name
        self.profileImage = profileImage
        self.gymId = gymId
        self.website = website
        self.mail = mail
    }
}

