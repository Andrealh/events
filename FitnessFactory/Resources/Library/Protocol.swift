//
//  Protocol.swift
//  SportCoach
//
//  Created by iroid on 17/12/20.
//

import Foundation

protocol AddFamilyDelegate : class{
    func addData(str: String)
}
protocol AddVarientDelegate : class{
    func addVaientData(str: String)
}
//protocol UpdateProfileDelegate: class {
//    func updateUserData(val: SignUpResponse)
//}
protocol AddNewVideoDelegate: class {
    func addNewVideo()
}
protocol GetSessionVideoURLDelegate: class {
    func videoURL(url: URL)
}
protocol EditSessionNameDelegate: class {
    func changeSessionName(name: String)
}
