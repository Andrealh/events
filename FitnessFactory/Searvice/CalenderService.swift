//
//  CalenderService.swift
//  FitnessFactory
//
//  Created by iroid on 29/06/21.
//

import Foundation
class CalenderService {
    static let shared = { CalenderService() }()
    
    //MARK:- Workout Day
    func workoutDay(parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: calenderListURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Workout Day
    func swipeGymDay(parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: swipeGymDayURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Workout Day List
    func workoutDayList(page:Int,parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: workoutListURL+"\(page)", parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Nutrition Day List
    func nutritionDayList(page:Int,parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: calenderNutritionListURL+"\(page)", parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Event Day List
    func eventDayList(page:Int,parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: calenderEventListURL+"\(page)", parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
}
