//
//  MyWorkout.swift
//  FitnessFactory
//
//  Created by iroid on 20/07/21.
//

import Foundation
import Alamofire

class MyWorkoutService {
    static let shared = { MyWorkoutService() }()
    
    //MARK:-  Workout List
    func workoutList(Page:Int,Pageparameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: myWorkoutLitsURL+"?page=\(Page)") { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Create new workout
    func addWorkout(imageData:Data?, parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestWithImageData(urlString: addWorkoutURL, parameters: parameters,images: imageData,imageParameterName: IMAGE) { (statusCode, response) in
            success(statusCode,response)
        }failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Create new workout
    func addGifForWorkout(imageData:Data?, parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.uploadFile(urlString: addWorkoutExerciseURL, parameters: parameters, images: imageData){ (statusCode, response) in
            success(statusCode,response)
        }failure: { (error) in
            failure(error)
        }
        
    }
    
    //MARK:- Create new workout
    func addGifForExercise(imageData:Data?, parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()){
        print("2")
        APIClient.shared.uploadFile(urlString: AddworkoutExerciseURL, parameters: parameters, images: imageData){ (statusCode, response) in
            print(response)
            success(statusCode,response)
        }failure: { (error) in
            failure(error)
        }
        
    }
    
    //MARK:- Edit workout exercise
    func addWorkoutExerciseSet(imageData:Data?, parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()){
        print(parameters)
        APIClient.shared.requestWithImageData(urlString: AddworkoutExerciseURL, parameters: parameters,images: imageData,imageParameterName: IMAGE) { (statusCode, response) in
            
            success(statusCode,response)
        }failure: { (error) in
            failure(error)
        }
    }
    //MARK:- Edit workout exercise
    func updateOrder(parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()){
        print(parameters)
//        Alamofire.request(updateExcerciseOrder, method: .post, parameters: parameters)
//            .responseData{ response in
//                print(response)
//            }
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: updateExcerciseOrder, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }

    }
    
    
    
//    func updateOrderVC(parameters : String)
//    {
//        let url = URL(string: updateExcerciseOrder)!
//        var request = URLRequest(url: url)
//        request.httpMethod = "POST"
//
//        // insert json data to the request
//        request.httpBody = parameters
//
//        let task = URLSession.shared.dataTask(with: request) { data, response, error in
//            guard let data = data, error == nil else {
//                print(error?.localizedDescription ?? "No data")
//                return
//            }
//            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
//            if let responseJSON = responseJSON as? [String: Any] {
//                print(responseJSON)
//            }
//        }
//    }
    //MARK:- Create new workout
    func editGifForExercise(imageData:Data?, parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.uploadFile(urlString: EditWorkoutExerciseURL, parameters: parameters, images: imageData){ (statusCode, response) in
            success(statusCode,response)
        }failure: { (error) in
            failure(error)
        }
        
    }
    
    //MARK:- Edit workout exercise
    func editWorkoutExerciseSet(imageData:Data?, parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestWithImageData(urlString: EditWorkoutExerciseURL, parameters: parameters,images: imageData,imageParameterName: IMAGE) { (statusCode, response) in
            success(statusCode,response)
        }failure: { (error) in
            failure(error)
        }
    }
    
    func editGifForWorkout(imageData:Data?, parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.uploadFile(urlString: editWorkoutExerciseURL, parameters: parameters, images: imageData){ (statusCode, response) in
            success(statusCode,response)
        }failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Edit workout exercise
    func editWorkoutExercise(imageData:Data?, parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestWithImageData(urlString: editWorkoutExerciseURL, parameters: parameters,images: imageData,imageParameterName: IMAGE) { (statusCode, response) in
            success(statusCode,response)
        }failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Edit workout exercise
    func AddWorkoutExercise(imageData:Data?, parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestWithImageData(urlString: editWorkoutExerciseURL, parameters: parameters,images: imageData,imageParameterName: IMAGE) { (statusCode, response) in
            success(statusCode,response)
        }failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Edit workout
    func editWorkout(imageData:Data?, parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestWithImageData(urlString: editWorkoutURL, parameters: parameters,images: imageData,imageParameterName: IMAGE) { (statusCode, response) in
            success(statusCode,response)
        }failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:-  Workout Details
    func workoutDetails(id:Int ,Pageparameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: workoutDetailsURL+"\(id)") { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:-  Workout Details
    func pickWorkoutDetails(id:Int ,Pageparameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: pickWorkoutDetailsURL+"\(id)") { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:-  Workout Delete
    func workoutDelete(id:Int,Pageparameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .delete, urlString: workoutDeleteURL+"\(id)") { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:-  Workout Delete
    func removeWorkout(parameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: removeWorkoutURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Create new workout exercise
    func addWorkoutExercise(imageData:Data?, parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestWithImageData(urlString: addWorkoutExerciseURL, parameters: parameters,images: imageData,imageParameterName: IMAGE) { (statusCode, response) in
            success(statusCode,response)
        }failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:-  Workout Exercise Details
    func workoutExerciseDetails(id:Int ,Pageparameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: workoutExerciseDetailsURL+"\(id)") { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    
    
    
    //MARK:-  Workout exercise Delete
    func workoutExerciseDelete(id:Int,Pageparameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .delete, urlString: workoutExerciseDeleteURL+"\(id)") { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:-  Workout exercise List
    func workoutExerciseList(Id:Int,Page:Int,Pageparameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: workoutExerciListURL+"\(Id)"+"?page=\(Page)") { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:-  Workout exercise set List
    func workoutExerciseSetList(Id:Int,Page:Int,Pageparameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: workoutExerciMultipalSetListURL+"\(Id)"+"?page=\(Page)") { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    func workoutScheduleDateList(Id:Int,Pageparameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: workoutScheduleDateURL+"\(Id)") { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Add Workout Exercise Set
    func addWorkoutExerciseSet(parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: AddworkoutExerciseURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Add Workout Exercise Set
    func addWorkoutService(parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: addWorkoutURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    
    //MARK:- Edit Workout Exercise Set
//    func editWorkoutExerciseSet(parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()) {
//        APIClient.shared.requestAPIWithParameters(method: .put, urlString: EditWorkoutExerciseURL, parameters: parameters) { (statusCode, response) in
//            success(statusCode,response)
//        } failure: { (error) in
//            failure(error)
//        }
//    }
    
//    func editWorkoutExerciseSet(imageData:Data?, parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()){
//        APIClient.shared.requestWithImageData(urlString: EditWorkoutExerciseURL, parameters: parameters,images: imageData,imageParameterName: IMAGE) { (statusCode, response) in
//            success(statusCode,response)
//        }failure: { (error) in
//            failure(error)
//        }
//    }
    
    //MARK:-  Workout exercise Set Delete
    func workoutExerciseSetDelete(id:Int,Pageparameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .delete, urlString: removeWorkoutExerciseSetDeleteURL+"\(id)") { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:-  Workout Schedule
    func workoutSchedule(parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: workoutScheduleURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:-  Workout Schedule
    func searchWorkOutExercise(parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: workoutExerciseSearchURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
}
