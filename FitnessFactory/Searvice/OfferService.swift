//
//  OfferService.swift
//  FitnessFactory
//
//  Created by iroid on 08/07/21.
//

import Foundation
class OfferService {
    static let shared = { OfferService() }()
        
    //MARK:- GYM Offer List
    func gymOfferList(Page:Int,Pageparameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: offerListURL+"\(Page)") { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
}
