//
//  PickOutWorkoutService.swift
//  FitnessFactory
//
//  Created by iroid on 05/07/21.
//

import Foundation
class PickOutWorkoutService {
    static let shared = { PickOutWorkoutService() }()
    
    //MARK:- Workout set  List
    func workoutSetList(Id:Int,Pageparameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: workoutSetListURL+"\(Id)") { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Pick Workout List
    func pickWorkoutList(Id:Int,Page:Int,Pageparameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: pickWorkoutLitsURL+"\(Id)"+"?page=\(Page)") { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Pick Exercise List
    func pickExerciseList(Id:Int,Pageparameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: pickExerciseListURL+"\(Id)") { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    func addToMyWorkout(Id:Int,parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: addToMyWorkoutURL+"\(Id)") { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
}
