//
//  HomeAddGymService.swift
//  FitnessFactory
//
//  Created by iroid on 22/06/21.
//

import UIKit

class HomeAddGymService {
    static let shared = {
        HomeAddGymService()
    }()
    
    //MARK:- Gyms Near You
    func GymsNearYou(parameters: [String: Any] = [:],pageNumber:Int?,success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: nearByGymURL+"\(pageNumber ?? 1)", parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Gyms Near You
    func MealItem(parameters: [String: Any] = [:],pageNumber:Int?,success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: mealItemListURl+"\(pageNumber ?? 1)", parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Gyms Near You
    func searchRecipeList(parameters: [String: Any] = [:],pageNumber:Int?,success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: searchRecipeListURl+"\(pageNumber ?? 1)", parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Gyms Near You
    func GymsNearYouFilterOption(parameters: [String: Any] = [:],pageNumber:Int?,success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: nearByGymURL+"\(pageNumber ?? 1)", parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    //MARK:- Gyms Near You Filter
    func GymsNearYouFilterOption(parameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: myAccountURL) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Gyms Category
    func gymCategory(parameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: gymCategoryURL) { (statusCode, response) in
            success(statusCode,response)
        }failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Gym Link Status
    func gymLinkStatus(parameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: gymLinkStatusURL) { (statusCode, response) in
            success(statusCode,response)
        }failure: { (error) in
            failure(error)
        }
    }
    
    
    //MARK:- Link Gyms
    func linkGyms(gymID:Int,success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: linkGymURL+"\(gymID)") { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Event List
    func eventList(Page:Int,success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: eventListURL+"\(Page)") { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Exercise List
    func exerciseList(Page:Int,success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: exerciseListURL+"\(Page)") { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Exercise List
    func workoutList(success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: workoutListURl) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Nutrition List
    func nutritionList(Page:Int,success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: nutritionListURL+"\(Page)") { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Cancel Gym Link
    func cancelGymLink(ID:Int,success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .delete, urlString: cancelGymLinkURL+"\(ID)") { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    
    //MARK:- Cancel Gym Link
    func removeMealItem(ID:Int,success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .delete, urlString: removeMealItemURL+"\(ID)") { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Cancel Gym Link
    func removeItemFromRecipe(ID:Int,success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .delete, urlString: removeItemFromRecipeURL+"\(ID)") { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Cancel Gym Link
    func removeItem(ID:Int,success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .delete, urlString: removeItemURL+"\(ID)") { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Cancel Gym Link
    func unLinkGym(ID:Int,success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .delete, urlString: unLinkGymURL+"\(ID)") { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
}
