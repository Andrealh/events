//
//  NutritionService.swift
//  FitnessFactory
//
//  Created by iroid on 02/07/21.
//

import Foundation
class NutritionService {
    static let shared = { NutritionService() }()
    
    //MARK:- Recipes List
    func recipesList(Id:Int,Page:Int,Pageparameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: recipeListURL+"\(Id)"+"?page=\(Page)") { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Recipes List
    func myRecipesList(Id:Int,Page:Int,Pageparameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: myRecipeListURL+"\(Id)"+"?page=\(Page)") { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Recipes List
    func storeMealItemList(Id:Int,Page:Int,Pageparameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: storeMealItemListURL+"\(Id)"+"?page=\(Page)") { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Recipes List
    func itemList(Page:Int,Pageparameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: itemListURL+"\(Page)") { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Recipes List
    func mealItemList(Id:Int,Page:Int,Pageparameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: nutritionItemListURL+"\(Id)"+"?page=\(Page)") { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Recipes List
    func recipeItemList(Id:Int,Page:Int,Pageparameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: recipeItemListURL+"\(Id)"+"?page=\(Page)") { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Recipes Detail
    func recipesDetail(Id:Int,Pageparameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: recipeDetailURL+"\(Id)") { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Recipes Detail
    func myRecipesDetail(Id:Int,Pageparameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: myRecipeDetailURL+"\(Id)") { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Nutrition Type
    func nutritionTypeList(Id:Int,Page:Int, Pageparameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: nutrtionTypeURL+"\(Id)"+"?page=\(Page)") { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Nutrition Type
    func recipeDetailList(Id:Int,Page:Int, Pageparameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: recipeDetailListURL+"\(Id)"+"?page=\(Page)") { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Your Meal List
    func yourMealList(Page:Int,parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: yourMealListURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Your Meal List
    func storeMealItem(parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: storeMealItemURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Your Meal List
    func storRecipeItem(parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: storeRecipeItemURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Your Meal List
    func storRecipe(parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: storeRecipeURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Your Meal List
    func storeItem(parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: storeItemURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    func CreateMeal(imageData:Data?, parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()){
            APIClient.shared.requestWithImageData(urlString: createMealURL, parameters: parameters,images: imageData,imageParameterName: IMAGE) { (statusCode, response) in
                success(statusCode,response)
            }failure: { (error) in
                failure(error)
            }
        }
    
    func CreateRecipe(imageData:Data?, parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()){
            APIClient.shared.requestWithImageData(urlString: createRecipeURL, parameters: parameters,images: imageData,imageParameterName: IMAGE) { (statusCode, response) in
                success(statusCode,response)
            }failure: { (error) in
                failure(error)
            }
        }
    
    
    func CreateOwnMeal(parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: createMealURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    func mealScheduleDateList(Id:Int,Pageparameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: mealScheduleDateURL+"\(Id)") { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    func mealSchedule(parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: mealScheduleURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    func addToMyMeal(parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: addToMyMealURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:-  meal Delete
    func mealDelete(id:Int,Pageparameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .delete, urlString: mealDeleteURL+"\(id)") { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:-  meal Delete
    func recipeDelete(id:Int,Pageparameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .delete, urlString: recipeDeleteURL+"\(id)") { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Edit meal
        func editMeal(imageData:Data?, parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()){
            APIClient.shared.requestWithImageData(urlString: editMealURL, parameters: parameters,images: imageData,imageParameterName: IMAGE) { (statusCode, response) in
                success(statusCode,response)
            }failure: { (error) in
                failure(error)
            }
        }
    
    //MARK:- Edit meal
        func editRecipe(imageData:Data?, parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()){
            APIClient.shared.requestWithImageData(urlString: editRecipeURL, parameters: parameters,images: imageData,imageParameterName: IMAGE) { (statusCode, response) in
                success(statusCode,response)
            }failure: { (error) in
                failure(error)
            }
        }
}
