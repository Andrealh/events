//
//  MyAccountService.swift
//  FitnessFactory
//
//  Created by iroid on 08/06/21.
//

import Foundation
class MyAccountService {
    static let shared = { MyAccountService() }()
    
    //MARK:- My Account
    func myAccount(parameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: myAccountURL) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Edit profile
    func editProfile(parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: editProfileURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    //MARK:- Delete Profile
    func deleteProfile(success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithGetMethod(method: .post, urlString: deleteProfileURL) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }

    
    //MARK:- Weight Goal
    func weightGoal(parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestWithMultipartData(urlString: weightGoalURl, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        }failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Other Goal
    func otherGoal(parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: otherGoalURl, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
   
    func gifList(Id:Int,Page:Int,parameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: gifListURL+"\(Id)"+"?page=\(Page)") { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    func imageList(Id:Int,parameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: imageListURL+"\(Id)") { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
}
