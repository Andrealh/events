//
//  APIManager.swift
//  SportCoach
//
//  Created by iroid on 01/01/21.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

class APIClient {
    static let shared = {
        APIClient(baseURL: server_url)
    }()
    
    var baseURL: URL?
    
    required init(baseURL: String) {
        self.baseURL = URL(string: baseURL)
    }
    
    func getHeader() -> HTTPHeaders {
        var headerDic: HTTPHeaders = [:]
        if Utility.getUserData() == nil{
            headerDic = [
                "Accept": "application/json",
                "Content-Type": "application/json"
            ]
        }else{
            if let accessToken = Utility.getAccessToken(){
//                print("nezuko")
//                print(accessToken)
                headerDic = [//"Bearer "
                    "Authorization":"Bearer " + accessToken,
                    "Accept": "application/json",
                    "Content-Type": "application/json"
                ]
            }else{
                headerDic = [
                    "Accept": "application/json",
                    "Content-Type": "application/json"
                ]
            }
        }
        return headerDic
    }
    
    func requestAPIWithParameters(method: HTTPMethod,urlString: String,parameters: [String:Any],success: @escaping(Int,Response) -> (),failure : @escaping(String) -> ()){
        Alamofire.request(urlString, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: getHeader()).responseObject { (response: DataResponse<Response>) in
            print( "response data ",response.result.value?.toJSON())
            switch response.result{
            case .success(let value):
                guard let statusCode = response.response?.statusCode else {
                    failure(value.message ?? "")
                    return
                }
                //print("status code" ,response.response?.statusCode )
                if (200..<300).contains(statusCode){
                    success(statusCode,value)
                }else if statusCode == 401{
                    if urlString == loginURL {
                        failure(value.message ?? "")
                    } else {
                        Utility.hideIndicator()
                        Utility.removeUserData()
                      //  failure(value.message ?? "")
                        Utility.setLoginRoot()
                    }
                    
                }else{
                    failure(value.message ?? "")
                }
                break
            case .failure(let error):
               // failure(error.localizedDescription)
                self.requestAPIWithParameters(method: method, urlString: urlString, parameters: parameters){(statusCode, response)in
                    success(statusCode,response)
                } failure: { (error) in
                    failure(error)
                }
                break
            }
        }
    }
    
    func requestAPIWithGetMethod(method: HTTPMethod,urlString: String,success: @escaping(Int,Response) -> (),failure : @escaping(String) -> ()){
        Alamofire.request(urlString, method: method, parameters: nil, encoding: JSONEncoding.default, headers: getHeader()).responseObject { (response: DataResponse<Response>) in
            print( "response data ",response.result.value?.toJSON())
            switch response.result{
            case .success(let value):
                guard let statusCode = response.response?.statusCode else {
                    failure(value.message ?? "")
                    return
                }
                
                if (200..<300).contains(statusCode) {
                    success(statusCode,value)
                } else if statusCode == 401{
                    if urlString == loginURL {
                        failure(value.message ?? "")
                    } else {
                        Utility.hideIndicator()
                        Utility.removeUserData()
                        //  failure(value.message ?? "")
                        Utility.setLoginRoot()
                    }
                } else {
                    failure(value.message ?? "")
                }
                break
            case .failure(let error):
                //failure(error.localizedDescription)
                self.requestAPIWithGetMethod(method: method, urlString: urlString) { (statusCode, response) in
                    success(statusCode,response)
                } failure: { (error) in
                    failure(error)
                }
                break
            }
        }
    }
    
    func requestWithImage(urlString: String,imageParameterName: String,images: Data?,videoParameterName: String,videoURL: URL?,audioParameterName: String,audioURL: URL?,bgImage: Data?,bgThumbnailParameter: String,bgThumbImage: Data?,parameters: [String:Any],success: @escaping(Int,Response) -> (),failure : @escaping(String) -> ()){
        Alamofire.upload(multipartFormData:{(multipartFormData) in
            if let image = images{
                multipartFormData.append(image, withName: imageParameterName,fileName: getFileName()+".jpg", mimeType: "image/jpg")
            }
            if let video = videoURL{
                do {
                    let data = try Data(contentsOf: video, options: .mappedIfSafe)
                    print(data)
                    multipartFormData.append(data, withName: videoParameterName, fileName: getFileName()+".mp4", mimeType: "video/mp4")
                } catch  {
                }
                if let thumbImage = bgThumbImage{
                    multipartFormData.append(thumbImage, withName: bgThumbnailParameter,fileName: getFileName()+".jpg", mimeType: "image/jpg")
                }
            }else if let image = bgImage{
                multipartFormData.append(image, withName: videoParameterName,fileName: getFileName()+".jpg", mimeType: "image/jpg")
            }
            if let audio = audioURL{
                do {
                    let data = try Data(contentsOf: audio)
                    print(data)
                    multipartFormData.append(data, withName: audioParameterName, fileName: getFileName()+".mp3", mimeType: "audio/m4a")
                } catch  {
                }
            }
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to:urlString,headers:getHeader()){ (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseObject { (response: DataResponse<Response>) in
                    print( "response data ",response.result.value?.toJSON())
                    switch response.result{
                    case .success(let value):
                        guard let statusCode = response.response?.statusCode else {
                            failure(value.message ?? "")
                            return
                        }
                        if (200..<300).contains(statusCode){
                            success(statusCode,value)
                        }else if statusCode == 401{
//                            failure(value.message ?? "")
                            if urlString == loginURL {
                                failure(value.message ?? "")
                            } else {
                                Utility.hideIndicator()
                                Utility.removeUserData()
                               // failure(value.message ?? "")
                                Utility.setLoginRoot()
                            }
                        }else{
                            failure(value.message ?? "")
                        }
                        break
                    case .failure(let error):
                        failure(error.localizedDescription)
                        break
                    }
                    
                }
            case .failure(let error):
                failure(error.localizedDescription)
            }
        }
    }
    
    func requestWithMultipartData(urlString: String,parameters: [String:Any],success: @escaping(Int,Response) -> (),failure : @escaping(String) -> ()){
        Alamofire.upload(multipartFormData:{(multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }

        }, to:urlString,headers:getHeader()){ (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
//                    uploadProgress(progress.fractionCompleted)
                })
                
                upload.responseObject { (response: DataResponse<Response>) in
                    print( "response data ",response.result.value?.toJSON())
                    switch response.result{
                    case .success(let value):
                        guard let statusCode = response.response?.statusCode else {
                            failure(value.message ?? "")
                            return
                        }
                        if (200..<300).contains(statusCode){
                            print(value.toJSON())
                            success(statusCode,value)
                        }else if statusCode == 401{
                         if urlString == loginURL {
                            failure(value.message ?? "")
                        } else {
                            Utility.hideIndicator()
                            Utility.removeUserData()
                            //failure(value.message ?? "")
                            Utility.setLoginRoot()
                        }
                        }else{
                            failure(value.message ?? "")
                        }
                        break
                    case .failure(let error):
                        failure(error.localizedDescription)
                        break
                    }
                    
                }
            case .failure(let error):
                failure(error.localizedDescription)
            }
        }
    }
    
    func requestWithImageData(urlString: String,parameters: [String:Any],images: Data?,imageParameterName: String,success: @escaping(Int,Response) -> (),failure : @escaping(String) -> ()){
        
        Alamofire.upload(multipartFormData:{(multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
            
            if let image = images{
                multipartFormData.append(image, withName: imageParameterName,fileName: getFileName()+".jpg", mimeType: "image/jpg")
            }
        }, to:urlString,headers:getHeader()){ (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
//                    uploadProgress(progress.fractionCompleted)
                })
                
                upload.responseObject { (response: DataResponse<Response>) in
                    print( "response data ",response.result.value?.toJSON())
                    switch response.result{
                    case .success(let value):
                        guard let statusCode = response.response?.statusCode else {
                            failure(value.message ?? "")
                            return
                        }
                        if (200..<300).contains(statusCode){
                            print(value.toJSON())
                            success(statusCode,value)
                        }else if statusCode == 401{
                            if urlString == loginURL {
                                failure(value.message ?? "")
                            } else {
                                Utility.hideIndicator()
                                Utility.removeUserData()
                                //failure(value.message ?? "")
                                Utility.setLoginRoot()
                            }
                        }else{
                            failure(value.message ?? "")
                        }
                        break
                    case .failure(let error):
                        failure(error.localizedDescription)
                        break
                    }
                    
                }
            case .failure(let error):
                //failure(error.localizedDescription)
                self.requestWithImageData(urlString: urlString, parameters: parameters, images: images, imageParameterName: imageParameterName) { (statusCode, response) in
                    success(statusCode,response)
                } failure: { (error) in
                    failure(error)
                }

            }
        }
    }
    
    func uploadFile(urlString: String,parameters: [String:Any],images: Data?,success: @escaping(Int,Response) -> (),failure : @escaping(String) -> ()) {
        Alamofire.upload(multipartFormData:{(multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
            
            if let image = images{
                multipartFormData.append(image, withName: "image",fileName: UUID().uuidString+".gif", mimeType: "image/gif")
            }
        }, to:urlString,headers:getHeader()){ (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
//                    uploadProgress(progress.fractionCompleted)
                })
                
                upload.responseObject { (response: DataResponse<Response>) in
                    switch response.result{
                    case .success(let value):
                        guard let statusCode = response.response?.statusCode else {
                            failure(value.message ?? "")
                            return
                        }
                        if (200..<300).contains(statusCode){
                            print(value.toJSON())
                            success(statusCode,value)
                        }else if statusCode == 401{
                            if urlString == loginURL {
                                failure(value.message ?? "")
                            } else {
                                Utility.hideIndicator()
                                Utility.removeUserData()
                                //failure(value.message ?? "")
                                Utility.setLoginRoot()
                            }
                        }else{
                            failure(value.message ?? "")
                        }
                        break
                    case .failure(let error):
                        failure(error.localizedDescription)
                        break
                    }
                    
                }
            case .failure(let error):
                //failure(error.localizedDescription)
                self.uploadFile(urlString: urlString, parameters: parameters, images: images) {
                    (statusCode, response) in
                    success(statusCode,response)
                } failure: { (error) in
                    failure(error)
                }
            }
        }
   }
    
    func requestWithMultipartDataImage(urlString: String,imageParameterName: String,images: Data?,videoParameterName: String,videoURL: URL?,parameters: [String:Any],uploadProgress: @escaping (_ val : Double) -> (),success: @escaping(Int,Response) -> (),failure : @escaping(String) -> ()){
        Alamofire.upload(multipartFormData:{(multipartFormData) in
            if let image = images{
                multipartFormData.append(image, withName: imageParameterName,fileName: getFileName()+".jpg", mimeType: "image/jpg")
            }
            if let video = videoURL{
                do {
                    let data = try Data(contentsOf: video, options: .mappedIfSafe)
                    print(data)
                    multipartFormData.append(data, withName: videoParameterName, fileName: getFileName()+".mp4", mimeType: "video/mp4")
                } catch  {
                }
            }
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to:urlString,headers:getHeader()){ (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                    uploadProgress(progress.fractionCompleted)
                })
                
                upload.responseObject { (response: DataResponse<Response>) in
                    switch response.result{
                    case .success(let value):
                        guard let statusCode = response.response?.statusCode else {
                            failure(value.message ?? "")
                            return
                        }
                        if (200..<300).contains(statusCode){
                            success(statusCode,value)
                        }else if statusCode == 401{
                            if urlString == loginURL {
                                failure(value.message ?? "")
                            } else {
                                Utility.hideIndicator()
                                Utility.removeUserData()
                              //  failure(value.message ?? "")
                                Utility.setLoginRoot()
                            }
                        }else{
                            failure(value.message ?? "")
                        }
                        break
                    case .failure(let error):
                        failure(error.localizedDescription)
                        break
                    }
                    
                }
            case .failure(let error):
                failure(error.localizedDescription)
            }
        }
    }
}
