//
//  SearchScreen.swift
//  FitnessFactory
//
//  Created by iroid on 16/09/21.
//

import UIKit

class SearchScreen: UIViewController {

    @IBOutlet weak var searchListTableView: UITableView!
    
    @IBOutlet weak var searchTextField: UITextField!
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    var page = 1
    var hasMorePage : Bool = false
    var searchText = ""
    var meta:MetaData!
    var mealId = Int()
    var selectRecipe : (() -> Void)?
    var searchRecipeList = [RecipeDetailResponse]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initalizedDetails()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- initalizedDetails Details
    func initalizedDetails(){
        let nib = UINib(nibName: "MealItemSearchListCell", bundle: nil)
        searchListTableView.register(nib, forCellReuseIdentifier: "MealItemSearchListCell")
        searchListTableView.tableFooterView = UIView()
    }
    
    // MARK: scroll method
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let size = scrollView.contentSize
        let inset = scrollView.contentInset
        let y = offset.y + bounds.size.height - inset.bottom
        let h = size.height
        let reload_distance:CGFloat = 10.0
        if y > (h + reload_distance) {
            print("called")
            if let metaTotal = self.meta?.total_results{
                if self.searchRecipeList.count != Int(metaTotal){
                    if self.hasMorePage{
                        self.hasMorePage = false
                        self.page += 1
                        getRecipeList(page: page, searchText: searchText)
                    }
                }
            }
        }
    }
    
    func appendDataCollectionView(data: Response){
            var indexPathArray: [IndexPath] = []
            let res = data.recipeDetailListResponse!
            for i in self.searchRecipeList.count..<self.searchRecipeList.count + res.count{
                indexPathArray.append(IndexPath(item: i, section: 0))
            }
            self.searchRecipeList.append(contentsOf: res)
            self.searchListTableView.insertRows(at: indexPathArray, with: .automatic)
        }
    
    @IBAction func onHome(_ sender: UIButton) {
        tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func onBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onCancel(_ sender: UIButton) {
        cancelButton.isHidden = true
        searchButton.isHidden = false
        searchTextField.text = ""
        searchRecipeList = []
        searchListTableView.reloadData()
        view.endEditing(true)
    }
    
}

extension SearchScreen : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        var numberOfScetion : Int = 0
//        if searchRecipeList.count > 0 {
//            numberOfScetion = searchRecipeList.count
//            tableView.backgroundView = nil
//        } else {
//            let noDataLabel = UILabel(frame: CGRect(x: 0, y: 50, width: tableView.bounds.size.width, height: 100))
//            noDataLabel.text = "There are no Recipe list"
//            noDataLabel.textColor = UIColor.darkGray
//            noDataLabel.textAlignment = .center
//            noDataLabel.numberOfLines = 0
//            tableView.backgroundView = noDataLabel
//        }
//        return numberOfScetion
        return searchRecipeList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MealItemSearchListCell") as! MealItemSearchListCell
        cell.recipeItem = searchRecipeList[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectRecipe = searchRecipeList[indexPath.row]
        storeRecipe(selectRecipe: selectRecipe, mealId: mealId)
    }
}

extension SearchScreen  {
    func getRecipeList(page:Int,searchText:String){
        //self.view.endEditing(true)
        if Utility.isInternetAvailable(){
           // Utility.showIndecator()
            let data = MealItemSearchListRequest(search: searchText, max_results: 20)
            HomeAddGymService.shared.searchRecipeList(parameters: data.toJSON(), pageNumber: page) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                self?.hasMorePage = true
                if let res = response.recipeDetailListResponse {
                    if page == 1{
                        self?.searchRecipeList = []
                        self?.searchRecipeList = res
                        self?.searchListTableView.reloadData()
                    } else {
                        self?.appendDataCollectionView(data: response)
                    }
                    if let meta = response.metaDataResponse{
                        self?.meta = meta
                    }
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                stronSelf.hasMorePage = true
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func storeRecipe(selectRecipe:RecipeDetailResponse,mealId:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            let data = StoreRecipeRequest(meal_id: mealId, image: selectRecipe.recipe_image, title: selectRecipe.recipe_name, desc: selectRecipe.recipe_description, recipe: selectRecipe.recipe_url, cal: selectRecipe.recipe_nutrition?.calories)
            NutritionService.shared.storRecipe(parameters: data.toJSON() ) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    self?.selectRecipe!()
                    self?.navigationController?.popViewController(animated: true)
//                    let alertController = UIAlertController(title: APPLICATION_NAME, message: res, preferredStyle: .alert)
//                    // Create the actions
//                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { [self]
//                        UIAlertAction in
//
//                        self?.navigationController?.popViewController(animated: true)
//
//                        // self?.mealItemTableView.tag = 0
//                        //self?.searchTextField.text = ""
//                        //self?.addMealView.isHidden = false
//                        // self?.mealItemTableView.reloadData()
//                        NSLog("OK Pressed")
//                    }
//                    // Add the actions
//                    alertController.addAction(okAction)
//                    // Present the controller
//                    self?.present(alertController, animated: true, completion: nil)
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                stronSelf.hasMorePage = true
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}

extension SearchScreen:UITextFieldDelegate{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        //let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        if searchTextField.text?.count == 0 {
            cancelButton.isHidden = true
            searchButton.isHidden = false
        }
//        let updatedString = searchTextField.text
//        searchText = updatedString ?? ""
//        DispatchQueue.main.asyncAfter(deadline: .now() ) {
//            self.searchGymList()
//        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == searchTextField{
            if let text = textField.text as NSString? {
                let txtAfterUpdate = text.replacingCharacters(in: range, with: string)
                searchText = txtAfterUpdate
                print(txtAfterUpdate)
                NSObject.cancelPreviousPerformRequests(withTarget: self,selector: #selector(searchGymList),object: textField)
                self.perform(#selector(searchGymList),with: textField,afterDelay: 0)
            }
        }
        return true
    }

    @objc func searchGymList(){
        let aString: String =  searchText
        let newString = aString.replacingOccurrences(of: " ", with: "%20", options:[], range: nil)
        print(newString)
        if newString.count > 0 {
            self.getRecipeList(page: 1, searchText: newString)
        }
       
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        cancelButton.isHidden = false
        searchButton.isHidden = true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
    }
}
