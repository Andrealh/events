//
//  CalIntakeScreen.swift
//  FitnessFactory
//
//  Created by iroid on 24/07/21.
//

import UIKit
import DropDown

class CalIntakeScreen: UIViewController {

    @IBOutlet weak var calPerDayLabel: UILabel!
    
    @IBOutlet weak var activityTextField: UITextField!
    @IBOutlet weak var currentWeightTextField: UITextField!
    @IBOutlet weak var ageTextField: UITextField!
    @IBOutlet weak var genderTextField: UITextField!
    @IBOutlet weak var currentHightTextField: UITextField!
    
    @IBOutlet weak var weightOptionButton: UIButton!
    @IBOutlet weak var activityOptionButton: UIButton!
    @IBOutlet weak var genderOptionButton: UIButton!
    @IBOutlet weak var heightoptionButton: UIButton!
    @IBOutlet weak var infoOptionButton: UIButton!
    
    //MARK:- variable
    var weightDropDown = DropDown()
    var genderDropDown = DropDown()
    var exerciseDropDown = DropDown()
    var heightDropDown = DropDown()
    var weightArray: [String] = []
    var heightArray: [String] = []
    var genderArray: [String] = []
    var exerciseArray: [String] = []
    var selectGenderOption = Int()
    var selectWeightOption = Int()
    var selectActivityOption = Int()
    var selectHeightOption = Int()
    var weight:Double = 0
    var height:Double = 0
    var age:Double = 0
    var BMR:Double = 0
    var caloriesPerDay:Double = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initalizedDetails()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- initalizedDetails Details
    func initalizedDetails(){
        weightArray = ["Lbs","Kgs"]
        heightArray = ["Cms","In"]
        genderArray = ["Male","Female"]
        exerciseArray = ["Little or No Exercise",
                         "Exercise 1-3 Times / week",
                         "Exercise 4-5 Times / week",
                        "Intense Exercise 3-4 Times / week",
                        "Intense Exercise 6-7 Times / week",
                        "Very Intense Exercise Daily or Physical Job"
        ]
        WeightDropDownFilterButton()
        ActivityDropDownFilterButton()
        HeightDropDownFilterButton()
        GenderDropDownFilterButton()
    }
    func HeightDropDownFilterButton(){
        heightDropDown.backgroundColor = UIColor(white: 1, alpha: 1)
        heightDropDown.selectionBackgroundColor = UIColor(white: 1, alpha: 1)
        heightDropDown.shadowColor = UIColor(white: 0.6, alpha: 1)
        heightDropDown.shadowOpacity = 0
        heightDropDown.shadowRadius = 0
        heightDropDown.cornerRadius = 9
        heightDropDown.separatorColor = UIColor.primaryColour
        heightDropDown.animationduration = 0
        heightDropDown.textColor =  #colorLiteral(red: 0.02745098039, green: 0.02745098039, blue: 0.02352941176, alpha: 0.4017110475)
        heightDropDown.selectedTextColor =  #colorLiteral(red: 0.4244436026, green: 0.4218436182, blue: 0.5152064562, alpha: 1)
        heightDropDown.anchorView = weightOptionButton
        heightDropDown.bottomOffset = CGPoint(x: 0, y: 50)
        heightDropDown.direction = .bottom
        heightDropDown.cellNib = UINib(nibName: "DropDownCell", bundle: Bundle(for: DropDownCell.self))
        heightDropDown.dataSource = heightArray
        heightDropDown.dismissMode = .onTap
        heightDropDown.selectRow(-1)
        heightDropDown.selectionAction = { [weak self] (index, item) in
            self?.selectHeightOption = index
       print("here")
            print(item)
            self?.heightoptionButton.setTitle(item, for: .normal)
            
            if index == 0{
                self?.currentHightTextField.placeholder = "Current Height In Centimeters"
            }
            else if index == 1{
                self?.currentHightTextField.placeholder = "Current Height In Inches"
            }
        }
    }
    func WeightDropDownFilterButton() {
        weightDropDown.backgroundColor = UIColor(white: 1, alpha: 1)
        weightDropDown.selectionBackgroundColor = UIColor(white: 1, alpha: 1)
        weightDropDown.shadowColor = UIColor(white: 0.6, alpha: 1)
        weightDropDown.shadowOpacity = 0
        weightDropDown.shadowRadius = 0
        weightDropDown.cornerRadius = 9
        weightDropDown.separatorColor = UIColor.primaryColour
        weightDropDown.animationduration = 0
        weightDropDown.textColor =  #colorLiteral(red: 0.02745098039, green: 0.02745098039, blue: 0.02352941176, alpha: 0.4017110475)
        weightDropDown.selectedTextColor =  #colorLiteral(red: 0.4244436026, green: 0.4218436182, blue: 0.5152064562, alpha: 1)
        weightDropDown.anchorView = weightOptionButton
        weightDropDown.bottomOffset = CGPoint(x: 0, y: 50)
        weightDropDown.direction = .bottom
        weightDropDown.cellNib = UINib(nibName: "DropDownCell", bundle: Bundle(for: DropDownCell.self))
        weightDropDown.dataSource = weightArray
        weightDropDown.dismissMode = .onTap
        weightDropDown.selectRow(-1)
        weightDropDown.selectionAction = { [weak self] (index, item) in
            self?.selectWeightOption = index
       
            self?.weightOptionButton.setTitle(item, for: .normal)
        }
    }
    
    func ActivityDropDownFilterButton() {
        exerciseDropDown.backgroundColor = UIColor(white: 1, alpha: 1)
        exerciseDropDown.selectionBackgroundColor = UIColor(white: 1, alpha: 1)
        exerciseDropDown.shadowColor = UIColor(white: 0.6, alpha: 1)
        exerciseDropDown.shadowOpacity = 0
        exerciseDropDown.shadowRadius = 0
        exerciseDropDown.cornerRadius = 9
        exerciseDropDown.separatorColor = UIColor.primaryColour
        exerciseDropDown.animationduration = 0
        exerciseDropDown.textColor =  #colorLiteral(red: 0.02745098039, green: 0.02745098039, blue: 0.02352941176, alpha: 0.4017110475)
        exerciseDropDown.selectedTextColor =  #colorLiteral(red: 0.4244436026, green: 0.4218436182, blue: 0.5152064562, alpha: 1)
        exerciseDropDown.anchorView = activityOptionButton
        exerciseDropDown.bottomOffset = CGPoint(x: 0, y: 50)
        exerciseDropDown.direction = .bottom
        exerciseDropDown.cellNib = UINib(nibName: "DropDownCell", bundle: Bundle(for: DropDownCell.self))
        exerciseDropDown.dataSource = exerciseArray
        exerciseDropDown.dismissMode = .onTap
        exerciseDropDown.selectRow(-1)
        exerciseDropDown.selectionAction = { [weak self] (index, item) in
            self?.selectActivityOption = index

            self?.activityTextField.text = item
        }
    }
    
    func GenderDropDownFilterButton() {
        genderDropDown.backgroundColor = UIColor(white: 1, alpha: 1)
        genderDropDown.selectionBackgroundColor = UIColor(white: 1, alpha: 1)
        genderDropDown.shadowColor = UIColor(white: 0.6, alpha: 1)
        genderDropDown.shadowOpacity = 0
        genderDropDown.shadowRadius = 0
        genderDropDown.cornerRadius = 9
        genderDropDown.separatorColor = UIColor.primaryColour
        genderDropDown.animationduration = 0
        genderDropDown.textColor =  #colorLiteral(red: 0.02745098039, green: 0.02745098039, blue: 0.02352941176, alpha: 0.4017110475)
        genderDropDown.selectedTextColor =  #colorLiteral(red: 0.4244436026, green: 0.4218436182, blue: 0.5152064562, alpha: 1)
        genderDropDown.anchorView = genderOptionButton
        genderDropDown.bottomOffset = CGPoint(x: 0, y: 50)
        genderDropDown.direction = .bottom
        genderDropDown.cellNib = UINib(nibName: "DropDownCell", bundle: Bundle(for: DropDownCell.self))
        genderDropDown.dataSource = genderArray
        genderDropDown.dismissMode = .onTap
        genderDropDown.selectRow(-1)
        genderDropDown.selectionAction = { [weak self] (index, item) in
            self?.selectGenderOption = index
 
            self?.genderTextField.text = item
        }
    }
    
    func checkValidation() -> String?{
        if self.ageTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter your age"
        }
        if self.genderTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please select your gender"
        }
        if self.currentWeightTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter your weight"
        }
        if self.currentHightTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter your current height in cm"
        }
        if self.activityTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please select your activity"
        }
        
        return nil
    }
    
    @IBAction func onGender(_ sender: UIButton) {
        view.endEditing(true)
       
        genderDropDown.show()
    }
    
    @IBAction func onWeight(_ sender: UIButton) {
        view.endEditing(true)
        weightDropDown.show()
    }
    @IBAction func onHeight(_ sender: UIButton){
        view.endEditing(true)
        heightDropDown.show()
    }
    
    @IBAction func onActivity(_ sender: UIButton) {
        view.endEditing(true)
        exerciseDropDown.show()
    }
    
    @IBAction func onCalculate(_ sender: UIButton) {
        if let error = self.checkValidation(){
            Utility.showAlert(vc: self, message: error)
        } else {
            calPerDayLabel.isHidden = false
            calculateCalories()
        }
    }
    @IBAction func onMoreInfo(_ sender: UIButton){
        let websiteUrl = "https://www.calculator.net/bmr-calculator.html"
        guard let url = URL(string: websiteUrl) else {
           
            return
        }
        UIApplication.shared.open(url, completionHandler: { success in
            if success {
                print("opened")
            } else {
                if let url = URL(string: "https://fitnessfac.ca") {
                    UIApplication.shared.open(url)
                }
            }
        })
    }
    
    @IBAction func onBack(_ sender: UIButton) {
       // tabBarController?.tabBar.isHidden = false
        self.navigationController?.popViewController(animated: true)
    }
    
    func calculateCalories(){
        
        var male: Bool = true
        if genderTextField.text == "Male"
        {
            male = true
        }
        else if genderTextField.text == "Female"
        {
            male = false
        }
        
        var weightInKg: Bool = true
        if weightOptionButton.titleLabel?.text == "Kgs"
        {
            weightInKg = true
        }
        else if weightOptionButton.titleLabel?.text == "Lbs"
        {
            weightInKg = false
        }
        var heightInCms : Bool = true
        if heightoptionButton.titleLabel?.text == "In"{
            heightInCms = false;
        }else{
            
        }
        age = 5 * Double(ageTextField.text ?? "0")!
        print("age : ", age)
        
        height = (heightInCms ? 6.25 : 15.88) * Double(currentHightTextField.text ?? "0")!
        print("height : ", height)
        
        weight = (weightInKg ? 10 : 4.536) * Double(currentWeightTextField.text ?? "0")!
        print("weight : ", weight)
        
        if male{
            BMR = (weight) + (height) - (age) + 5
        }
        else{
            BMR = (weight) + (height) - (age) - 161
        }
        
        print("BMR : ", BMR)
        if BMR > 0 {
            if selectActivityOption == 0{
                caloriesPerDay = BMR * 1.2
            }
            else if selectActivityOption == 1{
                caloriesPerDay = BMR * 1.375
            }
            else if selectActivityOption == 2{
                caloriesPerDay = BMR * 1.55
            }
            else if selectActivityOption == 3{
                caloriesPerDay = BMR * 1.725
            }
            else if selectActivityOption == 4{
                caloriesPerDay = BMR * 1.9
            }
            else {
                caloriesPerDay = BMR * 2
            }
            
            caloriesPerDay = caloriesPerDay.rounded(toPlaces: 0)
            calPerDayLabel.text = "\(Int(caloriesPerDay)) Cal per day"
        }
        else{
            calPerDayLabel.text = "something wrong"
        }
        
    }
}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

extension CalIntakeScreen : UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
       // textField.returnKeyType = .next
    }
}
