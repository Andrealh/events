//
//  WorkoutDeleteConformationScreen.swift
//  FitnessFactory
//
//  Created by iroid on 20/07/21.

import UIKit

class WorkoutDeleteConformationScreen: UIViewController {
    
    var removeWorkout : (() -> Void)?

    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var TextLabel: UILabel!
    
    var isExerciseSet: Bool = false
    var isWorkout: Bool = false
    var WorkoutId = Int()
    var removeExercise: WorkoutExerciseMultipleSetListResponse?
    var delegate : hideBackgroundDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initalizedDetails()
    }
    

    //MARK:- initalizedDetails Details
    func initalizedDetails(){
        backgroundView.roundCorners(corners: [.layerMinXMinYCorner, .layerMaxXMinYCorner], radius: 52)
        if #available(iOS 13.0, *) {
            backgroundView.layer.cornerCurve = .continuous
        } else {
            // Fallback on earlier versions
        }
        
        if isWorkout{
            TextLabel.text = "Are you sure you want to remove this workout?"
        }
        else{
            if isExerciseSet{
                TextLabel.text = "Are you sure you want to remove this exercise?"
            }
            else{
                TextLabel.text = "Are you sure you want to remove this exercise?"
            }
        }
    }

    @IBAction func onYes(_ sender: UIButton) {
        delegate?.hideView()
        if isWorkout{
            workoutDeleteAPI()
        }
        else{
            if isExerciseSet{
                workoutExerciseSetDeleteAPI()
            }
            else{
                workoutExerciseDeleteAPI()
            }
        }
    }
    @IBAction func onNo(_ sender: UIButton) {
        delegate?.hideView()
        self.dismiss(animated: true, completion: nil)
    }
}

extension WorkoutDeleteConformationScreen {
    //MARK:- workout Delete API
    func workoutDeleteAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            MyWorkoutService.shared.workoutDelete(id: WorkoutId){ (statusCode, response) in
                Utility.hideIndicator()
                print(response.message ?? "")
               // if response.message ?? "" == "workout delete successfully."{
                if response.message ?? "" == "workout delete successfully."{
                    self.dismiss(animated: true, completion: { [weak self] in
                        self?.removeWorkout?()
                    })
                }
                else{
                    Utility.showAlert(vc: self, message: response.message ?? "")
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:- workout Exercise Delete API
    func workoutExerciseDeleteAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            MyWorkoutService.shared.workoutExerciseDelete(id: WorkoutId){ (statusCode, response) in
                Utility.hideIndicator()
                print(response.message ?? "")
                if response.message ?? "" == "Workout exercise remove successfully."{
                    self.dismiss(animated: true, completion: { [weak self] in
                        self?.removeWorkout?()
                    })
                }
                else{
                    Utility.showAlert(vc: self, message: response.message ?? "")
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:- workout Exercise set Delete API
    func workoutExerciseSetDeleteAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            MyWorkoutService.shared.workoutExerciseSetDelete(id: WorkoutId){ (statusCode, response) in
                Utility.hideIndicator()
                print(response.message ?? "")
                if response.message ?? "" == "Workout exercise remove successfully."{
                    
                    self.dismiss(animated: true, completion: { [weak self] in
                        self?.removeWorkout?()
                    })
                }
                else{
                    Utility.showAlert(vc: self, message: response.message ?? "")
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}
