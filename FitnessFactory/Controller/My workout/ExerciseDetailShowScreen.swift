//
//  ExerciseDetailShowScreen.swift
//  FitnessFactory
//
//  Created by iroid on 03/09/21.
//

import UIKit

class ExerciseDetailShowScreen: UIViewController {
    @IBOutlet weak var backGroundView: UIView!
    @IBOutlet weak var addWorkoutView: UIView!
    @IBOutlet weak var selectImageView: UIView!
    @IBOutlet weak var descriptionView: dateSportView!
    @IBOutlet weak var workoutTitle: UILabel!
    @IBOutlet weak var titleWorkoutTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var workoutImageView: UIImageView!
    @IBOutlet weak var selectView: UIView!
    @IBOutlet weak var removeButton: UIButton!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var editExerciseSet  : WorkoutExerciseMultipleSetListResponse?
    var showPickWorkoutData : PickWorkoutExerciseListResponse?
    var delegate : hideBackgroundDelegate?
    var isFromPickworkout : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initalizedDetails()
    }

    //MARK:- initalizedDetails Details
    func initalizedDetails(){
        
        addWorkoutView.roundCorners(corners: [.layerMinXMinYCorner, .layerMaxXMinYCorner], radius: 52)
        if #available(iOS 13.0, *) {
            addWorkoutView.layer.cornerCurve = .continuous
        } else {
            // Fallback on earlier versions
        }
        if isFromPickworkout {
            self.descriptionLabel.text = showPickWorkoutData?.desc
            self.workoutTitle.text = showPickWorkoutData?.title
            
            if let image = showPickWorkoutData?.image {
                Utility.setImage(image, imageView: workoutImageView)
                selectImageView.isHidden = false
            } else {
                selectImageView.isHidden = true
                descriptionView.isHidden = true
            }
        } else {
            self.descriptionLabel.text = editExerciseSet?.desc
            self.workoutTitle.text = editExerciseSet?.title
            
            if let image = editExerciseSet?.image {
                Utility.setImage(image, imageView: workoutImageView)
                selectImageView.isHidden = false
            } else {
                selectImageView.isHidden = true
                descriptionView.isHidden = true
            }
        }
        
    }

   
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.dismiss(animated: false, completion: { [weak self] in
            self?.delegate?.hideView()
        })
    }
    @IBAction func onOkay(_ sender: UIButton) {
        self.dismiss(animated: false, completion: { [weak self] in
            self?.delegate?.hideView()
        })
    }
}
