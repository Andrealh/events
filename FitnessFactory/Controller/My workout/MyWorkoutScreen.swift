//
//  MyWorkoutScreen.swift
//  FitnessFactory
//
//  Created by iroid on 20/07/21.
//

import UIKit
import SkeletonView

class MyWorkoutScreen: UIViewController {
    
    @IBOutlet weak var myWorkoutTableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var createYourOwnButton: UIButton!
    @IBOutlet weak var backgroundView: UIView!
    
    var workoutNameList = [WorkoutResponse]()
    var pickWorkoutId = Int()
    var meta:Meta!
    var hasMorePage : Bool = false
    var page = 1
    var isFromCalender = Bool()
    var isFromAddMore = Bool()
//    var titleName = String()
    var isBack : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initalizedDetails()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        tabBarController?.tabBar.isHidden = false
//    }
//    
//    override func viewDidDisappear(_ animated: Bool) {
//        tabBarController?.tabBar.isHidden = true
//    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        createYourOwnButton.layoutIfNeeded()
        createYourOwnButton.addLineDashedStroke(pattern: [12,6], radius: 42, color: UIColor.primaryColour.cgColor)
    }
    
    //MARK:- initalizedDetails Details
    func initalizedDetails(){
        let nib = UINib(nibName: "ExcercisesCell", bundle: nil)
        myWorkoutTableView.register(nib, forCellReuseIdentifier: "ExcercisesCell")
        myWorkoutTableView.tableFooterView = UIView()
        myWorkoutTableView.rowHeight = 80
        myWorkoutTableView.estimatedRowHeight = 80
        myWorkoutTableView.isSkeletonable = true
        let gradient = SkeletonGradient(baseColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.1310794454))
        myWorkoutTableView.showAnimatedGradientSkeleton(usingGradient: gradient, animation: nil, transition: .crossDissolve(0.5))
        self.workoutAPI(Id: self.pickWorkoutId, Page: 1)
//        titleLabel.text = titleName
    }
    
    // MARK: scroll method
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let size = scrollView.contentSize
        let inset = scrollView.contentInset
        let y = offset.y + bounds.size.height - inset.bottom
        let h = size.height
        let reload_distance:CGFloat = 10.0
        if y > (h + reload_distance) {
            print("called")
            if let metaTotal = self.meta?.total{
                if self.workoutNameList.count != metaTotal{
                    if self.hasMorePage{
                        self.hasMorePage = false
                        self.page += 1
                        workoutAPI(Id: pickWorkoutId, Page: page)
                    }
                }
            }
        }
    }
    
    func appendDataCollectionView(data: [WorkoutResponse]){
        var indexPathArray: [IndexPath] = []
        for i in self.workoutNameList.count..<self.workoutNameList.count + data.count{
            indexPathArray.append(IndexPath(item: i, section: 0))
        }
        self.workoutNameList.append(contentsOf: data)
        self.myWorkoutTableView.insertRows(at: indexPathArray, with: .automatic)
    }
    
    @IBAction func onBack(_ sender: UIButton) {
        //tabBarController?.tabBar.isHidden = false
       if isBack {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: ExerciseScreen.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
//            tabBarController?.selectedIndex = 1
//            self.navigationController?.popToRootViewController(animated: true)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    @IBAction func onCreateYourOwn(_ sender: UIButton) {
        backgroundView.isHidden = false
        let vc =  STORYBOARD.myWorkout.instantiateViewController(withIdentifier: "AddWorkoutScreen") as! AddWorkoutScreen
        vc.modalPresentationStyle = .overFullScreen
        vc.AddWorkout = true
        vc.isEdit = false
        vc.delegate = self
        vc.createWorkout = { [weak self] value in
           // self?.workoutAPI(Id: self?.pickWorkoutId ?? 0, Page: 1)
            self?.tabBarController?.tabBar.isHidden = true
            let controller = STORYBOARD.myWorkout.instantiateViewController(withIdentifier:"ExerciseDetailsScreen") as! ExerciseDetailsScreen
            controller.workoutId = value.workout_id ?? 0
            controller.delegate = self
            controller.editWorkoutDelegate = self
            controller.isFromCalenderScreen = self!.isFromCalender
            controller.isFromAddMore = self!.isFromAddMore
            controller.isFromCreate = true
            controller.onBack = {[weak self] in
                self?.workoutAPI(Id: self?.pickWorkoutId ?? 0, Page: 1)
            }
            self?.navigationController?.pushViewController(controller, animated: true)
        }
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func onHome(_ sender: UIButton) {
        //tabBarController?.tabBar.isHidden = false
        tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: true)
    }
}

extension MyWorkoutScreen : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return workoutNameList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExcercisesCell") as! ExcercisesCell
        if !workoutNameList.isEmpty {
            cell.Workout = workoutNameList[indexPath.row]
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let controller = STORYBOARD.myWorkout.instantiateViewController(withIdentifier:"WorkoutDetailsScreen") as! WorkoutDetailsScreen
//        controller.WorkoutId = workoutNameList[indexPath.row].workout_id ?? 0
//        controller.delegate = self
//        self.navigationController?.pushViewController(controller, animated: true)
        tabBarController?.tabBar.isHidden = true
        let controller = STORYBOARD.myWorkout.instantiateViewController(withIdentifier:"ExerciseDetailsScreen") as! ExerciseDetailsScreen
        controller.workoutId = workoutNameList[indexPath.row].workout_id ?? 0
        controller.delegate = self
        controller.editWorkoutDelegate = self
        controller.isFromCalenderScreen = isFromCalender
        controller.isFromAddMore = isFromAddMore
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            let alertController = UIAlertController(title: "Are you sure?", message: "You want to delete this workout?", preferredStyle: .alert)
            
            // Create the actions
            let okAction = UIAlertAction(title: "Yes, delete it!", style: UIAlertAction.Style.default) { [self]
                UIAlertAction in
                let selectWorkout = workoutNameList[indexPath.row]
                workoutNameList.remove(at: indexPath.row)
                workoutDeleteAPI(workoutId: selectWorkout.workout_id ?? 0)
                //self.getNearByGym(category: "", searchText: "")
                NSLog("OK Pressed")
            }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default) { [self]
                UIAlertAction in
                //self.getNearByGym(category: "", searchText: "")
                NSLog("OK Pressed")
            }
            // Add the actions
            okAction.setValue(UIColor.red, forKey: "titleTextColor")
            cancelAction.setValue(UIColor.gray, forKey: "titleTextColor")
            alertController.addAction(cancelAction)
            alertController.addAction(okAction)
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
           
        }
    }
}

extension MyWorkoutScreen {
    //MARK:- workout API
    func workoutAPI(Id:Int,Page:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
           // Utility.showIndecator()
            MyWorkoutService.shared.workoutList(Page: Page){ [self] (statusCode, response) in
                Utility.hideIndicator()
                hasMorePage = true
                if let res = response.workoutResponse{
                    if page == 1{
                        self.workoutNameList = []
                        self.workoutNameList = res
                        print(self.workoutNameList.toJSON())
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            self.myWorkoutTableView.stopSkeletonAnimation()
                            self.view.hideSkeleton(reloadDataAfter: true, transition: .crossDissolve(0.25))
                            if workoutNameList.count == 0 {
                                let noDataLabel = UILabel(frame: CGRect(x: 0, y: 50, width: myWorkoutTableView.bounds.size.width, height: 100))
                                noDataLabel.text = "You have no workouts."
                                noDataLabel.textColor = UIColor.darkGray
                                noDataLabel.textAlignment = .center
                                noDataLabel.numberOfLines = 0
                                myWorkoutTableView.backgroundView = noDataLabel
                            } else {
                                myWorkoutTableView.backgroundView = nil
                            }
                            self.myWorkoutTableView.reloadData()
                        }
                    } else {
                        self.appendDataCollectionView(data: res)
                    }
                    if let meta = response.metaResponse{
                        self.meta = meta
                    }
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                stronSelf.hasMorePage = true
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:- workout Delete API
    func workoutDeleteAPI(workoutId:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            MyWorkoutService.shared.workoutDelete(id: workoutId){ (statusCode, response) in
                Utility.hideIndicator()
                print(response.message ?? "")
               // if response.message ?? "" == "workout delete successfully."{
//                if response.message ?? "" == "workout delete successfully."{
//                    self.dismiss(animated: false, completion: { [weak self] in
//                        self?.removeWorkout?()
//                    })
//                }
//                else{
//                    Utility.showAlert(vc: self, message: response.message ?? "")
//                }
                self.myWorkoutTableView.reloadData()
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}

extension MyWorkoutScreen: WorkOutDeleteDelegate {
    func WorkOutDelete() {
        workoutAPI(Id: pickWorkoutId, Page: 1)
    }
}

extension MyWorkoutScreen : hideBackgroundDelegate {
    func hideView() {
        backgroundView.isHidden = true
    }
}

extension MyWorkoutScreen : EditWorkOutDelegate {
    func editWorkOut() {
        workoutAPI(Id: pickWorkoutId, Page: 1)
    }
}

extension MyWorkoutScreen : SkeletonTableViewDataSource {
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "ExcercisesCell"
    }
    
    func collectionSkeletonView(_ skeletonView: UITableView, numberOfRowsInSection section: Int) -> Int {
        2
    }
}






