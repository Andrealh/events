//
//  ListOfGifsScreen.swift
//  FitnessFactory
//
//  Created by iroid on 30/07/21.
//

import UIKit

class ListOfGifsScreen: UIViewController {
    
    @IBOutlet weak var listOfGifsCollectionView: UICollectionView!
    
    //MARK:- Variables
    var meta:Meta!
    var hasMorePage : Bool = false
    var page = 1
    var listOfGifArray = [GifListResponse]()
    var isFromWorkout = Bool()
    var delegate : SelectImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initalizedDetails()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onBack(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onHome(_ sender: UIButton) {
        //tabBarController?.tabBar.isHidden = false
        tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    //MARK:- initalizedDetails Details
    func initalizedDetails(){
        let nib = UINib(nibName: "ImageCell", bundle: nil)
        listOfGifsCollectionView.register(nib, forCellWithReuseIdentifier: "ImageCell")
        if isFromWorkout {
            gifListAPI(Id: 1, Page: 1)
        } else {
            gifListAPI(Id: 2, Page: 1)
        }
        
    }
    
    // MARK: scroll method
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let size = scrollView.contentSize
        let inset = scrollView.contentInset
        let y = offset.y + bounds.size.height - inset.bottom
        let h = size.height
        let reload_distance:CGFloat = 10.0
        if y > (h + reload_distance) {
            print("called")
            if let metaTotal = self.meta?.total{
                if self.listOfGifArray.count != metaTotal{
                    if self.hasMorePage{
                        self.hasMorePage = false
                        self.page += 1
                        gifListAPI(Id: 1, Page: page)
                    }
                }
            }
        }
    }
    
    func appendDataCollectionView(data: [GifListResponse]){
        var indexPathArray: [IndexPath] = []
        for i in self.listOfGifArray.count..<self.listOfGifArray.count + data.count{
            indexPathArray.append(IndexPath(item: i, section: 0))
        }
        self.listOfGifArray.append(contentsOf: data)
        self.listOfGifsCollectionView.insertItems(at: indexPathArray)
    }
}

extension ListOfGifsScreen : UICollectionViewDelegate , UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listOfGifArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as! ImageCell
        cell.item = listOfGifArray[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = CGFloat(154)
        let width = collectionView.frame.size.width/2
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            let img = listOfGifArray[indexPath.row].gif ?? ""
            self.delegate.selectImageOfWorkout(selectImg: img)
            self.dismiss(animated: true, completion: nil)
    }
}

extension ListOfGifsScreen {
    //MARK:- List of Gif API
    func gifListAPI(Id:Int,Page:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            MyAccountService.shared.gifList(Id: Id,Page: Page){ [self] (statusCode, response) in
                Utility.hideIndicator()
                self.hasMorePage = true
                if let res = response.gifListResponse{
                    if page == 1{
                        listOfGifArray = res
                        listOfGifsCollectionView.reloadData()
                    } else {
                        self.appendDataCollectionView(data: res)
                    }
                    if let meta = response.metaResponse{
                        self.meta = meta
                    }
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                stronSelf.hasMorePage = true
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}
