//
//  WorkoutDetailsScreen.swift
//  FitnessFactory
//
//  Created by iroid on 20/07/21.
//

import UIKit

protocol WorkOutDeleteDelegate {
    func WorkOutDelete()
}

class WorkoutDetailsScreen: UIViewController {
    
    @IBOutlet weak var editView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var addExerciseButton: UIButton!
    @IBOutlet weak var exerciseNameListTableView: UITableView!
    
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var safeAreaButtomView: UIView!
    var WorkoutId: Int = 0
    var imageUrl: String = ""
    var isFromCalenderScreen: Bool = false
    var meta:Meta!
    var exerciseList = [WorkoutExerciseResponse]()
    var hasMorePage : Bool = false
    var page = 1
    
    var delegate:WorkOutDeleteDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initalizedDetails()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        addExerciseButton.layoutIfNeeded()
        addExerciseButton.addLineDashedStroke(pattern: [12,6], radius: 42, color: #colorLiteral(red: 0.8039215686, green: 0.9490196078, blue: 0.4392156863, alpha: 1))
    }
    
    //MARK:- initalizedDetails Details
    func initalizedDetails(){
        let nib = UINib(nibName: "ExcercisesCell", bundle: nil)
        exerciseNameListTableView.register(nib, forCellReuseIdentifier: "ExcercisesCell")
        exerciseNameListTableView.tableFooterView = UIView()
        workoutDetailsAPI()
        exerciseListAPI(Id: WorkoutId, Page: 1)
        addExerciseButton.isHidden = isFromCalenderScreen
        editView.isHidden = isFromCalenderScreen
        buttonView.isHidden = isFromCalenderScreen
        safeAreaButtomView.isHidden = isFromCalenderScreen
        
    }
    
    // MARK: scroll method
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let size = scrollView.contentSize
        let inset = scrollView.contentInset
        let y = offset.y + bounds.size.height - inset.bottom
        let h = size.height
        let reload_distance:CGFloat = 10.0
        if y > (h + reload_distance) {
            print("called")
            if let metaTotal = self.meta?.total{
                if self.exerciseList.count != metaTotal{
                    if self.hasMorePage{
                        self.hasMorePage = false
                        self.page += 1
                        exerciseListAPI(Id: WorkoutId, Page: page)
                    }
                }
            }
        }
    }
    
    func appendDataCollectionView(data: [WorkoutExerciseResponse]){
        var indexPathArray: [IndexPath] = []
        for i in self.exerciseList.count..<self.exerciseList.count + data.count{
            indexPathArray.append(IndexPath(item: i, section: 0))
        }
        self.exerciseList.append(contentsOf: data)
        self.exerciseNameListTableView.insertRows(at: indexPathArray, with: .automatic)
    }
    
    @IBAction func onBack(_ sender: UIButton) {
        if isFromCalenderScreen{
           // self.tabBarController?.tabBar.isHidden = false
        }
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onEdit(_ sender: Any) {
        let vc =  STORYBOARD.myWorkout.instantiateViewController(withIdentifier: "AddWorkoutScreen") as! AddWorkoutScreen
        vc.modalPresentationStyle = .overFullScreen
        vc.AddWorkout = true
        vc.isEdit = true
        vc.id = WorkoutId
        vc.titleString = titleLabel.text ?? ""
        vc.descriptionString = descriptionLabel.text ?? ""
        vc.imageString = imageUrl
        vc.dismissScreen = { [weak self] in
            self?.delegate?.WorkOutDelete()
            self?.workoutDetailsAPI()
        }
        vc.dismissRemoveScreen = { [weak self] in
            let vc =  STORYBOARD.myWorkout.instantiateViewController(withIdentifier: "WorkoutDeleteConformationScreen") as! WorkoutDeleteConformationScreen
            vc.modalPresentationStyle = .overFullScreen
            vc.isWorkout = true
            vc.WorkoutId = self?.WorkoutId ?? 0
            vc.removeWorkout =  { [weak self] in
                self?.delegate?.WorkOutDelete()
                self?.navigationController?.popViewController(animated: true)
            }
            self?.present(vc, animated: false, completion: nil)
        }
        self.present(vc, animated: false, completion: nil)
    }
    
    @IBAction func onAddExercise(_ sender: UIButton) {
        let vc =  STORYBOARD.myWorkout.instantiateViewController(withIdentifier: "AddWorkoutScreen") as! AddWorkoutScreen
        vc.modalPresentationStyle = .overFullScreen
        vc.id = WorkoutId
        vc.isEdit = false
        vc.AddWorkout = false
        
        vc.dismissScreen = { [weak self] in
            self?.exerciseListAPI(Id: self?.WorkoutId ?? 0, Page: 1)
        }
        self.present(vc, animated: false, completion: nil)
    }
    @IBAction func onAddWoroutToDate(_ sender: UIButton) {
        let vc =  STORYBOARD.nutrition.instantiateViewController(withIdentifier: "SelectDateForMealScreen") as! SelectDateForMealScreen
        vc.isFromNutrition = false
        vc.mealId = WorkoutId
        // vc.workoutId = WorkoutId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func onHome(_ sender: UIButton) {
        tabBarController?.tabBar.isHidden = false
        tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: true)
    }
}

extension WorkoutDetailsScreen : UITableViewDelegate , UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return exerciseList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExcercisesCell") as! ExcercisesCell
        cell.exerciseList = exerciseList[indexPath.row]
       //
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let controller = STORYBOARD.myWorkout.instantiateViewController(withIdentifier:"ExerciseDetailsScreen") as! ExerciseDetailsScreen
//       // controller.delegate = self
//        controller.exerciseId = exerciseList[indexPath.row].workout_exercise_id ?? 0
//        controller.isFromCalenderScreen = isFromCalenderScreen
//        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    //    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    //        if(self.exerciseList.count > 0 && self.metaData != nil){
    //            if(self.exerciseList.count - 1 == indexPath.row){
    //                if(self.metaData.currentPage! < self.metaData.lastPage!){
    //                    exerciseListAPI(Id: WorkoutId, Page: self.metaData.currentPage! + 1)
    //                   // self.getFomattedPostList(page: self.metaData.currentPage! + 1,showLoader: true)
    //                }
    //            }
    //        }
    //    }
}


extension WorkoutDetailsScreen {
    //MARK:- workout Detsials API
    func workoutDetailsAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            MyWorkoutService.shared.workoutDetails(id: WorkoutId){ (statusCode, response) in
                Utility.hideIndicator()
                if let res = response.workoutDetailsResponse{
                    self.titleLabel.text = res.title
                    self.descriptionLabel.text = res.desc
                    self.imageUrl = res.image ?? ""
                }
                
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:- Exercise List
    func exerciseListAPI(Id:Int,Page:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            MyWorkoutService.shared.workoutExerciseList(Id: Id, Page: Page){ [self] (statusCode, response) in
                Utility.hideIndicator()
                self.hasMorePage = true
                if let res = response.workoutExerciseResponse{
                    if page == 1{
                        self.exerciseList = res
                        print(self.exerciseList.toJSON())
                        self.exerciseNameListTableView.reloadData()
                    } else {
                        self.appendDataCollectionView(data: res)
                    }
                    if let meta = response.metaResponse{
                        self.meta = meta
                    }
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                stronSelf.hasMorePage = true
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    
}

extension WorkoutDetailsScreen: WorkOutExerciseDeleteDelegate{
    func WorkOutExerciseDelete() {
        exerciseListAPI(Id: WorkoutId, Page: 1)
    }
}
