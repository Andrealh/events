//
//  ListOfImageScreen.swift
//  FitnessFactory
//
//  Created by iroid on 30/07/21.
//

import UIKit

class ListOfImageScreen: UIViewController {

    @IBOutlet weak var listOfImageCollectionView: UICollectionView!
    
    var listOfImageArray = [GifListResponse]()
    var isFromWorkout = Bool()
    var delegate : SelectImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initalizedDetails()
    }
    
    //MARK:- initalizedDetails Details
    func initalizedDetails(){
        let nib = UINib(nibName: "ImageCell", bundle: nil)
        listOfImageCollectionView.register(nib, forCellWithReuseIdentifier: "ImageCell")
        if isFromWorkout {
            imageListAPI(Id: 1)
        } else {
            imageListAPI(Id: 2)
        }
        
    }
    
    @IBAction func onBack(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onHome(_ sender: UIButton) {
        //tabBarController?.tabBar.isHidden = false
        tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: true)
    }
}

extension ListOfImageScreen : UICollectionViewDelegate , UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listOfImageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as! ImageCell
        cell.item = listOfImageArray[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = CGFloat(154)
        let width = collectionView.frame.size.width/2
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let img = listOfImageArray[indexPath.row].gif ?? ""
        self.delegate.selectImageOfWorkout(selectImg: img)
        self.dismiss(animated: true, completion: nil)
    }
}

protocol  SelectImage : NSObject {
    func selectImageOfWorkout(selectImg: String)
}

extension ListOfImageScreen {
    //MARK:- List of Gif API
    func imageListAPI(Id:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            MyAccountService.shared.imageList(Id: Id){ [self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response.gifListResponse{
                    listOfImageArray = []
                    listOfImageArray.append(contentsOf: res)
                    listOfImageCollectionView.reloadData()
                }
                
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}
