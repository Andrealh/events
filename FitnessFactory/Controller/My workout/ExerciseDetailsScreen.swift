//
//  ExerciseDetailsScreen.swift
//  FitnessFactory
//
//  Created by iroid on 22/07/21.
//

import UIKit
import SkeletonView
 

class ExerciseDetailsScreen: UIViewController {
    
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var addWorkoutToDateButton: UIButton!
    @IBOutlet weak var pauseBUtton: UIButton!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var startTimerButton: UIButton!
    var lastContentOffset: CGFloat = 0
    @IBOutlet weak var exerciseImageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var exerciseSetTableView: UITableView!
   
    
    @IBOutlet weak var timerView: UIView!
    @IBOutlet weak var timerView1: UIButton!
    @IBOutlet weak var timerView2: UIView!
    @IBOutlet weak var timerView3: UIButton!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var workoutImageView: UIView!

    @IBOutlet weak var AddButtonHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var addExerciseBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var StackViewConstarint: NSLayoutConstraint!
    @IBOutlet weak var TableViewHeightConstarint: NSLayoutConstraint!
    @IBOutlet weak var buttonViewConstarint: NSLayoutConstraint!
    @IBOutlet weak var ImageViewHeightConstarint: NSLayoutConstraint!
    var delegate: WorkOutDeleteDelegate?
    var editWorkoutDelegate : EditWorkOutDelegate?
    
    var exerciseSetList = [WorkoutExerciseMultipleSetListResponse]()
    var exerciseSetList2 = [WorkoutExerciseMultipleSetListResponse]()
    var exerciseImage: String = ""
    var exerciseSetListId: Int = 0
    var hasMorePage : Bool = false
    var page = 1
    var meta:Meta!
    var isFromCalenderScreen: Bool = false
    var workoutId: Int = 0
    var isFromHome = Bool()
    var isFromAddMore = Bool()
    var onBack:(() -> Void)?
    var isFromCreate = Bool()
    private var timer: Timer?
    private var dateTimer: Date?
    var resumeTapped = false
    private var interval = Double()
    private var Totalinterval = Double()
  
    override func viewDidLoad() {
        super.viewDidLoad()
        initalizedDetails()
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longPressed))
        self.exerciseSetTableView.addGestureRecognizer(longPressRecognizer)
        let tap = UITapGestureRecognizer(target: self, action: #selector(doubleTapped))
            tap.numberOfTapsRequired = 2
            view.addGestureRecognizer(tap)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        exerciseSetTableView.reloadData()
    }
    override func viewWillAppear(_ animated: Bool) {
        exerciseSetTableView.reloadData()
    }
    override func viewWillDisappear(_ animated: Bool) {
        if(exerciseSetList.count != 0){
            var qxr : [workout_exercises_new_order] = []
            
            for i in 0...exerciseSetList.count - 1 {
                let x = workout_exercises_new_order(id: exerciseSetList[i].workout_exercise_id!, order: i)
                print(x.id)
                
                qxr.append(x.self)
            }
            do {
            let data = orderRequest(workout_exercises_new_order: qxr, workout_id: exerciseSetList[0].workout_id)
                MyWorkoutService.shared.updateOrder(parameters: data.toJSON()) { [weak self] (statusCode, response) in
                        } failure: { [weak self] (error) in
                            guard let stronSelf = self else { return }
                            Utility.hideIndicator()
                            Utility.showAlert(vc: stronSelf, message: error)
                        }
            } catch { print(error) }
            
                    }
        timer?.invalidate()
    }
    func jsonToDictionary(from text: String) -> [String: Any]? {
        guard let data = text.data(using: .utf8) else { return nil }
        let anyResult = try? JSONSerialization.jsonObject(with: data, options: [])
        return anyResult as? [String: Any]
    }

    //MARK:- initalizedDetails Details
    func initalizedDetails(){
        Totalinterval = 0.0
        let nib = UINib(nibName: "workoutDetailCell", bundle: nil)
        exerciseSetTableView.register(nib, forCellReuseIdentifier: "workoutDetailCell")
        exerciseSetTableView.tableFooterView = UIView()

        workoutDetailsAPI(workoutId: workoutId)
        workoutExerciseSetListAPI(Id: workoutId, Page: 1)
        editButton.isHidden = isFromCalenderScreen
        if isFromCalenderScreen {
            addWorkoutToDateButton.isHidden = true
            tabBarController?.tabBar.isHidden = false
        }
    }
    var heightSub : CGFloat = 0
    // MARK: scroll method
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        titleLabel.isHidden = true;
//        descriptionLabel.isHidden = true;
        if(descriptionLabel.calculateMaxLines() > 2 && titleLabel.calculateMaxLines() > 2){
            heightSub = -160
        }else{
            if(descriptionLabel.calculateMaxLines() > 4 || titleLabel.calculateMaxLines() > 3){
                if(descriptionLabel.calculateMaxLines() >= 3 && titleLabel.calculateMaxLines() >= 2){
                    heightSub = -160
                }else{
                    heightSub = -150
                    
                }
            }
            else{
            if(descriptionLabel.calculateMaxLines() > 2 || titleLabel.calculateMaxLines() > 2){
                heightSub = -130
                
            }else{
                    heightSub = -180}
            }}
        if(exerciseSetList.count > 3 ){
        let z = scrollView.contentOffset.y
        if self.lastContentOffset < scrollView.contentOffset.y {
            if(TableViewHeightConstarint.constant > -10){
                if(z > 80 ){
                        if(StackViewConstarint.constant - z <= heightSub){
                        UIView.animate(withDuration: 0.25) {
                        self.view.layoutIfNeeded()
                        self.titleLabel.isHidden = true
                        self.descriptionLabel.isHidden = true
                            self.editButton.isHidden = true
                    };
                            TableViewHeightConstarint.constant = 0
                            StackViewConstarint.constant = CGFloat(heightSub);
                            buttonViewConstarint.constant = -2
                            ImageViewHeightConstarint.constant = -2
                          //  AddButtonHeightConstraint.constant = 10
                            addExerciseBottomConstraint.constant = 0
                        }else{
                            if(StackViewConstarint.constant <= 40){
                                self.titleLabel.isHidden = true
                               self.descriptionLabel.isHidden = true
                            }
                            StackViewConstarint.constant -= z
                        }
                }else{
                    if(TableViewHeightConstarint.constant - z <= 0){
                        if(StackViewConstarint.constant - z <= heightSub){
                            
                        
                        TableViewHeightConstarint.constant = 0
                        UIView.animate(withDuration: 0.5) {
                            self.view.layoutIfNeeded()
                                                        self.titleLabel.isHidden = true
                                                       self.descriptionLabel.isHidden = true
                        }
                            StackViewConstarint.constant = heightSub;
                            buttonViewConstarint.constant = -2
                            ImageViewHeightConstarint.constant = -2
                          //  AddButtonHeightConstraint.constant = 10
                            addExerciseBottomConstraint.constant = 3}
                        else{
                            if(StackViewConstarint.constant <= 40){
                                self.titleLabel.isHidden = true
                               self.descriptionLabel.isHidden = true
                            }
                            StackViewConstarint.constant -= z
                        }
                    }else{
                        TableViewHeightConstarint.constant -= z
                        StackViewConstarint.constant -= z
                        if(addExerciseBottomConstraint.constant > -5 ){
                        addExerciseBottomConstraint.constant -= 1;
                        }
                        
                    }}
                
            }
        } else if self.lastContentOffset > scrollView.contentOffset.y {
            print(z)
            print(StackViewConstarint.constant)
            print(TableViewHeightConstarint.constant)
            if(TableViewHeightConstarint.constant < 20){
                if(TableViewHeightConstarint.constant >= 13 && TableViewHeightConstarint.constant < 20 && StackViewConstarint.constant >= -45 && StackViewConstarint.constant <= 24 ){
                    TableViewHeightConstarint.constant = 20
                    titleLabel.isHidden = false
                    descriptionLabel.isHidden = false
                    buttonViewConstarint.constant = 20.5
                    ImageViewHeightConstarint.constant = 20.5
                    AddButtonHeightConstraint.constant = 50
                        StackViewConstarint.constant = 24
                    editButton.isHidden = false
                }
                print("x")
               // TableViewHeightConstarint.constant = 20
                if( TableViewHeightConstarint.constant + (z * -1) >= 20){
                    if(StackViewConstarint.constant + (z
                       * -1) >= 24){
                    TableViewHeightConstarint.constant = 20
                    titleLabel.isHidden = false
                    descriptionLabel.isHidden = false
                    buttonViewConstarint.constant = 20.5
                    ImageViewHeightConstarint.constant = 20.5
                    AddButtonHeightConstraint.constant = 50
                        editButton.isHidden = false
                        StackViewConstarint.constant = 24}
                    else{
                        if(StackViewConstarint.constant >= -20){
                            self.titleLabel.isHidden = false
                            editButton.isHidden = false
                           self.descriptionLabel.isHidden = false
                        }
                        StackViewConstarint.constant += z * -1
                    }
                }else{
                    print("0x")
                    StackViewConstarint.constant += z * -1
                    TableViewHeightConstarint.constant += z * -1
                    if(buttonViewConstarint.constant + (z * -1) <=  20.5){
                        buttonViewConstarint.constant += z * -1;
                        ImageViewHeightConstarint.constant += z * -1;
                    }
                }
            }else{
                print("xo")
            }
        } else {
            // didn't move
        }
        }
        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let size = scrollView.contentSize
        let inset = scrollView.contentInset
        let y = offset.y + bounds.size.height - inset.bottom
        let h = size.height
        let reload_distance:CGFloat = 10.0
        if y > (h + reload_distance) {
            if let metaTotal = self.meta?.total{
                if self.exerciseSetList.count != metaTotal{
                    if self.hasMorePage{
                        self.hasMorePage = false
                        self.page += 1
                        self.workoutExerciseSetListAPI(Id: self.workoutId , Page: page)
                    }
                }
            }
        }
    }

    
    func appendDataCollectionView(data: [WorkoutExerciseMultipleSetListResponse]){
        var indexPathArray: [IndexPath] = []
        for i in self.exerciseSetList.count..<self.exerciseSetList.count + data.count{
            indexPathArray.append(IndexPath(item: i, section: 0))
        }
        self.exerciseSetList.append(contentsOf: data)
       // self.exerciseSetList[indexPathArray].color
        self.exerciseSetTableView.insertRows(at: indexPathArray, with: .automatic)
    }
    
    @objc func timerUpdate() {
        if (dateTimer != nil) {
            interval = Date().timeIntervalSince(dateTimer!)
            interval = interval + Totalinterval
            let millisec = Int(interval * 100) % 100
            let seconds = Int(interval) % 60
            let minutes = Int(interval) / 60
            timerLabel.text = String(format: "%02d:%02d:%02d", minutes, seconds, millisec)
        }
    }
    
    func timerStart() {
        dateTimer = Date()
        timer = Timer.scheduledTimer(timeInterval: 0.07, target: self, selector: #selector(timerUpdate), userInfo: nil, repeats: true)
        RunLoop.main.add(timer!, forMode: RunLoop.Mode.common)
//        let timer = Timer(timeInterval: 1, target: self, selector: #selector(update), userInfo: nil, repeats: true)
//        RunLoop.main.add(timer, forMode: RunLoop.Mode.common)
    }
   
    @IBAction func onStartTimer(_ sender: UIButton) {
        timerView.isHidden = false
        startTimerButton.isHidden = true
       timerStart()
        //runTimer()
    }
    
    
    @IBAction func onPause(_ sender: UIButton) {
        if self.resumeTapped == false {
            timer?.invalidate()
            self.resumeTapped = true
            self.pauseBUtton.setTitle("Resume",for: .normal)
            Totalinterval = interval
        } else {
            timerStart()
            self.resumeTapped = false
            self.pauseBUtton.setTitle("Pause",for: .normal)
        }
    }
    
    
    @IBAction func onReset(_ sender: UIButton) {
        self.resumeTapped = false
        self.pauseBUtton.setTitle("Pause",for: .normal)
        timer?.invalidate()
        Totalinterval = 0.0
        timerLabel.text = "00:00:00"
        startTimerButton.isHidden = false
        timerView.isHidden = true
        //timerStart()
    }
    
    @IBAction func onBack(_ sender: UIButton) {
        tabBarController?.tabBar.isHidden = false
        if isFromCreate {
            onBack!()
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onHome(_ sender: UIButton) {
        //tabBarController?.tabBar.isHidden = false
        tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func onAddtoWorkout(_ sender: UIButton) {
        let vc =  STORYBOARD.nutrition.instantiateViewController(withIdentifier: "SelectDateForMealScreen") as! SelectDateForMealScreen
        vc.isFromNutrition = false
        vc.mealId = workoutId
       // vc.isFromCalender = isFromCalenderScreen
        vc.isFromAddMore = isFromAddMore
        // vc.workoutId = WorkoutId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func onEdit(_ sender: UIButton) {
        backgroundView.isHidden = false
        let vc =  STORYBOARD.myWorkout.instantiateViewController(withIdentifier: "AddWorkoutScreen") as! AddWorkoutScreen
        vc.modalPresentationStyle = .overFullScreen
        vc.AddWorkout = true
        vc.isEdit = true
        vc.id = workoutId
        vc.delegate = self
        vc.titleString = titleLabel.text ?? ""
        vc.descriptionString = descriptionLabel.text ?? ""
        vc.imageString = exerciseImage
        //vc.delegate = self
        vc.dismissScreen = { [weak self] in
            self?.editWorkoutDelegate?.editWorkOut()
            self?.workoutDetailsAPI(workoutId: self?.workoutId ?? 0)
        }
        vc.dismissRemoveScreen = { [weak self] in
            self?.backgroundView.isHidden = false
            let vc =  STORYBOARD.myWorkout.instantiateViewController(withIdentifier: "WorkoutDeleteConformationScreen") as! WorkoutDeleteConformationScreen
            vc.modalPresentationStyle = .overFullScreen
            vc.isWorkout = true
            vc.delegate = self
            vc.WorkoutId = self?.workoutId ?? 0
            vc.removeWorkout =  { [weak self] in
                self?.delegate?.WorkOutDelete()
                //self?.delegate?.WorkOutExerciseDelete()
                self?.navigationController?.popViewController(animated: true)
            }
            self?.present(vc, animated: true, completion: nil)
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func onAddSet(_ sender: UIButton) {
        backgroundView.isHidden = false
        let vc =  STORYBOARD.myWorkout.instantiateViewController(withIdentifier: "AddExerciseSetScreen") as! AddExerciseSetScreen
        vc.modalPresentationStyle = .overFullScreen
        vc.isEdit = false
        vc.delegate = self
        vc.count = exerciseSetList.count
        vc.workoutId = workoutId
        vc.workoutExerciseId = workoutId
        vc.dismissScreen = { [weak self] in
            self?.workoutExerciseSetListAPI(Id: self?.workoutId ?? 0, Page: 1)
            self?.exerciseSetTableView.reloadData()
        }
        self.present(vc, animated: true, completion: nil)
        exerciseSetTableView.reloadData()
    }

    
    @IBAction func onEditButton(_ sender: UIButton)
    {
    }
}

extension ExerciseDetailsScreen : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return exerciseSetList.count
    }
    @IBAction func didTapEditExcercise(){
        if(exerciseSetTableView.isEditing){
            exerciseSetTableView.isEditing = false
        }else{
            exerciseSetTableView.isEditing = true
        }
    }


        
    @objc func doubleTapped() {
        exerciseSetTableView.isEditing.toggle()
    }

    @objc func longPressed(sender: UILongPressGestureRecognizer) {
        exerciseSetTableView.isEditing.toggle()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "workoutDetailCell") as! workoutDetailCell
        cell.item = exerciseSetList[indexPath.row]
        
        if !isFromHome || isFromCalenderScreen{
            cell.editButton.isHidden = false
        }
        cell.onEdit = {[weak self] in
            self?.backgroundView.isHidden = false
            let vc =  STORYBOARD.myWorkout.instantiateViewController(withIdentifier: "AddExerciseSetScreen") as! AddExerciseSetScreen
            vc.modalPresentationStyle = .overFullScreen
            vc.isEdit = true
            vc.workoutId = self?.workoutId ?? 0
            vc.delegate = self
            vc.workoutExerciseId = self?.exerciseSetList[indexPath.row].workout_exercise_id ?? 0
            vc.editExericeSet = self?.exerciseSetList[indexPath.row]
            vc.dismissScreen = { [weak self] in
                self?.workoutExerciseSetListAPI(Id: self?.workoutId ?? 0, Page: 1)
            }
            vc.dismissRemoveScreen = { [weak self] in
                self?.backgroundView.isHidden = false
                let vc =  STORYBOARD.myWorkout.instantiateViewController(withIdentifier: "WorkoutDeleteConformationScreen") as! WorkoutDeleteConformationScreen
                vc.modalPresentationStyle = .overFullScreen
                vc.isWorkout = false
                vc.isExerciseSet = true
                vc.delegate = self
                vc.WorkoutId = self?.exerciseSetList[indexPath.row].workout_exercise_id ?? 0
                vc.removeWorkout =  { [weak self] in
                    self?.workoutExerciseSetListAPI(Id: self?.workoutId ?? 0, Page: 1)
                }
                self?.present(vc, animated: true, completion: nil)
            }
            self?.present(vc, animated: true, completion: nil)
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.backgroundView.isHidden = false
        let vc =  STORYBOARD.myWorkout.instantiateViewController(withIdentifier: "ExerciseDetailShowScreen") as! ExerciseDetailShowScreen
        vc.modalPresentationStyle = .overFullScreen
        vc.delegate = self
        vc.editExerciseSet = exerciseSetList[indexPath.row]

        self.present(vc, animated: true, completion: nil)

    }
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let _ : WorkoutExerciseMultipleSetListResponse = exerciseSetList[sourceIndexPath.row]
        exerciseSetList.swapAt(sourceIndexPath.row, destinationIndexPath.row);
        
//        MyWorkoutService.shared.updateOrder(parameters: <#T##[String : Any]#>, success: <#T##(Int, Response?) -> ()#>, failure: <#T##(String) -> ()#>)
    }

    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            let alertController = UIAlertController(title: "Are you sure?", message: "You want to delete this exercise?", preferredStyle: .alert)
            
            // Create the actions
            let okAction = UIAlertAction(title: "Yes, delete it!", style: UIAlertAction.Style.default) { [self]
                UIAlertAction in
                let selectWorkout = exerciseSetList[indexPath.row]
                exerciseSetList.remove(at: indexPath.row)
                workoutExerciseSetDeleteAPI(workoutId: selectWorkout.workout_exercise_id ?? 0)
                //self.getNearByGym(category: "", searchText: "")
                NSLog("OK Pressed")
            }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default) { [self]
                UIAlertAction in
                //self.getNearByGym(category: "", searchText: "")
                NSLog("OK Pressed")
            }
            // Add the actions
            okAction.setValue(UIColor.red, forKey: "titleTextColor")
            cancelAction.setValue(UIColor.gray, forKey: "titleTextColor")
            alertController.addAction(cancelAction)
            alertController.addAction(okAction)
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
           
        }
    }
}

extension ExerciseDetailsScreen {
    //MARK:- workout Exercise Detsials API
    func workoutExerciseDetailsAPI(workoutId:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            MyWorkoutService.shared.workoutExerciseDetails(id: workoutId){ (statusCode, response) in
                Utility.hideIndicator()
                if let res = response.workoutDetailsResponse{
                    self.titleLabel.text = res.title
                    self.descriptionLabel.text = res.desc
                    self.exerciseImage = res.image ?? ""
                    Utility.setImage(self.exerciseImage, imageView: self.exerciseImageView)
                }
                
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    
    
    //MARK:- workout Exercise set Delete API
    func workoutExerciseSetDeleteAPI(workoutId : Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            MyWorkoutService.shared.workoutExerciseSetDelete(id: workoutId){ [self] (statusCode, response) in
                Utility.hideIndicator()
//                if response.message ?? "" == "Workout exercise remove successfully."{
//
//                    self.dismiss(animated: false, completion: { [weak self] in
//                        self?.removeWorkout?()
//                    })
//                }
//                else{
//                    Utility.showAlert(vc: self, message: response.message ?? "")
//                }
                if exerciseSetList.count == 0 {
                    //addWorkoutToDateButton.isHidden = true
                }
                self.exerciseSetTableView.reloadData()
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:- workout Detsials API
    func workoutDetailsAPI(workoutId : Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            MyWorkoutService.shared.workoutDetails(id: workoutId){ [self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response.workoutDetailsResponse{
                    self.titleLabel.text = res.title
                    self.descriptionLabel.text = res.desc
                    self.exerciseImage = res.image ?? ""
                    if exerciseImage != defaultWorkoutImage {
                        workoutImageView.alpha = 0.5
                        Utility.setImage(self.exerciseImage, imageView: self.exerciseImageView)
                    } else {
                        workoutImageView.alpha = 1
                    }
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:- workout Exercise Set List API
    func workoutExerciseSetListAPI(Id:Int,Page:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            MyWorkoutService.shared.workoutExerciseSetList(Id: Id, Page: Page){ [self] (statusCode, response) in
                Utility.hideIndicator()
                self.hasMorePage = true
                if let res = response.workoutMultipalExerciseResponse{
                    if page == 1{
                        self.exerciseSetList = res
                        if(self.exerciseSetList.isEmpty)
                        {
                        }else{
                                self.exerciseSetList.sorted{ $0.order ?? Int.max < $1.order ?? Int.max }
                        }
                        
                        self.exerciseSetList2 = self.exerciseSetList;
                        if  !isFromCalenderScreen{
                            addWorkoutToDateButton.isHidden = false
                        }
                        if exerciseSetList.count == 0 {
                            let noDataLabel = UILabel(frame: CGRect(x: 0, y: 50, width: exerciseSetTableView.bounds.size.width, height: 100))
                            noDataLabel.text = "There are no exercises."
                            noDataLabel.textColor = UIColor.darkGray
                            noDataLabel.textAlignment = .center
                            noDataLabel.numberOfLines = 0
                            exerciseSetTableView.backgroundView = noDataLabel
                        } else {
                            exerciseSetTableView.backgroundView = nil
                        }
                        self.exerciseSetTableView.reloadData()
//                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//                            self.exerciseSetTableView.stopSkeletonAnimation()
//                            self.view.hideSkeleton(reloadDataAfter: true, transition: .crossDissolve(0.25))
//                          
//                        }
                    } else {
                        self.appendDataCollectionView(data: res)
                    }
                    if let meta = response.metaResponse{
                        self.meta = meta
                    }
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                stronSelf.hasMorePage = true
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}

extension ExerciseDetailsScreen : hideBackgroundDelegate {
    func hideView() {
        backgroundView.isHidden = true
    }
}

protocol EditWorkOutDelegate {
    func editWorkOut()
}

protocol WorkOutExerciseDeleteDelegate {
    func WorkOutExerciseDelete()
}

extension ExerciseDetailsScreen : SkeletonTableViewDataSource {
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "workoutDetailCell"
    }
    
    func collectionSkeletonView(_ skeletonView: UITableView, numberOfRowsInSection section: Int) -> Int {
        2
    }
}
