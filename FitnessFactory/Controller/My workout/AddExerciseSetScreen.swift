//
//  AddExerciseSetScreen.swift
//  FitnessFactory
//
//  Created by iroid on 23/07/21.
//

import UIKit
import DropDown

class AddExerciseSetScreen: UIViewController, UIColorPickerViewControllerDelegate {
    
    var dismissScreen : (() -> Void)?
    var dismissRemoveScreen : (() -> Void)?
    @IBOutlet weak var BackgroundView : UIView!
    @IBOutlet weak var inView : UIView!
    @IBOutlet weak var inBgView : UIView!
    @IBOutlet weak var exerciseNameListTableView: UITableView!
    @IBOutlet weak var tableViewheightConstraint: NSLayoutConstraint!
    @IBOutlet weak var backGroundView: UIView!
    @IBOutlet weak var selectView: UIView!
    @IBOutlet weak var exerciseImageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var noButton: UIButton!
    @IBOutlet weak var weightTextField: UITextField!
    @IBOutlet weak var ExerciseNameTextField: UITextField!
    @IBOutlet weak var repsTextField: UITextField!
    @IBOutlet weak var weightButton: UIButton!
    @IBOutlet weak var mutipleSetTextField: UITextField!
    
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var SelectColor : UIButton!
    
    var mycolor : String  = ""
    var weightDropDown = DropDown()
    var weightArray: [String] = []
    var selectedWeight: String = "Lbs"
    var delegate : hideBackgroundDelegate?
    var searchText = ""
    var exerciseNameList = [SearchExerciseResponse]()
    var count  = 0;
    
    var secoundDropDown = DropDown()
    var secoundArray: [String] = []
    var selectedSecound: String = "Reps"
    
    var tap = UITapGestureRecognizer()
    
    var isEdit: Bool = false
    var noText: String = ""
    var weightText: String = ""
    var fileType : Bool = false
    var workOutImageData: Data?
    var imageString: String = ""
    
    var workoutExerciseId: Int = 0
    var workoutId:Int = 0
    var editExericeSet  : WorkoutExerciseMultipleSetListResponse?
    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        inView.layer.cornerRadius = 10;
//        inView.layer.masksToBounds = true;
//       // inView.heightAnchor = 82
//        inBgView.layer.cornerRadius = 10;
//        inBgView.layer.masksToBounds = true;
//        initalizedDetails()
//       // NavView.backgroundColor = UIColor(hex: appThemeColor)
//        backGroundView.backgroundColor = UIColor(hex: appThemeColor)
//        headerView.backgroundColor = UIColor(hex: appThemeColor)
//    }
    
    override func viewDidLoad() {
                super.viewDidLoad()
        
                inView.layer.cornerRadius = 10;
                inView.layer.masksToBounds = true;
               // inView.heightAnchor = 82
                inBgView.layer.cornerRadius = 10;
                inBgView.layer.masksToBounds = true;
                initalizedDetails()
        }
        
        //MARK:- initalizedDetails Details
        func initalizedDetails(){
            let nib = UINib(nibName: "genderOptionCell", bundle: nil)
            exerciseNameListTableView.register(nib, forCellReuseIdentifier: "genderOptionCell")
            exerciseNameListTableView.layer.cornerRadius = 9
            exerciseNameListTableView.tableFooterView = UIView()
            
            weightArray = ["Lbs","Kgs"]
            currentWeightDropDownFilterButton()
            
            secoundArray = ["Reps","Sec","Min"]
            currentSecoundDropDownFilterButton()
            
            backGroundView.roundCorners(corners: [.layerMinXMinYCorner, .layerMaxXMinYCorner], radius: 52)
            if #available(iOS 13.0, *) {
                backGroundView.layer.cornerCurve = .continuous
            } else {
                // Fallback on earlier versions
            }
            
            
            self.backGroundView.layer.cornerRadius = 52
                    self.backGroundView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
            self.backGroundView.layer.masksToBounds = false
                    
            
           // tap = UITapGestureRecognizer(target: self, action: #selector(dismissView))
           // self.view.addGestureRecognizer(tap)
            print(editExericeSet?.toJSON())
            if isEdit{
                titleLabel.text = "Edit Exercise"
                removeButton.isHidden = false
                ExerciseNameTextField.text = editExericeSet?.title
                repsTextField.text = editExericeSet?.workoutExerciseData?.number
                noButton.setTitle(editExericeSet?.workoutExerciseData?.number_type, for: .normal)
                weightTextField.text = editExericeSet?.workoutExerciseData?.total_weight
                weightButton.setTitle(editExericeSet?.workoutExerciseData?.total_weight_type, for: .normal)
                mutipleSetTextField.text = String(editExericeSet?.setCount ?? 0)
                descriptionTextView.text = editExericeSet?.desc
                workoutExerciseId = editExericeSet?.workout_exercise_id ?? 0
                if let image = editExericeSet?.image {
                    Utility.setImage(image, imageView: exerciseImageView)
                    selectView.isHidden = true
                }else{
                    selectView.isHidden = false
                }
                
            }
            else
            {
                titleLabel.text = "Add Exercise"
                removeButton.isHidden = true
            }
            //self.searchWorkOutExercise(searchText: "Barbell")
        }

    @objc func dismissView() {
        delegate?.hideView()
        backGroundView.removeGestureRecognizer(tap)
        self.dismiss(animated: false, completion: nil)
    }
    
    func currentWeightDropDownFilterButton() {
        weightDropDown.backgroundColor = UIColor(white: 1, alpha: 1)
        weightDropDown.selectionBackgroundColor = UIColor(white: 1, alpha: 1)
        weightDropDown.shadowColor = UIColor(white: 0.6, alpha: 1)
        weightDropDown.shadowOpacity = 0
        weightDropDown.shadowRadius = 0
        weightDropDown.cornerRadius = 9
        weightDropDown.separatorColor = UIColor.primaryColour
        weightDropDown.animationduration = 0
        weightDropDown.textColor =  #colorLiteral(red: 0.02745098039, green: 0.02745098039, blue: 0.02352941176, alpha: 0.4017110475)
        weightDropDown.selectedTextColor =  #colorLiteral(red: 0.4244436026, green: 0.4218436182, blue: 0.5152064562, alpha: 1)
        weightDropDown.anchorView = weightButton
        weightDropDown.bottomOffset = CGPoint(x: 0, y: 50)
        weightDropDown.direction = .bottom
        weightDropDown.cellNib = UINib(nibName: "DropDownCell", bundle: Bundle(for: DropDownCell.self))
        weightDropDown.dataSource = weightArray
        weightDropDown.dismissMode = .onTap
        weightDropDown.selectRow(-1)
        weightDropDown.selectionAction = { [weak self] (index, item) in
            if index == 0{
                //                self?.dropUpStackView.isHidden = true
                //                self?.dropUpStackViewConstant.constant = 0
                //                self?.dropOffCustomTime = false
            }else{
                //                self?.dropUpStackView.isHidden = false
                //                self?.dropUpStackViewConstant.constant = 144
                //                self?.dropOffCustomTime = true
            }
            self?.weightButton.setTitle(item, for: .normal)
            self?.selectedWeight = item
            //            self?.updateButton.isUserInteractionEnabled = true
            //            self?.updateButton.alpha = 1
        }
    }
    
    func currentSecoundDropDownFilterButton() {
        secoundDropDown.backgroundColor = UIColor(white: 1, alpha: 1)
        secoundDropDown.selectionBackgroundColor = UIColor(white: 1, alpha: 1)
        secoundDropDown.shadowColor = UIColor(white: 0.6, alpha: 1)
        secoundDropDown.shadowOpacity = 0
        secoundDropDown.shadowRadius = 0
        secoundDropDown.cornerRadius = 9
        secoundDropDown.separatorColor = UIColor.primaryColour
        secoundDropDown.animationduration = 0
        secoundDropDown.textColor =  #colorLiteral(red: 0.02745098039, green: 0.02745098039, blue: 0.02352941176, alpha: 0.4017110475)
        secoundDropDown.selectedTextColor =  #colorLiteral(red: 0.4244436026, green: 0.4218436182, blue: 0.5152064562, alpha: 1)
        secoundDropDown.anchorView = noButton
        secoundDropDown.bottomOffset = CGPoint(x: 0, y: 50)
        secoundDropDown.direction = .bottom
        secoundDropDown.cellNib = UINib(nibName: "DropDownCell", bundle: Bundle(for: DropDownCell.self))
        secoundDropDown.dataSource = secoundArray
        secoundDropDown.dismissMode = .onTap
        secoundDropDown.selectRow(-1)
        secoundDropDown.selectionAction = { [weak self] (index, item) in
            if index == 0{
                //                self?.dropUpStackView.isHidden = true
                //                self?.dropUpStackViewConstant.constant = 0
                //                self?.dropOffCustomTime = false
            }else{
                //                self?.dropUpStackView.isHidden = false
                //                self?.dropUpStackViewConstant.constant = 144
                //                self?.dropOffCustomTime = true
            }
            self?.noButton.setTitle(item, for: .normal)
            self?.selectedSecound = item
            //            self?.updateButton.isUserInteractionEnabled = true
            //            self?.updateButton.alpha = 1
        }
    }
    
    func goFurther(){
        backGroundView.removeGestureRecognizer(tap)
        self.dismiss(animated: false, completion: { [weak self] in
            self?.dismissScreen?()
        })
    }
    
    func openCamera(){
        let imagePicker = UIImagePickerController()
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true//false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func openGallery(){
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker,animated: true,completion: nil)
    }
    
    func openGif(){
        let vc =  STORYBOARD.myWorkout.instantiateViewController(withIdentifier: "ListOfGifsScreen") as! ListOfGifsScreen
        vc.modalPresentationStyle = .overFullScreen
        vc.isFromWorkout = true
        vc.delegate = self
        self.present(vc, animated: false, completion: nil)
    }
    
    func openFitnessLibary(){
        let vc =  STORYBOARD.myWorkout.instantiateViewController(withIdentifier: "ListOfImageScreen") as! ListOfImageScreen
        vc.modalPresentationStyle = .overFullScreen
        vc.isFromWorkout = true
        vc.delegate = self
        self.present(vc, animated: false, completion: nil)
    }
    
    @IBAction func selectColor( _ sender:UIButton){
        if #available(iOS 14.0, *) {
            let colorPickerVC = UIColorPickerViewController()
            colorPickerVC.delegate = self
            present(colorPickerVC, animated: true)
        } else {
            SelectColor.isHidden = true;
        }
    }
    
    @available(iOS 14.0, *)
    func colorPickerViewControllerDidFinish(_ viewController: UIColorPickerViewController) {
        inBgView.backgroundColor = viewController.selectedColor
        mycolor = viewController.selectedColor.htmlRGBColor
    }
    @available(iOS 14.0, *)
    func colorPickerViewControllerDidSelectColor(_ viewController: UIColorPickerViewController) {
        inBgView.backgroundColor = viewController.selectedColor
    }
    @IBAction func onAddPhoto(_ sender: UIButton) {
        self.view.endEditing(true)
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        // Create the actions
        let takePhoto = UIAlertAction(title: "Take photo", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openCamera()
        }
        let galleryPhoto = UIAlertAction(title: "Photo Library", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openGallery()
        }
        let fitnessPhoto = UIAlertAction(title: "Fitness Factory Photo Library", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openFitnessLibary()
        }
        let gif = UIAlertAction(title: "Fitness Factory Photo Library", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openGif()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        // Add the actions
        alertController.addAction(takePhoto)
        alertController.addAction(galleryPhoto)
        alertController.addAction(cancelAction)
       // alertController.addAction(fitnessPhoto)
        alertController.addAction(gif)
        //   Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    //MARK: - button clicked event
    @IBAction func onNo(_ sender: UIButton) {
        self.view.endEditing(true)
        secoundDropDown.show()
    }
    
    @IBAction func onWeight(_ sender: UIButton) {
        self.view.endEditing(true)
        weightDropDown.show()
    }
    
    @IBAction func onSave(_ sender: UIButton) {
        self.view.endEditing(true)
        if let error = self.checkValidation(){
            Utility.showAlert(vc: self, message: error)
        } else {
            if isEdit{
                editExerciseSet()
            }
            else{
                addExerciseSet()
            }
        }
    }
    
    @IBAction func onCancel(_ sender: Any) {
        delegate?.hideView()
        backGroundView.removeGestureRecognizer(tap)
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func onRemove(_ sender: UIButton) {
        self.view.endEditing(true)
        
        self.dismiss(animated: true, completion: { [weak self] in
            self?.dismissRemoveScreen?()
        })
    }
    
    //MARK:- Check Validation
    func checkValidation() -> String?{
        
        if self.ExerciseNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            return "Please enter exercise name"
        }
        else if self.repsTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter exercise set"
        }
        else if self.weightTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter weight"
        }
        //        else if self.mutipleSetTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
        //            return "Please enter no of set"
        //        }
        return nil
    }
}
extension AddExerciseSetScreen :UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else {
            return
        }
        
        exerciseImageView.image = pickedImage
        fileType = false
        //        profileImageData = pickedImage.pngData()
        workOutImageData = pickedImage.jpegData(compressionQuality:0.6)!
        selectView.isHidden = true
        
        self.dismiss(animated: false, completion: { () -> Void in
        })
    }
}

extension AddExerciseSetScreen : SelectImage {
    func selectImageOfWorkout(selectImg: String) {
        selectView.isHidden = true
        fileType = true
        imageString = selectImg
        Utility.setImage(selectImg, imageView: exerciseImageView)
        //fileType = URL(string: selectImg)!.pathExtension
    }
}

extension AddExerciseSetScreen{
    func addExerciseSet(){
        self.view.endEditing(true)
        
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            if(mycolor == ""){
                
            }else{
                mycolor.remove(at: mycolor.startIndex)}
            let data = AddWorkoutExerciseSetRequest(workout_id: "\(workoutExerciseId)", name: ExerciseNameTextField.text, number: repsTextField.text, number_type: selectedSecound, total_weight: weightTextField.text, total_weight_type: selectedWeight, no_sets: mutipleSetTextField.text ?? "0", desc: descriptionTextView.text, color: mycolor, order: String(count + 1))
            var imageData:Data?
            if fileType {
                if(exerciseImageView.image != nil){
                    imageData = try! Data(contentsOf:URL(string: imageString)! )
                }
                MyWorkoutService.shared.addGifForExercise(imageData: imageData, parameters: data.toJSON() ){ [weak self] (statusCode, response) in
                    print("1")
                    Utility.hideIndicator()
                    if let _ = response{
                        self?.delegate?.hideView()
                        self?.goFurther()
                    }
                } failure: { [weak self] (error) in
                    guard let stronSelf = self else { return }
                    Utility.hideIndicator()
                    Utility.showAlert(vc: stronSelf, message: error)
                }
            } else {
                if(exerciseImageView.image != nil){
                    imageData = exerciseImageView.image!.jpegData(compressionQuality:0.4)!
                }
                MyWorkoutService.shared.addWorkoutExerciseSet(imageData: imageData,parameters: data.toJSON()) { [weak self] (statusCode, response) in
                    Utility.hideIndicator()
                    if let _ = response{
                        self?.delegate?.hideView()
                        self?.goFurther()
                    }
                } failure: { [weak self] (error) in
                    guard let stronSelf = self else { return }
                    Utility.hideIndicator()
                    Utility.showAlert(vc: stronSelf, message: error)
                }
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func editExerciseSet(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            if(mycolor == ""){
                
            }else{
                mycolor.remove(at: mycolor.startIndex)}
            let data = EditWorkoutExerciseSetRequest(workout_exercise_id: "\(workoutExerciseId)", name: ExerciseNameTextField.text, number: repsTextField.text, number_type: selectedSecound, total_weight: weightTextField.text, total_weight_type: selectedWeight, no_sets: mutipleSetTextField.text ?? "0", desc: descriptionTextView.text,color: mycolor)
           
            var imageData:Data?
            if fileType {
                if(exerciseImageView.image != nil){
                    imageData = try! Data(contentsOf:URL(string: imageString)! )
                }
                MyWorkoutService.shared.editGifForExercise(imageData: imageData, parameters: data.toJSON()){ [weak self] (statusCode, response) in
                    Utility.hideIndicator();
                    if let _ = response{
                        self?.delegate?.hideView()
                        self?.goFurther()
                    }
                } failure: { [weak self] (error) in
                    guard let stronSelf = self else { return }
                    Utility.hideIndicator()
                    Utility.showAlert(vc: stronSelf, message: error)
                }
            } else {
                if(exerciseImageView.image != nil){
                    imageData = exerciseImageView.image!.jpegData(compressionQuality:0.4)!
                }
                MyWorkoutService.shared.editWorkoutExerciseSet(imageData: workOutImageData, parameters: data.toJSON()){ [weak self] (statusCode, response) in
                    Utility.hideIndicator();
                    if let _ = response{
                        self?.delegate?.hideView()
                        self?.goFurther()
                    }
                } failure: { [weak self] (error) in
                    guard let stronSelf = self else { return }
                    Utility.hideIndicator()
                    Utility.showAlert(vc: stronSelf, message: error)
                }
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func searchWorkOutExercise(searchText:String){
//            self.view.endEditing(true)
            if Utility.isInternetAvailable(){
                // selectDateList.map(String.init).joined(separator: ",")
                let data = SearchWorkoutRequest(id:workoutId, search: searchText)
                MyWorkoutService.shared.searchWorkOutExercise(parameters: data.toJSON()) { [weak self] (statusCode, response) in
                    Utility.hideIndicator()
                    if let res = response?.searchExerciseResponse {
                        self?.exerciseNameList = res
                        if self?.exerciseNameList.count ?? 0 > 0 {
                            self?.exerciseNameListTableView.isHidden  = false
                        } else {
                            self?.exerciseNameListTableView.isHidden  = true
                        }
                        self?.exerciseNameListTableView.reloadData()
                    }
                } failure: { [weak self] (error) in
                    guard let stronSelf = self else { return }
                    Utility.hideIndicator()
                    Utility.showAlert(vc: stronSelf, message: error)
                }
            } else {
                Utility.hideIndicator()
                Utility.showNoInternetConnectionAlertDialog(vc: self)
            }
        }
}

extension AddExerciseSetScreen : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return exerciseNameList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "genderOptionCell") as! genderOptionCell
        cell.item = exerciseNameList[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        ExerciseNameTextField.text = exerciseNameList[indexPath.row].title
        exerciseNameListTableView.isHidden = true
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            if self.exerciseNameList.count >= 3 {
                self.tableViewheightConstraint.constant = 112
            } else if self.exerciseNameList.count == 0 {
                self.tableViewheightConstraint.constant = 0
            } else {
                self.tableViewheightConstraint.constant = tableView.contentSize.height
            }
        }
    }
}

extension AddExerciseSetScreen:UITextFieldDelegate{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
//        if textField == ExerciseNameTextField{
//            let updatedString = ExerciseNameTextField.text
//            searchText = updatedString ?? ""
//            DispatchQueue.main.asyncAfter(deadline: .now() ) {
//                self.searchUserList()
//            }
//        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == ExerciseNameTextField{
            if let text = textField.text as NSString? {
                let txtAfterUpdate = text.replacingCharacters(in: range, with: string)
                searchText = txtAfterUpdate
                NSObject.cancelPreviousPerformRequests(withTarget: self,selector: #selector(searchUserList),object: textField)
                self.perform(#selector(searchUserList),with: textField,afterDelay: 0)
            }
        }
        return true
    }

    @objc func searchUserList(){
        if searchText.count > 0 {
            self.searchWorkOutExercise(searchText: searchText )
            //self.getMealItemList(page: 1, searchText: newString)
        } else {
            exerciseNameListTableView.isHidden = true
        }
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        //addMealView.isHidden = true
        exerciseNameListTableView.isHidden = true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
    }
}
extension UIColor {
    var rgbComponents:(red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        if getRed(&r, green: &g, blue: &b, alpha: &a) {
            return (r,g,b,a)
        }
        return (0,0,0,0)
    }
    // hue, saturation, brightness and alpha components from UIColor**
    var hsbComponents:(hue: CGFloat, saturation: CGFloat, brightness: CGFloat, alpha: CGFloat) {
        var hue:CGFloat = 0
        var saturation:CGFloat = 0
        var brightness:CGFloat = 0
        var alpha:CGFloat = 0
        if getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: &alpha){
            return (hue,saturation,brightness,alpha)
        }
        return (0,0,0,0)
    }
    var htmlRGBColor:String {
        return String(format: "#%02x%02x%02x", Int(rgbComponents.red * 255), Int(rgbComponents.green * 255),Int(rgbComponents.blue * 255))
    }
    var htmlRGBaColor:String {
        return String(format: "#%02x%02x%02x%02x", Int(rgbComponents.red * 255), Int(rgbComponents.green * 255),Int(rgbComponents.blue * 255),Int(rgbComponents.alpha * 255) )
    }
}

