//
//  AddWorkoutScreen.swift
//  FitnessFactory
//
//  Created by iroid on 20/07/21.
//

import UIKit

class AddWorkoutScreen: UIViewController {
    @IBOutlet weak var backGroundView: UIView!
    @IBOutlet weak var addWorkoutView: UIView!
    @IBOutlet weak var selectImageView: UIView!
    @IBOutlet weak var descriptionView: dateSportView!
    @IBOutlet weak var workoutTitle: UILabel!
    @IBOutlet weak var titleWorkoutTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var workoutImageView: UIImageView!
    @IBOutlet weak var selectView: UIView!
    @IBOutlet weak var removeButton: UIButton!

    var tap = UITapGestureRecognizer()
    
    var profileImageData: Data?
    
    var isEdit:Bool = false
    var AddWorkout: Bool = false
    var id: Int = 0
    var titleString: String = ""
    var descriptionString: String = ""
    var imageString: String = ""
    var fileType : Bool = false
    var delegate : hideBackgroundDelegate?
    var dismissScreen : (() -> Void)?
    var dismissRemoveScreen : (() -> Void)?
    var createWorkout : ((_ createWorkout:WorkoutResponse) -> Void)?
    var mycolor : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initalizedDetails()
    }
    
    //MARK:- initalizedDetails Details
    func initalizedDetails(){
        
        addWorkoutView.roundCorners(corners: [.layerMinXMinYCorner, .layerMaxXMinYCorner], radius: 52)
        if #available(iOS 13.0, *) {
            addWorkoutView.layer.cornerCurve = .continuous
        } else {
            // Fallback on earlier versions
        }
        
        tap = UITapGestureRecognizer(target: self, action: #selector(dismissView))
        backGroundView.addGestureRecognizer(tap)
        
        if isEdit{
            if AddWorkout{
                workoutTitle.text = "Edit Workout"
                removeButton.isHidden = false
                titleWorkoutTextField.text = titleString
                descriptionTextView.text = descriptionString
                if imageString != defaultWorkoutImage {
                    selectView.isHidden = true
                    Utility.setImage(imageString, imageView: workoutImageView)
                }
                selectImageView.isHidden = false
                descriptionView.isHidden = false
            }
            else{
                workoutTitle.text = "Edit Exercise"
                selectView.isHidden = true
                removeButton.isHidden = false
                titleWorkoutTextField.text = titleString
                descriptionTextView.text = descriptionString
                Utility.setImage(imageString, imageView: workoutImageView)
            }
        }
        else{
            if AddWorkout{
                workoutTitle.text = "Add Workout"
                titleWorkoutTextField.placeholder = "Title of Workout"
                selectView.isHidden = false
                removeButton.isHidden = true
                
            }
            else{
                workoutTitle.text = "Add Exercise"
                titleWorkoutTextField.placeholder = "Title of Exercise"
                removeButton.isHidden = true
            }
        }
    }
    
    
    @objc func dismissView() {
        delegate?.hideView()
        backGroundView.removeGestureRecognizer(tap)
        self.dismiss(animated: false, completion: nil)
    }
    func openCamera(){
        let imagePicker = UIImagePickerController()
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            print("Button capture")
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true//false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func openGallery(){
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker,animated: true,completion: nil)
    }
    
    func openGif(){
        let vc =  STORYBOARD.myWorkout.instantiateViewController(withIdentifier: "ListOfGifsScreen") as! ListOfGifsScreen
        vc.modalPresentationStyle = .overFullScreen
        vc.isFromWorkout = true
        vc.delegate = self
        self.present(vc, animated: false, completion: nil)
    }
    
    func openFitnessLibary(){
        let vc =  STORYBOARD.myWorkout.instantiateViewController(withIdentifier: "ListOfImageScreen") as! ListOfImageScreen
        vc.modalPresentationStyle = .overFullScreen
        vc.isFromWorkout = true
        vc.delegate = self
        self.present(vc, animated: false, completion: nil)
    }
    
    
    @IBAction func onAddPhoto(_ sender: UIButton) {
        self.view.endEditing(true)
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        // Create the actions
        let takePhoto = UIAlertAction(title: "Take photo", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openCamera()
        }
        let galleryPhoto = UIAlertAction(title: "Photo Library", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openGallery()
        }
        let fitnessPhoto = UIAlertAction(title: "Fitness Factory Photo Library", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openFitnessLibary()
        }
        let gif = UIAlertAction(title: "Fitness Factory Photo Library", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openGif()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        // Add the actions
        alertController.addAction(takePhoto)
        alertController.addAction(galleryPhoto)
        alertController.addAction(cancelAction)
       // alertController.addAction(fitnessPhoto)
        alertController.addAction(gif)
        //   Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func onSave(_ sender: UIButton) {
        self.view.endEditing(true)
        if let error = self.checkValidation(){
            Utility.showAlert(vc: self, message: error)
        } else {
            delegate?.hideView()
            if isEdit{
                if AddWorkout{
                    editWorkOutPost()
                }
                else{
                    editWorkOutExercise()
                }
            }
            else{
                if AddWorkout{
                    addWorkOutPost()
                }
                else{
                    addWorkoutExercise()
                }
            }
        }
    }
    
    @IBAction func onRemove(_ sender: UIButton) {
        self.view.endEditing(true)
        
        self.dismiss(animated: false, completion: { [weak self] in
            self?.dismissRemoveScreen?()
        })
    }
    //MARK:- Check Validation
    func checkValidation() -> String?{
        if self.titleWorkoutTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            if !AddWorkout{
                return "Please enter title of Exercise"
            }
            else{
                return "Please enter title of Workout"
            }
        }
//        if  workoutImageView.image == nil && isEdit == false {
//            return "Please select image"
//        }
        //        if self.descriptionTextView.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
        //            if !AddWorkout{
        //                return "Please enter description of Exercise"
        //            }
        //            else{
        //                return "Please enter description of Workout"
        //            }
        //        }
        return nil
    }
    
    func goFurther(){
        self.dismiss(animated: true, completion: { [weak self] in
            self?.dismissScreen?()
        })
    }
    
}



extension AddWorkoutScreen :UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else {
            return
        }
        workoutImageView.image = pickedImage
        fileType = false
        //        profileImageData = pickedImage.pngData()
        profileImageData = pickedImage.jpegData(compressionQuality:0.6)!
        selectView.isHidden = true
        imageString = pickedImage.toJpegString(compressionQuality: 0.6)!
        
        self.dismiss(animated: true, completion: { () -> Void in
        })
    }
    
//    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
//        let isGif = imageString.contains(".gif") ? true : false
//        fileType = isGif
//        self.dismiss(animated: true, completion: { () -> Void in
//        })
//    }
}

extension AddWorkoutScreen{
    //    func addWorkOutPost(){
    //            self.view.endEditing(true)
    //            if Utility.isInternetAvailable(){
    //                Utility.showIndecator()
    //                let data = AddWorkoutRequest(title: self.titleWorkoutTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), desc: self.descriptionTextView.text?.trimmingCharacters(in: .whitespacesAndNewlines))
    //
    //                    var imageData:Data?
    //                    if(workoutImageView.image != nil){
    //                        imageData = workoutImageView.image!.jpegData(compressionQuality:0.4)!
    //                    }
    //                    MyWorkoutService.shared.addWorkout(imageData: imageData,parameters: data.toJSON()) { [weak self] (statusCode, response) in
    //                        Utility.hideIndicator()
    //                        if let res = response{
    //                            print(res.toJSON())
    //                            self?.backGroundView.removeGestureRecognizer(self!.tap)
    //                            self?.goFurther()
    //                        }
    //                    } failure: { [weak self] (error) in
    //                        guard let stronSelf = self else { return }
    //                        Utility.hideIndicator()
    //                        Utility.showAlert(vc: stronSelf, message: error)
    //                    }
    //            }else{
    //                Utility.hideIndicator()
    //                Utility.showNoInternetConnectionAlertDialog(vc: self)
    //            }
    //        }
    
    func addWorkOutPost(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            let data = AddWorkoutRequest(title: self.titleWorkoutTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines))
            MyWorkoutService.shared.addWorkoutService(parameters: data.toJSON()) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response?.addWorkoutResponse{
                    print(res.toJSON())
                    self?.backGroundView.removeGestureRecognizer(self!.tap)
                    self?.dismiss(animated: true, completion: { [weak self] in
                        self?.createWorkout!(res)
                    })
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    
    func editWorkOutPost(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            let data = EditWorkoutRequest(id: "\(id)",title: self.titleWorkoutTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), desc: self.descriptionTextView.text?.trimmingCharacters(in: .whitespacesAndNewlines))
//            var imageData:Data?
//            if(workoutImageView.image != nil){
//                imageData = workoutImageView.image!.jpegData(compressionQuality:0.4)!
//            }
//            MyWorkoutService.shared.editWorkout(imageData: imageData,parameters: data.toJSON()) { [weak self] (statusCode, response) in
//                Utility.hideIndicator()
//                if let res = response{
//                    print(res.toJSON())
//                    self?.goFurther()
//                }
//            } failure: { [weak self] (error) in
//                guard let stronSelf = self else { return }
//                Utility.hideIndicator()
//                Utility.showAlert(vc: stronSelf, message: error)
//            }
            
            var imageData:Data?
            if imageString.contains(".gif") {
                
//            }
            //if fileType{
                if(workoutImageView.image != nil){
                    imageData = try! Data(contentsOf:URL(string: imageString)! )
                }
                MyWorkoutService.shared.editWorkout(imageData: imageData,parameters: data.toJSON()) { [weak self] (statusCode, response) in
                    Utility.hideIndicator()
                    if let res = response{
                        print(res.toJSON())
                        self?.goFurther()
                    }
                } failure: { [weak self] (error) in
                    guard let stronSelf = self else { return }
                    Utility.hideIndicator()
                    Utility.showAlert(vc: stronSelf, message: error)
                }
            } else {
                if(workoutImageView.image != nil){
                    imageData = workoutImageView.image!.jpegData(compressionQuality:0.4)!
                }
                MyWorkoutService.shared.editWorkout(imageData: imageData,parameters: data.toJSON()) { [weak self] (statusCode, response) in
                    Utility.hideIndicator()
                    if let res = response{
                        print(res.toJSON())
                        self?.goFurther()
                    }
                } failure: { [weak self] (error) in
                    guard let stronSelf = self else { return }
                    Utility.hideIndicator()
                    Utility.showAlert(vc: stronSelf, message: error)
                }
            }
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:- Create new workout exercise
    func addWorkoutExercise(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            let data = AddWorkoutExerciseRequest(workout_id: "\(id)", title: self.titleWorkoutTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), desc: self.descriptionTextView.text?.trimmingCharacters(in: .whitespacesAndNewlines),
                                                color: mycolor)
            var imageData:Data?
            if fileType {
                if(workoutImageView.image != nil){
                    imageData = try! Data(contentsOf:URL(string: imageString)! )
                }
                MyWorkoutService.shared.addGifForWorkout(imageData: imageData, parameters: data.toJSON() ){ [weak self] (statusCode, response) in
                    Utility.hideIndicator()
                    if let res = response{
                        print(res.toJSON())
                        self?.backGroundView.removeGestureRecognizer(self!.tap)
                        self?.goFurther()
                    }
                } failure: { [weak self] (error) in
                    guard let stronSelf = self else { return }
                    Utility.hideIndicator()
                    Utility.showAlert(vc: stronSelf, message: error)
                }
            } else {
                if(workoutImageView.image != nil){
                    imageData = workoutImageView.image!.jpegData(compressionQuality:0.4)!
                }
                MyWorkoutService.shared.addWorkoutExercise(imageData: imageData,parameters: data.toJSON()) { [weak self] (statusCode, response) in
                    Utility.hideIndicator()
                    
                    if let res = response {
                        print(res.toJSON())
                        self?.goFurther()
                    }
                } failure: { [weak self] (error) in
                    guard let stronSelf = self else { return }
                    Utility.hideIndicator()
                    Utility.showAlert(vc: stronSelf, message: error)
                }
            }
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func editWorkOutExercise(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            let data = EditWorkoutRequest(id: "\(id)",title: self.titleWorkoutTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), desc: self.descriptionTextView.text?.trimmingCharacters(in: .whitespacesAndNewlines))
            var imageData:Data?
            if fileType {
                if(workoutImageView.image != nil){
                    imageData = try! Data(contentsOf:URL(string: imageString)! )
                }
                MyWorkoutService.shared.editGifForWorkout(imageData: imageData, parameters: data.toJSON() ){ [weak self] (statusCode, response) in
                    Utility.hideIndicator()
                    if let res = response{
                        print(res.toJSON())
                        self?.goFurther()
                    }
                } failure: { [weak self] (error) in
                    guard let stronSelf = self else { return }
                    Utility.hideIndicator()
                    Utility.showAlert(vc: stronSelf, message: error)
                }
            } else {
                if(workoutImageView.image != nil){
                    imageData = workoutImageView.image!.jpegData(compressionQuality:0.4)!
                }
                MyWorkoutService.shared.editWorkoutExercise(imageData: imageData,parameters: data.toJSON()) { [weak self] (statusCode, response) in
                    Utility.hideIndicator()
                    if let res = response{
                        print(res.toJSON())
                        self?.goFurther()
                    }
                } failure: { [weak self] (error) in
                    guard let stronSelf = self else { return }
                    Utility.hideIndicator()
                    Utility.showAlert(vc: stronSelf, message: error)
                }
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}

extension AddWorkoutScreen : SelectImage {
    func selectImageOfWorkout(selectImg: String) {
        selectView.isHidden = true
        fileType = true
        imageString = selectImg
        Utility.setImage(selectImg, imageView: workoutImageView)
        //fileType = URL(string: selectImg)!.pathExtension
    }
}

