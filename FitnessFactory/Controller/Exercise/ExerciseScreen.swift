//
//  ExerciseScreen.swift
//  fitness_factory
//
//  Created by iroid on 23/06/21.
//

import UIKit

class ExerciseScreen: UIViewController {

    @IBOutlet weak var exerciseTableView: UITableView!
    var exerciseNameList = [ExerciseList]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initalizedDetails()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
    }
    
    //MARK:- initalizedDetails Details
    func initalizedDetails(){
        exerciseNameList = [
            ExerciseList(title: "My Workouts", img: "my_workout"),
            ExerciseList(title: "Pick a Workout", img: "pick_workout"),
            ExerciseList(title: "Yoga and Pilates", img: "yoga_pilates"),
            ExerciseList(title: "Stretches", img: "streches")
        ]
        let nib = UINib(nibName: "ExerciseCell", bundle: nil)
        exerciseTableView.register(nib, forCellReuseIdentifier: "ExerciseCell")
        exerciseTableView.tableFooterView = UIView()
        
    }

    @IBAction func onProfile(_ sender: UIButton) {
        tabBarController?.tabBar.isHidden = true
        let storyBoard: UIStoryboard = UIStoryboard(name: "Profile", bundle: nil)
         let vc = storyBoard.instantiateViewController(withIdentifier: "ProfileScreen") as! ProfileScreen

         let navigationController = self.navigationController


         vc.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Close", style: .plain, target: vc, action: #selector(vc.closeView))
         vc.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: vc, action: nil)
        
         let transition = CATransition()
         transition.duration = 0.2
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.moveIn
        transition.subtype = CATransitionSubtype.fromLeft
         navigationController?.view.layer.add(transition, forKey: nil)
         navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func onHome(_ sender: UIButton) {
        //tabBarController?.tabBar.isHidden = false
        tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: true)
    }
    
}

extension ExerciseScreen : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return exerciseNameList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExerciseCell") as! ExerciseCell
        cell.item = exerciseNameList[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       //tabBarController?.tabBar.isHidden = true
        
        if indexPath.row == 0{
            let controller = STORYBOARD.myWorkout.instantiateViewController(withIdentifier:"MyWorkoutScreen") as! MyWorkoutScreen
            controller.pickWorkoutId = indexPath.row
//            controller.titleName = exerciseNameList[indexPath.row].title ?? ""
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else{
            let controller = STORYBOARD.exercise.instantiateViewController(withIdentifier:"PickOutWorkoutScreen") as! PickOutWorkoutScreen
            controller.pickWorkoutId = indexPath.row
            controller.titleName = exerciseNameList[indexPath.row].title ?? ""
            controller.isFromPickworkout = true
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
