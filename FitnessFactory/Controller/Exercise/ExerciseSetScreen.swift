//
//  ExerciseSetScreen.swift
//  fitness_factory
//
//  Created by iroid on 23/06/21.
//

import UIKit
import SkeletonView

class ExerciseSetScreen: UIViewController {

    @IBOutlet weak var workoutImageView: UIImageView!
    
    @IBOutlet weak var exerciseTitleLabel: UILabel!
   
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var exerciseSetTableView: UITableView!
    
    @IBOutlet weak var addToMyWorkoutButton: UIButton!
  
    @IBOutlet weak var backgroundView: UIView!
    
    @IBOutlet weak var TableViewHeightConstraint : NSLayoutConstraint!
    @IBOutlet weak var backButton: UIButton!
    var workoutSetId = Int()
    var setTitleName = String()
    var workoutSetList = [PickWorkoutExerciseListResponse]()
    var isFromHome : Bool = false
    var isFromPickWorkout : Bool = false
    var isHeight : CGFloat = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        initalizedDetails()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- initalizedDetails Details
    func initalizedDetails(){
        exerciseTitleLabel.text = setTitleName
        let nib = UINib(nibName: "workoutDetailCell", bundle: nil)
        exerciseSetTableView.register(nib, forCellReuseIdentifier: "workoutDetailCell")
        exerciseSetTableView.tableFooterView = UIView()
//        exerciseSetTableView.rowHeight = 84
//        exerciseSetTableView.estimatedRowHeight = 84
        exerciseSetTableView.isSkeletonable = true
        let gradient = SkeletonGradient(baseColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.1310794454))
        exerciseSetTableView.showAnimatedGradientSkeleton(usingGradient: gradient, animation: nil, transition: .crossDissolve(0.5))
        pickWorkoutDetailsAPI(workoutId: workoutSetId)
        exerciseSetListAPI(Id: workoutSetId)
        isHeight = TableViewHeightConstraint.constant
    }
    
   
    
    func setUpData(workOutDetail:[WorkoutExerciseMultipleSetListResponse]){
//        for i in 0...workOutDetail.count - 1 {
//            let detail = workOutDetail[0]
//            Utility.setImage(detail.i, imageView: workoutImageView)
//            exerciseTitleLabel.text = detail.exercise?.title
//        }
     //   exerciseSetTableView.reloadData()
    }

    func backScreen(){
        tabBarController?.tabBar.isHidden = false
//        for controller in self.navigationController!.viewControllers as Array {
//            if controller.isKind(of: ExerciseScreen.self) {
//                self.navigationController!.popToViewController(controller, animated: true)
//                break
//            }
//        }
        let controller = STORYBOARD.myWorkout.instantiateViewController(withIdentifier:"MyWorkoutScreen") as! MyWorkoutScreen
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        controller.isBack = true
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func backToHome(){
        tabBarController?.tabBar.isHidden = false
        self.tabBarController?.selectedIndex = 1
        self.navigationController?.popToRootViewController(animated: true)
        let controller = STORYBOARD.myWorkout.instantiateViewController(withIdentifier:"MyWorkoutScreen") as! MyWorkoutScreen
        controller.isBack = true
        UIApplication.topViewController()?.navigationController?.pushViewController(controller, animated: true)
       // self.navigationController?.pushViewController(controller, animated: true)
        //self.tabBarController?.selectedViewController?.navigationController?.pushViewController(controller, animated: true)
    }
    var lastContentOffset: CGFloat = 0
    var heightSub : CGFloat = 0
    @IBAction func onAddWorkout(_ sender: UIButton) {
        addToMyWorkoutAPI(Id: workoutSetId)
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(descriptionLabel.calculateMaxLines() > 2 && exerciseTitleLabel.calculateMaxLines() > 2){
            heightSub = -200
        }else{
            if(descriptionLabel.calculateMaxLines() > 4 || exerciseTitleLabel.calculateMaxLines() > 3){
                if(descriptionLabel.calculateMaxLines() >= 3 && exerciseTitleLabel.calculateMaxLines() >= 2){
                    heightSub = -200
                }else{
                    heightSub = -180
                    
                }
            }
            else{
            if(descriptionLabel.calculateMaxLines() > 2 || exerciseTitleLabel.calculateMaxLines() > 2){
                heightSub = -170
                
            }else{
                    heightSub = -100}
            }}
        if(workoutSetList.count > 3 ){
            print(TableViewHeightConstraint.constant)
        let z = scrollView.contentOffset.y
        if self.lastContentOffset < scrollView.contentOffset.y {
            if(TableViewHeightConstraint.constant - z <= -10){
                descriptionLabel.isHidden = true
                if(TableViewHeightConstraint.constant - z <= heightSub){
            descriptionLabel.isHidden = true
                exerciseTitleLabel.isHidden = true
            TableViewHeightConstraint.constant = heightSub
                }else{
                    if(TableViewHeightConstraint.constant <= -100){
                        exerciseTitleLabel.isHidden = true
                    }
                    TableViewHeightConstraint.constant -= z;
                    descriptionLabel.isHidden = true
                }
            }else{
                TableViewHeightConstraint.constant -= z;
                descriptionLabel.isHidden = true
            }
        } else if self.lastContentOffset > scrollView.contentOffset.y {
            if(TableViewHeightConstraint.constant >= -30){
            descriptionLabel.isHidden = false
                exerciseTitleLabel.isHidden = false
            TableViewHeightConstraint.constant = 50
            }else{
                if(TableViewHeightConstraint.constant + (z * -1) >= 10){
                    descriptionLabel.isHidden = false
                        exerciseTitleLabel.isHidden = false
                    TableViewHeightConstraint.constant = 50
                }else{
                    if(TableViewHeightConstraint.constant >= -10){
                    descriptionLabel.isHidden = false
                        exerciseTitleLabel.isHidden = false}
                    TableViewHeightConstraint.constant += z * -1
                }
            }
        } else {
            // didn't move
        }
        }
    }
    @IBAction func onBack(_ sender: UIButton) {
        if isFromHome {
            tabBarController?.tabBar.isHidden = false
        }
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension ExerciseSetScreen : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return workoutSetList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "workoutDetailCell") as! workoutDetailCell
        cell.pickitem = workoutSetList[indexPath.row]
        cell.editButton.isHidden = isFromPickWorkout
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.backgroundView.isHidden = false
        let vc =  STORYBOARD.myWorkout.instantiateViewController(withIdentifier: "ExerciseDetailShowScreen") as! ExerciseDetailShowScreen
        vc.modalPresentationStyle = .overFullScreen
        vc.delegate = self
        vc.isFromPickworkout = true
        vc.showPickWorkoutData = workoutSetList[indexPath.row]
        self.present(vc, animated: true, completion: nil)
    }
}

extension ExerciseSetScreen {
    //MARK:- RecipeList
    func exerciseSetListAPI(Id:Int) {
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            PickOutWorkoutService.shared.pickExerciseList(Id: Id) { (statusCode, response) in
                Utility.hideIndicator()
                if let res = response.pickWorkoutExerciseListResponse{
                    self.workoutSetList = []
                    self.workoutSetList.append(contentsOf: res)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.exerciseSetTableView.stopSkeletonAnimation()
                        self.view.hideSkeleton(reloadDataAfter: true, transition: .crossDissolve(0.25))
                        self.exerciseSetTableView.reloadData()
                    }
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:- workout Detsials API
    func pickWorkoutDetailsAPI(workoutId : Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
           // Utility.showIndecator()
            MyWorkoutService.shared.pickWorkoutDetails(id: workoutId){ (statusCode, response) in
                Utility.hideIndicator()
                if let res = response.pickWorkoutDetailsResponse{
                    self.exerciseTitleLabel.text = res.title
                    self.descriptionLabel.text = res.desc
                    Utility.setImage(res.image, imageView: self.workoutImageView)
                }
                
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func addToMyWorkoutAPI(Id:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
           // selectDateList.map(String.init).joined(separator: ",")
            PickOutWorkoutService.shared.addToMyWorkout(Id:Id) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response?.message {
                    let alertController = UIAlertController(title: "Added!", message: "This workout was added to 'My Workouts'.", preferredStyle: .alert)
                    // Create the actions
                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                        UIAlertAction in
                        if self!.isFromHome {
                            self?.backToHome()
                        } else {
                            self?.backScreen()
                        }
                        NSLog("OK Pressed")
                    }
                    // Add the actions
                    alertController.addAction(okAction)
                    // Present the controller
                    self?.present(alertController, animated: true, completion: nil)
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}

extension ExerciseSetScreen : hideBackgroundDelegate {
    func hideView() {
        backgroundView.isHidden = true
    }
}

extension ExerciseSetScreen: SkeletonTableViewDataSource {
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "workoutDetailCell"
    }
    
    func collectionSkeletonView(_ skeletonView: UITableView, numberOfRowsInSection section: Int) -> Int {
        2
    }
}
extension UILabel {
    func calculateMaxLines() -> Int {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(Float.infinity))
        let charSize = font.lineHeight
        let text = (self.text ?? "") as NSString
        let textSize = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        let linesRoundedUp = Int(ceil(textSize.height/charSize))
        return linesRoundedUp
    }
}
