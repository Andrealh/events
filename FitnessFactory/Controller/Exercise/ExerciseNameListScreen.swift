//
//  ExerciseNameListScreen.swift
//  fitness_factory
//
//  Created by iroid on 23/06/21.
//

import UIKit
import SkeletonView

class ExerciseNameListScreen: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var exerciseNameListTableView: UITableView!
    @IBOutlet weak var addMyWorkoutButton: UIButton!
    @IBOutlet weak var addWorkoutView: UIView!
    
    var exerciseId = Int()
    var titleName = String()
    var pickExerciseList = [WorkoutListResponse]()
    var isFromHome = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initalizedDetails()
    }
    
    //MARK:- initalizedDetails Details
    func initalizedDetails(){
        let nib = UINib(nibName: "RecipeListCell", bundle: nil)
        exerciseNameListTableView.register(nib, forCellReuseIdentifier: "RecipeListCell")
        exerciseNameListTableView.tableFooterView = UIView()
        exerciseNameListTableView.rowHeight = 80
        exerciseNameListTableView.estimatedRowHeight = 80
        exerciseNameListTableView.isSkeletonable = true
        let gradient = SkeletonGradient(baseColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.1310794454))
        exerciseNameListTableView.showAnimatedGradientSkeleton(usingGradient: gradient, animation: nil, transition: .crossDissolve(0.25))
        titleLabel.text = titleName
        if isFromHome {
            addWorkoutView.isHidden = true
        } else {
            addWorkoutView.isHidden = false
        }
        exerciseListAPI(Id: exerciseId)
    }

    @IBAction func onBack(_ sender: UIButton) {
       
        tabBarController?.tabBar.isHidden = false
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onHome(_ sender: UIButton) {
        tabBarController?.tabBar.isHidden = false
        tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func onAddWorkout(_ sender: UIButton) {
        addToMyWorkoutAPI(Id: exerciseId)
    }
}

extension ExerciseNameListScreen : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pickExerciseList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecipeListCell") as! RecipeListCell
        cell.item = pickExerciseList[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller = STORYBOARD.exercise.instantiateViewController(withIdentifier:"ExerciseSetScreen") as! ExerciseSetScreen
        controller.workoutSetId = pickExerciseList[indexPath.row].admin_workout_subcategory_id ?? 0
        controller.isFromHome = false
        navigationController?.pushViewController(controller, animated: true)
    }
}

extension ExerciseNameListScreen {
    //MARK:- Exercise List
    func exerciseListAPI(Id:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            PickOutWorkoutService.shared.pickExerciseList(Id: Id) { (statusCode, response) in
                Utility.hideIndicator()
                if let res = response.workoutListResponse{
                    self.pickExerciseList = []
                    self.pickExerciseList.append(contentsOf: res)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.exerciseNameListTableView.stopSkeletonAnimation()
                        self.view.hideSkeleton(reloadDataAfter: true, transition: .crossDissolve(0.25))
                        self.exerciseNameListTableView.reloadData()
                    }
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func addToMyWorkoutAPI(Id:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
           // selectDateList.map(String.init).joined(separator: ",")
            PickOutWorkoutService.shared.addToMyWorkout(Id:Id) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response?.message {
                    let alertController = UIAlertController(title: "Added!", message: "This workout was added to 'My Workouts'.", preferredStyle: .alert)
                    let OKAction = UIAlertAction(title:  "OK", style: .default, handler: nil)
                    alertController.addAction(OKAction)
                    self?.present(alertController, animated: true, completion: nil)
                   
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}
extension ExerciseNameListScreen : SkeletonTableViewDataSource {
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "RecipeListCell"
    }
    
    func collectionSkeletonView(_ skeletonView: UITableView, numberOfRowsInSection section: Int) -> Int {
        2
    }
}
