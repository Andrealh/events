//
//  PickOutWorkoutScreen.swift
//  fitness_factory
//
//  Created by iroid on 23/06/21.
//

import UIKit
import SkeletonView

class PickOutWorkoutScreen: UIViewController {
    
    @IBOutlet weak var pickWorkoutTableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var workoutNameList = [pickWorkoutResponse]()
    var pickWorkoutId = Int()
    var titleName = String()
    var hasMorePage : Bool = false
    var page = 1
    var meta:Meta!
    var isFromPickworkout : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initalizedDetails()
        // Do any additional setup after loading the view.
    }
    func initalizedDetails(){
        let nib = UINib(nibName: "ExcercisesCell", bundle: nil)
        pickWorkoutTableView.register(nib, forCellReuseIdentifier: "ExcercisesCell")
        pickWorkoutTableView.tableFooterView = UIView()
        pickWorkoutTableView.rowHeight = 80
        pickWorkoutTableView.estimatedRowHeight = 80
        pickWorkoutTableView.isSkeletonable = true
        let gradient = SkeletonGradient(baseColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.1310794454))
        pickWorkoutTableView.showAnimatedGradientSkeleton(usingGradient: gradient, animation: nil, transition: .crossDissolve(0.5))
        titleLabel.text = titleName
        pickWorkoutAPI(Id: pickWorkoutId, Page: 1)
    }
    
    // MARK: scroll method
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let size = scrollView.contentSize
        let inset = scrollView.contentInset
        let y = offset.y + bounds.size.height - inset.bottom
        let h = size.height
        let reload_distance:CGFloat = 10.0
        if y > (h + reload_distance) {
            print("called")
            if let metaTotal = self.meta?.total{
                if self.workoutNameList.count != metaTotal{
                    if self.hasMorePage{
                        self.hasMorePage = false
                        self.page += 1
                        pickWorkoutAPI(Id: pickWorkoutId, Page: page)
                    }
                }
            }
        }
    }
    
    func appendDataCollectionView(data: [pickWorkoutResponse]){
        var indexPathArray: [IndexPath] = []
        for i in self.workoutNameList.count..<self.workoutNameList.count + data.count{
            indexPathArray.append(IndexPath(item: i, section: 0))
        }
        self.workoutNameList.append(contentsOf: data)
        self.pickWorkoutTableView.insertRows(at: indexPathArray, with: .automatic)
    }
    
    @IBAction func onBack(_ sender: UIButton) {
       // tabBarController?.tabBar.isHidden = false
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onHome(_ sender: UIButton) {
       // tabBarController?.tabBar.isHidden = false
        tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: true)
    }
}

extension PickOutWorkoutScreen : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return workoutNameList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExcercisesCell") as! ExcercisesCell
        cell.pickWorkout = workoutNameList[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tabBarController?.tabBar.isHidden = true
        let controller = STORYBOARD.exercise.instantiateViewController(withIdentifier:"ExerciseSetScreen") as! ExerciseSetScreen
        controller.workoutSetId = workoutNameList[indexPath.row].admin_workout_id ?? 0
        controller.isFromPickWorkout = isFromPickworkout
//        controller.titleName = workoutNameList[indexPath.row].title ?? ""
 //       controller.isFromHome = false
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

extension PickOutWorkoutScreen {
    //MARK:- RecipeList
    func pickWorkoutAPI(Id:Int,Page:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
           // Utility.showIndecator()
            PickOutWorkoutService.shared.pickWorkoutList(Id: Id,Page: Page) { [self] (statusCode, response) in
                Utility.hideIndicator()
                hasMorePage = true
                if let res = response.pickWorkoutResponse{
                    if page == 1{
                        self.workoutNameList = res
                        print(self.workoutNameList.toJSON())
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            self.pickWorkoutTableView.stopSkeletonAnimation()
                            self.view.hideSkeleton(reloadDataAfter: true, transition: .crossDissolve(0.25))
                            self.pickWorkoutTableView.reloadData()
                        }
                    }else{
                        self.appendDataCollectionView(data: res)
                    }
                    if let meta = response.metaResponse{
                        self.meta = meta
                    }
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                stronSelf.hasMorePage = true
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}

extension PickOutWorkoutScreen : SkeletonTableViewDataSource {
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "ExcercisesCell"
    }
    
    func collectionSkeletonView(_ skeletonView: UITableView, numberOfRowsInSection section: Int) -> Int {
        2
    }
}
