//
//  SignUpScreen.swift
//  FitnessFactory
//
//  Created by iroid on 04/06/21.
//

import UIKit
import DropDown
import MapKit
import CoreLocation
import CountryPickerView
 
class SignUpScreen: UIViewController, CLLocationManagerDelegate{

    //MARK:- UITextField IBOutlet
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var postalCodeTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var birthdayTextField: UITextField!
    @IBOutlet weak var countryNameTextField: UITextField!
    @IBOutlet weak var genderTextField: UITextField!
    @IBOutlet weak var cityNameTextField: UITextField!
    
    //MARK:- UITableView IBOutlet
    @IBOutlet weak var genderOptionTableView: UITableView!
    
    //MARK:- UITextView IBOutlet
    @IBOutlet weak var termsPrivacyTextView: UITextView!

    
    //MARK:- UIButton IBOutlet
    @IBOutlet weak var genderButton: UIButton!
    @IBOutlet weak var showPasswordButton: UIButton!

    //MARK:- Variable
    let locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var getLat:String = ""
    var getLong:String = ""
    var datePicker = UIDatePicker()
    var birthDate = ""
    var date = ""
    var pickUpArray: [String] = []
    var selectGender = Int()
    let countryPickerView = CountryPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initalizedDetails()
    }
    
    //MARK:- initalizedDetails Details
    func initalizedDetails(){
        pickUpDate(birthdayTextField)
        let calendar = Calendar.current
        var minDateComponent = calendar.dateComponents([.day,.month,.year], from: Date())
        minDateComponent.day = 01
        minDateComponent.month = 01
        minDateComponent.year = 1900
        let minDate = calendar.date(from: minDateComponent)
        datePicker.minimumDate = minDate! as Date
        datePicker.maximumDate = Calendar.current.date(byAdding: .year, value: -13, to: Date())
        pickUpArray = ["Male","Female","Other"]
        let nib = UINib(nibName: "genderOptionCell", bundle: nil)
        genderOptionTableView.register(nib, forCellReuseIdentifier: "genderOptionCell")
        genderOptionTableView.layer.borderWidth = 1.5
        genderOptionTableView.layer.borderColor = UIColor.signUpDropDownBorder.cgColor
        genderOptionTableView.backgroundColor = UIColor.clear
        genderOptionTableView.layer.cornerRadius = 10
        genderButton.isSelected = true
      //  countryList.delegate = self
        countryPickerView.delegate = self
        countryPickerView.dataSource = self
        setupMap()
        setUpTextView()
    }
    
    override public func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        initalizedDetails()
    }
    
    func checkValidation() -> String?{
        if self.firstNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter your first name"
        }
        else if self.lastNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter your last name"
        }
        else if self.emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter your email"
        }
        else if !self.emailTextField.text.isEmailValid(){
            return "Please enter a valid email address"
        }
        else if self.passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter your password"
        }
        else if self.passwordTextField.text!.count < 8{
            return "Password must be longer than 8 characters."
        }
    return nil
    }
    
    //MARK:- setUpTextView
    func setUpTextView()  {
        let Tap = UITapGestureRecognizer(target: self, action: #selector(self.taponView(_:)))
        termsPrivacyTextView?.addGestureRecognizer(Tap)
        
        let myString:NSString = termsPrivacyTextView.text as! NSString
        var myMutableString = NSMutableAttributedString()
        myMutableString = NSMutableAttributedString(string:myString as String)
        
        let textColor = UIColor.signUpTextStyle1
        myMutableString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "Montserrat-Medium", size: 12)!, range: myString.range(of: "By clicking Create Account, you agree to our"))
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: textColor, range: myString.range(of: "By clicking Create Account, you agree to our"))
        
        myMutableString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "Montserrat-Medium", size: 12)!, range: myString.range(of: "and"))
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: textColor, range: myString.range(of: "and"))

        let textHighlightColor = UIColor.signUpTextStyle2
        myMutableString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "Montserrat-Bold", size: 12)!, range: myString.range(of: "Terms of Use"))
        myMutableString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "Montserrat-Bold", size: 12)!, range: myString.range(of: "Privacy Policy."))

        myMutableString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: myString.range(of: "Terms of Use") )
        myMutableString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: myString.range(of: "Privacy Policy."))
        
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: textHighlightColor, range: myString.range(of: "Terms of Use"))
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: textHighlightColor, range: myString.range(of: "Privacy Policy."))
        
        termsPrivacyTextView?.attributedText = myMutableString
        termsPrivacyTextView?.textAlignment = .left
       // termsPrivacyTextView.font = UIFont(name: "Brother 1816", size: 14.0)
    }
    
    @objc func taponView(_ recognizer: UITapGestureRecognizer?) {
        let location: CGPoint? = recognizer?.location(in: termsPrivacyTextView)
        let tappedWord = word(atPosition: CGPoint(x: location?.x ?? 0.0, y: location?.y ?? 0.0))
        if (tappedWord == "Terms") || (tappedWord == "of") || (tappedWord == "Use") || (tappedWord == "Terms of Use")
        {
            view.endEditing(true)
            closeGenderView()
            UIApplication.shared.openURL(URL(string:"https://fitnessfac.ca/terms")!)
        }else if (tappedWord == "Privacy") || (tappedWord == "Policy") || (tappedWord == "Privacy Policy.")
        {
            view.endEditing(true)
            closeGenderView()
            UIApplication.shared.openURL(URL(string:"https://fitnessfac.ca/privacy")!)
        }
    }
    
    func word(atPosition position: CGPoint) -> String? {
        var position = position
        position.y += termsPrivacyTextView.contentOffset.y
        let tapPosition: UITextPosition? = termsPrivacyTextView.closestPosition(to: position)
        var textRange: UITextRange? = nil
        if let aPosition = tapPosition {
            textRange = termsPrivacyTextView.tokenizer.rangeEnclosingPosition(aPosition, with: .word, inDirection: UITextDirection(rawValue: UITextLayoutDirection.right.rawValue))
        }
        var tappedWord: String = ""
        if let aRange = textRange {
            tappedWord = termsPrivacyTextView.text(in: aRange) ?? ""
        }
        var start = Int()
        if let aStart = textRange?.start {
            start = termsPrivacyTextView.offset(from: termsPrivacyTextView.beginningOfDocument, to: aStart)
        }
        if start > 0 {
            let lastChar = termsPrivacyTextView.text[termsPrivacyTextView.text.index(termsPrivacyTextView.text.startIndex, offsetBy: start - 1)]
            if lastChar == "#" {
                tappedWord = "#\(tappedWord)"
            }
        }
        return tappedWord
    }
    
    func setupMap(){
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        if(CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == .authorizedAlways) {
            if currentLocation != nil{
                currentLocation = locationManager.location
            }
        }
        centerMapOnUserLocation()
    }
    
    // MARK:- Center Map On UserLocation
    func centerMapOnUserLocation() {
        
        guard let coordinate = locationManager.location?.coordinate else { return }
        getLat = String(coordinate.latitude )
        getLong = String(coordinate.longitude )
    }
    func pickUpDate(_ textField : UITextField){
        closeGenderView()
        datePicker.maximumDate = Date()
        // DatePicker
        
        self.datePicker = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.datePicker.backgroundColor = UIColor.white
        self.datePicker.datePickerMode = UIDatePicker.Mode.date
      
            if #available(iOS 14.0, *) {
                datePicker.preferredDatePickerStyle = .wheels
            } else {
                //datePicker.preferredDatePickerStyle = .automatic
            }
            

        textField.inputView = self.datePicker
        
//        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.signUpTextStyle1
        toolBar.sizeToFit()

        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        birthdayTextField.inputAccessoryView = toolBar
    }
    
    @objc func doneClick() {
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateStyle = .medium
        dateFormatter1.timeStyle = .none
        birthDate = formattedDateFromString(dateString: "\(datePicker.date)", withFormat: "yyyy-MM-dd")!
        date = formattedDateFromString(dateString: "\(datePicker.date)", withFormat: "yyyy-MM-dd")!
        birthdayTextField.text = dateFormatter1.string(from: datePicker.date)
        birthdayTextField.resignFirstResponder()
    
    }
    func formattedDateFromString(dateString: String, withFormat format: String) -> String? {
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
        if let date = inputFormatter.date(from: dateString) {
            let outputFormatter = DateFormatter()
            outputFormatter.dateFormat = format
            return outputFormatter.string(from: date)
        }
        return nil
    }

//    func selectedCountry(country: Country) {
//            self.countryNameTextField.text = "\(country.name!)"
//    }
    
    
    func closeGenderView()  {
        genderOptionTableView.isHidden = true
        genderTextField.layer.borderWidth = 0
        genderButton.isSelected = true
    }
    @objc func cancelClick() {
        birthdayTextField.resignFirstResponder()
    }

    
    //MARK:- IBAction
    @IBAction func onCreateAccount(_ sender: UIButton) {
        if let error = self.checkValidation(){
            Utility.showAlert(vc: self, message: error)
        } else {
            createAccountApi()
        }
    }
    @IBAction func onCountryList(_ sender: UIButton) {
        view.endEditing(true)
        closeGenderView()
        countryPickerView.showCountriesList(from: self)
//        let navController = UINavigationController(rootViewController: countryList)
//        self.present(navController, animated: true, completion: nil)
    }
    @IBAction func onGenderSelection(_ sender: UIButton) {
        view.endEditing(true)
        if genderButton.isSelected {
            genderTextField.layer.borderWidth = 1.5
            genderButton.isSelected = false
            genderOptionTableView.isHidden = false
        } else {
            genderButton.isSelected = true
            genderTextField.layer.borderWidth = 0
            genderOptionTableView.isHidden = true
        }
    }
    
    @IBAction func onLogin(_ sender: UIButton) {
//        let controller = STORYBOARD.login.instantiateViewController(withIdentifier:"LogInScreen") as! LogInScreen
//        navigationController?.pushViewController(controller, animated: true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onShowPassword(_ sender: UIButton) {
        view.endEditing(true)
        passwordTextField.layer.borderWidth = 2.5
        if passwordTextField.text?.count != 0 {
            if showPasswordButton.isSelected {
                showPasswordButton.isSelected = false
                showPasswordButton.setImage(UIImage(named: "Hide"), for: .normal)
                passwordTextField.isSecureTextEntry = true
            } else {
                showPasswordButton.isSelected = true
                showPasswordButton.setImage(UIImage(named: "Show"), for: .normal)
                passwordTextField.isSecureTextEntry = false
            }
        }
    }
}

//MARK:- UITextFieldDelegate
extension SignUpScreen:UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        closeGenderView()
      //  textField.returnKeyType = .next
        if textField != passwordTextField {
            passwordTextField.layer.borderWidth = 0
        }
        textField.layer.borderWidth = 1.5
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layer.borderWidth = 0
    }
    
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        view.endEditing(true)
//
//    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == postalCodeTextField{
            let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789- "
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            return (string == filtered)
        } else if textField == userNameTextField{
            let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-"
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            return (string == filtered)
        } else if textField == cityNameTextField{
            let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            return (string == filtered)
        } else if textField == postalCodeTextField{
            let ACCEPTABLE_CHARACTERS = "0123456789 "
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            return (string == filtered)
        } else if textField == firstNameTextField || textField == lastNameTextField{
            let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz- "
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            return (string == filtered)
        } else {
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
      }
}

extension SignUpScreen : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pickUpArray.count
    }
  
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "genderOptionCell") as! genderOptionCell
        cell.titleLabel.text = pickUpArray[indexPath.row]
        cell.sepratorLabel.backgroundColor = UIColor.signUpDropDownSeprator
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        closeGenderView()
        if indexPath.row == 0 {
            genderTextField.text = "Male"
            selectGender = 1
        } else if indexPath.row == 1{
            genderTextField.text = "Female"
            selectGender = 2
        } else if indexPath.row == 2{
            genderTextField.text = "Other"
            selectGender = 3
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
}


extension SignUpScreen {
    //MARK:- Login API
    func createAccountApi(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            let data = CreateAccountRequest(firstName: firstNameTextField.text,
                                            lastName: lastNameTextField.text,
                                            userName: userNameTextField.text,
                                            email: emailTextField.text,
                                            password: passwordTextField.text,
                                            passwordConfirmation: passwordTextField.text,
                                            country: countryNameTextField.text,
                                            city: cityNameTextField.text,
                                            postalCode: postalCodeTextField.text,
                                            gender: selectGender,
                                            dob: date,
                                            latitude: getLat,
                                            longtitude: getLong)
            LoginService.shared.createAccount(parameters: data.toJSON()) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response.loginResponse{
                    Utility.saveUserData(data: res.toJSON())
//                    let controller = STORYBOARD.home.instantiateViewController(withIdentifier:"LinkYourGymScreen") as! LinkYourGymScreen
//                    self?.navigationController?.pushViewController(controller, animated: true)
                    let controller = STORYBOARD.login.instantiateViewController(withIdentifier:"LogInScreen") as! LogInScreen
                    self?.navigationController?.pushViewController(controller, animated: true)
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}

extension SignUpScreen : CountryPickerViewDelegate {
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        self.countryNameTextField.text = "\(country.name)"
    }
}

extension SignUpScreen: CountryPickerViewDataSource {
    func preferredCountries(in countryPickerView: CountryPickerView) -> [Country] {
        return ["CA"].compactMap { countryPickerView.getCountryByCode($0) }
    }
    
    func sectionTitleForPreferredCountries(in countryPickerView: CountryPickerView) -> String? {
        return "Countries"
    }
}


