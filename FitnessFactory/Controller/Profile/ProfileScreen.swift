//
//  ProfileScreen.swift
//  FitnessFactory
//
//  Created by iroid on 07/06/21.
//

import UIKit
import Messages
import MessageUI

class ProfileScreen: UIViewController {
    //MARK:- UILabel IBOutlet
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var birthdayLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var currentWeightLabel: UILabel!
    @IBOutlet weak var goalWeightLabel: UILabel!
    @IBOutlet weak var timeFrameLabel: UILabel!
    @IBOutlet weak var workoutPerWeekLabel: UILabel!
    @IBOutlet weak var currentWeightOptionLabel: UILabel!
    @IBOutlet weak var goalWeightOptionLabel: UILabel!
    @IBOutlet weak var whyGoalLabel: UILabel!
    @IBOutlet weak var supplementaryGoalLabel: UILabel!
    @IBOutlet weak var supplementrayTitleLabel: UILabel!
    @IBOutlet weak var whyGoalTitleLabel: UILabel!
    @IBOutlet weak var websiteLabel: UILabel!
    @IBOutlet weak var mailLabel: UILabel!
    
    //MARK:- UIView IBOutlet
    @IBOutlet weak var weightGoalView: UIView!
    @IBOutlet weak var otherGoalView: UIView!
    @IBOutlet weak var setWeightGoalView: UIView!
    @IBOutlet weak var setWhyGoalView: UIView!
    @IBOutlet weak var gymView: UIView!
    @IBOutlet weak var gymDetailsView: UIView!
    @IBOutlet weak var InfoView: UIView!
    @IBOutlet weak var gymName: UILabel!
    @IBOutlet weak var gymJoinDate: UILabel!
    @IBOutlet weak var gymPhoneNumber: UILabel!
    @IBOutlet weak var gymAddress: UILabel!
    @IBOutlet weak var profileText: UILabel!
    @IBOutlet weak var editButton : UIButton!
    
    //MARK:- UIImageView IBOutlet
    @IBOutlet weak var gymProfileImageView: UIImageView!
    @IBOutlet weak var arrowDirection : UIImageView!
    //MARK:- UIScrollView IBOutlet
    @IBOutlet weak var profileScrollView: UIScrollView!
    
    //MARK:- Variable
    var myAccountDetail:MyAccountResponse?
    var birthday = ""
    var gymLinkStatus : GymLinkStatusResponse?
    let inputFormatter = DateFormatter()
    var startDate = Date()
    var endDate = Date()
    var email = String()
    
    //MARK:- UIBUtton
    // Thisw UI mark button is not initalized but it should be initalized when  the user is allowed to enter the app whithout internet. tHE APP DOES NOT ALLOW USER TO USE APP WITHOUT A PROPER INTERNET CONNECTION WHICH IS NOT THE BEST THING TO PROPOSE TO THE USER AND THIS WILL AFFECT THE USERS EXPIRENCE IN A DRASTIC WAY.
    // MARK A
    override func viewDidLoad() {
        super.viewDidLoad()
        initalizedDetails()
    
//        print("thi is here");
//        print(myAccountDetail?.firstName)
//        if(myAccountDetail?.goalWeight ==  nil){
//            print("here");
//            editButton.setTitle("Set your goal", for: .normal);
//        }else{
//            editButton.setTitle("Edit your goal", for: .normal);
//        }
//        var x : String;
//        if(myAccountDetail?.firstName == nil || myAccountDetail?.firstName == ""){
//            x = "?"
//        }else{
//            
//            x = String(myAccountDetail?.firstName![1] ?? "X");
//            x.append(contentsOf: String(myAccountDetail?.lastName![1] ?? "X"));
//        }
//        profileText.text = x;
    }
        // This function needs to chnage to porvide async functionality in the code and have the sync label values available when the vieW appears.
    
    override func viewWillAppear(_ animated: Bool) {
        profileScrollView.scrollToTop()
    }
    
    //MARK:- initalizedDetails Details
     func initalizedDetails(){
         editButton.isHidden = true;
        inputFormatter.dateFormat = "yyyy-MM-dd"
        myAccountAPI()
    }
    
    func SetupData()  {
        if(myAccountDetail?.goalWeight ==  nil){
            print("here");
            editButton.setTitle("Set your goal", for: .normal);
        }else{
            editButton.setTitle("Edit your goal", for: .normal);
        }
        var x : String;
        if(myAccountDetail?.firstName == nil || myAccountDetail?.firstName == ""){
            x = "?"
        }else{
            
            x = String(myAccountDetail?.firstName![0] ?? "X");
            x.append(contentsOf: String(myAccountDetail?.lastName![0
                                                                  ] ?? "X"));
        }
        profileText.text = x.uppercased();
        if myAccountDetail?.supplementryGoal != nil || myAccountDetail?.whyGoal != nil {
            let title1 = (myAccountDetail?.supplementryGoal == nil) ? true : false
            supplementrayTitleLabel.isHidden = title1
            let title2 = (myAccountDetail?.whyGoal == nil) ? true : false
            whyGoalTitleLabel.isHidden = title2
            otherGoalView.isHidden = false
            setWhyGoalView.isHidden = true
            whyGoalLabel.text = myAccountDetail?.whyGoal
            supplementaryGoalLabel.text = myAccountDetail?.supplementryGoal
        } else {
            otherGoalView.isHidden = true
            setWhyGoalView.isHidden = false
        }
        
        
            weightGoalView.isHidden = false
            setWeightGoalView.isHidden = true
            currentWeightLabel.text = String(myAccountDetail?.currentWeight ?? 0)
            goalWeightLabel.text = String(myAccountDetail?.goalWeight ?? 0)
            if let date = inputFormatter.date(from: myAccountDetail?.start_date ?? "") {
                startDate = date
            }
            if let date = inputFormatter.date(from: myAccountDetail?.end_date ?? "") {
                endDate = date
            }
            let remainingDate = endDate.days(from: startDate)
            timeFrameLabel.text = String(remainingDate) + " Days"
            workoutPerWeekLabel.text = String(myAccountDetail?.workoutWeek ?? 0) + " Days"
            currentWeightOptionLabel.text = myAccountDetail?.currentWeightType
            goalWeightOptionLabel.text = myAccountDetail?.goalWeightType
//        let titleString = (myAccountDetail?.firstName ?? "") + " " + (myAccountDetail?.lastName ?? "")
      //  editButton.setTitle(titleString, for: .normal)
        
        nameLabel.text = (myAccountDetail?.firstName ?? "") + " " + (myAccountDetail?.lastName ?? "")
        nameLabel.text?.append("  ");
        nameLabel.addImage(imageName: "Edit_recipes", afterLabel: true)
        userNameLabel.text = myAccountDetail?.userName
        if myAccountDetail?.gender == 1 {
            genderLabel.text = "Male"
        } else if myAccountDetail?.gender == 2 {
            genderLabel.text = "Female"
        } else {
            genderLabel.text = "Other"
        }
//        let result = (myAccountDetail?.gender == 1) ? "Male" : "Female"
//        genderLabel.text = result
        if(myAccountDetail?.dob == "" || myAccountDetail?.dob == nil){
            birthdayLabel.text = "--/--/----"
        }else{
        birthday = formattedDateFromString(dateString: myAccountDetail?.dob ?? "", withFormat: "d MMM YYYY") ?? ""
            birthdayLabel.text = birthday}
        if(myAccountDetail?.city == "" || myAccountDetail?.city == nil){
            addressLabel.text = "--"
        }else{
        let address = (myAccountDetail?.city ?? "") + ", " + (myAccountDetail?.country ?? "")
            addressLabel.text = address + " " + (myAccountDetail?.postalCode ?? "") + "."}
        
        if myAccountDetail?.gym_data != nil {
            gymDetailsView.isHidden = false
            gymView.isHidden = true
        } else {
            gymDetailsView.isHidden = true
            gymView.isHidden = false
        }
        //        gymView.isHidden = myAccountDetail?.gym_select ?? false
        //        gymDetailsView.isHidden = !(myAccountDetail?.gym_select ?? false)
        gymName.text = myAccountDetail?.gym_data?.name
        if myAccountDetail?.gym_data?.member_since != nil {
            let timeStamp = myAccountDetail?.gym_data?.member_since ?? 0
            let date = Date(timeIntervalSince1970: TimeInterval(timeStamp))
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "d MMM, YYYY"
            dateFormatter.timeZone = TimeZone(identifier: "UTC")
            //        let unixTimeStamp: Double = Double(timeStamp) / 1000.0
            //        let exactDate = NSDate.init(timeIntervalSince1970: unixTimeStamp)
            //        let dateFormatt = DateFormatter();
            
            //print(dateFormatt.string(from: exactDate as Date))
            let tier = myAccountDetail?.tier ?? ""
            gymJoinDate.text = "\(tier) Member since \(dateFormatter.string(from: date as Date))"
        } else {
            gymJoinDate.text = ""
        }
        
        gymPhoneNumber.text = myAccountDetail?.gym_data?.owner_phone
        gymAddress.text = myAccountDetail?.gym_data?.address
        websiteLabel.text = myAccountDetail?.gym_data?.website
        mailLabel.text = myAccountDetail?.gym_data?.owner_email
        email = myAccountDetail?.gym_data?.owner_email ?? ""
        Utility.setImage(myAccountDetail?.gym_data?.profile , imageView: gymProfileImageView)
        
//        weightGoalView.isHidden = true
//    otherGoalView.isHidden = true
//     setWeightGoalView.isHidden = true
//     setWhyGoalView.isHidden = true
//     //   gymView.isHidden = true
//      //   gymDetailsView.isHidden = true
//        //InfoView.isHidden = true;
    }
   
    func formattedDateFromString(dateString: String, withFormat format: String) -> String? {
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "yyyy-MM-dd"
        if let date = inputFormatter.date(from: dateString) {
            let outputFormatter = DateFormatter()
            outputFormatter.dateFormat = format
            return outputFormatter.string(from: date)
        }
        return nil
    }
    
    @objc func closeView() {
        let transition = CATransition()
        transition.duration = 0.2
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.reveal
        transition.subtype = CATransitionSubtype.fromRight
        navigationController?.view.layer.add(transition, forKey: nil)
        //self.tabBarController?.tabBar.isHidden = false
        _ = navigationController?.popViewController(animated: false)
    }
    
    
    //MARK:- IBAction
    @IBAction func onLinkGym(_ sender: UIButton) {
        tabBarController?.tabBar.isHidden = true
        let controller = STORYBOARD.home.instantiateViewController(withIdentifier:"GymsNearYouScreen") as! GymsNearYouScreen
        controller.isFromProfile = true
        navigationController?.pushViewController(controller, animated: true)
       // arrowDirection.transform =  arrowDirection.transform.rotated(by: .pi)
    }
    
    @IBAction func onCall(_ sender: UIButton) {
        Utility.dialNumber(number: self.myAccountDetail?.gym_data?.owner_phone ?? "")
    }
    
    @IBAction func onWebsite(_ sender: UIButton) {

        let websiteUrl = myAccountDetail?.gym_data?.website ?? "https://fitnessfac.ca"
        guard let url = URL(string: websiteUrl) else {
           
            return
        }
        UIApplication.shared.open(url, completionHandler: { success in
            if success {
                print("opened")
            } else {
                if let url = URL(string: "https://fitnessfac.ca") {
                    UIApplication.shared.open(url)
                }
            }
        })
    }
    
    @IBAction func onEmail(_ sender: UIButton) {
        print("here");
        if MFMailComposeViewController.canSendMail() {
                let mail = MFMailComposeViewController()
                mail.mailComposeDelegate = self
            mail.setToRecipients(["contact@fitnessfac.ca"])
                mail.setMessageBody("<p>You're so awesome!</p>", isHTML: true)

                present(mail, animated: true)
            } else {
               
            }
//        guard MFMailComposeViewController.canSendMail() else {
//                    return
//                }
//                let composer = MFMailComposeViewController()
//                composer.mailComposeDelegate = self
//                composer.setToRecipients([email]) // email id of the recipient
//                composer.setSubject("")
//                composer.setMessageBody("", isHTML: false)
//                present(composer, animated: true, completion: nil)
    }
    
    @IBAction func onGymDetail(_ sender: UIButton) {
        // tabBarController?.tabBar.isHidden = true
        let controller = STORYBOARD.home.instantiateViewController(withIdentifier:"GymsNearYouScreen") as! GymsNearYouScreen
        controller.isFromProfile = true
        //controller.gymSelect = ((myAccountDetail?.gym_select) != nil) ?? false
        controller.gymDetail = myAccountDetail?.gym_data
        controller.isFromProfile = true
        controller.gymSelectStatus = myAccountDetail?.gym_select
        controller.gymLinkId = gymLinkStatus?.link_gym_id ?? 0
        controller.editData =  { [weak self] value in
            print(value)
            self?.myAccountDetail = value
            self?.SetupData()
        }
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func onHome(_ sender: UIButton) {
        let transition = CATransition()
        transition.duration = 0.2
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.reveal
        transition.subtype = CATransitionSubtype.fromRight
        navigationController?.view.layer.add(transition, forKey: nil)
        tabBarController?.selectedIndex = 0
        //self.tabBarController?.tabBar.isHidden = false
        _ = navigationController?.popToRootViewController(animated: false)
    }
    //MARK:- May not be required as the functions might change to the dropdown menu where one needs to tap the pre built button to click view it in detail.
    
    
//    @IBAction func onWeightGoal(_ sender: UIButton) {
//        //  tabBarController?.tabBar.isHidden = true
//        let controller = STORYBOARD.profile.instantiateViewController(withIdentifier:"WeightGoalScreen") as! WeightGoalScreen
//        controller.editData =  { [weak self] value in
//            print(value)
//            self?.myAccountDetail = value
//            self?.SetupData()
//        }
//        navigationController?.pushViewController(controller, animated: true)
//    }
    @IBAction func onEditWeightGoal(_ sender: UIButton) {
        // tabBarController?.tabBar.isHidden = true
        let controller = STORYBOARD.profile.instantiateViewController(withIdentifier:"WeightGoalScreen") as! WeightGoalScreen
        controller.weightGoalData = myAccountDetail
        controller.editData =  { [weak self] value in
            print(value)
            self?.myAccountDetail = value
            self?.SetupData()
        }
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func onDevelope(_ sender: UIButton) {
        UIApplication.shared.open(URL(string: "https://fitnessfac.ca/")!, options: [:], completionHandler: nil)
    }
    
    @IBAction func onSupplementaryGoal(_ sender: UIButton) {
//        if(InfoView.isHidden == true){
//            InfoView.isHidden = false}
//        else{
//            InfoView.isHidden = true}
        //tabBarController?.tabBar.isHidden = true
        let controller = STORYBOARD.profile.instantiateViewController(withIdentifier:"OtherGoalScreen") as! OtherGoalScreen
        controller.editData =  { [weak self] value in
            print(value)
            self?.myAccountDetail = value
            self?.SetupData()
        }
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func onEditSupplementaryGoal(_ sender: UIButton) {
        //tabBarController?.tabBar.isHidden = true
        let controller = STORYBOARD.profile.instantiateViewController(withIdentifier:"OtherGoalScreen") as! OtherGoalScreen
        controller.otherGoalData = myAccountDetail
        controller.editData =  { [weak self] value in
            print(value)
            self?.myAccountDetail = value
            self?.SetupData()
        }
        navigationController?.pushViewController(controller, animated: true)
    }
    @IBAction func onEditProfile(_ sender: UIButton) {
        // tabBarController?.tabBar.isHidden = true
        let controller = STORYBOARD.profile.instantiateViewController(withIdentifier:"EditProfileScreen") as! EditProfileScreen
        controller.editAccountDetail = myAccountDetail
        controller.editData =  { [weak self] value in
            print(value)
            self?.myAccountDetail = value
            self?.SetupData()
        }
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func onBack(_ sender: UIButton) {
        //        self.tabBarController?.tabBar.isHidden = false
        //        self.navigationController?.popViewController(animated: true)
        closeView()
    }
    @IBAction func onChangePassword(_ sender: UIButton) {
        //tabBarController?.tabBar.isHidden = true
        let controller = STORYBOARD.profile.instantiateViewController(withIdentifier:"ChangePasswordScreen") as! ChangePasswordScreen
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func onLogout(_ sender: UIButton) {
        let alertController = UIAlertController(title: "Sign out?", message: "Are you sure you want to Logout?", preferredStyle: .alert)
        let resendAction = UIAlertAction(title: "Sign out", style: .default, handler: { [weak self] (action) in
            self?.logOut()
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            alertController.dismiss(animated: true, completion: nil)
        })
        alertController.addAction(cancelAction)
        alertController.addAction(resendAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
extension ProfileScreen {
    //MARK:- Login API
    func myAccountAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            MyAccountService.shared.myAccount() { (statusCode, response) in
                Utility.hideIndicator()
                if let res = response.myAccountResponse{
                    self.myAccountDetail = res
                    self.SetupData()
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        } else {
            Utility.hideIndicator()
            let alertController = UIAlertController(title: APPLICATION_NAME, message:"It seems you are not connected to the internet. Kindly connect and try again", preferredStyle: .alert)
            // Create the actions
            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                UIAlertAction in
                self.navigationController?.popViewController(animated: true)
                NSLog("OK Pressed")
            }
            // Add the actions
            alertController.addAction(okAction)
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
            //Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:- LogOut API
    func logOut(){
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            let parameter = [DEVICE_ID:DEVICE_UNIQUE_IDETIFICATION]
            LoginService.shared.logOut(parameters: parameter) { (statusCode, response) in
                DispatchQueue.main.async {
                    Utility.hideIndicator()
                    Utility.removeUserData()
                    let vc = STORYBOARD.login.instantiateViewController(withIdentifier: "LogInScreen") as! LogInScreen
                    let navVC = UINavigationController(rootViewController: vc)
                    navVC.interactivePopGestureRecognizer?.isEnabled = false
                    appDelegate.window?.rootViewController = navVC
                    appDelegate.window?.makeKeyAndVisible()
                }
            } failure: { [weak self] (error) in
                guard let strongSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: strongSelf, message: error)
            }
            
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}

extension ProfileScreen : MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate
{
    func messageComposeViewController(_ controller: MFMessageComposeViewController,didFinishWith result: MessageComposeResult) {
            switch (result) {
                case .cancelled:
                    print("Message was cancelled")
                    dismiss(animated: true, completion: nil)
                case .failed:
                    print("Message failed")
                    dismiss(animated: true, completion: nil)
                case .sent:
                    print("Message was sent")
                    dismiss(animated: true, completion: nil)
                default:
                    break
            }
        }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}
extension StringProtocol {
    subscript(_ offset: Int)                     -> Element     { self[index(startIndex, offsetBy: offset)] }
    subscript(_ range: Range<Int>)               -> SubSequence { prefix(range.lowerBound+range.count).suffix(range.count) }
    subscript(_ range: ClosedRange<Int>)         -> SubSequence { prefix(range.lowerBound+range.count).suffix(range.count) }
    subscript(_ range: PartialRangeThrough<Int>) -> SubSequence { prefix(range.upperBound.advanced(by: 1)) }
    subscript(_ range: PartialRangeUpTo<Int>)    -> SubSequence { prefix(range.upperBound) }
    subscript(_ range: PartialRangeFrom<Int>)    -> SubSequence { suffix(Swift.max(0, count-range.lowerBound)) }
}
extension UILabel
{
    func addImage(imageName: String, afterLabel bolAfterLabel: Bool = false)
    {
        let attachment: NSTextAttachment = NSTextAttachment()
        attachment.image = UIImage(named: imageName)
        attachment.bounds = CGRect(x: 0, y: 0, width: 20, height: 20)
        let attachmentString: NSAttributedString = NSAttributedString(attachment: attachment)

        if (bolAfterLabel)
        {
            let strLabelText: NSMutableAttributedString = NSMutableAttributedString(string: self.text!)
            strLabelText.append(attachmentString)

            self.attributedText = strLabelText
        }
        else
        {
            let strLabelText: NSAttributedString = NSAttributedString(string: self.text!)
            let mutableAttachmentString: NSMutableAttributedString = NSMutableAttributedString(attributedString: attachmentString)
            mutableAttachmentString.append(strLabelText)

            self.attributedText = mutableAttachmentString
        }
    }

    func removeImage()
    {
        let text = self.text
        self.attributedText = nil
        self.text = text
    }
}
