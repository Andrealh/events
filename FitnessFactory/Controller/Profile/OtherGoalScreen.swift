//
//  OtherGoalScreen.swift
//  FitnessFactory
//
//  Created by iroid on 09/06/21.
//

import UIKit

class OtherGoalScreen: UIViewController {
    
    //MARK:- UITextView IBOutlet
    @IBOutlet weak var whyGoalTextView: UITextView!
    @IBOutlet weak var supplementryTextView: UITextView!
    
    //MARK:- UIButton IBOutlet
    @IBOutlet weak var updateButton: UIButton!

    //MARK:- Variable
    var whyGoalPlaceHolderText = "(Why I go to the gym, Main reason)"
    var supplementryPlaceHolderText = "(Other benefit that going to the gym may provide)"
    var otherGoalData:MyAccountResponse?
    var editData:((_ editDataRes:MyAccountResponse) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initalizedDetails()
    }
    
    //MARK:- initalizedDetails Details
    func initalizedDetails(){
            if whyGoalPlaceHolderText != otherGoalData?.whyGoal && otherGoalData?.whyGoal != nil{
                whyGoalTextView.textColor = #colorLiteral(red: 0.02745098039, green: 0.02745098039, blue: 0.02352941176, alpha: 1)
                whyGoalTextView.text = otherGoalData?.whyGoal
            } else {
                whyGoalTextView.textColor = #colorLiteral(red: 0.02745098039, green: 0.02745098039, blue: 0.02352941176, alpha: 0.4017110475)
                whyGoalTextView.text = whyGoalPlaceHolderText
            }
        
            if supplementryPlaceHolderText != otherGoalData?.supplementryGoal && otherGoalData?.supplementryGoal != nil{
                supplementryTextView.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                supplementryTextView.text = otherGoalData?.supplementryGoal
            } else {
                supplementryTextView.textColor = #colorLiteral(red: 0.02745098039, green: 0.02745098039, blue: 0.02352941176, alpha: 0.4017110475)
                supplementryTextView.text = supplementryPlaceHolderText
            }
    }
    
    func checkValidation() -> String?{
        if self.whyGoalTextView.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter your why i go to the gym"
        }
        else if self.supplementryTextView.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter your supplementary goal"
        }
        return nil
    }
    
    //MARK:- IBAction
    @IBAction func onBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onSet(_ sender: UIButton) {
//        if let error = self.checkValidation(){
//            Utility.showAlert(vc: self, message: error)
//        } else {
            otherGoalAPI()
      //  }
    }
    
    @IBAction func onHome(_ sender: UIButton) {
        tabBarController?.tabBar.isHidden = false
        tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: true)
    }
}

//MARK:UITextViewDelegate
extension OtherGoalScreen:UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == whyGoalTextView {
            if whyGoalTextView.text == whyGoalPlaceHolderText {
                whyGoalTextView.text = ""
            }
            whyGoalTextView.textColor = UIColor.profileTextStyle1
        }
        if textView == supplementryTextView {
            if supplementryTextView.text == supplementryPlaceHolderText {
                supplementryTextView.text = ""
            }
            supplementryTextView.textColor = UIColor.profileTextStyle1
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if whyGoalTextView.text.trimmingCharacters(in: .whitespaces).isEmpty  {
            whyGoalTextView.text = whyGoalPlaceHolderText
            whyGoalTextView.textColor = UIColor.profileTextStyle2
        }
        if supplementryTextView.text.trimmingCharacters(in: .whitespaces).isEmpty  {
            supplementryTextView.text = supplementryPlaceHolderText
            supplementryTextView.textColor = UIColor.profileTextStyle2
        }
        
//        if whyGoalTextView.text == whyGoalPlaceHolderText || supplementryTextView.text == supplementryPlaceHolderText {
//            updateButton.isUserInteractionEnabled = false
//            updateButton.alpha = 0.5
//        } else {
//            updateButton.isUserInteractionEnabled = true
//            updateButton.alpha = 1
//        }
        updateButton.isUserInteractionEnabled = true
        updateButton.alpha = 1
    }
    
    @objc func textViewDidChange(_ textView: UITextView) {
    }
}

extension OtherGoalScreen {
    //MARK:- Other Goal API
    func otherGoalAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            var data : OtherGoalRequest?
            if whyGoalTextView.text == whyGoalPlaceHolderText && supplementryTextView.text == supplementryPlaceHolderText {
                data = OtherGoalRequest(whyGoal:"", supplementaryGoal:"")
            } else if whyGoalTextView.text == whyGoalPlaceHolderText {
                data = OtherGoalRequest(whyGoal:"", supplementaryGoal:supplementryTextView.text)
            }  else if supplementryTextView.text == supplementryPlaceHolderText  {
                data = OtherGoalRequest(whyGoal:whyGoalTextView.text, supplementaryGoal:"")
            } else {
                data = OtherGoalRequest(whyGoal:whyGoalTextView.text, supplementaryGoal:supplementryTextView.text)
            }
            
            MyAccountService.shared.otherGoal(parameters: data!.toJSON()) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response?.myAccountResponse{
                    self?.editData?(res)
                    self?.navigationController?.popViewController(animated: true)
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}
