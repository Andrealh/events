//
//  ChangePasswordScreen.swift
//  FitnessFactory
//
//  Created by iroid on 08/06/21.
//

import UIKit

class ChangePasswordScreen: UIViewController {
    //MARK:- UITextField IBOutlet
    @IBOutlet weak var currentPasswordTextField: UITextField!
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var reTypeNewPasswordTextField: UITextField!
    //MARK:- UIButton IBOutlet
    @IBOutlet weak var updateButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initalizedDetails()
    }
    
    //MARK:- initalizedDetails Details
    func initalizedDetails(){
        updateButton.isUserInteractionEnabled = false
    }
   
    func checkValidation() -> String?{
        if self.newPasswordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0 < 8{
                    return "The new password must be at least 8 characters."
        }else if self.newPasswordTextField.text != self.reTypeNewPasswordTextField.text{
            return "Please make sure the passwords you've entered match."
        }
        return nil
    }
    //MARK:- IBAction
    @IBAction func onBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onUpdate(_ sender: UIButton) {
        if let error = self.checkValidation(){
            Utility.showAlert(vc: self, message: error)
        } else {
            changePasswordAPI()
        }
    }
    
    @IBAction func onHome(_ sender: UIButton) {
        tabBarController?.tabBar.isHidden = false
        tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: true)
    }
}

//MARK:- UITextFieldDelegate
extension ChangePasswordScreen:UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
       // textField.returnKeyType = .next
        textField.layer.borderWidth = 1.5
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layer.borderWidth = 1
        if self.currentPasswordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count != 0 &&
         self.newPasswordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count != 0 && self.reTypeNewPasswordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count != 0{
            updateButton.isUserInteractionEnabled = true
            updateButton.alpha = 1
        } else {
            updateButton.isUserInteractionEnabled = false
            updateButton.alpha = 0.5
        }
        textField.layer.borderWidth = 0
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        view.endEditing(true)
      }
}

extension ChangePasswordScreen {
    //MARK:- Login API
    func changePasswordAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            let data = ChangePasswordRequest(oldPassword: currentPasswordTextField.text,
                                             password: newPasswordTextField.text,
                                             passwordConfirmation: reTypeNewPasswordTextField.text)
            LoginService.shared.changePassword(parameters: data.toJSON()) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response.message{
                    let alertController = UIAlertController(title: APPLICATION_NAME, message: res, preferredStyle: .alert)

                    // Create the actions
                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                        UIAlertAction in
                        NSLog("OK Pressed")
                        self?.navigationController?.popViewController(animated: true)
                    }
                    // Add the actions
                    alertController.addAction(okAction)

                    // Present the controller
                    self?.present(alertController, animated: true, completion: nil)
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}
