//
//  EditProfileScreen.swift
//  FitnessFactory
//
//  Created by iroid on 08/06/21.
//

import UIKit
 import MapKit
import CoreLocation
import CountryPickerView

class EditProfileScreen: UIViewController, CLLocationManagerDelegate  {
    //MARK:- UITextField IBOutlet
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var postalCodeTextField: UITextField!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var birthdayTextField: UITextField!
    @IBOutlet weak var countryNameTextField: UITextField!
    @IBOutlet weak var genderTextField: UITextField!
    @IBOutlet weak var cityNameTextField: UITextField!
    
    //MARK:- UITableView IBOutlet
    @IBOutlet weak var genderOptionTableView: UITableView!
    
    //MARK:- UIButton IBOutlet
    @IBOutlet weak var genderButton: UIButton!
    @IBOutlet weak var updateButton: UIButton!
    @IBOutlet weak var deleteProfileButton: UIButton!
    
    //MARK:- Variable
    let locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var getLat:String = ""
    var getLong:String = ""
    var editAccountDetail:MyAccountResponse?
    var editData:((_ editDataRes:MyAccountResponse) -> Void)?
    var datePicker = UIDatePicker()
    //    var birthDate = ""
    var date = ""
    var pickUpArray: [String] = []
    var selectGender = Int()
   // var countryList = CountryList()
    var birthday = ""
    var gender = Int()
    var isEdit = Bool()
    let countryPickerView = CountryPickerView()

    override func viewDidLoad() {
        super.viewDidLoad()
        initalizedDetails()
        // Do any additional setup after loading the view.
        
    }
    
    //MARK:- initalizedDetails Details
    func initalizedDetails(){
        enableAddProfileButton(isEnable: false)
        pickUpDate(birthdayTextField)
        let calendar = Calendar.current
        var minDateComponent = calendar.dateComponents([.day,.month,.year], from: Date())
        minDateComponent.day = 01
        minDateComponent.month = 01
        minDateComponent.year = 1900
        let minDate = calendar.date(from: minDateComponent)
        datePicker.minimumDate = minDate! as Date
        datePicker.maximumDate = Calendar.current.date(byAdding: .year, value: -13, to: Date())
        pickUpArray = ["Male","Female","Other"]
        let nib = UINib(nibName: "genderOptionCell", bundle: nil)
        genderOptionTableView.register(nib, forCellReuseIdentifier: "genderOptionCell")
        genderOptionTableView.layer.borderWidth = 1.5
        genderOptionTableView.layer.borderColor = UIColor.profileButtonBackgroundStyle1.cgColor
        genderOptionTableView.layer.cornerRadius = 10
        genderButton.isSelected = true
      //  countryList.delegate = self
        countryPickerView.delegate = self
        countryPickerView.dataSource = self
        setUpTextFieldChangeEvent()
        SetupData()
        setupMap()
    }
    
    override public func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        initalizedDetails()
    }
    
    func SetupData()  {
        firstNameTextField.text = editAccountDetail?.firstName
        lastNameTextField.text = editAccountDetail?.lastName
        userNameTextField.text = editAccountDetail?.userName
        countryNameTextField.text = editAccountDetail?.country
        cityNameTextField.text = editAccountDetail?.city
        postalCodeTextField.text = editAccountDetail?.postalCode
//        let result = (editAccountDetail?.gender == 1) ? "Male" : "Female"
//        genderTextField.text = result
        if editAccountDetail?.gender == 1 {
            genderTextField.text = "Male"
        } else if editAccountDetail?.gender == 2 {
            genderTextField.text = "Female"
        } else {
            genderTextField.text = "Other"
        }
        selectGender = editAccountDetail?.gender ?? 0
        birthday = formattedDateFromString(dateString: editAccountDetail?.dob ?? "", withFormat: "d MMM YYYY") ?? ""
        birthdayTextField.text = birthday
        date = editAccountDetail?.dob ?? ""
    }
    
    func setupMap(){
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        if(CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == .authorizedAlways) {
            if currentLocation != nil{
                currentLocation = locationManager.location
            }
        }
        centerMapOnUserLocation()
    }
    
    // MARK:- Center Map On UserLocation
    func centerMapOnUserLocation() {
        
        guard let coordinate = locationManager.location?.coordinate else { return }
        getLat = String(locationManager.location?.coordinate.latitude ?? 0.0)
        getLong = String(locationManager.location?.coordinate.longitude ?? 0.0)
    }
    
    
    
    func setUpTextFieldChangeEvent(){
        self.firstNameTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        self.lastNameTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        self.userNameTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        self.countryNameTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        self.cityNameTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        self.postalCodeTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        self.genderTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        self.birthdayTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        self.checkAllValue()
    }
    
    func checkAllValue() {
        if !self.checkTextFieldValueNill(textField: self.firstNameTextField) && !self.checkTextFieldValueNill(textField: self.lastNameTextField) && !self.checkTextFieldValueNill(textField: self.userNameTextField) && !self.checkTextFieldValueNill(textField: self.countryNameTextField) && !self.checkTextFieldValueNill(textField: self.cityNameTextField) && !self.checkTextFieldValueNill(textField: self.postalCodeTextField) && !self.checkTextFieldValueNill(textField: self.genderTextField) && !self.checkTextFieldValueNill(textField: self.birthdayTextField)
        {
            enableAddProfileButton(isEnable: true)
        } else {
            enableAddProfileButton(isEnable: false)
        }
    }
    
    func enableAddProfileButton(isEnable:Bool){
        isEdit = isEnable
        self.updateButton.alpha = isEnable ? 1 : 0.5
        self.updateButton.isUserInteractionEnabled = isEnable
    }
    func checkTextFieldValueNill(textField: UITextField) -> Bool{
        return textField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0
    }
    func pickUpDate(_ textField : UITextField){
        datePicker.maximumDate = Date()
        // DatePicker
        
        self.datePicker = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.datePicker.backgroundColor = UIColor.white
        self.datePicker.datePickerMode = UIDatePicker.Mode.date
        
        if #available(iOS 14.0, *) {
            datePicker.preferredDatePickerStyle = .wheels
        } else {
            //datePicker.preferredDatePickerStyle = .automatic
        }
        
        
        textField.inputView = self.datePicker
        
        //        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        birthdayTextField.inputAccessoryView = toolBar
    }
    
    @objc func doneClick() {
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateStyle = .medium
        dateFormatter1.timeStyle = .none
        //        birthDate = Utility.formattedDateFromString(dateString: "\(datePicker.date)", withFormat: "yyyy-MM-dd")!
        date = Utility.formattedDateFromString(dateString: "\(datePicker.date)", withFormat: "yyyy-MM-dd")!
        birthdayTextField.text = Utility.formattedDateFromString(dateString: "\(datePicker.date)", withFormat: "d MMM YYYY")!
        checkAllValue()
        birthdayTextField.resignFirstResponder()
        
    }
    func formattedDateFromString(dateString: String, withFormat format: String) -> String? {
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "yyyy-MM-dd"
        if let date = inputFormatter.date(from: dateString) {
            let outputFormatter = DateFormatter()
            outputFormatter.dateFormat = format
            return outputFormatter.string(from: date)
        }
        return nil
    }
    
//    func selectedCountry(country: Country) {
//        self.countryNameTextField.text = "\(country.name!)"
//        checkAllValue()
//    }
    
    func closeGenderView(){
        genderOptionTableView.isHidden = true
        genderTextField.layer.borderWidth = 0
        genderButton.isSelected = true
    }
    
    func checkValidation() -> String?{
        if postalCodeTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter your postal code"
        }
        else if self.postalCodeTextField.text!.count < 6{
            return "Postal code must be longer than 6 characters."
        }
        return nil
    }
    
    @objc func cancelClick() {
        birthdayTextField.resignFirstResponder()
    }
    
    //MARK:- IBAction
    @IBAction func onCountryList(_ sender: UIButton) {
        view.endEditing(true)
        closeGenderView()
        countryPickerView.showCountriesList(from: self)
//        let navController = UINavigationController(rootViewController: countryList)
//        self.present(navController, animated: true, completion: nil)
    }
    @IBAction func onBack(_ sender: UIButton) {
            self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onGenderSelection(_ sender: UIButton) {
        view.endEditing(true)
        if genderButton.isSelected {
            genderTextField.layer.borderWidth = 1.5
            genderButton.isSelected = false
            genderOptionTableView.isHidden = false
        } else {
            genderTextField.layer.borderWidth = 0
            genderButton.isSelected = true
            genderOptionTableView.isHidden = true
        }
    }
    
    @IBAction func onHome(_ sender: UIButton) {
        tabBarController?.tabBar.isHidden = false
        tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func onUpdate(_ sender: UIButton) {
        if let error = self.checkValidation(){
            Utility.showAlert(vc: self, message: error)
        } else {
            editProfileAPI()
        }
    }
    
    @IBAction func onDelete(_ sender: UIButton) {
        let alertController = UIAlertController(title: "Are you sure?", message: "You want to delete your profile?", preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Yes, delete it!", style: UIAlertAction.Style.default) { [self]
            UIAlertAction in
           
            DeleteProfileAPI()
            NSLog("OK Pressed")
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default) { [self]
            UIAlertAction in
            NSLog("OK Pressed")
        }
        // Add the actions
        okAction.setValue(UIColor.red, forKey: "titleTextColor")
        cancelAction.setValue(UIColor.gray, forKey: "titleTextColor")
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
}

extension EditProfileScreen : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pickUpArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "genderOptionCell") as! genderOptionCell
        cell.titleLabel.text = pickUpArray[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        closeGenderView()
        checkAllValue()
        if indexPath.row == 0 {
            genderTextField.text = "Male"
            selectGender = 1
        } else if indexPath.row == 1{
            genderTextField.text = "Female"
            selectGender = 2
        } else if indexPath.row == 2{
            genderTextField.text = "Other"
            selectGender = 3
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
}

//MARK:- UITextFieldDelegate
extension EditProfileScreen:UITextFieldDelegate{
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
       // textField.returnKeyType = .next
        textField.layer.borderWidth = 1.5
        closeGenderView()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layer.borderWidth = 0
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == postalCodeTextField{
            let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789- "
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            return (string == filtered)
        } else if textField == userNameTextField{
            let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-"
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            return (string == filtered)
        } else if textField == cityNameTextField{
            let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            return (string == filtered)
        } else if textField == postalCodeTextField{
            let ACCEPTABLE_CHARACTERS = "0123456789 "
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            return (string == filtered)
        } else if textField == firstNameTextField || textField == lastNameTextField{
            let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz- "
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            return (string == filtered)
        } else {
            return true
        }
    }
}
extension EditProfileScreen {
    //MARK:- Login API
    func DeleteProfileAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            MyAccountService.shared.deleteProfile(){ [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response?.myAccountResponse {
                    self?.editData?(res)
                    self?.navigationController?.popViewController(animated: true)
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}
extension EditProfileScreen {
    //MARK:- Login API
    func editProfileAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            let data = EditProfileRequest(firstName: firstNameTextField.text,
                                          lastName: lastNameTextField.text,
                                          userName: userNameTextField.text,
                                          country: countryNameTextField.text,
                                          city: cityNameTextField.text,
                                          postalCode: postalCodeTextField.text,
                                          gender: selectGender,
                                          dob: date,
                                          latitude: getLat,
                                          longtitude: getLong
            )
            MyAccountService.shared.editProfile(parameters: data.toJSON()) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response?.myAccountResponse {
                    self?.editData?(res)
                    self?.navigationController?.popViewController(animated: true)
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}


extension EditProfileScreen: CountryPickerViewDataSource {
    func preferredCountries(in countryPickerView: CountryPickerView) -> [Country] {
        return ["CA"].compactMap { countryPickerView.getCountryByCode($0) }
    }
    
    func sectionTitleForPreferredCountries(in countryPickerView: CountryPickerView) -> String? {
        return "Countries"
    }
}

extension EditProfileScreen : CountryPickerViewDelegate {
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        self.countryNameTextField.text = "\(country.name)"
        self.checkAllValue()
    }
}
