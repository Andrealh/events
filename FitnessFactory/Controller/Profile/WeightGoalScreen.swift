//
//  WeightGoalScreen.swift
//  FitnessFactory
//
//  Created by iroid on 09/06/21.
//

import UIKit
import DropDown

class WeightGoalScreen: UIViewController {
    
    //MARK:- UIButton IBOutlet
    @IBOutlet weak var updateButton: UIButton!
    @IBOutlet weak var currentWeightButton: UIButton!
    @IBOutlet weak var goalWeightButton: UIButton!
    
    //MARK:- UITextField IBOutlet
    @IBOutlet weak var currentTextField: UITextField!
    @IBOutlet weak var routineTextField: UITextField!
    @IBOutlet weak var workOutInWeekTextField: UITextField!
    @IBOutlet weak var expTextField: UITextField!
    @IBOutlet weak var goalTextField: UITextField!
    @IBOutlet weak var goalStartDateTextField: UITextField!
    @IBOutlet weak var goalEndDateTextField: UITextField!
    
    @IBOutlet weak var expView: UIView!

    //MARK:- variable
    var weightDropDown = DropDown()
    var goalDropDown = DropDown()
    var pickUpArray: [String] = []
    var currentWeight = "Lbs"
    var goalWeight = "Lbs"
    var weightGoalData:MyAccountResponse?
    var editData:((_ editDataRes:MyAccountResponse) -> Void)?
    var goalStartDatePicker = UIDatePicker()
    var goalEndDatePicker = UIDatePicker()
    var birthDate = ""
    var startDate = ""
    var endDate = ""
    var dateFormatter =  DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initalizedDetails()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- initalizedDetails Details
    func initalizedDetails(){
        pickUpArray = ["Lbs","Kgs"]
        dateFormatter.dateFormat = "yyyy-MM-dd"
        updateButton.isUserInteractionEnabled = false

        currentWeightDropDownFilterButton()
        //setFloatingTextField()
        goalStartPickupDate(goalStartDateTextField)
        goalEndPickupDate(goalEndDateTextField)
        goalStartDatePicker.minimumDate = Calendar.current.date(byAdding: .year, value: 0, to: Date())
        goalWeightDropDownFilterButton()
        if weightGoalData != nil {
            setUpData()
        }
    }
    
    func goalStartPickupDate(_ textField : UITextField){
        goalStartDatePicker.maximumDate = Date()
        // DatePicker
        self.goalStartDatePicker = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.goalStartDatePicker.backgroundColor = UIColor.white
        self.goalStartDatePicker.datePickerMode = UIDatePicker.Mode.date
      
            if #available(iOS 14.0, *) {
                goalStartDatePicker.preferredDatePickerStyle = .wheels
            } else {
                //datePicker.preferredDatePickerStyle = .automatic
            }
        textField.inputView = self.goalStartDatePicker
        
//        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.profileTextStyle1
        toolBar.sizeToFit()

        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneStartClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelStartDateClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        goalStartDateTextField.inputAccessoryView = toolBar
    }
    
    func goalEndPickupDate(_ textField : UITextField){
        goalEndDatePicker.maximumDate = Date()
        // DatePicker
        self.goalEndDatePicker = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.goalEndDatePicker.backgroundColor = UIColor.white
        self.goalEndDatePicker.datePickerMode = UIDatePicker.Mode.date
      
            if #available(iOS 14.0, *) {
                goalEndDatePicker.preferredDatePickerStyle = .wheels
            } else {
                //datePicker.preferredDatePickerStyle = .automatic
            }
        textField.inputView = self.goalEndDatePicker
        
//        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.profileTextStyle1
        toolBar.sizeToFit()

        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneEndClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelEndDateClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        goalEndDateTextField.inputAccessoryView = toolBar
    }
    
    @objc func doneStartClick() {
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateStyle = .medium
        dateFormatter1.timeStyle = .none
        birthDate = formattedDateFromString(dateString: "\(goalStartDatePicker.date)", withFormat: "yyyy-MM-dd")!
        startDate = formattedDateFromString(dateString: "\(goalStartDatePicker.date)", withFormat: "yyyy-MM-dd")!
        goalStartDateTextField.text = dateFormatter1.string(from: goalStartDatePicker.date)
        goalEndDatePicker.minimumDate = Calendar.current.date(byAdding: .day, value: +1, to: goalStartDatePicker.date)
        goalStartDateTextField.resignFirstResponder()
    }
    
    @objc func doneEndClick() {
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateStyle = .medium
        dateFormatter1.timeStyle = .none
        birthDate = formattedDateFromString(dateString: "\(goalEndDatePicker.date)", withFormat: "yyyy-MM-dd")!
        endDate = formattedDateFromString(dateString: "\(goalEndDatePicker.date)", withFormat: "yyyy-MM-dd")!
        goalEndDateTextField.text = dateFormatter1.string(from: goalEndDatePicker.date)
        goalEndDateTextField.resignFirstResponder()
    }
    
    @objc func cancelStartDateClick() {
        goalStartDateTextField.resignFirstResponder()
    }
    
    @objc func cancelEndDateClick() {
        goalEndDateTextField.resignFirstResponder()
    }
    
    func formattedDateFromString(dateString: String, withFormat format: String) -> String? {
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
        if let date = inputFormatter.date(from: dateString) {
            let outputFormatter = DateFormatter()
            outputFormatter.dateFormat = format
            return outputFormatter.string(from: date)
        }
        return nil
    }
    
    func dateformattedFromString(dateString: String, withFormat format: String) -> String? {
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "yyyy-MM-dd"
        if let date = inputFormatter.date(from: dateString) {
            let outputFormatter = DateFormatter()
            outputFormatter.dateFormat = format
            return outputFormatter.string(from: date)
        }
        return nil
    }

    func setUpData(){
        if weightGoalData != nil {
            print(weightGoalData?.toJSON())
            currentTextField.text = String(weightGoalData?.currentWeight ?? 0)
            currentWeight =  weightGoalData?.currentWeightType == "Kgs" ? "Kgs" : "Lbs"
            currentWeightButton.setTitle(currentWeight, for: .normal)
            goalTextField.text = String(weightGoalData?.goalWeight ?? 0)
            goalWeight =  weightGoalData?.goalWeightType == "Kgs" ? "Kgs" : "Lbs"
            goalWeightButton.setTitle(goalWeight, for: .normal)
            workOutInWeekTextField.text = String(weightGoalData?.workoutWeek ?? 0)
            routineTextField.text = weightGoalData?.routine
            expTextField.text = String(weightGoalData?.expYears ?? 0)
            let date1 = dateformattedFromString(dateString: weightGoalData?.start_date ?? "", withFormat: "MMM d, yyyy")
            startDate = dateformattedFromString(dateString: weightGoalData?.start_date ?? "", withFormat: "yyyy-MM-dd") ?? "00"
            if let date = dateFormatter.date(from: weightGoalData?.start_date ?? "") {
                goalEndDatePicker.minimumDate = Calendar.current.date(byAdding: .day, value: +1, to: date)
            }
            goalStartDateTextField.text = date1
            let date2 = dateformattedFromString(dateString: weightGoalData?.end_date ?? "", withFormat: "MMM d, yyyy") ?? "00"
            endDate = dateformattedFromString(dateString: weightGoalData?.end_date ?? "", withFormat: "yyyy-MM-dd") ?? "00"
            print("End date" , endDate)
            goalEndDateTextField.text = date2
        }
    }
    
   
    
    func currentWeightDropDownFilterButton() {
        weightDropDown.backgroundColor = UIColor.profileBackground
        weightDropDown.selectionBackgroundColor = UIColor.profileBackground
        weightDropDown.shadowColor = UIColor.profileBackground
        weightDropDown.shadowOpacity = 0
        weightDropDown.shadowRadius = 0
        weightDropDown.cornerRadius = 10
        weightDropDown.separatorColor = UIColor.profileButtonBackgroundStyle1
        
        weightDropDown.animationduration = 0
        weightDropDown.textColor = UIColor.profileTextStyle2
        weightDropDown.selectedTextColor = UIColor.profileTextStyle1
        weightDropDown.anchorView = currentWeightButton
        weightDropDown.bottomOffset = CGPoint(x: 0, y: 50)
        weightDropDown.direction = .bottom
        weightDropDown.cellNib = UINib(nibName: "DropDownCell", bundle: Bundle(for: DropDownCell.self))
        weightDropDown.dataSource = pickUpArray
        weightDropDown.dismissMode = .onTap
        weightDropDown.selectRow(-1)
        weightDropDown.selectionAction = { [weak self] (index, item) in
            if index == 0{
                //                self?.dropUpStackView.isHidden = true
                //                self?.dropUpStackViewConstant.constant = 0
                //                self?.dropOffCustomTime = false
            }else{
                //                self?.dropUpStackView.isHidden = false
                //                self?.dropUpStackViewConstant.constant = 144
                //                self?.dropOffCustomTime = true
            }
            self?.currentWeightButton.setTitle(item, for: .normal)
            self?.currentWeight = item
            self?.updateButton.isUserInteractionEnabled = true
            self?.updateButton.alpha = 1
        }
    }
    
        func goalWeightDropDownFilterButton() {
            goalDropDown.backgroundColor = UIColor.profileBackground
            goalDropDown.selectionBackgroundColor = UIColor.profileBackground
            goalDropDown.shadowColor = UIColor.profileBackground
            goalDropDown.shadowOpacity = 0
            goalDropDown.shadowRadius = 0
            goalDropDown.cornerRadius = 10
            goalDropDown.separatorColor = UIColor.profileButtonBackgroundStyle1
            goalDropDown.animationduration = 0
            goalDropDown.textColor = UIColor.profileTextStyle2
            goalDropDown.selectedTextColor = UIColor.profileTextStyle1
            goalDropDown.anchorView = goalWeightButton
            goalDropDown.bottomOffset = CGPoint(x: 0, y: 50)
            goalDropDown.direction = .bottom
            goalDropDown.cellNib = UINib(nibName: "DropDownCell", bundle: Bundle(for: DropDownCell.self))
            goalDropDown.dataSource = pickUpArray
            goalDropDown.dismissMode = .onTap
            goalDropDown.selectRow(-1)
            goalDropDown.selectionAction = { [weak self] (index, item) in
                if index == 0{
    //                self?.dropUpStackView.isHidden = true
    //                self?.dropUpStackViewConstant.constant = 0
    //                self?.dropOffCustomTime = false
                }else{
    //                self?.dropUpStackView.isHidden = false
    //                self?.dropUpStackViewConstant.constant = 144
    //                self?.dropOffCustomTime = true
                }
                self?.goalWeightButton.setTitle(item, for: .normal)
                self?.goalWeight = item
                self?.updateButton.isUserInteractionEnabled = true
                self?.updateButton.alpha = 1
            }
        }
    
    func checkValidation() -> String?{
        if self.currentTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter your current weight"
        }
        else if self.goalTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter your goal weight"
        }
        else if self.expTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter your experience in years"
        }
        else if self.workOutInWeekTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter your workout day in week"
        }
        else if self.routineTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter your routine"
        }
        return nil
    }
    
    //MARK:- IBAction
    @IBAction func onBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onSet(_ sender: UIButton) {
        if let error = self.checkValidation(){
            Utility.showAlert(vc: self, message: error)
        } else {
            weightGoalAPI()
        }
    }
    
    @IBAction func onCurrentWeight(_ sender: UIButton) {
        view.endEditing(true)
        weightDropDown.show()
    }
    
    
    @IBAction func onGoalWeight(_ sender: UIButton) {
        goalDropDown.show()
    }
    
    @IBAction func onHome(_ sender: UIButton) {
        tabBarController?.tabBar.isHidden = false
        tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: true)
    }
}

//MARK:- UITextFieldDelegate
extension WeightGoalScreen:UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //textField.returnKeyType = .next
        weightDropDown.hide()
        textField.layer.borderWidth = 1.5
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if currentTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count != 0 &&
            goalTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count != 0 &&
            expTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count != 0 &&
            workOutInWeekTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count != 0 &&
            routineTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count != 0{
            updateButton.isUserInteractionEnabled = true
            updateButton.alpha = 1
        } else {
            updateButton.isUserInteractionEnabled = false
            updateButton.alpha = 0.5
        }
        textField.layer.borderWidth = 0
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == workOutInWeekTextField {
            let ACCEPTABLE_CHARACTERS = "1234567"
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            return (string == filtered)
        } else {
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
    }
}

extension WeightGoalScreen {
    //MARK:- Other Goal API
    func weightGoalAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            let data = WeightGoalRequest(currentWeight: currentTextField.text ?? "",
                                         currentWeightType: currentWeight,
                                         goalWeight: goalTextField.text ?? "",
                                         goalWeightType: goalWeight,
                                         expYears: expTextField.text ?? "",
                                         workout_week: workOutInWeekTextField.text ?? "",
                                         start_date: startDate,
                                         end_date: endDate,
                                         routine: routineTextField.text
            )
            MyAccountService.shared.weightGoal(parameters: data.toJSON()) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response?.myAccountResponse{
                    self?.editData?(res)
                    self?.navigationController?.popViewController(animated: true)
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}
