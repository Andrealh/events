//
//  EventListScreen.swift
//  FitnessFactory
//
//  Created by iroid on 10/08/21.
//

import UIKit
import SkeletonView

class EventListScreen: UIViewController {
    
    @IBOutlet weak var eventListTableView: UITableView!
    //MARK:- Variables
    var meta:Meta!
    var hasMorePage : Bool = false
    var page = 1
    var eventList : [EventListResponse] = []
    var selectedBreakFast = 1
    var isFromHome = Bool()
    var selectDate = String()
    var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if isFromHome {
            eventListTableView.rowHeight = 202
            eventListTableView.estimatedRowHeight = 202
            eventListTableView.isSkeletonable = true
            let gradient = SkeletonGradient(baseColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.1310794454))
            eventListTableView.showAnimatedGradientSkeleton(usingGradient: gradient, animation: nil, transition: .crossDissolve(0.5))
        }
        initialDetail()
        // Do any additional setup after loading the view.
    }
    
    func initialDetail(){
        
        let eventsNib = UINib(nibName: "EventsTableViewCell", bundle: nil)
        eventListTableView.register(eventsNib, forCellReuseIdentifier: "EventsTableViewCell")
        if isFromHome{
            eventListAPI(page: 1)
        } else {
            eventListAPI(page: page, selecteDate: selectDate)
        }
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(refreshPostList), for: UIControl.Event.valueChanged)
       // eventListTableView.addSubview(refreshControl)
        //nutritionListAPI(page: 1)
        //setUpData()
    }
    
    
    
    // MARK: scroll method
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let size = scrollView.contentSize
        let inset = scrollView.contentInset
        let y = offset.y + bounds.size.height - inset.bottom
        let h = size.height
        let reload_distance:CGFloat = 10.0
        if y > (h + reload_distance) {
            print("called")
            if let metaTotal = self.meta?.total{
                if self.eventList.count != metaTotal{
                    if self.hasMorePage{
                        self.hasMorePage = false
                        self.page += 1
                        if isFromHome {
                            eventListAPI(page: page)
                        } else {
                            eventListAPI(page: page, selecteDate: selectDate)
                        }
                    }
                }
            }
        }
    }
    
    func appendDataCollectionView(data: Response){
        var indexPathArray: [IndexPath] = []
        let eventRes = data.eventListResponse!
        for i in self.eventList.count..<self.eventList.count + eventRes.count{
            indexPathArray.append(IndexPath(item: i, section: 0))
            self.eventList.append(contentsOf: eventRes)
            self.eventListTableView.insertRows(at: indexPathArray, with: .automatic)
        }
    }
    
    @objc func refreshPostList() {
        refreshControl.endRefreshing()
        if isFromHome{
            eventListAPI(page: 1)
        } else {
            eventListAPI(page: page, selecteDate: selectDate)
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}


extension EventListScreen: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var numOfSections : Int = 0
        if isFromHome {
            return eventList.count
        } else {
            if eventList.count > 0 {
                // tableViewCellHeight = 202 * eventList.count
                numOfSections = eventList.count
                tableView.backgroundView = nil
            } else {
                let noDataLabelView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
                let noDataLabel = UILabel(frame: CGRect(x: 0, y: 50, width: tableView.bounds.size.width, height: 50))
                noDataLabel.text = "No Events."
                noDataLabel.textColor = UIColor.darkGray
                noDataLabel.textAlignment = .center
                noDataLabel.numberOfLines = 1
                noDataLabelView.addSubview(noDataLabel)
                tableView.backgroundView = noDataLabelView
            }
            
            return numOfSections
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isFromHome {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EventsTableViewCell", for: indexPath) as! EventsTableViewCell
            cell.item = eventList[indexPath.row]
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EventsTableViewCell", for: indexPath) as! EventsTableViewCell
            cell.item = eventList[indexPath.row]
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            tableViewCellHeightEvent = Int(tableView.contentSize.height)
        }
    }
    
}

extension EventListScreen {
    
    func eventListAPI(page:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
         //   Utility.showIndecator()
            HomeAddGymService.shared.eventList(Page : page) { [self] (statusCode, response) in
                Utility.hideIndicator()
                hasMorePage = true
                if let res = response.eventListResponse {
                    if page == 1{
                        self.eventList = []
                        self.eventList = res
                        print(self.eventList.toJSON())
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            self.eventListTableView.stopSkeletonAnimation()
                            self.view.hideSkeleton(reloadDataAfter: true, transition: .crossDissolve(0.25))
                            self.eventListTableView.reloadData()
                        }
                    } else {
                        self.appendDataCollectionView(data: response)
                    }
                    if let meta = response.metaResponse{
                        self.meta = meta
                    }
                }
                
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:- Event List API
    func eventListAPI(page:Int,selecteDate:String){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            //  Utility.showIndecator()
            let data = WorkoutListRequest(date: selecteDate)
            CalenderService.shared.eventDayList(page: page, parameters: data.toJSON()) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                self?.hasMorePage = true
                if let res = response?.eventListResponse {
                    if page == 1{
                        self?.eventList = []
                        self?.eventList = res
                        self?.eventListTableView.reloadData()
                    } else {
                        self?.appendDataCollectionView(data: response!)
                    }
                    if let meta = response?.metaResponse{
                        self?.meta = meta
                    }
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                stronSelf.hasMorePage = true
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}

extension EventListScreen : SkeletonTableViewDataSource {
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "EventsTableViewCell"
    }
    
    func collectionSkeletonView(_ skeletonView: UITableView, numberOfRowsInSection section: Int) -> Int {
        2
    }
}

