//
//  FilterScreen.swift
//  FitnessFactory
//
//  Created by iroid on 21/06/21.
//

import UIKit
import KTCenterFlowLayout
class FilterScreen: UIViewController {

    @IBOutlet weak var applyButton: UIButton!
    @IBOutlet weak var clearAllSectionButton: UIButton!
    @IBOutlet weak var filterCollectionView: UICollectionView!
    
    var gymCategory:[GymCategoryResponse] = []
    var selectCategory = [Int]()
    var delegate : SelectCategory?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initialDetail()
    }
    
    func initialDetail(){
        let nib = UINib(nibName: "FilterCollectionViewCell", bundle: nil)
        filterCollectionView.register(nib, forCellWithReuseIdentifier: "FilterCollectionViewCell")
        let layout = KTCenterFlowLayout()
        layout.minimumInteritemSpacing = 10.0
        layout.minimumLineSpacing = 10.0
        filterCollectionView.collectionViewLayout = layout
        if selectCategory.count > 0 {
            clearAllSectionButton.isHidden = false
            applyButton.alpha = 1
            applyButton.isEnabled = true
        } else {
            clearAllSectionButton.isHidden = true
            applyButton.alpha = 0.5
            applyButton.isEnabled = false
        }
        gymCategoryAPI()
    }
    
    @IBAction func onBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
   
    
    @IBAction func onApply(_ sender: UIButton) {
        if applyButton.alpha == 0.5 {
            applyButton.isEnabled = false
        } else {
            applyButton.isEnabled = true
            delegate?.selectCategoryList(selecteCategory: selectCategory)
        }
      //  self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onHome(_ sender: UIButton) {
        tabBarController?.tabBar.isHidden = false
        tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func onRemoveSelection(_ sender: UIButton) {
        selectCategory = []
        filterCollectionView.reloadData()
       // applyButton.alpha = 0.5
        clearAllSectionButton.isHidden = true
       // applyButton.isEnabled = false
    }
    
}
extension FilterScreen:UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return gymCategory.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterCollectionViewCell", for: indexPath) as! FilterCollectionViewCell
        cell.item = gymCategory[indexPath.row]
        if selectCategory.contains(gymCategory[indexPath.row].gym_category_id ?? 0){
            cell.categoryView.backgroundColor = UIColor.primaryColour
        } else {
            cell.categoryView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        return CGSize(width:  Utility.labelWidth(height: 50, font: UIFont(name: "Montserrat-SemiBold", size: 14)!, text: gymCategory[indexPath.row].name ?? "")+42, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectData = gymCategory[indexPath.row]
        if((selectCategory.contains(selectData.gym_category_id ?? 0)) ){
            let index = self.selectCategory.firstIndex(of: selectData.gym_category_id ?? 0)
            selectCategory.remove(at: index ?? 0)
        } else {
            selectCategory.append(selectData.gym_category_id ?? 0)
        }
        applyButton.alpha = 1
        applyButton.isEnabled = true
        if selectCategory.count > 0 {
            clearAllSectionButton.isHidden = false
        } else {
            
            clearAllSectionButton.isHidden = true
        }
        filterCollectionView.reloadData()
    }
}

extension FilterScreen{
    func getNearByGym(){
//        self.view.endEditing(true)
//        if Utility.isInternetAvailable(){
//            Utility.showIndecator()
//            let data = HomeAddGymRequest(latitude: "21.4006", longitude: "72.9269")
//            HomeAddGymService.shared.GymsNearYou(parameters: data.toJSON(), pageNumber: pageNumber) { [weak self] (statusCode, response) in
//                Utility.hideIndicator()
//                if let res = response.nearByGymResponse{
//                    print(res.toJSON())
//                    if self?.pageNumber == 1{
//                        self?.nearByGymArray = res
//                        for i in res {
//                            let long =  CLLocationDegrees.init(Double(((i.longtitude ?? "0") as NSString).doubleValue))
//                            let lat = CLLocationDegrees.init(Double(((i.latitude ?? "0") as NSString).doubleValue))
//                            let marer = CustomPin(pinTitle: "\(i.gymId ?? 0)", Location:CLLocationCoordinate2DMake(lat, long), storeImage: UIImage.init(named: "mapPinDeselect")!)
//                            self?.mapView.addAnnotation(marer)
//                        }
//                    }
//                }
//            } failure: { [weak self] (error) in
//                guard let stronSelf = self else { return }
//                Utility.hideIndicator()
//                Utility.showAlert(vc: stronSelf, message: error)
//            }
//
//        } else {
//            Utility.hideIndicator()
//            Utility.showNoInternetConnectionAlertDialog(vc: self)
//        }
    }
    
    //MARK:- Gym Category API
    func gymCategoryAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            HomeAddGymService.shared.gymCategory() { (statusCode, response) in
                Utility.hideIndicator()
                if let res = response.gymCategory{
                    self.gymCategory = res
                    self.filterCollectionView.reloadData()
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}

protocol  SelectCategory : NSObject {
    func selectCategoryList(selecteCategory:[Int])
}
