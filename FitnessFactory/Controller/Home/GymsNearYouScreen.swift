//
//  GymsNearYouScreen.swift
//  FitnessFactory
//
//  Created by iroid on 17/06/21.
//

import UIKit
import MapKit
import Messages
import MessageUI

class GymsNearYouScreen: UIViewController {
    
    @IBOutlet weak var unLinkButton: UIButton!
    @IBOutlet weak var linkButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var currentLocationButton: UIButton!
    
    @IBOutlet weak var gymNearTableView: UITableView!
    
    @IBOutlet weak var mapTitleLabel: UILabel!
    @IBOutlet weak var mapLineLabel: UILabel!
    @IBOutlet weak var gymListLabel: UILabel!
    @IBOutlet weak var gymListLineLabel: UILabel!
    @IBOutlet weak var gymNameLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var mailLabel: UILabel!
    @IBOutlet weak var websiteLabel: UILabel!
    
    @IBOutlet weak var gymProfileImage: UIImageView!
    
    @IBOutlet weak var gymCloseAndOpenImageView: UIImageView!
    @IBOutlet weak var mapView: MKMapView!
    
    //MARK:- UIView IBOutlet
    @IBOutlet weak var gymListView: UIView!
    @IBOutlet weak var gymdetailView: UIView!
    @IBOutlet weak var mapGymView: UIView!
  
    //MARK:- UITextField IBOutlet
    @IBOutlet weak var searchTextField: UITextField!
    
    
    let locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var meta:Meta!
    var hasMorePage : Bool = false
    var page = 1
    var gymId = Int()
    var nearByGymArray:[NearByGymResponse] = []
    var selectCategory = [Int]()
    var metaData:Meta!
    var searchText = ""
    var isFromProfile = Bool()
    var getLat:CLLocationDegrees = CLLocationDegrees()
    var getLong:CLLocationDegrees = CLLocationDegrees()
    var gymSelect = Bool()
    var gymDetail : GymResponse?
    var editData:((_ editDataRes:MyAccountResponse) -> Void)?
    var tap : UITapGestureRecognizer?
    var gymSelectStatus : Bool!
    var gymLinkId = Int()
   
    var isCall = false
    var email = String()
    var websiteURL = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initialDetail()
        searchTextField
    }
    
    //MARK:- initalizedDetails Details
    func initialDetail(){
        let nib = UINib(nibName: "GymlistTableViewCell", bundle: nil)
        gymNearTableView.register(nib, forCellReuseIdentifier: "GymlistTableViewCell")
        self.gymListView.isHidden = true
//        gymdetailView.layer.borderWidth = 3.5
//        gymdetailView.layer.borderColor = UIColor.green.cgColor
//        gymdetailView.roundCorners(corners: [.layerMinXMinYCorner, .layerMaxXMinYCorner], radius: 42)
//        tap = UITapGestureRecognizer(target: self, action: #selector(dismissView))
        setUpdata()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if !isCall{
            NotificationCenter.default.addObserver(self, selector: #selector(onAllowLocationPermission(_:)), name: UIApplication.didBecomeActiveNotification , object: nil)
        }
           
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    func setUpdata(){
        if gymDetail != nil {
            mapGymView.isHidden = true
            currentLocationButton.isHidden = true
            let long =  CLLocationDegrees.init(Double(((gymDetail?.longtitude ?? "0") as NSString).doubleValue))
            let lat = CLLocationDegrees.init(Double(((gymDetail?.latitude ?? "0") as NSString).doubleValue))
            let marer = CustomPin(pinTitle: "\(gymDetail?.gymId ?? 0)", Location: CLLocationCoordinate2DMake(lat, long), storeImage: UIImage.init(named: "mapPinSelect")!, phoneNumber: gymDetail?.owner_phone ?? "", name: gymDetail?.name ?? "", profileImage: gymDetail?.profile ?? "", gymId: gymDetail?.gymId ?? 0, website: gymDetail?.website  ?? "" , mail: gymDetail?.owner_email ?? "")
            self.mapView.addAnnotation(marer)
            let currentlong =  CLLocationDegrees.init(Double(((currentLongitude) as NSString).doubleValue))
            let currentlat = CLLocationDegrees.init(Double(((currentLatitude) as NSString).doubleValue))
            let currantPin = CustomPin(pinTitle: "\(-1)", Location:CLLocationCoordinate2DMake(currentlat, currentlong), storeImage: UIImage.init(named: "currant_location_pin")!, phoneNumber: "", name: "", profileImage: "", gymId: 0, website: "",mail: "")
            self.mapView.addAnnotation(currantPin)
            gymdetailView.isHidden = false
            gymNameLabel.text = gymDetail?.name
            phoneNumberLabel.text = gymDetail?.owner_phone
            websiteURL = gymDetail?.website ?? ""
            websiteLabel.text = gymDetail?.website
            mailLabel.text = gymDetail?.owner_email
            email = gymDetail?.owner_email ?? ""
            Utility.setImage(gymDetail?.profile, imageView: gymProfileImage)
            
            if gymDetail?.is_status == "Close"{
                gymCloseAndOpenImageView.image = UIImage(named: "close")
            }else{
                gymCloseAndOpenImageView.image = UIImage(named: "open")
            }
          
            
            if !gymSelectStatus {
                linkButton.isHidden = true
                unLinkButton.isHidden = true
                cancelButton.isHidden = false
            } else {
                linkButton.isHidden = true
                unLinkButton.isHidden = false
                cancelButton.isHidden = true
            }
            let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: lat, longitude: long), latitudinalMeters: 7000, longitudinalMeters: 7000)
            mapView.setRegion(region, animated: true)
          //  centerMapOnUserLocation()
        } else {
            mapView.isHidden = false
            currentLocationButton.isHidden = false
            setupMap()
            //self.getNearByGym(category: "", searchText: "")
        }
    }
    
    func setupMap(){
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
            self.mapView.showsUserLocation = false
        }
        
        if(CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == .authorizedAlways) {
            if currentLocation != nil{
                currentLocation = locationManager.location
            }
            centerMapOnUserLocation()
            mapView.showAnnotations(mapView.annotations, animated: true)
//            let long =  CLLocationDegrees.init(Double(((currentLongitude) as NSString).doubleValue))
//            let lat = CLLocationDegrees.init(Double(((currentLatitude) as NSString).doubleValue))
//            let currantPin = CustomPin(pinTitle: "\(-1)", Location:CLLocationCoordinate2DMake(lat, long), storeImage: UIImage.init(named: "currant_location_pin")!, phoneNumber: "", name: "", profileImage: "", gymId: 0)
//            self.mapView.addAnnotation(currantPin)
            getNearByGym(category: "", searchText: "", page: 1)
            
        } else {
            Utility.showLocationAlert(vc: self, message: "Allow to location permission for get near your gym")
        }
        
    }
    
    func removePin(){
        if let annotations = self.mapView?.annotations {
            for _annotation in annotations {
                if let annotation = _annotation as? MKAnnotation
                {
                    self.mapView.removeAnnotation(annotation)
                    print(annotation)
                }
            }
        }
    }
    
    // MARK: scroll method
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let size = scrollView.contentSize
        let inset = scrollView.contentInset
        let y = offset.y + bounds.size.height - inset.bottom
        let h = size.height
        let reload_distance:CGFloat = 10.0
        if y > (h + reload_distance) {
            print("called")
            if let metaTotal = self.meta?.total{
                if self.nearByGymArray.count != metaTotal{
                    if self.hasMorePage{
                        self.hasMorePage = false
                        self.page += 1
                        getNearByGym(category: selectCategory.map(String.init).joined(separator: ","), searchText: searchText, page: page)
                    }
                }
            }
        }
    }
    
    func appendDataCollectionView(data: [NearByGymResponse]){
        var indexPathArray: [IndexPath] = []
        for i in self.nearByGymArray.count..<self.nearByGymArray.count + data.count{
            indexPathArray.append(IndexPath(item: i, section: 0))
        }
        self.nearByGymArray.append(contentsOf: data)
        self.gymNearTableView.insertRows(at: indexPathArray, with: .automatic)
    }
    
    @objc func onAllowLocationPermission(_ notification: NSNotification){
      //  if !isCall {
            setupMap()
           // getNearByGym(category: "", searchText: "")
     //   }
        
    }
    
    @objc func dismissView() {
        view.removeGestureRecognizer(tap!)
        gymdetailView.isHidden = true
        removePin()
        for i in nearByGymArray {
            let long =  CLLocationDegrees.init(Double(((i.longtitude ?? "0") as NSString).doubleValue))
            let lat = CLLocationDegrees.init(Double(((i.latitude ?? "0") as NSString).doubleValue))
            let marer = CustomPin(pinTitle: "\(i.gymId ?? 0)", Location:CLLocationCoordinate2DMake(lat, long), storeImage: UIImage.init(named: "mapPinSelect")!, phoneNumber: i.owner_phone ?? "", name: i.name ?? "", profileImage: i.profile ?? "", gymId: i.gymId ?? 0, website: i.website ?? "" , mail: i.owner_email ?? "")
            self.mapView.addAnnotation(marer)
        }
        //self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func onBack(_ sender: UIButton) {
        if !isFromProfile {
            self.tabBarController?.tabBar.isHidden = false
        }
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func onCurrentLocation(_ sender: UIButton) {
        centerMapOnUserLocation()
    }
    
    @IBAction func onFilter(_ sender: Any) {
        let controller = STORYBOARD.home.instantiateViewController(withIdentifier:"FilterScreen") as! FilterScreen
        controller.delegate = self
        controller.selectCategory = selectCategory
        navigationController?.pushViewController(controller, animated: true)
    }
    @IBAction func onMap(_ sender: UIButton) {
        DispatchQueue.main.async() {
            self.mapTitleLabel.textColor = UIColor.homeTextStyle1
            self.mapLineLabel.backgroundColor = UIColor.homeButtonBackgroundStyle1
            
            self.gymListLineLabel.backgroundColor = UIColor.clear
            self.gymListLabel.textColor = UIColor.homeTextStyle2
            self.mapView.isHidden = false
            self.gymListView.isHidden = true
            self.currentLocationButton.isHidden = false
        }
    }
    @IBAction func onCall(_ sender: UIButton) {
        isCall = true
        NotificationCenter.default.removeObserver(self, name: UIApplication.didBecomeActiveNotification, object: nil)
        Utility.dialNumber(number: phoneNumberLabel.text ?? "")
    }
    
    @IBAction func onLink(_ sender: UIButton) {
        linkGyms(gymId: gymId )
    }
    
    @IBAction func onWebsite(_ sender: UIButton) {
        UIApplication.shared.openURL(URL(string:websiteURL)!)
    }
    
    @IBAction func onEmail(_ sender: UIButton) {
        guard MFMailComposeViewController.canSendMail() else {
                    return
                }
                let composer = MFMailComposeViewController()
                composer.mailComposeDelegate = self
                composer.setToRecipients([email]) // email id of the recipient
                composer.setSubject("")
                composer.setMessageBody("", isHTML: false)
                present(composer, animated: true, completion: nil)
    }
    
    @IBAction func onUnlinkGym(_ sender: UIButton) {
        let alertController = UIAlertController(title: "Are you sure?", message: "You want to unlink with gym?", preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) { [self]
            UIAlertAction in
            gymunLinkGyms(gymId: gymDetail?.gymId ?? 0)
            //self.getNearByGym(category: "", searchText: "")
            NSLog("OK Pressed")
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default) { [self]
            UIAlertAction in
            //self.getNearByGym(category: "", searchText: "")
            NSLog("OK Pressed")
        }
        // Add the actions
        okAction.setValue(UIColor.red, forKey: "titleTextColor")
        cancelAction.setValue(UIColor.gray, forKey: "titleTextColor")
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func onCancelGymRequest(_ sender: UIButton) {
        let alertController = UIAlertController(title: "Are you sure?", message: "You want to cancel gym request?", preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) { [self]
            UIAlertAction in
            cancelGymLinkAPI(Id: gymLinkId)
            //self.getNearByGym(category: "", searchText: "")
            NSLog("OK Pressed")
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default) { [self]
            UIAlertAction in
            //self.getNearByGym(category: "", searchText: "")
            NSLog("OK Pressed")
        }
        // Add the actions
        okAction.setValue(UIColor.red, forKey: "titleTextColor")
        cancelAction.setValue(UIColor.gray, forKey: "titleTextColor")
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    @IBAction func onHome(_ sender: UIButton) {
        tabBarController?.tabBar.isHidden = false
        tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func onGymlist(_ sender: UIButton) {
        DispatchQueue.main.async() { [self] in
            mapTitleLabel.textColor = UIColor.homeTextStyle2
            mapLineLabel.backgroundColor = UIColor.clear
            
            gymListLineLabel.backgroundColor = UIColor.homeButtonBackgroundStyle1
            gymListLabel.textColor = UIColor.homeTextStyle1
            mapView.isHidden = true
            gymListView.isHidden = false
            gymdetailView.isHidden = true
            currentLocationButton.isHidden = true
        }
    }
}
extension GymsNearYouScreen: CLLocationManagerDelegate, MKMapViewDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //Get Current Location
//        let userLocation:CLLocation = locations[0] as CLLocation
//        let myAnnotation: MKPointAnnotation = MKPointAnnotation()
//        myAnnotation.coordinate = CLLocationCoordinate2DMake(userLocation.coordinate.latitude, userLocation.coordinate.longitude)
//        myAnnotation.title = "Current location"
//        self.mapView.addAnnotation(myAnnotation)
    }
    
    // MARK: Center Map On UserLocation
    func centerMapOnUserLocation() {
        guard let coordinate = locationManager.location?.coordinate else { return }
        getLat = locationManager.location?.coordinate.latitude ?? 0.0
        getLong = locationManager.location?.coordinate.longitude ?? 0.0
        let region = MKCoordinateRegion(center: coordinate, latitudinalMeters: 7000, longitudinalMeters: 7000)
        mapView.setRegion(region, animated: true)
    }
    
   
//    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
//        guard !(annotation is MKUserLocation) else {
//            let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "userLocation")
//            annotationView.image = UIImage(named:"currant_location_pin")
//            return annotationView
//        }
//        return nil
//    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var annotationView: MKAnnotationView?
        if annotation is MKUserLocation || annotation.title == "Current Location"  {
            mapView.removeAnnotation(annotation)

            let reuseID = "Locations"
            annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseID)
            if annotationView == nil {
                annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseID)
                annotationView?.canShowCallout = true
                let pin = MKAnnotationView(annotation: annotation,reuseIdentifier: reuseID)
                let pinImage = UIImage.init(named: "currant_location_pin")
                let size = CGSize(width: 0, height: 0)
                UIGraphicsBeginImageContext(size)
                pinImage!.draw(in: CGRect(x: 0, y: 0, width: 90, height: 90))
                let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
                pin.image = resizedImage
                pin.isEnabled = true
                pin.canShowCallout = false
                annotationView = pin
            }
        } else if let annotation = annotation as? CustomPin {
            let reuseID = "Location"
             annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseID)
            if annotationView == nil {
                annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseID)
                annotationView?.canShowCallout = true
                let pin = MKAnnotationView(annotation: annotation,reuseIdentifier: reuseID)
                let pinImage = annotation.storeImage
                var size = CGSize()
                if annotation.title == "-1"{
                     size = CGSize(width: 80, height: 80)
                } else{
                     size = CGSize(width: 40, height: 40)
                }
                UIGraphicsBeginImageContext(size)
                pinImage.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
                let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
                pin.image = resizedImage
                pin.isEnabled = true
                pin.canShowCallout = false
                annotationView = pin
            } else {
                let pin = MKAnnotationView(annotation: annotation,reuseIdentifier: reuseID)
                let pinImage = annotation.storeImage
                let size = CGSize(width: 40, height: 40)
                UIGraphicsBeginImageContext(size)
                pinImage.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
                let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
                pin.image = resizedImage
                pin.isEnabled = true
                pin.canShowCallout = false
                annotationView = pin
            }
        }
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if gymDetail == nil && view.annotation?.title != "-1" {
            if view.annotation is MKUserLocation {
                return
            }
            view.image  = UIImage(named: "mapPinSelect")
            let pin = view.annotation as! CustomPin
            gymNameLabel.text = pin.name
            phoneNumberLabel.text = pin.phoneNumber
            Utility.setImage(pin.profileImage, imageView: gymProfileImage)
            gymId = pin.gymId ?? 0
            websiteLabel.text = pin.website
            websiteURL = pin.website ?? ""
            email = pin.mail ?? ""
            mailLabel.text = pin.mail
            gymdetailView.isHidden = false
            linkButton.isHidden = false
            unLinkButton.isHidden = true
            cancelButton.isHidden = true
            //view.addGestureRecognizer(tap!)
        }
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        if view.annotation is MKUserLocation {
            return
        }
        if view.annotation?.title != "-1"{
            view.image  = UIImage(named: "mapPinDeselect")
        }
        
    }
}
extension GymsNearYouScreen: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSections: Int = 0
            if nearByGymArray.count > 0 {
                numOfSections = 1
                tableView.backgroundView = nil
            } else {
                let noDataImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
                noDataImageView.image = UIImage(named: "no_result")
                noDataImageView.contentMode = .scaleAspectFit
                tableView.backgroundView = noDataImageView
            }
            
            return numOfSections
            
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nearByGymArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GymlistTableViewCell", for: indexPath) as! GymlistTableViewCell
        cell.item = nearByGymArray[indexPath.row]
        cell.onCall = {[weak self] in
            self?.isCall = true
            NotificationCenter.default.removeObserver(self, name: UIApplication.didBecomeActiveNotification, object: nil)
            Utility.dialNumber(number: self?.nearByGymArray[indexPath.row].owner_phone ?? "")
        }
        cell.onMail = {[weak self] in
            self?.isCall = true
            NotificationCenter.default.removeObserver(self, name: UIApplication.didBecomeActiveNotification, object: nil)
            self?.email = self?.nearByGymArray[indexPath.row].owner_email ?? ""
            guard MFMailComposeViewController.canSendMail() else {
                let alertController = UIAlertController(title: "Cannot send email", message: "Please set up mail account to send mail", preferredStyle: .alert)
                
                // Create the actions
                let okAction = UIAlertAction(title: "Settings", style: UIAlertAction.Style.default) { [self]
                    UIAlertAction in
                   // UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
                    UIApplication.shared.open(URL(string: "App-prefs:General")!)


                    //self.getNearByGym(category: "", searchText: "")
                    NSLog("OK Pressed")
                }
                
                let cancelAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) { [self]
                    UIAlertAction in
                    //self.getNearByGym(category: "", searchText: "")
                    NSLog("OK Pressed")
                }
                // Add the actions
         
                alertController.addAction(cancelAction)
                alertController.addAction(okAction)
                // Present the controller
                self?.present(alertController, animated: true, completion: nil)
                return
            }
            let composer = MFMailComposeViewController()
            composer.mailComposeDelegate = self
            composer.setToRecipients([self!.email]) // email id of the recipient
            composer.setSubject("")
            composer.setMessageBody("", isHTML: false)
            self?.present(composer, animated: true, completion: nil)
//                        self?.isCall = true
//                        NotificationCenter.default.removeObserver(self, name: UIApplication.didBecomeActiveNotification, object: nil)
//            let email: String = self?.nearByGymArray[indexPath.row].owner_email ?? ""
//                    if MFMailComposeViewController.canSendMail() {
//                        let mail = MFMailComposeViewController()
//                        mail.mailComposeDelegate = self
//                        mail.setToRecipients([email])
//                        mail.setMessageBody("<p>You're so awesome!</p>", isHTML: true)
//
//                        self?.present(mail, animated: true)
//                    } else {
//                        print("error")
//                        //self.view.makeToast("Your device doesn't support this feature.")
//                    }
        }
        cell.onWebsite = {[weak self] in
            self?.isCall = true
            NotificationCenter.default.removeObserver(self, name: UIApplication.didBecomeActiveNotification, object: nil)
            self?.websiteURL = self?.nearByGymArray[indexPath.row].website ?? ""
            UIApplication.shared.openURL(URL(string:self?.websiteURL ?? "")!)
        }
        cell.onLink = {[weak self] in
            self?.gymId = self?.nearByGymArray[indexPath.row].gymId ?? 0
            self?.linkGyms(gymId: self?.gymId ?? 0)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
extension GymsNearYouScreen{
    
    func getNearByGym(category:String,searchText:String,page:Int){
       // self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            //Utility.showIndecator()
            //let data = HomeAddGymRequest(latitude: getLat, longitude: getLong, category: category, search: searchText)
            let data = HomeAddGymRequest(latitude:String(getLat), longitude: String(getLong), category: category, search: searchText)
          //  let data = HomeAddGymRequest(latitude:"43.65587360", longitude: "-79.43548480", category: category, search: searchText)
            HomeAddGymService.shared.GymsNearYou(parameters: data.toJSON(), pageNumber: page) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response.nearByGymResponse{
                    print(res.toJSON())
                        if self?.page == 1{
                            self?.nearByGymArray = res
                            self?.gymNearTableView.isHidden = false
                            self?.gymNearTableView.reloadData()
                            self?.gymdetailView.isHidden = true
                            self?.mapGymView.isHidden = false
                            self?.currentLocationButton.isHidden = false
                            self?.removePin()
                            for i in res {
                                let long =  CLLocationDegrees.init(Double(((i.longtitude ?? "0") as NSString).doubleValue))
                                let lat = CLLocationDegrees.init(Double(((i.latitude ?? "0") as NSString).doubleValue))
                                let marer = CustomPin(pinTitle: "\(i.gymId ?? 0)", Location:CLLocationCoordinate2DMake(lat, long), storeImage: UIImage.init(named: "mapPinDeselect")!, phoneNumber: i.owner_phone ?? "", name: i.name ?? "", profileImage: i.profile ?? "", gymId: i.gymId ?? 0,website: i.website ?? "",mail: i.owner_email ?? "")
                                self?.mapView.addAnnotation(marer)
                            }
                            let long =  CLLocationDegrees.init(Double(((currentLongitude) as NSString).doubleValue))
                            let lat = CLLocationDegrees.init(Double(((currentLatitude) as NSString).doubleValue))
                            let currantPin = CustomPin(pinTitle: "\(-1)", Location:CLLocationCoordinate2DMake(lat, long), storeImage: UIImage.init(named: "currant_location_pin")!, phoneNumber: "", name: "", profileImage: "", gymId: 0,website: "",mail: "")
                            self?.mapView.addAnnotation(currantPin)
                            let region = MKCoordinateRegion(center: CLLocationCoordinate2DMake(lat, long), latitudinalMeters: 7000, longitudinalMeters: 7000)
                            self?.mapView.setRegion(region, animated: true)
                        }
                        else {
                            self?.appendDataCollectionView(data: res)
                        }
                        if let meta = response.metaResponse{
                            self?.meta = meta
                        }
                    //                    if self?.pageNumber == 1{
                    //                        self?.nearByGymArray = res
                    //                        self?.gymNearTableView.reloadData()
                    //                        for i in res {
                    //                            let long =  CLLocationDegrees.init(Double(((i.longtitude ?? "0") as NSString).doubleValue))
                    //                            let lat = CLLocationDegrees.init(Double(((i.latitude ?? "0") as NSString).doubleValue))
                    //                            let marer = CustomPin(pinTitle: "\(i.gymId ?? 0)", Location:CLLocationCoordinate2DMake(lat, long), storeImage: UIImage.init(named: "mapPinDeselect")!)
                    //                            self?.mapView.addAnnotation(marer)
                    //                        }
                    //                    }
                   // else {
                        //self?.gymNearTableView.reloadData()
                        // self?.gymNearTableView.isHidden = true
                   // }
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
//                if error == "The latitude field is required." {
//                    Utility.showLocationAlert(vc: self!, message: "Allow to location permission for get near your gym")
//                }
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    func linkGyms(gymId:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            HomeAddGymService.shared.linkGyms(gymID: gymId) { (statusCode, response) in
                Utility.hideIndicator()
//                if let res = response.gymLinkStatusResponse{
//                    let controller = STORYBOARD.tabBar.instantiateViewController(withIdentifier:"TabBarScreen") as! TabBarScreen
//                    controller.isFromLink = false
//                    controller.gymDetail = res
//                    self.navigationController?.pushViewController(controller, animated: true)
//                    let controller = STORYBOARD.home.instantiateViewController(withIdentifier:"LinkYourGymScreen") as! LinkYourGymScreen
//                    self.navigationController?.pushViewController(controller, animated: true)
//                    
//                }
                if response.gymLinkStatusResponse == nil {
                    let controller = STORYBOARD.home.instantiateViewController(withIdentifier:"LinkYourGymScreen") as! LinkYourGymScreen
                    self.navigationController?.pushViewController(controller, animated: true)
                } else {
                    if response.isLinkRequest == true {
                        let controller = STORYBOARD.tabBar.instantiateViewController(withIdentifier:"TabBarScreen") as! TabBarScreen
                        let navVC = UINavigationController(rootViewController: controller)

                        controller.isFromLink = true
                        controller.gymDetail = response.gymLinkStatusResponse
//                            self.navigationController?.pushViewController(controller, animated: true)
                        navVC.interactivePopGestureRecognizer?.isEnabled = true
                        navVC.navigationBar.isHidden = true
                        appDelegate.window?.rootViewController = navVC
                        appDelegate.window?.makeKeyAndVisible()
                    } else {
                        let controller = STORYBOARD.tabBar.instantiateViewController(withIdentifier:"TabBarScreen") as! TabBarScreen
                        let navVC = UINavigationController(rootViewController: controller)
                        controller.isFromLink = false
                        controller.gymDetail = response.gymLinkStatusResponse
//                            self.navigationController?.pushViewController(controller, animated: true)
                        navVC.interactivePopGestureRecognizer?.isEnabled = true
                        navVC.navigationBar.isHidden = true
                        appDelegate.window?.rootViewController = navVC
                        appDelegate.window?.makeKeyAndVisible()
                    }
                }
//                Utility.showAlert(vc: self, message: response.message ?? "")
//                let alertController = UIAlertController(title: APPLICATION_NAME, message:"Gym Linked Successfully...", preferredStyle: .alert)
//                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
//                    UIAlertAction in
////                    let controller = STORYBOARD.home.instantiateViewController(withIdentifier:"BreakfastScreen") as! BreakfastScreen
////                    controller.isFromLink = true
////                    controller.gymId = gymId
////                    self.navigationController?.pushViewController(controller, animated: true)
//
//                    NSLog("OK Pressed")
//                }
//
//                alertController.addAction(okAction)
//                self.present(alertController, animated: true, completion: nil)
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func gymunLinkGyms(gymId:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            HomeAddGymService.shared.unLinkGym(ID: gymId) { (statusCode, response) in
                Utility.hideIndicator()
                if let res = response.myAccountResponse{
                    self.gymDetail = nil
                    self.editData?(res)
                    self.setupMap()
//                    let alertController = UIAlertController(title: APPLICATION_NAME, message: "Gym unlink successfully...", preferredStyle: .alert)
//
//                    // Create the actions
//                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { [self]
//                        UIAlertAction in
//
//                        //self.getNearByGym(category: "", searchText: "")
//                        NSLog("OK Pressed")
//                    }
//                    // Add the actions
//                    alertController.addAction(okAction)
//
//                    // Present the controller
//                    self.present(alertController, animated: true, completion: nil)
                }
                

            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func cancelGymLinkAPI(Id:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            HomeAddGymService.shared.cancelGymLink(ID : Id) { (statusCode, response) in
                Utility.hideIndicator()
                if let res = response.myAccountResponse {
                    self.gymDetail = nil
                    self.editData?(res)
                    self.setupMap()
//                    let alertController = UIAlertController(title: APPLICATION_NAME, message: "delete successfully...", preferredStyle: .alert)
//                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { [self]
//                        UIAlertAction in
//                        self.editData?(res)
//                        setupMap()
//                        //self.getNearByGym(category: "", searchText: "")
//                        NSLog("OK Pressed")
//                    }
//                    alertController.addAction(okAction)
//                    self.present(alertController, animated: true, completion: nil)
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}

extension GymsNearYouScreen : SelectCategory {
    func selectCategoryList(selecteCategory: [Int]) {
        selectCategory = selecteCategory
        self.getNearByGym(category: selectCategory.map(String.init).joined(separator: ","), searchText: "", page: 1)
    }
}

////MARK: UIScrollViewDelegate
//extension GymsNearYouScreen: UIScrollViewDelegate{
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        if(scrollView == self.gymNearTableView){
//            if(self.nearByGymArray.count > 0 && self.metaData != nil){
//                if(self.metaData.currentPage! < self.metaData.lastPage!){
//                    getNearByGym(category: selectCategory.map(String.init).joined(separator: ","), searchText: searchTextField.text ?? "", page: 1)
//                }
//            }
//        }
//    }
//}


extension GymsNearYouScreen:UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == searchTextField{
            if let text = textField.text as NSString? {
                let txtAfterUpdate = text.replacingCharacters(in: range, with: string)
                searchText = txtAfterUpdate
                print(txtAfterUpdate)
                NSObject.cancelPreviousPerformRequests(withTarget: self,selector: #selector(searchGymList),object: textField)
                self.perform(#selector(searchGymList),with: textField,afterDelay: 0.2)
            }
        }
        return true
    }
    
    @objc func searchGymList(){
        self.metaData = nil
        self.getNearByGym(category: selectCategory.map(String.init).joined(separator: ","), searchText: searchText, page: 1)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
    }
}

//extension GymsNearYouScreen : MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate
//{
//    func messageComposeViewController(_ controller: MFMessageComposeViewController,didFinishWith result: MessageComposeResult) {
//            switch (result) {
//                case .cancelled:
//                    print("Message was cancelled")
//                    dismiss(animated: true, completion: nil)
//                case .failed:
//                    print("Message failed")
//                    dismiss(animated: true, completion: nil)
//                case .sent:
//                    print("Message was sent")
//                    dismiss(animated: true, completion: nil)
//                default:
//                    break
//            }
//        }
//
//    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
//        controller.dismiss(animated: true)
//    }
//}

extension GymsNearYouScreen: MFMailComposeViewControllerDelegate{
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}
