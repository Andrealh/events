//
//  BreakfastScreen.swift
//  FitnessFactory
//
//  Created by iroid on 18/06/21.
//

import UIKit
import SwipeMenuViewController
import SkeletonView

class BreakfastScreen: UIViewController {
    
    
    @IBOutlet weak var gymOpenImageView: UIImageView!
    @IBOutlet weak var excercisesTitleLabel: UILabel!
    @IBOutlet weak var excercisesLineLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var gymNameLabel: UILabel!
    @IBOutlet weak var joinDateLabel: UILabel!
    @IBOutlet weak var nutritionListLabel: UILabel!
    @IBOutlet weak var nutritionListLineLabel: UILabel!
    @IBOutlet weak var eventsListLabel: UILabel!
    @IBOutlet weak var eventsListLineLabel: UILabel!
    
    @IBOutlet weak var gymListView: UIView!
    @IBOutlet weak var linkGymView: UIView!
    @IBOutlet weak var waitingGymApprovalView: UIView!
    @IBOutlet weak var linkYourGymView: UIView!
    @IBOutlet weak var swipeMenuView: SwipeMenuView!
    
    @IBOutlet weak var gymProfileImageView: UIImageView!
    
    var vc1 : ExerciseListScreen?
    var vc2 : NutritionListScreen?
    var vc3 : EventListScreen?
    var selectedBreakFast = 1
    var isFromLink : Bool = false
    var isFromSkip : Bool = false
    var gymDetail : GymLinkStatusResponse?
    var gymId = Int()
    var index = Int()
    var itemArray = [String]()
    var viewControllerArray: [UIViewController] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialDetail()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        gymLinkStatusAPI()
        changeValueContoller()
        //setUpWorkout()
       // setUpSwipeMenu()
        //swipeMenuView.currentIndex =
    }
    
    func initialDetail(){
        setUpSwipeMenu()
    }
    
    func setUpData()  {
        if isFromLink {
            waitingGymApprovalView.isHidden = true
            linkGymView.isHidden = false
            linkYourGymView.isHidden = true
            gymNameLabel.text = gymDetail?.name
            //joinDateLabel.text = "Member scince " + "\(gymDetail?.member_since ?? "")"
            let timeStamp = gymDetail?.member_since ?? 0
            let date = Date(timeIntervalSince1970: TimeInterval(timeStamp))
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "d MMM, YYYY"
            dateFormatter.timeZone = TimeZone(identifier: "UTC")
            //        let unixTimeStamp: Double = Double(timeStamp) / 1000.0
            //        let exactDate = NSDate.init(timeIntervalSince1970: unixTimeStamp)
            //        let dateFormatt = DateFormatter();
            
            //print(dateFormatt.string(from: exactDate as Date))
            let tier = gymDetail?.tier ?? ""
            joinDateLabel.text = "\(tier) Member since \(dateFormatter.string(from: date as Date))"
            Utility.setImage(gymDetail?.profile, imageView: gymProfileImageView)
            if gymDetail?.is_status == "Close"{
                gymOpenImageView.image = UIImage(named: "close")
            }else{
                gymOpenImageView.image = UIImage(named: "open")
            }
            //setUpSwipeMenu()
        } else if isFromSkip {
            linkYourGymView.isHidden = false
            waitingGymApprovalView.isHidden = true
            linkGymView.isHidden = true
        } else {
            waitingGymApprovalView.isHidden = false
            linkGymView.isHidden = true
            linkYourGymView.isHidden = true
        }
        //setUpSwipeMenu()
    }
    
    func setUpWorkout(){
        self.excercisesTitleLabel.textColor = UIColor.homeTextStyle1
        self.excercisesLineLabel.backgroundColor = UIColor.homeButtonBackgroundStyle1
        self.nutritionListLineLabel.backgroundColor = UIColor.clear
        self.nutritionListLabel.textColor = UIColor.homeTextStyle2
        self.eventsListLineLabel.backgroundColor = UIColor.clear
        self.eventsListLabel.textColor = UIColor.homeTextStyle2
        self.selectedBreakFast = 1
    }
    
    //MARK:- setUpSwipeMenu
    func setUpSwipeMenu(){
        vc1 = STORYBOARD.home.instantiateViewController(withIdentifier: "ExerciseListScreen") as? ExerciseListScreen
        vc1?.vc = self
        vc1?.isFromHome = true
        vc2 = STORYBOARD.home.instantiateViewController(withIdentifier: "NutritionListScreen") as? NutritionListScreen
        vc2?.vc = self
        vc2?.isFromHome = true
        vc3 = STORYBOARD.home.instantiateViewController(withIdentifier: "EventListScreen") as? EventListScreen
        vc3?.isFromHome = true
        //            vc3.isRideHistory = false
        swipeMenuView.dataSource = self
        swipeMenuView.delegate = self
        itemArray = ["","",""]
        viewControllerArray = [vc1!,vc2!,vc3!]
        var options: SwipeMenuViewOptions = .init()
        options.tabView.height = 70
        options.tabView.style = .flexible
        // options.tabView.backgroundColor = #colorLiteral(red: 0.9764705882, green: 0.9764705882, blue: 0.9843137255, alpha: 1)
        options.tabView.itemView.selectedTextColor = #colorLiteral(red: 1, green: 0.2666666667, blue: 0.3764705882, alpha: 1)
        options.tabView.itemView.margin = 60
        options.tabView.itemView.width = self.view.frame.width / 3
        //                options.tabView.margin = 20
        options.tabView.addition = .none
        options.tabView.itemView.textColor   =  #colorLiteral(red: 1, green: 0.2666666667, blue: 0.3764705882, alpha: 0.6)
        //                options.tabView.itemView.font = UIFont(name: "HelveticaNeue-Bold", size: 14)!
        options.tabView.needsAdjustItemViewWidth = false
        swipeMenuView.reloadData(options: options)
        swipeMenuView.jump(to: index, animated: true)
    }
    
  
    @IBAction func onProfile(_ sender: UIButton) {
//        self.tabBarController?.tabBar.isHidden = true
//        let controller = STORYBOARD.profile.instantiateViewController(withIdentifier:"ProfileScreen") as! ProfileScreen
//        controller.gymLinkStatus = gymDetail
//        self.navigationController?.pushViewController(controller, animated: true)
        tabBarController?.tabBar.isHidden = true
        let storyBoard: UIStoryboard = UIStoryboard(name: "Profile", bundle: nil)
         let vc = storyBoard.instantiateViewController(withIdentifier: "ProfileScreen") as! ProfileScreen
        vc.gymLinkStatus = gymDetail
         let navigationController = self.navigationController


         vc.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Close", style: .plain, target: vc, action: #selector(vc.closeView))
         vc.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: vc, action: nil)
        
         let transition = CATransition()
         transition.duration = 0.2
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.moveIn
        transition.subtype = CATransitionSubtype.fromLeft
         navigationController?.view.layer.add(transition, forKey: nil)
         navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func onFindLink(_ sender: UIButton) {
        self.tabBarController?.tabBar.isHidden = true
        let controller = STORYBOARD.home.instantiateViewController(withIdentifier:"GymsNearYouScreen") as! GymsNearYouScreen
        controller.isFromProfile = false
        navigationController?.pushViewController(controller, animated: true)
    }
    
    
    @IBAction func onCancel(_ sender: UIButton) {
        let alertController = UIAlertController(title: "Are you sure?", message: "You want to cancel gym request?", preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) { [self]
            UIAlertAction in
            cancelGymLinkAPI(Id: gymDetail?.link_gym_id ?? 0)
            //self.getNearByGym(category: "", searchText: "")
            NSLog("OK Pressed")
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default) { [self]
            UIAlertAction in
            //self.getNearByGym(category: "", searchText: "")
            NSLog("OK Pressed")
        }
        // Add the actions
        okAction.setValue(UIColor.red, forKey: "titleTextColor")
        cancelAction.setValue(UIColor.gray, forKey: "titleTextColor")
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func changeValueContoller(){
      //  if self.selectedBreakFast == 1{
            vc1?.initialDetail()
       // }else if self.selectedBreakFast == 2{
            vc2?.initialDetail()
       // }else if self.selectedBreakFast == 3{
            vc3?.initialDetail()
       // }
    }
}


extension BreakfastScreen {
    func cancelGymLinkAPI(Id:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            HomeAddGymService.shared.cancelGymLink(ID : Id) { (statusCode, response) in
                Utility.hideIndicator()
                if let res = response.myAccountResponse {
                    let alertController = UIAlertController(title: APPLICATION_NAME, message: "Cancel gym request successfully!!", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                        UIAlertAction in
                        self.waitingGymApprovalView.isHidden = true
                        self.linkYourGymView.isHidden = false
                        NSLog("OK Pressed")
                    }
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
                    
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}

extension  BreakfastScreen {
    //MARK:- Gym Category API
    func gymLinkStatusAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            // Utility.showIndecator()
            HomeAddGymService.shared.gymLinkStatus() { [self] (statusCode, response) in
                Utility.hideIndicator()
                if response.gymLinkStatusResponse == nil{
                    isFromSkip = true
                    isFromLink = false
                    
                } else if response.isLinkRequest == true {
                    isFromLink = true
                    gymDetail = response.gymLinkStatusResponse
                } else {
                    isFromLink = false
                }
                setUpData()
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}


//MARK:- SwipeMenuViewDelegate
extension BreakfastScreen : SwipeMenuViewDelegate,SwipeMenuViewDataSource{
    func swipeMenuView(_ swipeMenuView: SwipeMenuView, viewWillSetupAt currentIndex: Int) {
        print("viewWillSetupAt",currentIndex)
    }
    
    func swipeMenuView(_ swipeMenuView: SwipeMenuView, viewDidSetupAt currentIndex: Int) {
        print("viewDidSetupAt",currentIndex)
    }
    
    
    
    func swipeMenuView(_ swipeMenuView: SwipeMenuView, willChangeIndexFrom fromIndex: Int, to toIndex: Int) {
        print(fromIndex)
        if toIndex == 0{
            DispatchQueue.main.async() {
                if self.selectedBreakFast != 1{
                    self.excercisesTitleLabel.textColor = UIColor.homeTextStyle1
                    self.excercisesLineLabel.backgroundColor = UIColor.homeButtonBackgroundStyle1
                    self.nutritionListLineLabel.backgroundColor = UIColor.clear
                    self.nutritionListLabel.textColor = UIColor.homeTextStyle2
                    self.eventsListLineLabel.backgroundColor = UIColor.clear
                    self.eventsListLabel.textColor = UIColor.homeTextStyle2
                    self.selectedBreakFast = 1
                }
            }
        }
        else if toIndex == 1{
            DispatchQueue.main.async() {
                if self.selectedBreakFast != 2{
                    self.excercisesTitleLabel.textColor = UIColor.homeTextStyle2
                    self.excercisesLineLabel.backgroundColor = UIColor.clear
                    
                    self.nutritionListLineLabel.backgroundColor = UIColor.homeButtonBackgroundStyle1
                    self.nutritionListLabel.textColor = UIColor.homeTextStyle1
                    
                    self.eventsListLabel.textColor = UIColor.homeTextStyle2
                    self.eventsListLineLabel.backgroundColor = UIColor.clear
                    self.selectedBreakFast = 2
                }
            }
        } else {
            DispatchQueue.main.async() {
                if self.selectedBreakFast != 3{
                    self.excercisesTitleLabel.textColor = UIColor.homeTextStyle2
                    self.excercisesLineLabel.backgroundColor = UIColor.clear
                    self.nutritionListLineLabel.backgroundColor = UIColor.clear
                    self.nutritionListLabel.textColor = UIColor.homeTextStyle2
                    self.eventsListLineLabel.backgroundColor = UIColor.homeButtonBackgroundStyle1
                    self.eventsListLabel.textColor = UIColor.homeTextStyle1
                    self.selectedBreakFast = 3
                }
            }
        }
        print(toIndex)
    }
    
    
    func numberOfPages(in swipeMenuView: SwipeMenuView) -> Int {
        return itemArray.count
    }
    
    func swipeMenuView(_ swipeMenuView: SwipeMenuView, titleForPageAt index: Int) -> String {
        return itemArray[index]
    }
    
    func swipeMenuView(_ swipeMenuView: SwipeMenuView, viewControllerForPageAt index: Int) -> UIViewController {
        return viewControllerArray[index]
    }
}
