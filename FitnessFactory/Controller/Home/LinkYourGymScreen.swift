//
//  LinkYourGymScreen.swift
//  FitnessFactory
//
//  Created by iroid on 22/06/21.
//

import UIKit

class LinkYourGymScreen: UIViewController {
  
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func onFindAndLink(_ sender: UIButton) {
        let controller = STORYBOARD.home.instantiateViewController(withIdentifier:"GymsNearYouScreen") as! GymsNearYouScreen
        navigationController?.pushViewController(controller, animated: true)
    }
    
    
    
    @IBAction func onSkip(_ sender: UIButton) {
//        let controller = STORYBOARD.home.instantiateViewController(withIdentifier:"BreakfastScreen") as! BreakfastScreen
//        controller.isFromSkip = true
//        navigationController?.pushViewController(controller, animated: true)
        let controller = STORYBOARD.tabBar.instantiateViewController(withIdentifier:"TabBarScreen") as! TabBarScreen
        controller.isFromSkip = true
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
}
