//
//  NutritionListScreen.swift
//  FitnessFactory
//
//  Created by iroid on 10/08/21.
//

import UIKit
import SkeletonView

class NutritionListScreen: UIViewController {
    
    @IBOutlet weak var nutritionListTableView: UITableView!
    
    //MARK:- Variables
    var meta:Meta!
    var hasMorePage : Bool = false
    var page = 1
    var nutritionList : [PickNutrtionResponse] = []
    var nutritionMealList  :[MealListResponse] = []
    var selectedBreakFast = 1
    var vc = UIViewController()
    var refreshControl = UIRefreshControl()
    var isFromHome = Bool()
    var selectDate = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if isFromHome {
            nutritionListTableView.rowHeight = 80
            nutritionListTableView.estimatedRowHeight = 80
            nutritionListTableView.isSkeletonable = true
            let gradient = SkeletonGradient(baseColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.1310794454))
            nutritionListTableView.showAnimatedGradientSkeleton(usingGradient: gradient, animation: nil, transition: .crossDissolve(0.5))
        }
        initialDetail()
        // Do any additional setup after loading the view.
    }
    
    func initialDetail(){
        // self.tabBarController?.tabBar.isHidden = false
        
        let nutritionNib = UINib(nibName: "ExcercisesCell", bundle: nil)
        nutritionListTableView.register(nutritionNib, forCellReuseIdentifier: "ExcercisesCell")
        let nutritionNib2 = UINib(nibName: "RemoveWorkoutCell", bundle: nil)
        nutritionListTableView.register(nutritionNib2, forCellReuseIdentifier: "RemoveWorkoutCell")
        
        //eventListAPI(page: 1)
        if isFromHome {
            nutritionListAPI(page: 1)
        } else {
            nutritionListAPI(page: 1, selecteDate: selectDate)
        }
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(refreshPostList), for: UIControl.Event.valueChanged)
       // nutritionListTableView.addSubview(refreshControl)
        //setUpData()
    }
    
    
    // MARK: scroll method
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let size = scrollView.contentSize
        let inset = scrollView.contentInset
        let y = offset.y + bounds.size.height - inset.bottom
        let h = size.height
        let reload_distance:CGFloat = 10.0
        if y > (h + reload_distance) {
            print("called")
            if let metaTotal = self.meta?.total{
                
                if isFromHome {
                    if self.nutritionList.count != metaTotal{
                        if self.hasMorePage{
                            self.hasMorePage = false
                            self.page += 1
                            nutritionListAPI(page: page)
                        }
                    }
                } else {
                    if self.nutritionMealList.count != metaTotal{
                        if self.hasMorePage{
                            self.hasMorePage = false
                            self.page += 1
                            nutritionListAPI(page: page, selecteDate: selectDate)
                        }
                    }
                    
                }
            }
        }
    }
    
    func appendDataCollectionView(data: Response){
        var indexPathArray: [IndexPath] = []
        let nutritionRes = data.yourMealListResponse!
        for i in self.nutritionMealList.count..<self.nutritionMealList.count + nutritionRes.count{
            indexPathArray.append(IndexPath(item: i, section: 0))
        }
        self.nutritionMealList.append(contentsOf: nutritionRes)
        self.nutritionListTableView.insertRows(at: indexPathArray, with: .automatic)
    }
    
    func appendDataCollectionView2(data: Response){
        var indexPathArray: [IndexPath] = []
        let nutritionRes = data.pickNutritionResponse!
        for i in self.nutritionList.count..<self.nutritionList.count + nutritionRes.count{
            indexPathArray.append(IndexPath(item: i, section: 0))
        }
        self.nutritionList.append(contentsOf: nutritionRes)
        self.nutritionListTableView.insertRows(at: indexPathArray, with: .automatic)
    }
    
    @objc func refreshPostList() {
        refreshControl.endRefreshing()
        if isFromHome {
            nutritionListAPI(page: 1)
        } else {
            nutritionListAPI(page: 1, selecteDate: selectDate)
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}


extension NutritionListScreen: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var numOfSections : Int = 0
        if isFromHome {
            return nutritionList.count
        } else {
            if nutritionMealList.count > 0 {
                numOfSections = nutritionMealList.count
                tableView.backgroundView = nil
            } else {
                let noDataLabelView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
                let noDataLabel = UILabel(frame: CGRect(x: 0, y: 50, width: tableView.bounds.size.width, height: 50))
                noDataLabel.text = "No Meals."
                noDataLabel.textColor = UIColor.darkGray
                noDataLabel.textAlignment = .center
                noDataLabel.numberOfLines = 1
                noDataLabelView.addSubview(noDataLabel)
                tableView.backgroundView = noDataLabelView
            }
            
            return numOfSections
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isFromHome {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ExcercisesCell", for: indexPath) as! ExcercisesCell
            cell.pickNutrition = nutritionList[indexPath.row]
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RemoveWorkoutCell", for: indexPath) as! RemoveWorkoutCell
            cell.yourMealList = nutritionMealList[indexPath.row]
            cell.onRemove = {[weak self] in
                let alertController = UIAlertController(title: "Are you sure?", message: "You want to remove Nutrition?", preferredStyle: .alert)
                // Create the actions
                let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) { [self]
                    UIAlertAction in
                    let selectMeal = self?.nutritionMealList[indexPath.row]
                    self?.nutritionMealList.remove(at: indexPath.row)
                    self?.removeWorkoutAPI(calenderId: selectMeal?.calendar_id ?? 0)
                    NSLog("OK Pressed")
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default) { [self]
                    UIAlertAction in
                    //self.getNearByGym(category: "", searchText: "")
                    NSLog("OK Pressed")
                }
                // Add the actions
                okAction.setValue(UIColor.red, forKey: "titleTextColor")
                cancelAction.setValue(UIColor.gray, forKey: "titleTextColor")
                alertController.addAction(cancelAction)
                alertController.addAction(okAction)
                // Present the controller
                self?.present(alertController, animated: true, completion: nil)
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isFromHome {
            let controller = STORYBOARD.nutrition.instantiateViewController(withIdentifier:"MealItelListScreen") as! MealItelListScreen
            controller.id = nutritionList[indexPath.row].nutrition_id ?? 0
            controller.titleName = nutritionList[indexPath.row].title ?? ""
            controller.isFromHome = true
            vc.navigationController?.pushViewController(controller, animated: true)
        } else {
            let controller = STORYBOARD.nutrition.instantiateViewController(withIdentifier:"CreateRecipeScreen") as! CreateRecipeScreen
            controller.selectMeal = nutritionMealList[indexPath.row]
            controller.isFromcalender = true
            vc.navigationController?.pushViewController(controller, animated: true)
//            if nutritionMealList[indexPath.row].is_admin_meal == "0" {
//                let controller = STORYBOARD.nutrition.instantiateViewController(withIdentifier:"MealSearchScreen") as! MealSearchScreen
//                controller.selectMeal = nutritionMealList[indexPath.row]
//                controller.mealId = nutritionMealList[indexPath.row].meal_id ?? 0
//                controller.isNotDefaultMeal = true
//                controller.isFromCalender = true
//                vc.navigationController?.pushViewController(controller, animated: true)
//            } else {
//                let controller = STORYBOARD.nutrition.instantiateViewController(withIdentifier:"MyMealPlanAndRecipeScreen") as! MyMealPlanAndRecipeScreen
//                controller.isFromCalender = true
//                controller.mealId = nutritionMealList[indexPath.row].meal_id ?? 0
//                controller.selectMeal = nutritionMealList[indexPath.row]
//                controller.isFromCalender = true
//    //            controller.id = nutritionList[indexPath.row].nutrition_recipe__id ?? 0
//    //            controller.titleName = nutritionList[indexPath.row].title ?? ""
//    //            controller.isFromHome = true
//                vc.navigationController?.pushViewController(controller, animated: true)
//            }
        }
        // self.tabBarController?.tabBar.isHidden = true
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            tableViewCellHeightNutrition = Int(tableView.contentSize.height)
        }
    }
    
//    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
//        return true
//    }
//
//    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
//        if (editingStyle == .delete) {
//            let selectMeal = nutritionMealList[indexPath.row]
//            nutritionMealList.remove(at: indexPath.row)
//            mealDeleteAPI(mealId: selectMeal.meal_id ?? 0)
//        }
//    }
}

extension NutritionListScreen {
    //MARK:- meal Delete API
    func mealDeleteAPI(mealId:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
           // Utility.showIndecator()
            NutritionService.shared.mealDelete(id: mealId){ (statusCode, response) in
                Utility.hideIndicator()
                print(response.message ?? "")
                if let res = response.message {
                    self.nutritionListTableView.reloadData()
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    func nutritionListAPI(page:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
         //  Utility.showIndecator()
            HomeAddGymService.shared.nutritionList(Page : page) { [self] (statusCode, response) in
                Utility.hideIndicator()
                hasMorePage = true
                if let res = response.pickNutritionResponse {
                    if page == 1{
                        self.nutritionList = []
                        self.nutritionList = res
                        print(self.nutritionList.toJSON())
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            self.nutritionListTableView.stopSkeletonAnimation()
                            self.view.hideSkeleton(reloadDataAfter: true, transition: .crossDissolve(0.25))
                            self.nutritionListTableView.reloadData()
                        }
                    } else {
                        self.appendDataCollectionView2(data: response)
                    }
                    if let meta = response.metaResponse{
                        self.meta = meta
                    }
                }
                
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                //stronSelf.hasMorePage = true
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:- workout Delete API
    func removeWorkoutAPI(calenderId:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            let data = removeWorkoutRequest(type: "2", calendar_id: calenderId)
            MyWorkoutService.shared.removeWorkout(parameters:data.toJSON()){ (statusCode, response) in
                Utility.hideIndicator()
                print(response.message ?? "")
               // if response.message ?? "" == "workout delete successfully."{
//                if response.message ?? "" == "workout delete successfully."{
//                    self.dismiss(animated: false, completion: { [weak self] in
//                        self?.removeWorkout?()
//                    })
//                }
//                else{
//                    Utility.showAlert(vc: self, message: response.message ?? "")
//                }
                self.nutritionListTableView.reloadData()
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:- Nutrition List API
    func nutritionListAPI(page:Int,selecteDate:String){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            //  Utility.showIndecator()
            let data = WorkoutListRequest(date: selecteDate)
            CalenderService.shared.nutritionDayList(page: page, parameters: data.toJSON()) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                self?.hasMorePage = true
                if let res = response?.yourMealListResponse {
                    if page == 1{
                        self?.nutritionMealList = []
                        self?.nutritionMealList = res
                        self?.nutritionListTableView.reloadData()
                    } else {
                        self?.appendDataCollectionView(data: response!)
                    }
                    if let meta = response?.metaResponse{
                        self?.meta = meta
                    }
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}

extension NutritionListScreen : SkeletonTableViewDataSource {
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "ExcercisesCell"
    }
    
    func collectionSkeletonView(_ skeletonView: UITableView, numberOfRowsInSection section: Int) -> Int {
        2
    }
}
