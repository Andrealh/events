//
//  HomeScreen.swift
//  FitnessFactory
//
//  Created by iroid on 07/06/21.
//

import UIKit

class HomeScreen: UIViewController {
    //MARK:- UIView IBOutlet
    @IBOutlet weak var headerView: UIView!
    
    //MARK:- UIView IBOutlet
    @IBOutlet weak var userNameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initalizedDetails()
    }
    
    //MARK:- initalizedDetails Details
    func initalizedDetails(){
        headerView.roundCorners(corners: [.layerMaxXMaxYCorner, .layerMinXMaxYCorner], radius: 42)
    }
    
    
    //MARK:- IBAction
    @IBAction func onProfile(_ sender: UIButton) {
        tabBarController?.tabBar.isHidden = true
        let storyBoard: UIStoryboard = UIStoryboard(name: "Profile", bundle: nil)
         let vc = storyBoard.instantiateViewController(withIdentifier: "ProfileScreen") as! ProfileScreen

         let navigationController = self.navigationController


         vc.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Close", style: .plain, target: vc, action: #selector(vc.closeView))
         vc.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: vc, action: nil)
        
         let transition = CATransition()
         transition.duration = 0.2
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.moveIn
        transition.subtype = CATransitionSubtype.fromLeft
         navigationController?.view.layer.add(transition, forKey: nil)
         navigationController?.pushViewController(vc, animated: false)
    }
    
}
