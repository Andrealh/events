//
//  ExerciseListScreen.swift
//  FitnessFactory
//
//  Created by iroid on 09/08/21.
//

import UIKit
import SkeletonView

class ExerciseListScreen: UIViewController {
    
    @IBOutlet weak var breakfastTableView: UITableView!
    
    //MARK:- Variables
    var meta:Meta!
    var hasMorePage : Bool = false
    var page = 1
    var exerciseList : [WorkoutListResponse] = []
    var workoutList : [WorkoutResponse] = []
    var myWorkoutList : [pickWorkoutResponse] = []
    var selectedBreakFast = 1
    var vc = UIViewController()
    var isFromHome = Bool()
    var selectDate = String()
    var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if isFromHome {
            breakfastTableView.rowHeight = 80
            breakfastTableView.estimatedRowHeight = 80
            breakfastTableView.isSkeletonable = true
            let gradient = SkeletonGradient(baseColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.1310794454))
            breakfastTableView.showAnimatedGradientSkeleton(usingGradient: gradient, animation: nil, transition: .crossDissolve(0.5))
        }
        initialDetail()
        // Do any additional setup after loading the view.
    }
    
    func initialDetail(){
        // self.tabBarController?.tabBar.isHidden = false
        let nib = UINib(nibName: "ExcercisesCell", bundle: nil)
        breakfastTableView.register(nib, forCellReuseIdentifier: "ExcercisesCell")
        let workoutCell = UINib(nibName: "RemoveWorkoutCell", bundle: nil)
        breakfastTableView.register(workoutCell, forCellReuseIdentifier: "RemoveWorkoutCell")
       
        //eventListAPI(page: 1)
        //nutritionListAPI(page: 1)
        if isFromHome {
            myWorkoutListAPI()
        } else {
            workoutListAPI(page: 1, selecteDate: selectDate)
        }
        
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(refreshPostList), for: UIControl.Event.valueChanged)
       // breakfastTableView.addSubview(refreshControl)
        //setUpData()
    }
    
    
    
    
    // MARK: scroll method
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let size = scrollView.contentSize
        let inset = scrollView.contentInset
        let y = offset.y + bounds.size.height - inset.bottom
        let h = size.height
        let reload_distance:CGFloat = 10.0
        if y > (h + reload_distance) {
            print("called")
            if let metaTotal = self.meta?.total{
                if isFromHome {
                    if self.exerciseList.count != metaTotal{
                        if self.hasMorePage{
                            self.hasMorePage = false
                            self.page += 1
                            exerciseListAPI(page: page)
                        }
                    }
                } else {
                    if self.workoutList.count != metaTotal{
                        if self.hasMorePage{
                            self.hasMorePage = false
                            self.page += 1
                            workoutListAPI(page: page, selecteDate: selectDate)
                        }
                    }
                }
            }
        }
    }
    
    func appendDataCollectionView(data: Response){
        var indexPathArray: [IndexPath] = []
        let workoutRes = data.workoutListResponse!
        for i in self.exerciseList.count..<self.exerciseList.count + workoutRes.count {
            indexPathArray.append(IndexPath(item: i, section: 0))
        }
        self.exerciseList.append(contentsOf: workoutRes)
        self.breakfastTableView.insertRows(at: indexPathArray, with: .automatic)
    }
    
    @objc func refreshPostList() {
        refreshControl.endRefreshing()
        if isFromHome {
            myWorkoutListAPI()
        } else {
            workoutListAPI(page: 1, selecteDate: selectDate)
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
}


extension ExerciseListScreen: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var numOfSections: Int = 0
        if isFromHome {
            return myWorkoutList.count
        } else {
            if workoutList.count > 0 {
                numOfSections = workoutList.count
                tableView.backgroundView = nil
            } else {
                let noDataLabelView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
                let noDataLabel = UILabel(frame: CGRect(x: 0, y: 50, width: tableView.bounds.size.width, height: 50))
                noDataLabel.text = "No Workouts."
                noDataLabel.textColor = UIColor.darkGray
                noDataLabel.textAlignment = .center
                noDataLabel.numberOfLines = 1
                noDataLabelView.addSubview(noDataLabel)
                tableView.backgroundView = noDataLabelView
            }
            return numOfSections
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isFromHome {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ExcercisesCell", for: indexPath) as! ExcercisesCell
            cell.pickWorkout = myWorkoutList[indexPath.row]
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RemoveWorkoutCell", for: indexPath) as! RemoveWorkoutCell
            cell.item = workoutList[indexPath.row]
            cell.onRemove = {[weak self] in
                let alertController = UIAlertController(title: "Are you sure?", message: "You want to remove workout?", preferredStyle: .alert)
                // Create the actions
                let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) { [self]
                    UIAlertAction in
                    let selectWorkout = self?.workoutList[indexPath.row]
                    self?.workoutList.remove(at: indexPath.row)
                    self?.removeWorkoutAPI(calenderId: selectWorkout?.calendar_id ?? 0)
                    NSLog("OK Pressed")
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default) { [self]
                    UIAlertAction in
                    //self.getNearByGym(category: "", searchText: "")
                    NSLog("OK Pressed")
                }
                // Add the actions
                okAction.setValue(UIColor.red, forKey: "titleTextColor")
                cancelAction.setValue(UIColor.gray, forKey: "titleTextColor")
                alertController.addAction(cancelAction)
                alertController.addAction(okAction)
                // Present the controller
                self?.present(alertController, animated: true, completion: nil)
                
            }
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isFromHome {
            tabBarController?.tabBar.isHidden = true
            let controller = STORYBOARD.exercise.instantiateViewController(withIdentifier:"ExerciseSetScreen") as! ExerciseSetScreen
            controller.workoutSetId = myWorkoutList[indexPath.row].admin_workout_id ?? 0
            controller.setTitleName = myWorkoutList[indexPath.row].title ?? ""
            controller.isFromHome = true
            controller.isFromPickWorkout = true
            vc.navigationController?.pushViewController(controller, animated: true)
        } else {
            let controller = STORYBOARD.myWorkout.instantiateViewController(withIdentifier:"ExerciseDetailsScreen") as! ExerciseDetailsScreen
            controller.workoutId = workoutList[indexPath.row].workout_id ?? 0
            controller.isFromCalenderScreen = true
//            controller.delegate = self
            vc.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            tableViewCellHeightExercise = Int(tableView.contentSize.height)
        }
    }
    
//    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
//        return true
//    }
//
//    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
//        if (editingStyle == .delete) {
//            let selectWorkout = workoutList[indexPath.row]
//            workoutList.remove(at: indexPath.row)
//            workoutDeleteAPI(workoutId: selectWorkout.workout_id ?? 0)
//        }
//    }
}

extension ExerciseListScreen {
    func exerciseListAPI(page:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            HomeAddGymService.shared.exerciseList(Page : page) { [self] (statusCode, response) in
                Utility.hideIndicator()
                hasMorePage = true
                if let res = response.workoutListResponse {
                    if page == 1{
                        self.exerciseList = res
                        print(self.exerciseList.toJSON())
                        self.breakfastTableView.reloadData()
                    } else {
                        self.appendDataCollectionView(data: response)
                    }
                    if let meta = response.metaResponse{
                        self.meta = meta
                    }
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                stronSelf.hasMorePage = true
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:- workout Delete API
    func workoutDeleteAPI(workoutId:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            MyWorkoutService.shared.workoutDelete(id: workoutId){ (statusCode, response) in
                Utility.hideIndicator()
                print(response.message ?? "")
               // if response.message ?? "" == "workout delete successfully."{
//                if response.message ?? "" == "workout delete successfully."{
//                    self.dismiss(animated: false, completion: { [weak self] in
//                        self?.removeWorkout?()
//                    })
//                }
//                else{
//                    Utility.showAlert(vc: self, message: response.message ?? "")
//                }
                self.breakfastTableView.reloadData()
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:- workout Delete API
    func removeWorkoutAPI(calenderId:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            let data = removeWorkoutRequest(type: "1", calendar_id: calenderId)
            MyWorkoutService.shared.removeWorkout(parameters:data.toJSON()){ (statusCode, response) in
                Utility.hideIndicator()
                print(response.message ?? "")
               // if response.message ?? "" == "workout delete successfully."{
//                if response.message ?? "" == "workout delete successfully."{
//                    self.dismiss(animated: false, completion: { [weak self] in
//                        self?.removeWorkout?()
//                    })
//                }
//                else{
//                    Utility.showAlert(vc: self, message: response.message ?? "")
//                }
                self.breakfastTableView.reloadData()
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func myWorkoutListAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
          //  Utility.showIndecator()
            HomeAddGymService.shared.workoutList() { [self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response.pickWorkoutResponse {
                    self.myWorkoutList = []
                    self.myWorkoutList = res
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.breakfastTableView.stopSkeletonAnimation()
                        self.view.hideSkeleton(reloadDataAfter: true, transition: .crossDissolve(0.25))
                        self.breakfastTableView.reloadData()
                    }
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    //MARK:- workout List API
    func workoutListAPI(page:Int,selecteDate:String){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            //Utility.showIndecator()
            let data = WorkoutListRequest(date: selecteDate)
            CalenderService.shared.workoutDayList(page: page, parameters: data.toJSON()) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                self?.hasMorePage = true
                if let res = response?.workoutResponse {
                    if page == 1{
                        self?.workoutList = []
                        self?.workoutList = res
                        self?.breakfastTableView.reloadData()
                    } else {
                        self!.appendDataCollectionView(data: response!)
                    }
                    if let meta = response?.metaResponse{
                        self?.meta = meta
                    }
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                stronSelf.hasMorePage = true
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}


extension ExerciseListScreen : SkeletonTableViewDataSource {
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "ExcercisesCell"
    }
    
    func collectionSkeletonView(_ skeletonView: UITableView, numberOfRowsInSection section: Int) -> Int {
        2
    }
}
