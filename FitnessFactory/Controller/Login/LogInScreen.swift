//
//  LogInScreen.swift
//  FitnessFactory
//
//  Created by iroid on 04/06/21.
//

import UIKit

class LogInScreen: UIViewController {
    //MARK:- UIView IBOutlet
    @IBOutlet weak var logInView: UIView!
    
    //MARK:- UIButton IBOutlet
    @IBOutlet weak var showPasswordButton: UIButton!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var logInButton: UIButton!
    //MARK:- UITextField IBOutlet
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initalizedDetails()
    }
  
    
    //MARK:- initalizedDetails Details
    func initalizedDetails(){
        forgotPasswordButton.isHidden = true
        logInView.roundCorners(corners: [.layerMinXMinYCorner, .layerMaxXMinYCorner], radius: 52)
        if #available(iOS 13.0, *) {
            logInView.layer.cornerCurve = .continuous
        } else {
            // Fallback on earlier versions
        }
        navigationController?.navigationBar.isHidden = true
    }
    
    func checkValidation() -> String?{
        if self.emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter your email or username"
        }
        else if self.passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter your password"
        }
        else if self.passwordTextField.text!.count < 8{
            return "Password must be longer than 8 characters."
        }
        return nil
    }
    
    func goFurther(){
        let controller = STORYBOARD.home.instantiateViewController(withIdentifier:"HomeScreen") as! HomeScreen
        navigationController?.pushViewController(controller, animated: true)
    }
    //MARK:- IBAction
    @IBAction func onFogetPassword(_ sender: UIButton) {
        let controller = STORYBOARD.login.instantiateViewController(withIdentifier:"ForgetPasswordScreen") as! ForgetPasswordScreen
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func onFoget2(_ sender: UIButton) {
        let controller = STORYBOARD.login.instantiateViewController(withIdentifier:"resendEmailScreen") as! resendEmailScreen
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func onLogin(_ sender: UIButton) {
        if let error = self.checkValidation(){
            Utility.showAlert(vc: self, message: error)
            print(error)
        } else {
            loginAPI()
        }
    }
    
    @IBAction func onShowPassword(_ sender: UIButton) {
        view.endEditing(true)
        passwordTextField.layer.borderWidth = 1.5
        if passwordTextField.text?.count != 0 {
            if showPasswordButton.isSelected {
                showPasswordButton.isSelected = false
                showPasswordButton.setImage(UIImage(named: "Hide"), for: .normal)
                passwordTextField.isSecureTextEntry = true
            } else {
                showPasswordButton.isSelected = true
                showPasswordButton.setImage(UIImage(named: "Show"), for: .normal)
                passwordTextField.isSecureTextEntry = false
            }
        }
    }
    @IBAction func onSignup(_ sender: UIButton) {
        let controller = STORYBOARD.signUp.instantiateViewController(withIdentifier:"SignUpScreen") as! SignUpScreen
        navigationController?.pushViewController(controller, animated: true)
    }
}

//MARK:- UITextFieldDelegate
extension LogInScreen:UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField != passwordTextField {
            passwordTextField.layer.borderWidth = 0
        }
        textField.layer.borderWidth = 1.5
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layer.borderWidth = 0
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
      }
}

extension LogInScreen {
    //MARK:- Login API
    func loginAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            let data = LoginRequest(email: self.emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), password: self.passwordTextField.text ?? "")
            LoginService.shared.login(parameters: data.toJSON()) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response?.loginResponse{
                    Utility.saveUserData(data: res.toJSON())
                    self?.gymLinkStatusAPI()
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
                if(error == "Email not verified"){
                    self?.forgotPasswordButton.isHidden = false;
                }
                print("!11111111")
                print(error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}

extension  LogInScreen {
    //MARK:- Gym Category API
    func gymLinkStatusAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
           // Utility.showIndecator()
            HomeAddGymService.shared.gymLinkStatus() { (statusCode, response) in
                Utility.hideIndicator()
               // if let res = response.gymLinkStatusResponse{
                    if response.gymLinkStatusResponse == nil {
                        let controller = STORYBOARD.home.instantiateViewController(withIdentifier:"LinkYourGymScreen") as! LinkYourGymScreen
                        self.navigationController?.pushViewController(controller, animated: true)
                    } else {
                        if response.isLinkRequest == true {
                            let controller = STORYBOARD.tabBar.instantiateViewController(withIdentifier:"TabBarScreen") as! TabBarScreen
                            controller.isFromLink = true
                            controller.gymDetail = response.gymLinkStatusResponse
                            self.navigationController?.pushViewController(controller, animated: true)
                        } else {
                            let controller = STORYBOARD.tabBar.instantiateViewController(withIdentifier:"TabBarScreen") as! TabBarScreen
                            controller.isFromLink = false
                            controller.gymDetail = response.gymLinkStatusResponse
                            self.navigationController?.pushViewController(controller, animated: true)
                        }
                    }
                //}
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}

