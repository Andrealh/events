//
//  resendEmail.swift
//  Fitness Factory
//
//  Created by Rushi Patel on 2021-12-03.
//

import Foundation
import UIKit
class resendEmailScreen: UIViewController {
    //MARK:- UIButton UITextField
    @IBOutlet weak var emailTextField : UITextField!
    //MARK:- UIView IBOutlet
    @IBOutlet weak var forgetPasswordView: UIView!
    //MARK:- UIButton IBOutlet
    @IBOutlet weak var resendEmailButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initalizedDetails()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- initalizedDetails Details
    func initalizedDetails(){
        forgetPasswordView.roundCorners(corners: [.layerMinXMinYCorner, .layerMaxXMinYCorner], radius: 52)
        if #available(iOS 13.0, *) {
            forgetPasswordView.layer.cornerCurve = .continuous
        } else {
            // Fallback on earlier versions
        }
    }
    
    func checkValidation() -> String?{
        if self.emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter your email"
        }
        else if !self.emailTextField.text.isEmailValid(){
            return "Please enter a valid email address"
        }
        return nil
    }
    
    @IBAction func onBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onSendEmail(_ sender: UIButton) {
        if let error = self.checkValidation(){
            Utility.showAlert(vc: self, message: error)
        } else {
            resendEmail()
        }
    }

}
extension resendEmailScreen:UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.layer.borderWidth = 1.5
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layer.borderWidth = 0
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
    }
}

extension resendEmailScreen {
    //MARK:- Login API
    func resendEmail(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            let data = ForgetPasswordRequest(email: self.emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines))
            LoginService.shared.resendPassword(parameters: data.toJSON()) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response?.message{
                    //Utility.showAlert(vc: self!, message: res)
                    let alertController = UIAlertController(title: APPLICATION_NAME, message: res, preferredStyle: .alert)

                    // Create the actions
                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                        UIAlertAction in
                        NSLog("OK Pressed")
                        let controller = STORYBOARD.login.instantiateViewController(withIdentifier:"LogInScreen") as! LogInScreen
                        self?.navigationController?.pushViewController(controller, animated: true)
                    }
                    // Add the actions
                    alertController.addAction(okAction)

                    // Present the controller
                    self?.present(alertController, animated: true, completion: nil)
                   
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}
