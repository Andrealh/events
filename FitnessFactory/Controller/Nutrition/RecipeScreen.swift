//
//  RecipeScreen.swift
//  FitnessFactory
//
//  Created by iroid on 05/07/21.
//

import UIKit

class RecipeScreen: UIViewController {
    
    @IBOutlet weak var recepieImageView: UIImageView!
    
    @IBOutlet weak var recipeTitleLabel: UILabel!
    @IBOutlet weak var recipeCalLabel: UILabel!
    @IBOutlet weak var recipeDescriptionLabel: UILabel!
    
    @IBOutlet weak var recipeTextView: UITextView!
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var addRecipeView: UIView!
    
    @IBOutlet weak var recipeItemTableView: UITableView!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var editRecipeButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    
    var recipeDetailId : Int = 0
    var recipedetail : MealRecipeListResponse?
    var isFromMymeal = Bool()
    var itemList = [StoreRecipeItemListResponse]()
    var myItemList = [MyMealItemListResponse]()
    var delegate : WorkOutDeleteDelegate?
    var recipeURL = String()
    var onback: (() -> Void)?
    var isCraeteRecipe = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initalizedDetails()
    }
    
    //MARK:- initalizedDetails Details
    func initalizedDetails(){
        let nib = UINib(nibName: "RecipeItemCell", bundle: nil)
        recipeItemTableView.register(nib, forCellReuseIdentifier: "RecipeItemCell")
        let nib2 = UINib(nibName: "RecipeItemListCell", bundle: nil)
        recipeItemTableView.register(nib2, forCellReuseIdentifier: "RecipeItemListCell")
        recipeItemTableView.tableFooterView = UIView()
        if isFromMymeal {
            editButton.isHidden = false
            setUpData(recipeDetail: recipedetail!)
            //myMealRecipeDetailAPI(Id: recipeDetailId)
            RecipeItemListApi(Id: recipeDetailId, Page: 1)
        } else {
            editButton.isHidden = true
            RecipeDetailAPI(Id: recipeDetailId)
        }
    }
    
    func setUpData(recipeDetail : MealRecipeListResponse)  {
        
        recipeTitleLabel.text = recipeDetail.title
        //recipeCalLabel.text = String(recipeDetail.cal ?? 0) + " Cal"
        recipeCalLabel.text = ""
        if recipedetail?.recipe == nil || recipedetail?.recipe == ""{
            //recipeItemTableView.tag = 1
            //recipeURL = recipedetail?.recipe ?? ""
            editRecipeButton.isHidden = true
            addRecipeView.isHidden = false
            recipeTextView.isHidden = true
        } else {
            editRecipeButton.isHidden = false
            recipeTextView.isHidden = false
            addRecipeView.isHidden = true
            if recipeDetail.recipe != ""  {
                recipeTextView.text = recipeDetail.recipe
            } else {
                recipeTextView.text = "   --"
            }
            //recipeTextView.text = recipedetail?.recipe
        }
        recipeDescriptionLabel.text = recipeDetail.desc
        if recipedetail?.image != defaultRecipeImage {
            Utility.setImage(recipeDetail.image, imageView: recepieImageView)
        }
    }
    
    func setUpDataForMyMeal(recipeDetail : NutritionRecipeListResponse)  {
        recipeTitleLabel.text = recipeDetail.title
        recipeCalLabel.text = String(recipeDetail.cal ?? 0) + " Cal"
        addRecipeView.isHidden = true
        recipeTextView.isHidden = false
            if recipeDetail.recipe != "" {
                recipeTextView.text = recipeDetail.recipe
            } else {
                recipeTextView.text = "   --"
            }
            //recipeTextView.text = recipedetail?.recipe
        recipeDescriptionLabel.text = recipeDetail.desc
        Utility.setImage(recipeDetail.image, imageView: recepieImageView)
    }
    
    @IBAction func onAddRecipe(_ sender: UIButton) {
        backgroundView.isHidden = false
        let vc =  STORYBOARD.nutrition.instantiateViewController(withIdentifier: "AddRecipeStepScreen") as!  AddRecipeStepScreen
        vc.modalPresentationStyle = .overFullScreen
        vc.editRecipe = recipedetail
        vc.delegate = self
        vc.updateRecipe = { [weak self] value in
            print(value)
            self?.recipedetail = value
            if value.recipe == nil || value.recipe == "" {
                self?.editRecipeButton.isHidden = true
                self?.addRecipeView.isHidden = false
                self?.recipeTextView.isHidden = true
                self?.recipeTextView.text = "   ---"
            } else {
                self?.editRecipeButton.isHidden = false
                self?.recipeTextView.isHidden = false
                self?.addRecipeView.isHidden = true
                self?.recipeTextView.text = value.recipe
            }
            
            //self?.navigationController?.popViewController(animated: true)
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func onRecipeDetail(_ sender: UIButton) {
        backgroundView.isHidden = false
        let vc =  STORYBOARD.nutrition.instantiateViewController(withIdentifier: "AddRecipeStepScreen") as!  AddRecipeStepScreen
        vc.modalPresentationStyle = .overFullScreen
        vc.editRecipe = recipedetail
        vc.delegate = self
        vc.updateRecipe = { [weak self] value in
            print(value)
            self?.recipedetail = value
            if value.recipe == nil || value.recipe == "" {
                self?.editRecipeButton.isHidden = true
                self?.addRecipeView.isHidden = false
                self?.recipeTextView.isHidden = true
                self?.recipeTextView.text = "   ---"
            } else {
                self?.editRecipeButton.isHidden = false
                self?.recipeTextView.isHidden = false
                self?.addRecipeView.isHidden = true
                self?.recipeTextView.text = value.recipe
            }
            
            //self?.navigationController?.popViewController(animated: true)
        }
        self.present(vc, animated: true, completion: nil)
        //UIApplication.shared.openURL(URL(string:recipeURL)!)
    }
    
    @IBAction func onEdit(_ sender: UIButton) {
        backgroundView.isHidden = false
        let vc =  STORYBOARD.nutrition.instantiateViewController(withIdentifier: "EditMealScreen") as!  EditMealScreen
        vc.modalPresentationStyle = .overFullScreen
        vc.editRecipe = recipedetail
        vc.delegate = self
        vc.isEdit = true
        vc.updateRecipe = { [weak self] value in
            print(value)
            self?.recipedetail = value
            self?.recipeTitleLabel.text = value.title
            self?.recipeDescriptionLabel.text = value.desc
            if value.image != defaultRecipeImage {
                Utility.setImage(value.image, imageView: self?.recepieImageView)
            }
            if value.recipe != nil {
                self?.recipeTextView.text = value.recipe
            } else {
                self?.recipeTextView.text = "   ---"
            }
            
            //self?.navigationController?.popViewController(animated: true)
        }
        vc.dismissRemoveScreen = { [weak self] in
            let vc =  STORYBOARD.nutrition.instantiateViewController(withIdentifier: "RemoveMealScreen") as! RemoveMealScreen
            vc.modalPresentationStyle = .overFullScreen
            vc.isDeleteRecipe = true
            vc.mealId = self?.recipedetail?.meal_recipe_id ?? 0
            vc.delegate = self
//            vc.mealId = self?.selectMeal?.meal_id ?? 0
//            vc.removeMeal = self?.selectMealMealRecipeListResponse
//            vc.delegate = self
            vc.removeWorkout =  { [weak self]  in
                self?.delegate?.WorkOutDelete()
                self?.navigationController?.popViewController(animated: true)
            }
            self?.present(vc, animated: false, completion: nil)
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func onBack(_ sender: UIButton) {
        if isCraeteRecipe{
            onback!()
        }
        self.navigationController?.popViewController(animated: false)
    }
    
}

extension RecipeScreen : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if recipeItemTableView.tag == 1 {
//            tableView.backgroundView = nil
//            return 0
//        } else {
            var numberOfSection : Int = 0
            if isFromMymeal {
                if itemList.count > 0 {
                    numberOfSection = itemList.count
                    tableView.backgroundView = nil
                } else {
                    let noDataLabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
                    noDataLabel.text = "No items have been added."
                    noDataLabel.textColor = UIColor.darkGray
                    noDataLabel.textAlignment = .center
                    noDataLabel.numberOfLines = 1
                    tableView.backgroundView = noDataLabel
                }
                return numberOfSection
            } else {
                return myItemList.count
            }
//    }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isFromMymeal {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RecipeItemCell") as! RecipeItemCell
            cell.item = itemList[indexPath.row]
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RecipeItemCell") as! RecipeItemCell
            cell.myItem = myItemList[indexPath.row]
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            self.tableViewHeightConstraint.constant = tableView.contentSize.height
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if isFromMymeal {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RecipeItemListCell") as! RecipeItemListCell
            cell.onAdditem = {[weak self] in
                let controller = STORYBOARD.nutrition.instantiateViewController(withIdentifier:"MealSearchScreen") as! MealSearchScreen
                controller.mealId = self?.recipeDetailId ?? 0
                controller.titleName = self?.recipeTitleLabel.text ?? ""
                controller.selectMealItem  = { [weak self] in
                     self?.RecipeItemListApi(Id: self?.recipeDetailId ?? 0, Page: 1)
                    }
//                controller.storeMealItemList = self?.breakFastItemList ?? []
                self?.navigationController?.pushViewController(controller, animated: false)
            }
            return cell
        } else {
            let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 50))
            let label = UILabel(frame: CGRect(x: 25, y: 0, width: tableView.frame.size.width, height: 50))
            if self.itemList.count != 0 || self.myItemList.count != 0{
                label.text = "Items"
            } else {
                view.frame.size.height = 0
                label.frame.size.height = 0
            }
            label.font = UIFont(name: "Montserrat-ExtraBold", size: 24)
            label.textColor = UIColor.white
            view.addSubview(label)
            return view
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
            if isFromMymeal {
    //            if self.itemList.count == 0{
    //              /  return 0
    //            }
            } else {
                if self.myItemList.count == 0{
                    return 0
                }
            }
           return 50
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            if isFromMymeal {
                let alertController = UIAlertController(title: "Are you sure?", message: "You want to delete this item?", preferredStyle: .alert)
                // Create the actions
                let okAction = UIAlertAction(title: "Yes, delete it!", style: UIAlertAction.Style.default) { [self]
                    UIAlertAction in
                    let selectRecipe = itemList[indexPath.row]
                    itemList.remove(at: indexPath.row)
                    removeItem(itemId: selectRecipe.meal_recipe_item_id ?? 0)
                    // workoutDeleteAPI(workoutId: selectWorkout.workout_id ?? 0)
                    //self.getNearByGym(category: "", searchText: "")
                    NSLog("OK Pressed")
                }
                
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default) { [self]
                    UIAlertAction in
                    //self.getNearByGym(category: "", searchText: "")
                    NSLog("OK Pressed")
                }
                // Add the actions
                okAction.setValue(UIColor.red, forKey: "titleTextColor")
                cancelAction.setValue(UIColor.gray, forKey: "titleTextColor")
                alertController.addAction(cancelAction)
                alertController.addAction(okAction)
                // Present the controller
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
}

extension RecipeScreen {
    //MARK:- RecipeList
    func RecipeDetailAPI(Id:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            //Utility.showIndecator()
            NutritionService.shared.recipesDetail(Id: Id) { (statusCode, response) in
                Utility.hideIndicator()
                if let res = response.nutritionRecipeListResponse {
                    if let data = res.nutrition_item {
                        self.myItemList.append(contentsOf: data)
                    }
                    if self.myItemList.isEmpty{
                        self.tableViewHeightConstraint.constant = 50
                    } else {
                        self.tableViewHeightConstraint.constant = 250
                    }
                    DispatchQueue.main.async {
                        self.recipeItemTableView.reloadData()
                    }
                   
                    self.setUpDataForMyMeal(recipeDetail: res)
                }
                
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func myMealRecipeDetailAPI(Id:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            //Utility.showIndecator()
            NutritionService.shared.myRecipesDetail(Id: Id) { (statusCode, response) in
                Utility.hideIndicator()
                if let res = response.selectrecipe {
                    DispatchQueue.main.async {
                        self.recipeItemTableView.reloadData()
                    }
                    self.recipedetail = res
                    self.setUpData(recipeDetail: res)
                }
                
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func RecipeItemListApi(Id:Int,Page:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
          //  Utility.showIndecator()
            NutritionService.shared.recipeItemList(Id: Id, Page: Page) { (statusCode, response) in
                Utility.hideIndicator()
                if let res = response.storeRecipeItemListResponse{
                    self.itemList = []
                    self.itemList = res
//                    if self.itemList.isEmpty{
//                        self.tableViewHeightConstraint.constant = 50
//                    } else {
//                        self.tableViewHeightConstraint.constant = 250
//                    }
                    self.recipeItemTableView.reloadData()
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func removeItem(itemId : Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            HomeAddGymService.shared.removeItemFromRecipe(ID: itemId) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response.message {
                    self?.recipeItemTableView.reloadData()
                    if self?.itemList.count == 0 {
                        self?.tableViewHeightConstraint.constant = 250
                    }
//                        let alertController = UIAlertController(title: APPLICATION_NAME, message: res, preferredStyle: .alert)
//                        // Create the actions
//                        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
//                            UIAlertAction in
//                            //self?.stroeMealItemList(Id: self?.mealId ?? 0, Page: 1)
//                            self?.MyPlanRecipeListTableview.reloadData()
//                            NSLog("OK Pressed")
//                        }
//                        // Add the actions
//                        alertController.addAction(okAction)
//                        // Present the controller
//                        self?.present(alertController, animated: true, completion: nil)
                    }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}

extension RecipeScreen : hideBackgroundDelegate {
    func hideView() {
        backgroundView.isHidden = true
    }
}
