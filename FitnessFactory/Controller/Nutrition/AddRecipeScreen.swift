//
//  AddRecipeScreen.swift
//  FitnessFactory
//
//  Created by iroid on 23/09/21.
//

import UIKit

class AddRecipeScreen: UIViewController {

    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var backGroundView: UIView!
    @IBOutlet weak var addWorkoutView: UIView!
    @IBOutlet weak var recipeImageView: UIImageView!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var selectView: UIView!
    
    var delegate: hideBackgroundDelegate?
    var tap = UITapGestureRecognizer()
    var createRecipe:((_ createMeal:MealRecipeListResponse) -> Void)?
    var mealId = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initalizedDetails()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- initalizedDetails Details
    func initalizedDetails(){
        addWorkoutView.roundCorners(corners: [.layerMinXMinYCorner, .layerMaxXMinYCorner], radius: 52)
        if #available(iOS 13.0, *) {
            addWorkoutView.layer.cornerCurve = .continuous
        } else {
            // Fallback on earlier versions
        }
        tap = UITapGestureRecognizer(target: self, action: #selector(dismissView))
        backGroundView.addGestureRecognizer(tap)
    }
    
    @objc func dismissView() {
        delegate?.hideView()
        dismiss(animated: true, completion: nil)
    }
    
    func openCamera(){
        let imagePicker = UIImagePickerController()
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            print("Button capture")
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true//false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func openGallery(){
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker,animated: true,completion: nil)
    }
    
    func checkValidation() -> String?{
        if self.titleTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter your title recipe"
        }
        return nil
    }
    
    @IBAction func onSave(_ sender: UIButton) {
        if let error = self.checkValidation(){
            Utility.showAlert(vc: self, message: error)
        } else {
            createRecipeAPI(mealId: mealId)
        }
    }
    
    @IBAction func onAddPhoto(_ sender: UIButton) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        // Create the actions
        let takePhoto = UIAlertAction(title: "Take photo", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openCamera()
        }
        let galleryPhoto = UIAlertAction(title: "Photo Library", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openGallery()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        // Add the actions
        alertController.addAction(takePhoto)
        alertController.addAction(galleryPhoto)
        alertController.addAction(cancelAction)
//        alertController.addAction(fitnessPhoto)
//        alertController.addAction(gif)
        //   Present the controller
        self.present(alertController, animated: true, completion: nil)
    }

}

extension AddRecipeScreen :UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else {
            return
        }
        recipeImageView.image = pickedImage
        //        profileImageView.layer.borderWidth = 1.5
        //        editIconImage.isHidden = false
        //        plusIImageView.isHidden = true
        selectView.isHidden = true
        self.dismiss(animated: true, completion: { () -> Void in
        })
    }
}

extension AddRecipeScreen {
    func createRecipeAPI(mealId:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            let data = AddRecipeRequest(title: titleTextField.text, desc: descriptionTextView.text, meal_id: String(mealId))
            // NutritionService.shared.CreateOwnMeal(parameters: data.toJSON()) { [weak self] (statusCode, response) in
            var imageData:Data?
            if(recipeImageView.image != nil){
                imageData = recipeImageView.image!.jpegData(compressionQuality:0.4)!
            }
            NutritionService.shared.CreateRecipe(imageData: imageData,parameters: data.toJSON()) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response?.selectrecipe{
                    print(res.toJSON())
                    self?.dismiss(animated: false, completion: { [weak self] in
                        self?.createRecipe!(res)
                        //                                guard let strongSelf = self else {
                        //                                    return
                        //                                }
                        //                                let viewControllers: [UIViewController] = strongSelf.navigationController?.viewControllers
                        //                                for aViewController in viewControllers {
                        //                                    if aViewController is CreateRecipeScreen {
                        //                                        if let vc = aViewController as? CreateRecipeScreen{
                        //                                            vc.initalizedDetails()
                        //                                        }
                        //                                        strongSelf.navigationController!.popToViewController(aViewController, animated: true)
                        //                                    }
                        //                                }
                    })
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}
