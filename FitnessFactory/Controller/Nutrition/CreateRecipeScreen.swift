//
//  CreateRecipeScreen.swift
//  FitnessFactory
//
//  Created by iroid on 16/09/21.
//

import UIKit
import SkeletonView

class CreateRecipeScreen: UIViewController {

    @IBOutlet weak var createRecipe: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var recipeListTableView: UITableView!
    @IBOutlet weak var addRecipeToDateView: UIView!
    @IBOutlet weak var backgroundView: UIView!
    
    @IBOutlet weak var bottomView: UIView!
    
    var selectMeal : MealListResponse?
    var onback: (() -> Void)?
    var delegate : WorkOutDeleteDelegate?
    var recipeList = [MealRecipeListResponse]()
    var isFromcalender = Bool()
    
    override func viewWillAppear(_ animated: Bool) {
        recipeListTableView.reloadData()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        createRecipe.layoutIfNeeded()
        createRecipe.addLineDashedStroke(pattern: [12,6], radius: 35, color: UIColor.primaryColour.cgColor)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initalizedDetails()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- initalizedDetails Details
    func initalizedDetails(){
        titleLabel.text = selectMeal?.title
        let nib = UINib(nibName: "ExcercisesCell", bundle: nil)
        recipeListTableView.register(nib, forCellReuseIdentifier: "ExcercisesCell")
        recipeListTableView.tableFooterView = UIView()
        recipeListTableView.rowHeight = 80
        recipeListTableView.estimatedRowHeight = 80
        recipeListTableView.isSkeletonable = true
        let gradient = SkeletonGradient(baseColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.1310794454))
        recipeListTableView.showAnimatedGradientSkeleton(usingGradient: gradient, animation: nil, transition: .crossDissolve(0.5))
        yourRecipeListAPI(mealId: selectMeal?.meal_id ?? 0, page: 1)
    }
    
    @IBAction func onHome(_ sender: UIButton) {
        tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func onBack(_ sender: UIButton) {
        if self.selectMeal != nil{
            onback?()
        }
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onCreateRecipe(_ sender: Any) {
//        let controller = STORYBOARD.search.instantiateViewController(withIdentifier:"SearchScreen") as! SearchScreen
//        controller.mealId = selectMeal?.meal_id ?? 0
//        controller.selectRecipe = { [weak self] in
//            self?.yourRecipeListAPI(mealId: self?.selectMeal?.meal_id ?? 0, page: 1)
//        }
//        self.navigationController?.pushViewController(controller, animated: true)
        backgroundView.isHidden = false
        let vc =  STORYBOARD.nutrition.instantiateViewController(withIdentifier: "AddRecipeScreen") as! AddRecipeScreen
        vc.modalPresentationStyle = .overFullScreen
        vc.delegate = self
        vc.mealId = selectMeal?.meal_id ?? 0
        vc.createRecipe = {[weak self] value in
            let controller = STORYBOARD.nutrition.instantiateViewController(withIdentifier:"RecipeScreen") as! RecipeScreen
            controller.recipeDetailId = value.meal_recipe_id ?? 0
            controller.recipedetail = value
            controller.isFromMymeal = true
            controller.delegate = self
            controller.isCraeteRecipe = true
            controller.onback = {[weak self] in
                self?.backgroundView.isHidden = true
                self?.yourRecipeListAPI(mealId: self?.selectMeal?.meal_id ?? 0, page: 1)
            }
            self?.navigationController?.pushViewController(controller, animated: false)
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func onUpdateMeal(_ sender: UIButton) {
        backgroundView.isHidden = false
        let vc =  STORYBOARD.nutrition.instantiateViewController(withIdentifier: "EditMealScreen") as!  EditMealScreen
        vc.modalPresentationStyle = .overFullScreen
        vc.mealDetail = selectMeal
        vc.delegate = self
        vc.updateMeal = { [weak self] value in
            print(value)
            self?.selectMeal = value
            self?.titleLabel.text = value.title
            //self?.navigationController?.popViewController(animated: true)
        }
        vc.dismissRemoveScreen = { [weak self] in
            let vc =  STORYBOARD.nutrition.instantiateViewController(withIdentifier: "RemoveMealScreen") as! RemoveMealScreen
            vc.modalPresentationStyle = .overFullScreen
            vc.mealId = self?.selectMeal?.meal_id ?? 0
            vc.removeMeal = self?.selectMeal
            vc.delegate = self
            vc.removeWorkout =  { [weak self]  in
                self?.delegate?.WorkOutDelete()
                self?.navigationController?.popViewController(animated: true)
            }
            self?.present(vc, animated: false, completion: nil)
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func onAddToMeal(_ sender: UIButton) {
        let vc =  STORYBOARD.nutrition.instantiateViewController(withIdentifier: "SelectDateForMealScreen") as! SelectDateForMealScreen
        vc.isFromNutrition = true
        vc.isFromCalender = isFromcalender
        vc.mealId = selectMeal?.meal_id ?? 0
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension CreateRecipeScreen : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
 //       let numberOfScetion : Int = 0
//        if yourMealList.count > 0 {
//            numberOfScetion = yourMealList.count
//            tableView.backgroundView = nil
//        } else {
//            let noDataLabel = UILabel(frame: CGRect(x: 0, y: 50, width: tableView.bounds.size.width, height: 100))
//            noDataLabel.text = "There are no Recipes,please Create"
//            noDataLabel.textColor = UIColor.darkGray
//            noDataLabel.textAlignment = .center
//            noDataLabel.numberOfLines = 0
//            tableView.backgroundView = noDataLabel
//        }
        return recipeList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "NutritionTableViewCell") as! NutritionTableViewCell
//        cell.mealItem = recipeList[indexPath.row]
//        return cell
//
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExcercisesCell") as! ExcercisesCell
        cell.mealRecipe = recipeList[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller = STORYBOARD.nutrition.instantiateViewController(withIdentifier:"RecipeScreen") as! RecipeScreen
        controller.recipeDetailId = self.recipeList[indexPath.row].meal_recipe_id ?? 0
        controller.recipedetail = self.recipeList[indexPath.row]
        controller.isFromMymeal = true
        controller.delegate = self
        self.navigationController?.pushViewController(controller, animated: false)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            let alertController = UIAlertController(title: "Are you sure?", message: "You want to delete this meal?", preferredStyle: .alert)
            
            // Create the actions
            let okAction = UIAlertAction(title: "Yes, delete it!", style: UIAlertAction.Style.default) { [self]
                UIAlertAction in
                let selectMeal = recipeList[indexPath.row]
                recipeList.remove(at: indexPath.row)
                recipeDeleteAPI(recipeId: selectMeal.meal_recipe_id ?? 0)
                //self.getNearByGym(category: "", searchText: "")
                NSLog("OK Pressed")
            }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default) { [self]
                UIAlertAction in
                //self.getNearByGym(category: "", searchText: "")
                NSLog("OK Pressed")
            }
            // Add the actions
            okAction.setValue(UIColor.red, forKey: "titleTextColor")
            cancelAction.setValue(UIColor.gray, forKey: "titleTextColor")
            alertController.addAction(cancelAction)
            alertController.addAction(okAction)
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
           
        }
    }
}

extension CreateRecipeScreen {
    func yourRecipeListAPI(mealId:Int,page:Int){
        if Utility.isInternetAvailable(){
          //  Utility.showIndecator()
            NutritionService.shared.recipeDetailList(Id:mealId, Page: page) { [self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response.mealRecipeListResponse{
                    recipeList = []
                    recipeList.append(contentsOf: res)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.recipeListTableView.stopSkeletonAnimation()
                        self.view.hideSkeleton(reloadDataAfter: true, transition: .crossDissolve(0.25))
                        if recipeList.count == 0 {
                            addRecipeToDateView.isHidden = true
                            bottomView.isHidden = true
                            let noDataLabel = UILabel(frame: CGRect(x: 0, y: 50, width: recipeListTableView.bounds.size.width, height: 100))
                            noDataLabel.text = "There are no Recipes."
                            noDataLabel.textColor = UIColor.darkGray
                            noDataLabel.textAlignment = .center
                            noDataLabel.numberOfLines = 0
                            recipeListTableView.backgroundView = noDataLabel
                        } else {
                            addRecipeToDateView.isHidden = false
                            bottomView.isHidden = false
                            recipeListTableView.backgroundView = nil
                        }
                        self.recipeListTableView.reloadData()
                    }
                }
            } failure: { [weak self] (error) in
                guard let strongSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: strongSelf, message: error)
            }
            
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:- meal Delete API
    func recipeDeleteAPI(recipeId:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
           // Utility.showIndecator()
            NutritionService.shared.recipeDelete(id: recipeId){ [self] (statusCode, response) in
                Utility.hideIndicator()
                print(response.message ?? "")
                if let res = response.message {
                    if recipeList.count == 0 {
                        addRecipeToDateView.isHidden = true
                        bottomView.isHidden = true
                        let noDataLabel = UILabel(frame: CGRect(x: 0, y: 50, width: recipeListTableView.bounds.size.width, height: 100))
                        noDataLabel.text = "There are not recipes."
                        noDataLabel.textColor = UIColor.darkGray
                        noDataLabel.textAlignment = .center
                        noDataLabel.numberOfLines = 0
                        recipeListTableView.backgroundView = noDataLabel
                    } else {
                        addRecipeToDateView.isHidden = false
                        bottomView.isHidden = false
                        recipeListTableView.backgroundView = nil
                    }
                    self.recipeListTableView.reloadData()
//                    let alertController = UIAlertController(title: APPLICATION_NAME, message: res, preferredStyle: .alert)
//
//                    // Create the actions
//                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
//                        UIAlertAction in
//                       // self.removeMeal?.title = nil
//                       // self.removeMeal?.image = nil
//                       // self.dismiss(animated: false, completion: { [weak self] in
////                                                self?.removeWorkout?()
////                                            })
//                        self.yourMealListTableView.reloadData()
//                        NSLog("OK Pressed")
//                    }
//                    // Add the actions
//                    alertController.addAction(okAction)
//
//                    // Present the controller
//                    self.present(alertController, animated: true, completion: nil)
                }
                
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}

extension CreateRecipeScreen : hideBackgroundDelegate {
    func hideView() {
        backgroundView.isHidden = true
        
    }
}


extension CreateRecipeScreen : SkeletonTableViewDataSource {
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "ExcercisesCell"
    }
    
    func collectionSkeletonView(_ skeletonView: UITableView, numberOfRowsInSection section: Int) -> Int {
        2
    }
}

extension CreateRecipeScreen : WorkOutDeleteDelegate {
    func WorkOutDelete() {
        yourRecipeListAPI(mealId: selectMeal?.meal_id ?? 0, page: 1)
    }
}
