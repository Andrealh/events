//
//  MyMealPlanAndRecipeScreen.swift
//  FitnessFactory
//
//  Created by iroid on 20/08/21.
//

import UIKit

class MyMealPlanAndRecipeScreen: UIViewController {
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var addMealToDateView: UIView!
    @IBOutlet weak var mealNameLabel: UILabel!
    @IBOutlet weak var MyPlanRecipeListTableview: UITableView!
    
    var recipesList = [MealRecipeListResponse]()
    var breakFastItemList = [StoreMealItemListResponse]()
    var mealId = Int()
    var isFromCalender = Bool()
    var isFromAddMore = Bool()
    var selectMeal : MealListResponse?
    var updateMealItemList:((_ editDataRes:MealListResponse) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "NutritionTableViewCell", bundle: nil)
        MyPlanRecipeListTableview.register(nib, forCellReuseIdentifier: "NutritionTableViewCell")
        let nib2 = UINib(nibName: "itemListCell", bundle: nil)
        MyPlanRecipeListTableview.register(nib2, forCellReuseIdentifier: "itemListCell")
        let nib3 = UINib(nibName: "SelectMealitemCell", bundle: nil)
        MyPlanRecipeListTableview.register(nib3, forCellReuseIdentifier: "SelectMealitemCell")
        mealNameLabel.text = selectMeal?.title
        RecipeListAPI(Id: mealId, Page: 1)
        stroeMealItemList(Id: mealId, Page: 1)
        MyPlanRecipeListTableview.tableFooterView = UIView()
        var frame = CGRect.zero
        frame.size.height = .leastNormalMagnitude
        MyPlanRecipeListTableview.tableHeaderView = UIView(frame: frame)
        if isFromAddMore {
            addMealToDateView.isHidden = false
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onBack(_ sender: UIButton) {
        tabBarController?.tabBar.isHidden = false
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onHome(_ sender: UIButton) {
        tabBarController?.tabBar.isHidden = false
        tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func onAddToDate(_ sender: UIButton) {
        let vc =  STORYBOARD.nutrition.instantiateViewController(withIdentifier: "SelectDateForMealScreen") as! SelectDateForMealScreen
        vc.isFromNutrition = true
        vc.mealId = mealId
        vc.isFromCalender = isFromCalender
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onEdit(_ sender: UIButton) {
        backgroundView.isHidden = false
        let vc =  STORYBOARD.nutrition.instantiateViewController(withIdentifier: "AddMealScreen") as!  AddMealScreen
        vc.modalPresentationStyle = .overFullScreen
        vc.isEdit = true
        vc.mealDetail = selectMeal
        vc.delegate = self
        vc.updateMeal = { [weak self] value in
            print(value)
            self?.selectMeal = value
            self?.mealNameLabel.text = value.title
            self?.updateMealItemList!((self?.selectMeal!)!)
            //self?.navigationController?.popViewController(animated: true)
        }
        vc.dismissRemoveScreen = { [weak self] in
            let vc =  STORYBOARD.nutrition.instantiateViewController(withIdentifier: "RemoveMealScreen") as! RemoveMealScreen
            vc.modalPresentationStyle = .overFullScreen
            vc.mealId = self?.selectMeal?.meal_id ?? 0
            vc.removeMeal = self?.selectMeal
            vc.delegate = self
            vc.removeWorkout =  { [weak self]  in
               // self?.delegate?.WorkOutDelete()
                self?.navigationController?.popViewController(animated: true)
            }
            self?.present(vc, animated: false, completion: nil)
        }
        self.present(vc, animated: true, completion: nil)
    }
}

extension MyMealPlanAndRecipeScreen : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 50))
        let label = UILabel(frame: CGRect(x: 25, y: 0, width: tableView.frame.size.width, height: 50))
        if section == 0{
            if self.recipesList.count != 0{
                label.text = "Recipes"
            }else{
                view.frame.size.height = 0
                label.frame.size.height = 0
            }
            label.font = UIFont(name: "Montserrat-ExtraBold", size: 24)
            label.textColor = UIColor.black
            view.addSubview(label)
            return view
        }else {
//            if self.breakFastItemList.count != 0{
//                label.text = "Items"
//            }else{
//                view.frame.size.height = 0
//                label.frame.size.height = 0
//            }
            let cell = tableView.dequeueReusableCell(withIdentifier: "itemListCell") as! itemListCell
            cell.onAdditem = {[weak self] in
                let controller = STORYBOARD.nutrition.instantiateViewController(withIdentifier:"MealSearchScreen") as! MealSearchScreen
                controller.mealId = self?.mealId ?? 0
                controller.titleName = self?.selectMeal?.title ?? ""
                controller.selectMealItem  = { [weak self] in
                    self?.stroeMealItemList(Id: self?.mealId  ?? 0, Page: 1)
                    }
                //controller.storeMealItemList = self?.breakFastItemList ?? []
                self?.navigationController?.pushViewController(controller, animated: false)
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            if self.recipesList.count == 0{
                return 0
            }
        }else if section == 1{
            if self.breakFastItemList.count == 0{
                return 50
            }
        }
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return recipesList.count
        } else {
            return breakFastItemList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NutritionTableViewCell") as! NutritionTableViewCell
            cell.mealItem = recipesList[indexPath.row]
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SelectMealitemCell") as! SelectMealitemCell
            cell.storeMealItem = breakFastItemList[indexPath.row]
            cell.onRemove = {[weak self] in
                let alertController = UIAlertController(title: "Are you sure?", message: "You want to remove item?", preferredStyle: .alert)
                // Create the actions
                let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) { [self]
                    UIAlertAction in
                    let foodItemId  = self?.breakFastItemList[indexPath.row].meal_item_id ?? 0
                    self?.breakFastItemList.remove(at: indexPath.row)
                    self?.removeMealItem(mealItemId: foodItemId )
                    //self.getNearByGym(category: "", searchText: "")
                    NSLog("OK Pressed")
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default) { [self]
                    UIAlertAction in
                    //self.getNearByGym(category: "", searchText: "")
                    NSLog("OK Pressed")
                }
                // Add the actions
                okAction.setValue(UIColor.red, forKey: "titleTextColor")
                cancelAction.setValue(UIColor.gray, forKey: "titleTextColor")
                alertController.addAction(cancelAction)
                alertController.addAction(okAction)
                // Present the controller
                self?.present(alertController, animated: true, completion: nil)
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            let controller = STORYBOARD.nutrition.instantiateViewController(withIdentifier:"RecipeScreen") as! RecipeScreen
            controller.recipeDetailId = self.recipesList[indexPath.row].meal_recipe_id ?? 0
            controller.isFromMymeal = true
            self.navigationController?.pushViewController(controller, animated: false)
        } else {
            
        }
    }
}

extension MyMealPlanAndRecipeScreen {
    //MARK:- RecipeList
    func RecipeListAPI(Id:Int,Page:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            NutritionService.shared.myRecipesList(Id: Id, Page: Page) { (statusCode, response) in
                Utility.hideIndicator()
                if let res = response.mealRecipeListResponse{
                    self.recipesList = []
                    self.recipesList = res
                    self.MyPlanRecipeListTableview.reloadData()
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func stroeMealItemList(Id:Int,Page:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            NutritionService.shared.storeMealItemList(Id: Id, Page: Page) { (statusCode, response) in
                Utility.hideIndicator()
                if let res = response.storeMealItemListResponse{
                    self.breakFastItemList = []
                    self.breakFastItemList = res
                    self.MyPlanRecipeListTableview.reloadData()
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func MealItemListApi(Id:Int,Page:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            NutritionService.shared.storeMealItemList(Id: Id, Page: Page) { (statusCode, response) in
                Utility.hideIndicator()
                if let res = response.storeMealItemListResponse{
                    self.breakFastItemList = []
                    self.breakFastItemList = res
                    self.MyPlanRecipeListTableview.reloadData()
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func removeMealItem(mealItemId : Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            HomeAddGymService.shared.removeMealItem(ID: mealItemId) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response.message{
                    self?.MyPlanRecipeListTableview.reloadData()
//                        let alertController = UIAlertController(title: APPLICATION_NAME, message: res, preferredStyle: .alert)
//                        // Create the actions
//                        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
//                            UIAlertAction in
//                            //self?.stroeMealItemList(Id: self?.mealId ?? 0, Page: 1)
//                            self?.MyPlanRecipeListTableview.reloadData()
//                            NSLog("OK Pressed")
//                        }
//                        // Add the actions
//                        alertController.addAction(okAction)
//                        // Present the controller
//                        self?.present(alertController, animated: true, completion: nil)
                    }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}


extension MyMealPlanAndRecipeScreen : hideBackgroundDelegate {
    func hideView() {
        backgroundView.isHidden = true
    }
}
