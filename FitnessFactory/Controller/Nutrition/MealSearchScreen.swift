//
//  MealSearchScreen.swift
//  FitnessFactory
//
//  Created by iroid on 23/07/21.
//

import UIKit

class MealSearchScreen: UIViewController {
    
    @IBOutlet weak var titleButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var addMealView: UIView!
    @IBOutlet weak var mealItemTableView: UITableView!

    
    var selectMeal : MealListResponse?
    var delegate:WorkOutDeleteDelegate?
    var isFromCalender = Bool()
    var mealItemList = [MealItemSearchResponse]()
    var storMealItemList = [StoreMealItemListResponse]()
    var selectMealItemList = [MealItemSearchResponse]()
    var itemList = [ItemListResponse]()
    var page = 1
    var hasMorePage : Bool = false
    var searchText = ""
    var meta:MetaData!
    var mealId = Int()
    var titleName = String()
    var selectMealItem : (() -> Void)?
    var isFromNutrition = Bool()
    var isNotDefaultMeal = Bool()
    // var storeMealItemList = [StoreMealItemListResponse]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initalizedDetails()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- initalizedDetails Details
    func initalizedDetails(){
        titleButton.setTitle(selectMeal?.title, for: .normal)
        let nib = UINib(nibName: "MealItemSearchListCell", bundle: nil)
        mealItemTableView.register(nib, forCellReuseIdentifier: "MealItemSearchListCell")
        let nib2 = UINib(nibName: "SelectMealitemCell", bundle: nil)
        mealItemTableView.register(nib2, forCellReuseIdentifier: "SelectMealitemCell")
        if selectMeal?.title != nil {
            titleButton.setTitle(selectMeal?.title, for: .normal)
        } else {
            titleButton.setTitle(titleName, for: .normal)
        }
        mealItemTableView.tableFooterView = UIView()
        searchTextField.addTarget(self, action: #selector(SetView), for: .touchDown)
        if isFromNutrition{
            getItemListApi(Page: 1)
            titleButton.setTitle("Items", for: .normal)
        } else if isNotDefaultMeal {
            addMealView.isHidden = false
            MealItemListApi(Id: mealId, Page: 1)
        }
    }
    
    // MARK: scroll method
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let size = scrollView.contentSize
        let inset = scrollView.contentInset
        let y = offset.y + bounds.size.height - inset.bottom
        let h = size.height
        let reload_distance:CGFloat = 10.0
        if y > (h + reload_distance) {
            print("called")
            if let metaTotal = self.meta?.total_results{
                if mealItemTableView.tag == 1 {
                    if self.mealItemList.count != Int(metaTotal){
                        if self.hasMorePage{
                            self.hasMorePage = false
                            self.page += 1
                            getMealItemList(page: page, searchText: searchText)
                        }
                    }
                } else if mealItemTableView.tag == 0{
                    if self.itemList.count != Int(metaTotal){
                        if self.hasMorePage{
                            self.hasMorePage = false
                            self.page += 1
                            getItemListApi(Page: page)
                        }
                    }
                }
            }
        }
    }
    
    func appendDataCollectionView(data: Response){
        if mealItemTableView.tag == 1 {
            var indexPathArray: [IndexPath] = []
            let res = data.mealItemSearchResponse!
            for i in self.mealItemList.count..<self.mealItemList.count + res.count{
                indexPathArray.append(IndexPath(item: i, section: 0))
            }
            self.mealItemList.append(contentsOf: res)
            self.mealItemTableView.insertRows(at: indexPathArray, with: .automatic)
        } else if mealItemTableView.tag == 0{
            var indexPathArray: [IndexPath] = []
            let res = data.itemListResponse!
            for i in self.itemList.count..<self.itemList.count + res.count{
                indexPathArray.append(IndexPath(item: i, section: 0))
            }
            self.itemList.append(contentsOf: res)
            self.mealItemTableView.insertRows(at: indexPathArray, with: .automatic)
        }
      
    }
    
    func appendDataCollectionView2(data: [ItemListResponse]){
        var indexPathArray: [IndexPath] = []
        for i in self.itemList.count..<self.itemList.count + data.count{
            indexPathArray.append(IndexPath(item: i, section: 0))
        }
        self.itemList.append(contentsOf: data)
        self.mealItemTableView.insertRows(at: indexPathArray, with: .automatic)
    }
    
    @objc func SetView(){
        mealItemTableView.isHidden = true
//        let noDataLabel = UIView(frame: CGRect(x: 0, y: 50, width: mealItemTableView.bounds.width, height: mealItemTableView.bounds.height))
//        mealItemTableView.backgroundView = noDataLabel
    }
    
    @IBAction func onBack(_ sender: UIButton) {
        // tabBarController?.tabBar.isHidden = false
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onCancel(_ sender: UIButton) {
        cancelButton.isHidden = true
        searchButton.isHidden = false
        searchTextField.text = ""
        mealItemTableView.tag = 0
        mealItemList = []
        mealItemTableView.reloadData()
        view.endEditing(true)
    }
    
    @IBAction func onEdit(_ sender: UIButton) {
        backgroundView.isHidden = false
        let vc =  STORYBOARD.nutrition.instantiateViewController(withIdentifier: "AddMealScreen") as!  AddMealScreen
        vc.modalPresentationStyle = .overFullScreen
        vc.isEdit = true
        vc.mealDetail = selectMeal
        vc.delegate = self
        vc.dismissScreen = { [weak self] in
            self?.delegate?.WorkOutDelete()
            self?.navigationController?.popViewController(animated: true)
        }
        vc.dismissRemoveScreen = { [weak self] in
            let vc =  STORYBOARD.nutrition.instantiateViewController(withIdentifier: "RemoveMealScreen") as! RemoveMealScreen
            vc.modalPresentationStyle = .overFullScreen
            vc.mealId = self?.selectMeal?.meal_id ?? 0
            vc.removeWorkout =  { [weak self] in
                self?.delegate?.WorkOutDelete()
                self?.navigationController?.popViewController(animated: true)
            }
            self?.present(vc, animated: false, completion: nil)
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func onAddMealToDate(_ sender: UIButton) {
        let vc =  STORYBOARD.nutrition.instantiateViewController(withIdentifier: "SelectDateForMealScreen") as! SelectDateForMealScreen
        vc.isFromNutrition = true
        vc.mealId = selectMeal?.meal_id ?? 0
        vc.isFromCalender = isFromCalender
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onHome(_ sender: UIButton) {
        tabBarController?.tabBar.isHidden = false
        tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: true)
    }
}

extension MealSearchScreen : hideBackgroundDelegate {
    func hideView() {
        backgroundView.isHidden = true
    }
}

extension MealSearchScreen : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var numOfSections: Int = 0
        if mealItemTableView.tag == 1{
            return mealItemList.count
        } else {
            if isNotDefaultMeal {
                if storMealItemList.count > 0 {
                    numOfSections = storMealItemList.count
                    tableView.backgroundView = nil
                } else {
                        let noDataLabel = UILabel(frame: CGRect(x: 0, y: 50, width: tableView.bounds.size.width, height: 50))
                        noDataLabel.text = "No Meals."
                        noDataLabel.textColor = UIColor.darkGray
                        noDataLabel.textAlignment = .center
                        noDataLabel.numberOfLines = 1
                        tableView.backgroundView = noDataLabel
                }
                return numOfSections
            } else {
                if itemList.count > 0 {
                    numOfSections = itemList.count
                    tableView.backgroundView = nil
                } else {
                    if isFromNutrition {
                        let noDataLabel = UILabel(frame: CGRect(x: 0, y: 50, width: tableView.bounds.size.width, height: 50))
                        noDataLabel.text = "No Items."
                        noDataLabel.textColor = UIColor.darkGray
                        noDataLabel.textAlignment = .center
                        noDataLabel.numberOfLines = 1
                        tableView.backgroundView = noDataLabel
                    }
                }
                return numOfSections
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if  mealItemTableView.tag == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MealItemSearchListCell") as! MealItemSearchListCell
            cell.mealItem = mealItemList[indexPath.row]
            return cell
        } else {
            if isNotDefaultMeal {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SelectMealitemCell") as! SelectMealitemCell
                cell.storeMealItem = storMealItemList[indexPath.row]
                cell.onRemove = {[weak self] in
                    let alertController = UIAlertController(title: "Are you sure?", message: "You want to remove item?", preferredStyle: .alert)
                    // Create the actions
                    let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) { [self]
                        UIAlertAction in
                        let itemId  = self?.storMealItemList[indexPath.row].meal_item_id ?? 0
                        self?.storMealItemList.remove(at: indexPath.row)
                        self?.removeMealItem(mealItemId: itemId)
                        NSLog("OK Pressed")
                    }
                    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default) {
                        UIAlertAction in
                        NSLog("Cancel Pressed")
                    }
                    // Add the actions
                    okAction.setValue(UIColor.red, forKey: "titleTextColor")
                    cancelAction.setValue(UIColor.gray, forKey: "titleTextColor")
                    alertController.addAction(cancelAction)
                    alertController.addAction(okAction)
                    // Present the controller
                    self?.present(alertController, animated: true, completion: nil)
                }
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SelectMealitemCell") as! SelectMealitemCell
                cell.myItem = itemList[indexPath.row]
                cell.onRemove = {[weak self] in
                    let alertController = UIAlertController(title: "Are you sure?", message: "You want to remove item?", preferredStyle: .alert)
                    // Create the actions
                    let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) { [self]
                        UIAlertAction in
                        let itemId  = self?.itemList[indexPath.row].myitem_id ?? 0
                        self?.itemList.remove(at: indexPath.row)
                        self?.removeItem(itemId: itemId)
                        NSLog("OK Pressed")
                    }
                    let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default) {
                        UIAlertAction in
                        NSLog("Cancel Pressed")
                    }
                    // Add the actions
                    okAction.setValue(UIColor.red, forKey: "titleTextColor")
                    cancelAction.setValue(UIColor.gray, forKey: "titleTextColor")
                    alertController.addAction(cancelAction)
                    alertController.addAction(okAction)
                    // Present the controller
                    self?.present(alertController, animated: true, completion: nil)
                }
                return cell
            }
            
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.tag == 1 {
            let selectItem = mealItemList[indexPath.row]
            if isFromNutrition {
                let item = ItemListResponse(myitem_id: Int(selectItem.food_id ?? "" ), food_id: Int(selectItem.food_id ?? ""), title: selectItem.food_name, desc: selectItem.food_description)
                storeItem(selectItem: selectItem, item: item)
            } else if isNotDefaultMeal {
                let item = StoreMealItemListResponse(meal_item_id: Int(selectItem.food_id ?? "" ), food_id: Int(selectItem.food_id ?? ""), title: selectItem.food_name, desc: selectItem.food_description)
                storeMealItem(selectMealItem: selectItem, mealId: mealId, item: item)
            } else {
               // storeMealItem(selectMealItem: selectItem, mealId: mealId)
                storeRecipeItem(selectMealItem: selectItem, mealId: mealId)
            }
        }
    }
}

extension MealSearchScreen {
    func getMealItemList(page:Int,searchText:String){
        //self.view.endEditing(true)
        if Utility.isInternetAvailable(){
           // Utility.showIndecator()
            let data = MealItemSearchListRequest(search: searchText, max_results: 20)
            HomeAddGymService.shared.MealItem(parameters: data.toJSON(), pageNumber: page) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                self?.hasMorePage = true
                self?.mealItemTableView.tag = 1
                self?.mealItemTableView.isHidden = false
                if let res = response.mealItemSearchResponse {
                    if page == 1{
                        self?.mealItemList = []
                        self?.mealItemList = res
                        self?.mealItemTableView.reloadData()
                    } else {
                        self?.appendDataCollectionView(data: response)
                    }
                    if let meta = response.metaDataResponse{
                        self?.meta = meta
                    }
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                stronSelf.hasMorePage = true
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func removeMealItem(mealItemId : Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            HomeAddGymService.shared.removeMealItem(ID: mealItemId) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response.message{
                    self?.mealItemTableView.reloadData()
//                    let alertController = UIAlertController(title: APPLICATION_NAME, message: res, preferredStyle: .alert)
//                    // Create the actions
//                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
//                        UIAlertAction in
//
//                        NSLog("OK Pressed")
//                    }
//                    // Add the actions
//                    alertController.addAction(okAction)
//                    // Present the controller
//                    self?.present(alertController, animated: true, completion: nil)
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                stronSelf.hasMorePage = true
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func removeItem(itemId : Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            HomeAddGymService.shared.removeItem(ID: itemId) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response.message{
                    self?.mealItemTableView.reloadData()
//                    let alertController = UIAlertController(title: APPLICATION_NAME, message: res, preferredStyle: .alert)
//                    // Create the actions
//                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
//                        UIAlertAction in
//
//                        NSLog("OK Pressed")
//                    }
//                    // Add the actions
//                    alertController.addAction(okAction)
//                    // Present the controller
//                    self?.present(alertController, animated: true, completion: nil)
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                stronSelf.hasMorePage = true
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func storeMealItem(selectMealItem:MealItemSearchResponse,mealId:Int,item:StoreMealItemListResponse){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            let data = StoreMealItemRequest(meal_id: mealId, food_id: Int(selectMealItem.food_id ?? ""), title: selectMealItem.food_name, desc: selectMealItem.food_description)
            NutritionService.shared.storeMealItem(parameters: data.toJSON() ) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response?.message{
                    self?.cancelButton.isHidden = true
                    self?.searchButton.isHidden = false
                    self?.searchTextField.text = ""
                    self?.mealItemTableView.tag = 0
                    self?.storMealItemList.append(item)
                    self?.mealItemTableView.reloadData()
//                    self?.selectMealItem!()
//                    self?.navigationController?.popViewController(animated: true)
//                    let alertController = UIAlertController(title: APPLICATION_NAME, message: res, preferredStyle: .alert)
//                    // Create the actions
//                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { [self]
//                        UIAlertAction in
//
//                        self?.navigationController?.popViewController(animated: true)
//
//                       c
//                        NSLog("OK Pressed")
//                    }
//                    // Add the actions
//                    alertController.addAction(okAction)
//                    // Present the controller
//                    self?.present(alertController, animated: true, completion: nil)
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                stronSelf.hasMorePage = true
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func storeRecipeItem(selectMealItem:MealItemSearchResponse,mealId:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            let data = StoreRecipeItemRequest(meal_recipe_id: mealId, food_id:  Int(selectMealItem.food_id ?? ""), title: selectMealItem.food_name, desc: selectMealItem.food_description)
            NutritionService.shared.storRecipeItem(parameters: data.toJSON() ) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response?.message{
                    self?.selectMealItem!()
                    self?.navigationController?.popViewController(animated: true)
//                    let alertController = UIAlertController(title: APPLICATION_NAME, message: res, preferredStyle: .alert)
//                    // Create the actions
//                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { [self]
//                        UIAlertAction in
//
//                        self?.navigationController?.popViewController(animated: true)
//
//                        // self?.mealItemTableView.tag = 0
//                        //self?.searchTextField.text = ""
//                        //self?.addMealView.isHidden = false
//                        // self?.mealItemTableView.reloadData()
//                        NSLog("OK Pressed")
//                    }
//                    // Add the actions
//                    alertController.addAction(okAction)
//                    // Present the controller
//                    self?.present(alertController, animated: true, completion: nil)
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                stronSelf.hasMorePage = true
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func getItemListApi(Page:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            NutritionService.shared.itemList(Page: Page) { [self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response.itemListResponse{
                    if page == 1{
                        itemList = []
                        itemList = res
                        mealItemTableView.reloadData()
                    } else {
                        appendDataCollectionView(data: response)
                    }
                    if let meta2 = response.metaDataResponse{
                        meta = meta2
                    }
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func storeItem(selectItem:MealItemSearchResponse,item:ItemListResponse){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            let data = StoreItemRequest(food_id: Int(selectItem.food_id ?? ""), title: selectItem.food_name, desc: selectItem.food_description)
            NutritionService.shared.storeItem(parameters: data.toJSON() ) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response?.message{
                    self?.cancelButton.isHidden = true
                    self?.searchButton.isHidden = false
                    self?.searchTextField.text = ""
                    self?.mealItemTableView.tag = 0
                    self?.itemList.append(item)
                    self?.mealItemTableView.reloadData()
                    //                    let alertController = UIAlertController(title: APPLICATION_NAME, message: res, preferredStyle: .alert)
                    //                    // Create the actions
                    //                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { [self]
                    //                        UIAlertAction in
                    //                        // self?.selectMealItem!()
                    //                       // self?.navigationController?.popViewController(animated: true)
                    //                         self?.searchTextField.text = ""
                    //                         self?.mealItemTableView.tag = 0
                    //                        //self?.searchTextField.text = ""
                    //                        //self?.addMealView.isHidden = false
                    //                        self?.itemList.append(item)
                    //                         self?.mealItemTableView.reloadData()
                    //                        NSLog("OK Pressed")
                    //                    }
                    //                    // Add the actions
                    //                    alertController.addAction(okAction)
                    //                    // Present the controller
                    //                    self?.present(alertController, animated: true, completion: nil)
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                stronSelf.hasMorePage = true
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func MealItemListApi(Id:Int,Page:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            NutritionService.shared.storeMealItemList(Id: Id, Page: Page) { (statusCode, response) in
                Utility.hideIndicator()
                if let res = response.storeMealItemListResponse{
                    self.storMealItemList = []
                    self.storMealItemList = res
                    self.mealItemTableView.reloadData()
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}



extension MealSearchScreen:UITextFieldDelegate{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        //let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        if searchTextField.text?.count == 0 {
            cancelButton.isHidden = true
            searchButton.isHidden = false
            mealItemTableView.isHidden = false
            mealItemTableView.tag = 0 
            mealItemTableView.reloadData()
        }
//        let updatedString = searchTextField.text
//        searchText = updatedString ?? ""
//        DispatchQueue.main.asyncAfter(deadline: .now() ) {
//            self.searchGymList()
//        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == searchTextField{
            if let text = textField.text as NSString? {
                let txtAfterUpdate = text.replacingCharacters(in: range, with: string)
                searchText = txtAfterUpdate
                print(txtAfterUpdate)
                NSObject.cancelPreviousPerformRequests(withTarget: self,selector: #selector(searchGymList),object: textField)
                self.perform(#selector(searchGymList),with: textField,afterDelay: 0.2)
            }
        }
        return true
    }

    @objc func searchGymList(){
        let aString: String =  searchText
        let newString = aString.replacingOccurrences(of: " ", with: "%20", options:[], range: nil)
        print(newString)
        if newString.count > 0 {
            self.getMealItemList(page: 1, searchText: newString)
        } else {
            mealItemTableView.isHidden = false
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        addMealView.isHidden = true
        cancelButton.isHidden = false
        searchButton.isHidden = true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
    }
}

