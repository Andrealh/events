//
//  MealItelListScreen.swift
//  FitnessFactory
//
//  Created by iroid on 02/07/21.
//

import UIKit

class MealItelListScreen: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var recipesTableView: UITableView!
    @IBOutlet weak var addMealView: UIView!
    
    var mealItemList = [MealItemListResponse]()
    var recipesList = [NutritionListResponse]()
    var id = Int()
    var nutritionId = Int()
    var titleName = String()
    var isFromHome : Bool = false
    var meta:Meta!
    var hasMorePage : Bool = false
    var page = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initalizedDetails()
    }
    
    //MARK:- initalizedDetails Details
    func initalizedDetails(){
        titleLabel.text = titleName
        let recipesNib = UINib(nibName: "NutritionTableViewCell", bundle: nil)
        recipesTableView.register(recipesNib, forCellReuseIdentifier: "NutritionTableViewCell")
        
        //        breakFastItemList = [
        //        BreakFastItem(itemName: "Egg Salad", cal: "49 Cal"),
        //        BreakFastItem(itemName: "Toast", cal: "8 Cal"),
        //        BreakFastItem(itemName: "Orange Juice", cal: "8 Cal"),
        //        BreakFastItem(itemName: "Beans", cal: "8 Cal"),
        //        ]
        
        var frame = CGRect.zero
        frame.size.height = .leastNormalMagnitude
        recipesTableView.tableHeaderView = UIView(frame: frame)
//        if isFromHome {
//            addMealView.isHidden = true
//        } else {
//            addMealView.isHidden = false
//        }
        
        RecipeListAPI(Id: id, Page: 1)
        MealItemListApi(Id: id, Page: 1)
    }
    
    // MARK: scroll method
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let size = scrollView.contentSize
        let inset = scrollView.contentInset
        let y = offset.y + bounds.size.height - inset.bottom
        let h = size.height
        let reload_distance:CGFloat = 10.0
        if y > (h + reload_distance) {
            print("called")
            if let metaTotal = self.meta?.total{
                if self.recipesList.count != metaTotal{
                    if self.hasMorePage{
                        self.hasMorePage = false
                        self.page += 1
                        RecipeListAPI(Id: id, Page: page)
                    }
                }
            }
        }
    }
    
    func appendDataCollectionView(data: [NutritionListResponse]){
        var indexPathArray: [IndexPath] = []
        for i in self.recipesList.count..<self.recipesList.count + data.count{
            indexPathArray.append(IndexPath(item: i, section: 0))
        }
        self.recipesList.append(contentsOf: data)
        self.recipesTableView.insertRows(at: indexPathArray, with: .automatic)
    }
    
    func backScreen(){
        tabBarController?.tabBar.isHidden = false
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: NutritionScreen.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    func backToHome(){
        tabBarController?.tabBar.isHidden = false
//        for controller in self.navigationController!.viewControllers as Array {
//            if controller.isKind(of: HomeScreen.self) {
//                self.navigationController!.popToViewController(controller, animated: true)
//                break
//            }
//        }
        tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func onAddToMyMeal(_ sender: UIButton) {
        addToMyMealAPI(Id: id)
    }
    @IBAction func onBack(_ sender: UIButton) {
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onHome(_ sender: UIButton) {
        tabBarController?.tabBar.isHidden = false
        tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: true)
    }
}

extension MealItelListScreen : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 50))
        let label = UILabel(frame: CGRect(x: 25, y: 0, width: tableView.frame.size.width, height: 50))
        if section == 0{
            if self.recipesList.count != 0{
                label.text = "Recipes"
            }else{
                view.frame.size.height = 0
                label.frame.size.height = 0
                
            }
        }else if section == 1{
            if self.mealItemList.count != 0{
                label.text = "Items"
            }else{
                view.frame.size.height = 0
                label.frame.size.height = 0
            }
        }
        label.font = UIFont(name: "Montserrat-ExtraBold", size: 24)
        label.textColor = UIColor.black
        view.addSubview(label)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            if self.recipesList.count == 0{
                return 0
            }
        }else if section == 1{
            if self.mealItemList.count == 0{
                return 0
            }
        }
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return recipesList.count
        } else {
            return mealItemList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NutritionTableViewCell") as! NutritionTableViewCell
        if indexPath.section == 0 {
            cell.item = recipesList[indexPath.row]
        } else {
            let mealItemDic = mealItemList[indexPath.row]
            cell.caloryLabel.text = mealItemDic.cal
            cell.itemNameLabel.text = mealItemDic.title
        }
        //        if indexPath.row == self.recipesList.count - 1 {
        //
        //        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            let controller = STORYBOARD.nutrition.instantiateViewController(withIdentifier:"RecipeScreen") as! RecipeScreen
            controller.recipeDetailId = self.recipesList[indexPath.row].nutrition_recipe__id ?? 0
            self.navigationController?.pushViewController(controller, animated: false)
        } else {
            
        }
    }
}

extension MealItelListScreen {
    //MARK:- RecipeList
    func RecipeListAPI(Id:Int,Page:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            NutritionService.shared.recipesList(Id: Id, Page: Page) { (statusCode, response) in
                Utility.hideIndicator()
                if let res = response.nutritionListResponse{
                    self.recipesList = []
                    self.recipesList = res
                    self.recipesTableView.reloadData()
                }
                
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func MealItemListApi(Id:Int,Page:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            NutritionService.shared.mealItemList(Id: Id, Page: Page) { (statusCode, response) in
                Utility.hideIndicator()
                if let res = response.mealItemListResponse{
                    self.mealItemList = []
                    self.mealItemList = res
                    self.recipesTableView.reloadData()
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func addToMyMealAPI(Id:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            // selectDateList.map(String.init).joined(separator: ",")
            let data = addToMyMealRequest(nutrition_id: id)
            NutritionService.shared.addToMyMeal(parameters: data.toJSON()) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response?.message {
                    let alertController = UIAlertController(title: "Added!", message: "This meal plan added to your meal", preferredStyle: .alert)
                    // Create the actions
                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                        UIAlertAction in
                        if self!.isFromHome {
                            self?.backToHome()
                        } else {
                            self?.backScreen()
                        }
                        NSLog("OK Pressed")
                    }
                    // Add the actions
                    alertController.addAction(okAction)
                    // Present the controller
                    self?.present(alertController, animated: true, completion: nil)
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}
