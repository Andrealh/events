//
//  SelectDateForMealScreen.swift
//  FitnessFactory
//
//  Created by iroid on 20/07/21.
//

import UIKit
import FSCalendar

class SelectDateForMealScreen: UIViewController, FSCalendarDelegate, FSCalendarDataSource, FSCalendarDelegateAppearance{
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var selectDateLabel: UILabel!
    @IBOutlet weak var calenderView: FSCalendar!
    @IBOutlet weak var monthLabel: UILabel!
    
    var selectDate = String()
    var calenderList : [WorkoutDayResponse] = []
    var workoutSelectDateList : [SelectWorkoutDayResponse] = []
    var workoutselectDateArray = [String]()
    var holidayDayList = [String]()
    var currentDate = String()
    let dateFormatter = DateFormatter()
    var selectDateList = [String]()
    var mealId = Int()
    var isFromNutrition = Bool()
    var isFromCalender = Bool()
    var isFromAddMore = Bool()
    
    fileprivate let formatter: DateFormatter = {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initalizedDetails()
    }
    
    //MARK:- initalizedDetails Details
    func initalizedDetails(){
        calenderView.customizeCalenderAppearance()
        calenderView.delegate = self
        calenderView.dataSource = self
        calenderView.headerHeight = 0
        selectDate = Date().string(format: "yyyy-MM")
        currentDate = Date().string(format: "yyyy-MM-dd")
        dateFormatter.dateFormat = "yyyy-MM"
        calenderListAPI(selectDate: selectDate)
        setMonthName()
        if isFromNutrition {
            selectDateLabel.text = "Select Dates to add this Item"
            mealSelectDateListAPI(id: mealId)
        } else {
            selectDateLabel.text = "Select Dates to add this Set"
            workoutSelectDateListAPI(id: mealId)
        }
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let dateString = self.formatter.string(from: date)
        //Remove timeStamp from date
        if workoutselectDateArray.contains(dateString){
            return #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        } else if dateString == currentDate {
            return UIColor.primaryColour
        } else if self.holidayDayList.contains(dateString) {
            return #colorLiteral(red: 0.9459537864, green: 0.4344328642, blue: 0.4142383933, alpha: 1)
        } else if date.removeTimeStamp!.compare(Date().removeTimeStamp!) == .orderedAscending {
            return UIColor(red: 0.027, green: 0.027, blue: 0.024, alpha: 0.3)
        } else if date.removeTimeStamp!.compare(Date().removeTimeStamp!) == .orderedDescending{
            return .black
        } else {
            return .black
        }
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillDefaultColorFor date: Date) -> UIColor? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let dateString = self.formatter.string(from: date)
        if workoutselectDateArray.contains(dateString){
            return #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        } else {
            return nil
        }
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, borderDefaultColorFor date: Date) -> UIColor? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let dateString = self.formatter.string(from: date)
        if workoutselectDateArray.contains(dateString){
            return .black
        } else {
            return nil
        }
    }
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let dateString = self.formatter.string(from: date)
       
//        else if holidayDayList.contains(dateString) {
//            return false
//        }
        if workoutselectDateArray.contains(dateString){
            return false
        } else if dateString == currentDate {
            return true
        } else if date .compare(Date()) == .orderedAscending {
            return false
        } else  {
            return true
        }
    }
    
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let dateString = self.formatter.string(from: date)

        if workoutselectDateArray.contains(dateString){
        }
        
        guard #available(iOS 13.0, *) else {
            // Code for earlier iOS versions
            if let cell = calendar.cell(for: date, at: monthPosition) {
                let index = calendar.collectionView.indexPath(for: cell as UICollectionViewCell)!
                calendar.collectionView.reloadItems(at: [index])
                selectDateList.append(dateString)
            }
                return
            }
        selectDateList.append(dateString)
    }
    
    func calendar(_ calendar: FSCalendar, didDeselect date: Date, at monthPosition: FSCalendarMonthPosition) {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let dateString = self.formatter.string(from: date)
        if let index = self.selectDateList.firstIndex(where: {$0 == dateString}){
            self.selectDateList.remove(at: index)
        }
    }
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        selectDate = dateFormatter.string(from: calenderView.currentPage)
        setMonthName()
        calenderListAPI(selectDate: selectDate)
    }
    
    func getNextMonth(date:Date)->Date {
        return  Calendar.current.date(byAdding: .month, value: 1, to:date)!
    }
    
    func getPreviousMonth(date:Date)->Date {
        return  Calendar.current.date(byAdding: .month, value: -1, to:date)!
    }
    
    func setMonthName(){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM yyyy"
        monthLabel.text = dateFormatter.string(from: self.calenderView.currentPage)
    }
    
    func backScreen(){
        tabBarController?.tabBar.isHidden = false
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: MyWorkoutScreen.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    func backToCalender(){
        tabBarController?.tabBar.isHidden = false
        tabBarController?.selectedIndex = 3
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func backToNutritionScreen(){
        tabBarController?.tabBar.isHidden = false
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: MyMealScreen.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    @IBAction func onNext(_ sender: UIButton) {
        let nextMonth  = getNextMonth(date: calenderView.currentPage)
        selectDate = dateFormatter.string(from: nextMonth)
        calenderView.setCurrentPage(nextMonth, animated: true)
        setMonthName()
        calenderListAPI(selectDate: selectDate)
    }
    
    @IBAction func onPrevious(_ sender: UIButton) {
        let nextMonth  = getPreviousMonth(date: calenderView.currentPage)
        selectDate = dateFormatter.string(from: nextMonth)
        calenderView.setCurrentPage(nextMonth, animated: true)
        setMonthName()
        calenderListAPI(selectDate: selectDate)
    }
    
    @IBAction func onBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onCurrentDate(_ sender: UIButton) {
        calenderView.setCurrentPage(Date(), animated: true)
     
    }
    
    @IBAction func onHome(_ sender: UIButton) {
        tabBarController?.tabBar.isHidden = false
        tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func onAdd(_ sender: UIButton) {
       // if selectDateList.count > 0{
            if isFromNutrition {
                mealScheduleAPI(mealId: mealId)
            } else {
                workoutScheduleAPI(mealId: mealId)
            }
//        }
//        else{
//            Utility.showAlert(vc: self, message: "Please select date.")
//        }
    }
}

extension SelectDateForMealScreen {
    //MARK:- Login API
    func calenderListAPI(selectDate:String){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            let data = WorkoutListRequest(date: selectDate)
            CalenderService.shared.workoutDay(parameters: data.toJSON()) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response?.workoutDayResponse {
                    self?.calenderList = []
                    self?.holidayDayList = []
                    self?.calenderList.append(contentsOf: res)
                    if self?.calenderList.count ?? 0 > 0 {
                        for i in 0...(self?.calenderList.count ?? 0) - 1 {
                            self?.holidayDayList.append(self?.calenderList[i].date ?? "")
                            //holidayDayList.append(contentsOf: calenderList[i].date ?? "")
                        }
                    }
                    DispatchQueue.main.async
                    {
                        self?.calenderView.reloadData()
                    }
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func workoutSelectDateListAPI(id:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            MyWorkoutService.shared.workoutScheduleDateList(Id: id) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response.selectWorkoutDayresponse {
                    self?.workoutSelectDateList = []
                    self?.workoutselectDateArray = []
                    self?.workoutSelectDateList.append(contentsOf: res)
                    if self?.workoutSelectDateList.count ?? 0 > 0 {
                        for i in 0...(self?.workoutSelectDateList.count ?? 0) - 1 {
                            self?.workoutselectDateArray.append(self?.workoutSelectDateList[i].date ?? "")
                            //holidayDayList.append(contentsOf: calenderList[i].date ?? "")
                        }
                    }
                    DispatchQueue.main.async
                    {
                        self?.calenderView.reloadData()
                    }
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func mealSelectDateListAPI(id:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            NutritionService.shared.mealScheduleDateList(Id: id) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response.selectWorkoutDayresponse {
                    self?.workoutSelectDateList = []
                    self?.workoutselectDateArray = []
                    self?.workoutSelectDateList.append(contentsOf: res)
                    if self?.workoutSelectDateList.count ?? 0 > 0 {
                        for i in 0...(self?.workoutSelectDateList.count ?? 0) - 1 {
                            self?.workoutselectDateArray.append(self?.workoutSelectDateList[i].date ?? "")
                            //holidayDayList.append(contentsOf: calenderList[i].date ?? "")
                        }
                    }
                    DispatchQueue.main.async
                    {
                        self?.calenderView.reloadData()
                    }
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func mealScheduleAPI(mealId:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            let data = MealScheduleRequest(date: selectDateList.joined(separator: ","), meal_id: mealId)
            NutritionService.shared.mealSchedule(parameters: data.toJSON()) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response?.message {
                    let alertController = UIAlertController(title: APPLICATION_NAME, message: "This recipe is added to your Nutrition", preferredStyle: .alert)
                    
                    // Create the actions
                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                        UIAlertAction in
                        if self!.isFromCalender {
                            self?.backToCalender()
                        } else {
                            self?.backToNutritionScreen()
                        }
                        
                        NSLog("OK Pressed")
                    }
                    // Add the actions
                    alertController.addAction(okAction)
                    
                    // Present the controller
                    self?.present(alertController, animated: true, completion: nil)
                    
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func workoutScheduleAPI(mealId:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            let data = workoutScheduleRequest(date: selectDateList.joined(separator: ","), workout_id: mealId)
            MyWorkoutService.shared.workoutSchedule(parameters: data.toJSON()) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response?.message {
                    
                    let alertController = UIAlertController(title: APPLICATION_NAME, message: "This Set is added to selected dates.", preferredStyle: .alert)
                    
                    // Create the actions
                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                        UIAlertAction in
                        if self!.isFromAddMore {
                            self?.backToCalender()
                        } else {
                            self?.backScreen()
                        }
                        NSLog("OK Pressed")
                        //                        self?.tabBarController?.selectedIndex = 1
                        //self?.navigationController?.popViewController(animated: true)
                    }
                    // Add the actions
                    alertController.addAction(okAction)
                    
                    // Present the controller
                    self?.present(alertController, animated: true, completion: nil)
                    
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}
