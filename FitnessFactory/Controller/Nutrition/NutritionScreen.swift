//
//  NutritionScreen.swift
//  FitnessFactory
//
//  Created by iroid on 28/06/21.
//

import UIKit

class NutritionScreen: UIViewController {
    
    @IBOutlet weak var nutritionListTableView: UITableView!
    
    var exerciseNameList = [ExerciseList]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initalizedDetails()
    }
    
    //MARK:- initalizedDetails Details
    func initalizedDetails(){
        exerciseNameList = [
            ExerciseList(title: "My Diet", img: "my_meals"),
            ExerciseList(title: "Grocery Cart", img: "item"),
            ExerciseList(title: "Meal Plans and Recipes", img: "meal_plan"),
            ExerciseList(title: "Snacks and Beverages", img: "snacks"),
            ExerciseList(title: "Calories Intake", img: "calory_intake")
        ]
        let nib = UINib(nibName: "ExerciseCell", bundle: nil)
        nutritionListTableView.register(nib, forCellReuseIdentifier: "ExerciseCell")
        nutritionListTableView.tableFooterView = UIView()
        
    }

    @IBAction func onProfile(_ sender: UIButton) {
        tabBarController?.tabBar.isHidden = true
        let storyBoard: UIStoryboard = UIStoryboard(name: "Profile", bundle: nil)
         let vc = storyBoard.instantiateViewController(withIdentifier: "ProfileScreen") as! ProfileScreen

         let navigationController = self.navigationController


         vc.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Close", style: .plain, target: vc, action: #selector(vc.closeView))
         vc.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: vc, action: nil)
        
         let transition = CATransition()
         transition.duration = 0.2
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.moveIn
        transition.subtype = CATransitionSubtype.fromLeft
         navigationController?.view.layer.add(transition, forKey: nil)
         navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func onHome(_ sender: UIButton) {
        //tabBarController?.tabBar.isHidden = false
        tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: true)
    }
}

extension NutritionScreen : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return exerciseNameList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExerciseCell") as! ExerciseCell
        cell.item = exerciseNameList[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            //tabBarController?.tabBar.isHidden = true
            let controller = STORYBOARD.nutrition.instantiateViewController(withIdentifier:"MyMealScreen") as! MyMealScreen
            controller.isFromMyMeal = true
            self.navigationController?.pushViewController(controller, animated: true)
         
        } else if indexPath.row == 4 {
            //tabBarController?.tabBar.isHidden = true
            let controller = STORYBOARD.calIntake.instantiateViewController(withIdentifier:"CalIntakeScreen") as! CalIntakeScreen
            self.navigationController?.pushViewController(controller, animated: true)
        } else if indexPath.row == 1 {
            //tabBarController?.tabBar.isHidden = true
            let controller = STORYBOARD.nutrition.instantiateViewController(withIdentifier:"MealSearchScreen") as! MealSearchScreen
            controller.isFromNutrition = true
            self.navigationController?.pushViewController(controller, animated: true)
        } else {
            //tabBarController?.tabBar.isHidden = true
            let controller = STORYBOARD.nutrition.instantiateViewController(withIdentifier:"PickNutritionScreen") as! PickNutritionScreen
            controller.pickWorkoutId = indexPath.row - 1
            controller.titleName = exerciseNameList[indexPath.row].title ?? ""
            self.navigationController?.pushViewController(controller, animated: true)
        }
//        tabBarController?.tabBar.isHidden = true
//        let controller = STORYBOARD.nutrition.instantiateViewController(withIdentifier:"PickNutritionScreen") as! PickNutritionScreen
//        controller.pickWorkoutId = indexPath.row
//        controller.titleName = exerciseNameList[indexPath.row].title ?? ""
//        self.navigationController?.pushViewController(controller, animated: true)
    }
}
