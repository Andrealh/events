//
//  PickNutritionScreen.swift
//  FitnessFactory
//
//  Created by iroid on 07/07/21.
//

import UIKit
import SkeletonView

class PickNutritionScreen: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var pickNutritionTableView: UITableView!
    
    var pickNutritionList = [PickNutrtionResponse]()
    var pickWorkoutId = Int()
    var titleName = String()
    var meta:Meta!
    var hasMorePage : Bool = false
    var page = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initalizedDetails()
    }
    
    //MARK:- initalizedDetails Details
    func initalizedDetails(){
        let nib = UINib(nibName: "ExcercisesCell", bundle: nil)
        pickNutritionTableView.register(nib, forCellReuseIdentifier: "ExcercisesCell")
        pickNutritionTableView.tableFooterView = UIView()
        pickNutritionTableView.rowHeight = 80
        pickNutritionTableView.estimatedRowHeight = 80
        pickNutritionTableView.isSkeletonable = true
        let gradient = SkeletonGradient(baseColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.1310794454))
        pickNutritionTableView.showAnimatedGradientSkeleton(usingGradient: gradient, animation: nil, transition: .crossDissolve(0.5))
        titleLabel.text = titleName
        pickNutritionAPI(Id: pickWorkoutId, Page: 1)
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        tabBarController?.tabBar.isHidden = false
//    }
//   
//    override func viewDidDisappear(_ animated: Bool) {
//        tabBarController?.tabBar.isHidden = true
//    }
    
    // MARK: scroll method
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let size = scrollView.contentSize
        let inset = scrollView.contentInset
        let y = offset.y + bounds.size.height - inset.bottom
        let h = size.height
        let reload_distance:CGFloat = 10.0
        if y > (h + reload_distance) {
            print("called")
            if let metaTotal = self.meta?.total{
                if self.pickNutritionList.count != metaTotal{
                    if self.hasMorePage{
                        self.hasMorePage = false
                        self.page += 1
                        pickNutritionAPI(Id: pickWorkoutId, Page: page)
                    }
                }
            }
        }
    }
    
    func appendDataCollectionView(data: [PickNutrtionResponse]){
        var indexPathArray: [IndexPath] = []
        for i in self.pickNutritionList.count..<self.pickNutritionList.count + data.count{
            indexPathArray.append(IndexPath(item: i, section: 0))
        }
        self.pickNutritionList.append(contentsOf: data)
        self.pickNutritionTableView.insertRows(at: indexPathArray, with: .automatic)
    }
    
    @IBAction func onBack(_ sender: UIButton) {
        // tabBarController?.tabBar.isHidden = false
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onHome(_ sender: UIButton) {
        //tabBarController?.tabBar.isHidden = false
        tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: true)
    }
    
}

extension PickNutritionScreen : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pickNutritionList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExcercisesCell") as! ExcercisesCell
        cell.pickNutrition = pickNutritionList[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tabBarController?.tabBar.isHidden = true
        let controller = STORYBOARD.nutrition.instantiateViewController(withIdentifier:"MealItelListScreen") as! MealItelListScreen
        controller.id = pickNutritionList[indexPath.row].nutrition_id ?? 0
        controller.titleName = pickNutritionList[indexPath.row].title ?? ""
        controller.isFromHome = false
        navigationController?.pushViewController(controller, animated: true)
    }
}

extension PickNutritionScreen {
    //MARK:- RecipeList
    func pickNutritionAPI(Id:Int,Page:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
           // Utility.showIndecator()
            NutritionService.shared.nutritionTypeList(Id: Id,Page: Page) { [self] (statusCode, response) in
                Utility.hideIndicator()
                hasMorePage = true
                if let res = response.pickNutritionResponse{
                    if page == 1 {
                        self.pickNutritionList = res
                        print(self.pickNutritionList.toJSON())
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            self.pickNutritionTableView.stopSkeletonAnimation()
                            self.view.hideSkeleton(reloadDataAfter: true, transition: .crossDissolve(0.25))
                            self.pickNutritionTableView.reloadData()
                        }
                    } else {
                        self.appendDataCollectionView(data: res)
                    }
                    if let meta = response.metaResponse{
                        self.meta = meta
                    }
                }
                
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}

extension PickNutritionScreen : SkeletonTableViewDataSource {
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "ExcercisesCell"
    }
    
    func collectionSkeletonView(_ skeletonView: UITableView, numberOfRowsInSection section: Int) -> Int {
        2
    }
}
