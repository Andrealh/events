//
//  EditMealScreen.swift
//  FitnessFactory
//
//  Created by iroid on 17/09/21.
//

import UIKit

class EditMealScreen: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var selectView: UIView!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var editView: UIView!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var removeButton: UIButton!
    
    var dismissRemoveScreen : (() -> Void)?
    var mealDetail : MealListResponse?
    var editRecipe : MealRecipeListResponse?
    var delegate: hideBackgroundDelegate?
    var updateMeal:((_ editDataRes:MealListResponse) -> Void)?
    var updateRecipe:((_ editDataRes:MealRecipeListResponse) -> Void)?
    var createMeal:((_ createMeal:MealListResponse) -> Void)?
    var tap = UITapGestureRecognizer()
    var isEdit = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initalizedDetails()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- initalizedDetails Details
    func initalizedDetails(){
        editView.roundCorners(corners: [.layerMinXMinYCorner, .layerMaxXMinYCorner], radius: 52)
        if #available(iOS 13.0, *) {
            editView.layer.cornerCurve = .continuous
        } else {
            // Fallback on earlier versions
        }
        if isEdit {
           // titleLabel.text = "Edit meal"
           // removeButton.isHidden = false
            titleTextField.text = editRecipe?.title
            if editRecipe?.image != nil && editRecipe?.image != defaultRecipeImage{
                selectView.isHidden = true
                Utility.setImage(editRecipe?.image, imageView: imageView)
            } else {
                selectView.isHidden = false
            }
            descriptionTextView.text = editRecipe?.desc
        } else {
          //  titleLabel.text = "Add meal"
           // removeButton.isHidden = true
            titleTextField.text = mealDetail?.title
            descriptionTextView.text = mealDetail?.desc
            if mealDetail?.image != nil {
                selectView.isHidden = true
                if mealDetail?.image != defaultMealImage {
                    Utility.setImage(mealDetail?.image, imageView: imageView)
                } else {
                    selectView.isHidden = false
                }
            } else {
                selectView.isHidden = false
            }
        }
       
        
        tap = UITapGestureRecognizer(target: self, action: #selector(dismissView))
        backgroundView.addGestureRecognizer(tap)
    }
    
    func openCamera(){
        let imagePicker = UIImagePickerController()
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            print("Button capture")
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true//false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func openGallery(){
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker,animated: true,completion: nil)
    }
    
    func checkValidation() -> String?{
        if self.titleTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter your title meal"
        }
        return nil
    }
    
    @objc func dismissView() {
        delegate?.hideView()
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onSave(_ sender: UIButton) {
        self.view.endEditing(true)
        if let error = self.checkValidation(){
            Utility.showAlert(vc: self, message: error)
        } else {
            if isEdit {
                editRecipe(recipeId: editRecipe?.meal_recipe_id ?? 0)
            } else {
                editMeal(id: mealDetail?.meal_id ?? 0)
            }
            
        }
    }
    
    @IBAction func onAddPhoto(_ sender: UIButton) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        // Create the actions
        let takePhoto = UIAlertAction(title: "Take photo", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openCamera()
        }
        let galleryPhoto = UIAlertAction(title: "Photo Library", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openGallery()
        }

        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        // Add the actions
        alertController.addAction(takePhoto)
        alertController.addAction(galleryPhoto)
        alertController.addAction(cancelAction)
//        alertController.addAction(fitnessPhoto)
//        alertController.addAction(gif)
        //   Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func onRemove(_ sender: UIButton) {
        self.view.endEditing(true)
        self.dismiss(animated: false, completion: { [weak self] in
            self?.dismissRemoveScreen?()
        })
    }
}


extension EditMealScreen :UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else {
            return
        }
        imageView.image = pickedImage
        //        profileImageView.layer.borderWidth = 1.5
        //        editIconImage.isHidden = false
        //        plusIImageView.isHidden = true
        selectView.isHidden = true
        self.dismiss(animated: true, completion: { () -> Void in
        })
    }
}

extension EditMealScreen {
    func editMeal(id:Int){
            self.view.endEditing(true)
            if Utility.isInternetAvailable(){
                Utility.showIndecator()
                let data = EditMealRequest(title: titleTextField.text, id: String(id), desc: descriptionTextView.text)
                    var imageData:Data?
                    if(imageView.image != nil){
                        imageData = imageView.image!.jpegData(compressionQuality:0.4)!
                    }
                    NutritionService.shared.editMeal(imageData: imageData,parameters: data.toJSON()) { [weak self] (statusCode, response) in
                        self?.mealDetail?.image = response?.selectmeal?.image
                        self?.mealDetail?.title = response?.selectmeal?.title
                        self?.mealDetail?.desc = response?.selectmeal?.desc
                        Utility.hideIndicator()
                        if let res = response?.selectmeal{
                            print(res.toJSON())
                            self?.backgroundView.removeGestureRecognizer(self!.tap)
                            self?.updateMeal!(res)
                            self?.delegate?.hideView()
                            self?.dismiss(animated: true, completion: {
                            })
                        }
                    } failure: { [weak self] (error) in
                        guard let stronSelf = self else { return }
                        Utility.hideIndicator()
                        Utility.showAlert(vc: stronSelf, message: error)
                    }
            } else {
                Utility.hideIndicator()
                Utility.showNoInternetConnectionAlertDialog(vc: self)
            }
        }
    
    func editRecipe(recipeId:Int){
            self.view.endEditing(true)
            if Utility.isInternetAvailable(){
                Utility.showIndecator()
                let data = EditRecipeRequest(title: titleTextField.text, id: String(recipeId), desc: descriptionTextView.text, recipe: "")
                    var imageData:Data?
                    if(imageView.image != nil){
                        imageData = imageView.image!.jpegData(compressionQuality:0.4)!
                    }
                    NutritionService.shared.editRecipe(imageData: imageData,parameters: data.toJSON()) { [weak self] (statusCode, response) in
                        self?.editRecipe?.image = response?.recipeDetailResponse?.image
                        self?.editRecipe?.title = response?.recipeDetailResponse?.title
                        self?.editRecipe?.desc = response?.recipeDetailResponse?.desc
                        Utility.hideIndicator()
                        if let res = response?.selectrecipe{
                            print(res.toJSON())
                            self?.backgroundView.removeGestureRecognizer(self!.tap)
                            self?.updateRecipe!(res)
                            self?.delegate?.hideView()
                            self?.dismiss(animated: true, completion: {
                            
                            })
                        }
                    } failure: { [weak self] (error) in
                        guard let stronSelf = self else { return }
                        Utility.hideIndicator()
                        Utility.showAlert(vc: stronSelf, message: error)
                    }
            }else{
                Utility.hideIndicator()
                Utility.showNoInternetConnectionAlertDialog(vc: self)
            }
        }
}
