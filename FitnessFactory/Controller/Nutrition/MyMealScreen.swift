//
//  MyMealScreen.swift
//  FitnessFactory
//
//  Created by iroid on 16/07/21.
//

import UIKit
import SkeletonView

class MyMealScreen: UIViewController {
    
    @IBOutlet weak var yourMealListTableView: UITableView!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var createOwnWorkoutButton: UIButton!
    
    var yourMealList = [MealListResponse]()
    var isFromCalender = Bool()
    var isFromMyMeal = Bool()
    var isFormAddMore = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initalizedDetails()
    }
    
   
//    override func viewDidDisappear(_ animated: Bool) {
//        tabBarController?.tabBar.isHidden = true
//    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        createOwnWorkoutButton.layoutIfNeeded()
        createOwnWorkoutButton.addLineDashedStroke(pattern: [12,6], radius: 35, color: UIColor.primaryColour.cgColor)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = false
        backgroundView.isHidden = true
        if let index = self.yourMealList.firstIndex(where: {$0.title == nil && $0.image == nil}){
            self.yourMealList.remove(at: index)
        }
        yourMealListTableView.reloadData()
    }
    
    //MARK:- initalizedDetails Details
    func initalizedDetails(){
        let nib = UINib(nibName: "ExcercisesCell", bundle: nil)
        yourMealListTableView.register(nib, forCellReuseIdentifier: "ExcercisesCell")
        yourMealListTableView.tableFooterView = UIView()
        yourMealListTableView.rowHeight = 80
        yourMealListTableView.estimatedRowHeight = 80
        yourMealListTableView.isSkeletonable = true
        let gradient = SkeletonGradient(baseColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.1310794454))
        yourMealListTableView.showAnimatedGradientSkeleton(usingGradient: gradient, animation: nil, transition: .crossDissolve(0.5))
        yourMealListAPI(search: "", page: 1, isShow: false)
        //        view.addGestureRecognizer(tap)
    }
  
    @IBAction func onCreateMeal(_ sender: UIButton) {
        backgroundView.isHidden = false
        let vc =  STORYBOARD.nutrition.instantiateViewController(withIdentifier: "AddMealScreen") as! AddMealScreen
        vc.modalPresentationStyle = .overFullScreen
        vc.isEdit = false
        vc.delegate = self
        vc.createMeal = {[weak self] value in
            let controller = STORYBOARD.nutrition.instantiateViewController(withIdentifier:"CreateRecipeScreen") as! CreateRecipeScreen
            controller.selectMeal = value
            controller.onback = {[weak self] in
                self?.yourMealListAPI(search: "", page: 1, isShow: true)
            }
            self?.navigationController?.pushViewController(controller, animated: true)
        }
        self.present(vc, animated: true, completion: nil)
       
    }
    @IBAction func onBack(_ sender: UIButton) {
       // tabBarController?.tabBar.isHidden = false
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onHome(_ sender: UIButton) {
       // tabBarController?.tabBar.isHidden = false
        tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: true)
    }
}

extension MyMealScreen : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        var numberOfScetion : Int = 0
//        if yourMealList.count > 0 {
//            numberOfScetion = yourMealList.count
//            tableView.backgroundView = nil
//        } else {
//            let noDataLabel = UILabel(frame: CGRect(x: 0, y: 50, width: tableView.bounds.size.width, height: 100))
//            noDataLabel.text = "There are no Meal,please Create"
//            noDataLabel.textColor = UIColor.darkGray
//            noDataLabel.textAlignment = .center
//            noDataLabel.numberOfLines = 0
//            tableView.backgroundView = noDataLabel
//        }
        return yourMealList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExcercisesCell") as! ExcercisesCell
        cell.yourMealList = yourMealList[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller = STORYBOARD.nutrition.instantiateViewController(withIdentifier:"CreateRecipeScreen") as! CreateRecipeScreen
        controller.selectMeal = yourMealList[indexPath.row]
        controller.delegate = self
        self.navigationController?.pushViewController(controller, animated: true)
//        if yourMealList[indexPath.row].is_admin_meal == "0" {
//            let controller = STORYBOARD.nutrition.instantiateViewController(withIdentifier:"MealSearchScreen") as! MealSearchScreen
//            controller.selectMeal = yourMealList[indexPath.row]
//            controller.mealId = yourMealList[indexPath.row].meal_id ?? 0
//            controller.delegate = self
//            controller.isNotDefaultMeal = true
//            self.navigationController?.pushViewController(controller, animated: true)
//        } else {
//            tabBarController?.tabBar.isHidden = true
//            let controller = STORYBOARD.nutrition.instantiateViewController(withIdentifier:"MyMealPlanAndRecipeScreen") as! MyMealPlanAndRecipeScreen
//            controller.mealId = yourMealList[indexPath.row].meal_id ?? 0
//            controller.selectMeal = yourMealList[indexPath.row]
//            controller.isFromCalender = isFromCalender
//            controller.isFromAddMore = isFormAddMore
//            controller.updateMealItemList =  { [weak self] value in
//                print(value)
//                self?.yourMealList[indexPath.row] = value
//                //self?.yourMealListTableView.reloadRows(at: position, with: .automatic)
//                //self?.navigationController?.popViewController(animated: true)
//            }
//            self.navigationController?.pushViewController(controller, animated: true)
//
//        }
        //tabBarController?.tabBar.isHidden = true
        //        let controller = STORYBOARD.nutrition.instantiateViewController(withIdentifier:"MealSearchScreen") as! MealSearchScreen
        //        controller.selectMeal = yourMealList[indexPath.row]
        //        controller.delegate = self
        //        controller.isFromCalender = isFromCalender
        //        self.navigationController?.pushViewController(controller, animated: true)
        
        
        //        controller.selectMeal = yourMealList[indexPath.row]
        //        controller.delegate = self
        //        controller.isFromCalender = isFromCalender
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            let alertController = UIAlertController(title: "Are you sure?", message: "You want to delete this meal?", preferredStyle: .alert)
            
            // Create the actions
            let okAction = UIAlertAction(title: "Yes, delete it!", style: UIAlertAction.Style.default) { [self]
                UIAlertAction in
                let selectMeal = yourMealList[indexPath.row]
                yourMealList.remove(at: indexPath.row)
                mealDeleteAPI(mealId: selectMeal.meal_id ?? 0)
                //self.getNearByGym(category: "", searchText: "")
                NSLog("OK Pressed")
            }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default) { [self]
                UIAlertAction in
                //self.getNearByGym(category: "", searchText: "")
                NSLog("OK Pressed")
            }
            // Add the actions
            okAction.setValue(UIColor.red, forKey: "titleTextColor")
            cancelAction.setValue(UIColor.gray, forKey: "titleTextColor")
            alertController.addAction(cancelAction)
            alertController.addAction(okAction)
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
           
        }
    }
}

extension MyMealScreen {
    //MARK:- Your Meal List API
    func yourMealListAPI(search:String,page:Int,isShow:Bool){
        if Utility.isInternetAvailable(){
            if isShow {
                Utility.showIndecator()
            }
            let data = YourMealListRequest(search: search)
            NutritionService.shared.yourMealList(Page: page, parameters: data.toJSON()) { [self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response?.yourMealListResponse{
                    yourMealList = []
                    yourMealList.append(contentsOf: res)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.yourMealListTableView.stopSkeletonAnimation()
                        self.view.hideSkeleton(reloadDataAfter: true, transition: .crossDissolve(0.25))
                        if yourMealList.count == 0 {
                            let noDataLabel = UILabel(frame: CGRect(x: 0, y: 50, width: yourMealListTableView.bounds.size.width, height: 100))
                            noDataLabel.text = "There are no Meals."
                            noDataLabel.textColor = UIColor.darkGray
                            noDataLabel.textAlignment = .center
                            noDataLabel.numberOfLines = 0
                            yourMealListTableView.backgroundView = noDataLabel
                        } else {
                            yourMealListTableView.backgroundView = nil
                        }
                        self.yourMealListTableView.reloadData()
                    }
                }
            } failure: { [weak self] (error) in
                guard let strongSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: strongSelf, message: error)
            }
            
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:- meal Delete API
    func mealDeleteAPI(mealId:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
           // Utility.showIndecator()
            NutritionService.shared.mealDelete(id: mealId){ [self] (statusCode, response) in
                Utility.hideIndicator()
                print(response.message ?? "")
                if let res = response.message {
                    if yourMealList.count == 0 {
                        let noDataLabel = UILabel(frame: CGRect(x: 0, y: 50, width: yourMealListTableView.bounds.size.width, height: 100))
                        noDataLabel.text = "There are no Meals."
                        noDataLabel.textColor = UIColor.darkGray
                        noDataLabel.textAlignment = .center
                        noDataLabel.numberOfLines = 0
                        yourMealListTableView.backgroundView = noDataLabel
                    } else {
                        yourMealListTableView.backgroundView = nil
                    }
                    self.yourMealListTableView.reloadData()
//                    let alertController = UIAlertController(title: APPLICATION_NAME, message: res, preferredStyle: .alert)
//
//                    // Create the actions
//                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
//                        UIAlertAction in
//                       // self.removeMeal?.title = nil
//                       // self.removeMeal?.image = nil
//                       // self.dismiss(animated: false, completion: { [weak self] in
////                                                self?.removeWorkout?()
////                                            })
//                        self.yourMealListTableView.reloadData()
//                        NSLog("OK Pressed")
//                    }
//                    // Add the actions
//                    alertController.addAction(okAction)
//
//                    // Present the controller
//                    self.present(alertController, animated: true, completion: nil)
                }
                
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}

extension MyMealScreen: WorkOutDeleteDelegate {
    func WorkOutDelete() {
        yourMealListAPI(search: "" , page:1, isShow: true)
    }
}

extension MyMealScreen : hideBackgroundDelegate {
    func hideView() {
        backgroundView.isHidden = true
    }
}

extension MyMealScreen : SkeletonTableViewDataSource {
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "ExcercisesCell"
    }
    
    func collectionSkeletonView(_ skeletonView: UITableView, numberOfRowsInSection section: Int) -> Int {
        2
    }
}
