//
//  AddRecipeStepScreen.swift
//  FitnessFactory
//
//  Created by iroid on 23/09/21.
//

import UIKit
import IQKeyboardManagerSwift

class AddRecipeStepScreen: UIViewController {

    @IBOutlet weak var backGroundView: UIView!
    @IBOutlet weak var recipeStepView: dateSportView!
    @IBOutlet weak var recipeStepTextView: UITextView!
    @IBOutlet weak var removeButton: UIButton!
    
    var tap = UITapGestureRecognizer()
    var delegate: hideBackgroundDelegate?
    var editRecipe : MealRecipeListResponse?
    var updateRecipe:((_ editDataRes:MealRecipeListResponse) -> Void)?
    var imageView = UIImageView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initalizedDetails()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = true
    }
    
    //MARK:- initalizedDetails Details
    func initalizedDetails(){
        IQKeyboardManager.shared.enable = false
        recipeStepView.roundCorners(corners: [.layerMinXMinYCorner, .layerMaxXMinYCorner], radius: 52)
        if #available(iOS 13.0, *) {
            recipeStepView.layer.cornerCurve = .continuous
        } else {
            // Fallback on earlier versions
        }
        if editRecipe?.recipe != nil {
            recipeStepTextView.text = editRecipe?.recipe
            removeButton.isHidden = false
        } else {
            removeButton.isHidden = true
        }
        imageView.image = UIImage(named: editRecipe?.image ?? "")
        tap = UITapGestureRecognizer(target: self, action: #selector(dismissView))
        backGroundView.addGestureRecognizer(tap)
    }

    @objc func dismissView() {
        delegate?.hideView()
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClose(_ sender: Any) {
        delegate?.hideView()
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onSave(_ sender: UIButton) {
        editRecipe(recipeId: editRecipe?.meal_recipe_id ?? 0)
    }
    
    @IBAction func onRemoveRecipe(_ sender: UIButton) {
        recipeStepTextView.text = ""
        editRecipe(recipeId: editRecipe?.meal_recipe_id ?? 0)
    }
}

extension AddRecipeStepScreen {
    func editRecipe(recipeId:Int){
            self.view.endEditing(true)
            if Utility.isInternetAvailable(){
                Utility.showIndecator()
                let data = EditRecipeRequest(title: editRecipe?.title, id: String(recipeId), desc: editRecipe?.desc, recipe: recipeStepTextView.text)
                    var imageData:Data?
                if(imageView.image != nil){
                        imageData = imageView.image!.jpegData(compressionQuality:0.4)!
                    }
                    NutritionService.shared.editRecipe(imageData: imageData,parameters: data.toJSON()) { [weak self] (statusCode, response) in
                        Utility.hideIndicator()
                        self?.editRecipe?.recipe = response?.selectrecipe?.recipe
                        if let res = response?.selectrecipe{
                            print(res.toJSON())
                            self?.backGroundView.removeGestureRecognizer(self!.tap)
                            self?.updateRecipe!(res)
                            self?.delegate?.hideView()
                            self?.dismiss(animated: true, completion: {
                            })
                        }
                    } failure: { [weak self] (error) in
                        guard let stronSelf = self else { return }
                        Utility.hideIndicator()
                        Utility.showAlert(vc: stronSelf, message: error)
                    }
            } else { 
                Utility.hideIndicator()
                Utility.showNoInternetConnectionAlertDialog(vc: self)
            }
        }
}
