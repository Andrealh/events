//
//  RemoveMealScreen.swift
//  FitnessFactory
//
//  Created by iroid on 20/07/21.
//

import UIKit

class RemoveMealScreen: UIViewController {
    
    @IBOutlet weak var addWorkoutView: UIView!
    @IBOutlet weak var backGroundView: UIView!
    
    var tap = UITapGestureRecognizer()
    var mealId = Int()
    var removeWorkout : (() -> Void)?
    var removeMeal : MealListResponse?
    var removeRecipe : NutritionRecipeListResponse?
    var delegate: hideBackgroundDelegate?
    var isDeleteRecipe = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initalizedDetails()
        // Do any additional setup after loading the view.
    }
    
    @objc func dismissView() {
        dismiss(animated: true, completion: nil)
        delegate?.hideView()
    }
    
    //MARK:- initalizedDetails Details
    func initalizedDetails(){
        addWorkoutView.roundCorners(corners: [.layerMinXMinYCorner, .layerMaxXMinYCorner], radius: 52)
        if #available(iOS 13.0, *) {
            addWorkoutView.layer.cornerCurve = .continuous
        } else {
            // Fallback on earlier versions
        }
        tap = UITapGestureRecognizer(target: self, action: #selector(dismissView))
        backGroundView.addGestureRecognizer(tap)
    }
    
    @IBAction func onYes(_ sender: UIButton) {
        if isDeleteRecipe {
            recipeDeleteAPI(recipeId: mealId)
        } else {
            mealDeleteAPI(mealId: mealId)
        }
        
    }
    
    @IBAction func onNo(_ sender: UIButton) {
        delegate?.hideView()
        self.dismiss(animated: false, completion: nil)
    }
}

extension RemoveMealScreen {
    //MARK:- meal Delete API
    func mealDeleteAPI(mealId:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            NutritionService.shared.mealDelete(id: mealId){ (statusCode, response) in
                Utility.hideIndicator()
                print(response.message ?? "")
                if let res = response.message {
                    self.removeMeal?.title = nil
                    self.removeMeal?.image = nil
                    self.dismiss(animated: true, completion: { [weak self] in
                                            self?.removeWorkout?()
                                        })
//                    let alertController = UIAlertController(title: APPLICATION_NAME, message: res, preferredStyle: .alert)
//
//                    // Create the actions
//                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
//                        UIAlertAction in
//                        self.removeMeal?.title = nil
//                        self.removeMeal?.image = nil
//                        self.dismiss(animated: true, completion: { [weak self] in
//                                                self?.removeWorkout?()
//                                            })
//
//                        NSLog("OK Pressed")
//                    }
//
//
//                    // Add the actions
//                    alertController.addAction(okAction)
//
//                    // Present the controller
//                    self.present(alertController, animated: true, completion: nil)
                }
                
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    
    //MARK:- meal Delete API
    func recipeDeleteAPI(recipeId:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
           // Utility.showIndecator()
            NutritionService.shared.recipeDelete(id: recipeId){ [self] (statusCode, response) in
                Utility.hideIndicator()
                print(response.message ?? "")
                if let res = response.message {
                    self.removeRecipe?.title = nil
                    self.removeRecipe?.image = nil
                    self.dismiss(animated: true, completion: { [weak self] in
                                            self?.removeWorkout?()
                                        })
//                    let alertController = UIAlertController(title: APPLICATION_NAME, message: res, preferredStyle: .alert)
//
//                    // Create the actions
//                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
//                        UIAlertAction in
//                       // self.removeMeal?.title = nil
//                       // self.removeMeal?.image = nil
//                       // self.dismiss(animated: false, completion: { [weak self] in
////                                                self?.removeWorkout?()
////                                            })
//                        self.yourMealListTableView.reloadData()
//                        NSLog("OK Pressed")
//                    }
//                    // Add the actions
//                    alertController.addAction(okAction)
//
//                    // Present the controller
//                    self.present(alertController, animated: true, completion: nil)
                }
                
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}

