//
//  AddMealScreen.swift
//  FitnessFactory
//
//  Created by iroid on 20/07/21.
//

import UIKit

class AddMealScreen: UIViewController {

    @IBOutlet weak var addWorkoutView: UIView!
    @IBOutlet weak var backGroundView: UIView!
    @IBOutlet weak var selectImageView: UIView!
    @IBOutlet weak var selectView: UIView!
    
    @IBOutlet weak var removeButton: UIButton!
    
    @IBOutlet weak var mealImageview: UIImageView!
    
    @IBOutlet weak var titleTextField: UITextField!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
        
    var tap = UITapGestureRecognizer()
    var isEdit = Bool()
    var dismissScreen : (() -> Void)?
    var dismissRemoveScreen : (() -> Void)?
    var mealDetail : MealListResponse?
    var delegate: hideBackgroundDelegate?
    var id = Int()
    var updateMeal:((_ editDataRes:MealListResponse) -> Void)?
    var createMeal:((_ createMeal:MealListResponse) -> Void)?
    var isAddRecipe = Bool()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        initalizedDetails()
    }
    
    //MARK:- initalizedDetails Details
    func initalizedDetails(){
        addWorkoutView.roundCorners(corners: [.layerMinXMinYCorner, .layerMaxXMinYCorner], radius: 52)
        if #available(iOS 13.0, *) {
            addWorkoutView.layer.cornerCurve = .continuous
        } else {
            // Fallback on earlier versions
        }
        tap = UITapGestureRecognizer(target: self, action: #selector(dismissView))
        backGroundView.addGestureRecognizer(tap)
        
        if isEdit{
            titleTextField.text = mealDetail?.title
            if mealDetail?.image != nil {
                selectView.isHidden = true
                Utility.setImage(mealDetail?.image, imageView: mealImageview)
            } else {
                selectView.isHidden = false
            }
            selectImageView.isHidden = false
            removeButton.isHidden = false
            titleLabel.text = "Edit Meal"
            id = mealDetail?.meal_id ?? 0
        } else {
                removeButton.isHidden = true
                titleLabel.text = "Add Meal"
            //selectImageView.isHidden = true
        }
    }

    
    @objc func dismissView() {
        delegate?.hideView()
        dismiss(animated: true, completion: nil)
    }
    
    func openCamera(){
        let imagePicker = UIImagePickerController()
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            print("Button capture")
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true//false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func openGallery(){
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker,animated: true,completion: nil)
    }
         
    func openGif(){
        let vc =  STORYBOARD.myWorkout.instantiateViewController(withIdentifier: "ListOfGifsScreen") as! ListOfGifsScreen
        vc.modalPresentationStyle = .overFullScreen
        vc.isFromWorkout = false
        vc.delegate = self
        self.present(vc, animated: false, completion: nil)
    }
    
    func openFitnessLibary(){
        let vc =  STORYBOARD.myWorkout.instantiateViewController(withIdentifier: "ListOfImageScreen") as! ListOfImageScreen
        vc.modalPresentationStyle = .overFullScreen
        vc.isFromWorkout = false
        vc.delegate = self
        self.present(vc, animated: false, completion: nil)
    }
    
    func checkValidation() -> String?{
        if self.titleTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter your title meal"
        } 
        return nil
    }
    
    func goFurther(){
        self.dismiss(animated: false, completion: { [weak self] in
            self?.dismissScreen?()
        })
    }
    
    @IBAction func onAddPhoto(_ sender: UIButton) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        // Create the actions
        let takePhoto = UIAlertAction(title: "Take photo", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openCamera()
        }
        let galleryPhoto = UIAlertAction(title: "Photo Library", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openGallery()
        }
        
        let fitnessPhoto = UIAlertAction(title: "Fitness Factory Photo Library", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openFitnessLibary()
        }
        let gif = UIAlertAction(title: "Select gif", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openGif()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        // Add the actions
        alertController.addAction(takePhoto)
        alertController.addAction(galleryPhoto)
        alertController.addAction(cancelAction)
//        alertController.addAction(fitnessPhoto)
//        alertController.addAction(gif)
        //   Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func onSave(_ sender: UIButton) {
        self.view.endEditing(true)
        if let error = self.checkValidation(){
            Utility.showAlert(vc: self, message: error)
        } else {
            self.delegate?.hideView()
            if isEdit {
                editMeal(id: id)
            } else {
                createMealAPI()
            }
        }
    }
    
    @IBAction func onRemove(_ sender: UIButton) {
        self.view.endEditing(true)
        self.dismiss(animated: false, completion: { [weak self] in
            self?.dismissRemoveScreen?()
        })
    }
}

extension AddMealScreen :UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else {
            return
        }
        mealImageview.image = pickedImage
        //        profileImageView.layer.borderWidth = 1.5
        //        editIconImage.isHidden = false
        //        plusIImageView.isHidden = true
        selectView.isHidden = true
        self.dismiss(animated: true, completion: { () -> Void in
        })
    }
}

extension AddMealScreen {
//    func createMealAPI(){
//            self.view.endEditing(true)
//            if Utility.isInternetAvailable(){
//                Utility.showIndecator()
//                    var imageData:Data?
//                    if(mealImageview.image != nil){
//                        imageData = mealImageview.image!.jpegData(compressionQuality:0.4)!
//                    }
//                    let data = AddMealRequest(title: titleTextField.text)
//                    NutritionService.shared.CreateMeal(imageData: imageData,parameters: data.toJSON()) { [weak self] (statusCode, response) in
//                        Utility.hideIndicator()
//                        if let res = response{
//                            print(res.toJSON())
//                            self?.goFurther()
//                        }
//                    } failure: { [weak self] (error) in
//                        guard let stronSelf = self else { return }
//                        Utility.hideIndicator()
//                        Utility.showAlert(vc: stronSelf, message: error)
//                    }
//            }else{
//                Utility.hideIndicator()
//                Utility.showNoInternetConnectionAlertDialog(vc: self)
//            }
//        }
    
    func createMealAPI(){
            self.view.endEditing(true)
            if Utility.isInternetAvailable(){
                Utility.showIndecator()
                let data = AddMealRequest(title: titleTextField.text, desc: descriptionTextView.text)
                   // NutritionService.shared.CreateOwnMeal(parameters: data.toJSON()) { [weak self] (statusCode, response) in
                var imageData:Data?
                if(mealImageview.image != nil){
                    imageData = mealImageview.image!.jpegData(compressionQuality:0.4)!
                }
                        NutritionService.shared.CreateMeal(imageData: imageData,parameters: data.toJSON()) { [weak self] (statusCode, response) in
                        Utility.hideIndicator()
                        if let res = response?.selectmeal{
                            print(res.toJSON())
                            self?.dismiss(animated: false, completion: { [weak self] in
                                self?.createMeal!(res)
//                                guard let strongSelf = self else {
//                                    return
//                                }
//                                let viewControllers: [UIViewController] = strongSelf.navigationController?.viewControllers
//                                for aViewController in viewControllers {
//                                    if aViewController is CreateRecipeScreen {
//                                        if let vc = aViewController as? CreateRecipeScreen{
//                                            vc.initalizedDetails()
//                                        }
//                                        strongSelf.navigationController!.popToViewController(aViewController, animated: true)
//                                    }
//                                }
                            })
                        }
                    } failure: { [weak self] (error) in
                        guard let stronSelf = self else { return }
                        Utility.hideIndicator()
                        Utility.showAlert(vc: stronSelf, message: error)
                    }
            }else{
                Utility.hideIndicator()
                Utility.showNoInternetConnectionAlertDialog(vc: self)
            }
        }
    
    func editMeal(id:Int){
            self.view.endEditing(true)
            if Utility.isInternetAvailable(){
                Utility.showIndecator()
                let data = EditMealRequest(title: titleTextField.text, id: String(id), desc: descriptionTextView.text)
                    var imageData:Data?
                    if(mealImageview.image != nil){
                        imageData = mealImageview.image!.jpegData(compressionQuality:0.4)!
                    }
                    NutritionService.shared.editMeal(imageData: imageData,parameters: data.toJSON()) { [weak self] (statusCode, response) in
                        self?.mealDetail?.image = response?.selectmeal?.image
                        self?.mealDetail?.title = response?.selectmeal?.title
                        Utility.hideIndicator()
                        if let res = response?.selectmeal{
                            print(res.toJSON())
                            self?.backGroundView.removeGestureRecognizer(self!.tap)
                            self?.updateMeal!(res)
                            self?.dismiss(animated: true, completion: nil)
                        }
                    } failure: { [weak self] (error) in
                        guard let stronSelf = self else { return }
                        Utility.hideIndicator()
                        Utility.showAlert(vc: stronSelf, message: error)
                    }
            }else{
                Utility.hideIndicator()
                Utility.showNoInternetConnectionAlertDialog(vc: self)
            }
        }
}

extension AddMealScreen : SelectImage {
    func selectImageOfWorkout(selectImg: String) {
        selectView.isHidden = true
        Utility.setImage(selectImg, imageView: mealImageview)
    }
}

protocol hideBackgroundDelegate {
    func hideView()
}

extension AddMealScreen : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == descriptionTextView {
            
        }
    }
}
