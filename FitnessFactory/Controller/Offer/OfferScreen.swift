//
//  OfferScreen.swift
//  FitnessFactory
//
//  Created by iroid on 28/06/21.
//

import UIKit
import SkeletonView

class OfferScreen: UIViewController {
    
    @IBOutlet weak var offerTableView: UITableView!
    
    var offerList = [GymOfferResponse]()
    var hasMorePage : Bool = false
    var page = 1
    var meta:Meta!
    var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initalizedDetails()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- initalizedDetails Details
    func initalizedDetails(){
        let nib = UINib(nibName: "OfferCell", bundle: nil)
        offerTableView.register(nib, forCellReuseIdentifier: "OfferCell")
        offerTableView.tableFooterView = UIView()
        offerTableView.rowHeight = 202
        offerTableView.estimatedRowHeight = 202
        offerTableView.isSkeletonable = true
        let gradient = SkeletonGradient(baseColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.1310794454))
        offerTableView.showAnimatedGradientSkeleton(usingGradient: gradient, animation: nil, transition: .crossDissolve(0.5))
        offerListAPI(Page: 1)
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(refreshOfferList), for: UIControl.Event.valueChanged)
        offerTableView.addSubview(refreshControl)
    }
    
    // MARK: scroll method
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let size = scrollView.contentSize
        let inset = scrollView.contentInset
        let y = offset.y + bounds.size.height - inset.bottom
        let h = size.height
        let reload_distance:CGFloat = 10.0
        if y > (h + reload_distance) {
            print("called")
            if let metaTotal = self.meta?.total{
                if self.offerList.count != metaTotal{
                    if self.hasMorePage{
                        self.hasMorePage = false
                        self.page += 1
                        offerListAPI(Page: page)
                    }
                }
            }
        }
    }
    
    func appendDataCollectionView(data: [GymOfferResponse]){
        var indexPathArray: [IndexPath] = []
        for i in self.offerList.count..<self.offerList.count + data.count{
            indexPathArray.append(IndexPath(item: i, section: 0))
        }
        self.offerList.append(contentsOf: data)
        self.offerTableView.insertRows(at: indexPathArray, with: .automatic)
    }
    
    @objc func refreshOfferList() {
        refreshControl.endRefreshing()
        offerListAPI(Page: 1)
    }
    
    @IBAction func onProfile(_ sender: Any) {
        tabBarController?.tabBar.isHidden = true
        let storyBoard: UIStoryboard = UIStoryboard(name: "Profile", bundle: nil)
         let vc = storyBoard.instantiateViewController(withIdentifier: "ProfileScreen") as! ProfileScreen

         let navigationController = self.navigationController


         vc.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Close", style: .plain, target: vc, action: #selector(vc.closeView))
         vc.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: vc, action: nil)
        
         let transition = CATransition()
         transition.duration = 0.2
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.moveIn
        transition.subtype = CATransitionSubtype.fromLeft
         navigationController?.view.layer.add(transition, forKey: nil)
         navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func onHome(_ sender: UIButton) {
        //tabBarController?.tabBar.isHidden = false
        tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: true)
    }
    
}

extension OfferScreen : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        var numOfSections: Int = 0
//        if offerList.count > 0 {
//            numOfSections = offerList.count
//            tableView.backgroundView = nil
//        } else {
//            let noDataLabel = UILabel(frame: CGRect(x: 0, y: 50, width: tableView.bounds.size.width, height: 50))
//            noDataLabel.text = "Currently , there is no list."
//            noDataLabel.textColor = UIColor.darkGray
//            noDataLabel.textAlignment = .center
//            noDataLabel.numberOfLines = 1
//            tableView.backgroundView = noDataLabel
//        }
//        return numOfSections
        return offerList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OfferCell") as! OfferCell
        cell.item = offerList[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectOffer = offerList[indexPath.row]
        if selectOffer.website != nil {
            UIApplication.shared.openURL(URL(string:selectOffer.website ?? "")!)
        }
    }
}

extension OfferScreen {
    //MARK:- Offer List API
    func offerListAPI(Page:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
          //  Utility.showIndecator()
            OfferService.shared.gymOfferList(Page: Page) { [self] (statusCode, response) in
                Utility.hideIndicator()
                self.hasMorePage = true
                if let res = response.gymOfferResponse{
                    if page == 1{
                        self.offerList = res
                        print(self.offerList.toJSON())
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            self.offerTableView.stopSkeletonAnimation()
                            self.view.hideSkeleton(reloadDataAfter: true, transition: .crossDissolve(0.25))
                            if offerList.count > 0 {
                                offerTableView.backgroundView = nil
                            } else {
                                let noDataLabel = UILabel(frame: CGRect(x: 0, y: 50, width: offerTableView.bounds.size.width, height: 50))
                                noDataLabel.text = "No Offers available at the moment."
                                noDataLabel.textColor = UIColor.darkGray
                                noDataLabel.textAlignment = .center
                                noDataLabel.numberOfLines = 1
                                offerTableView.backgroundView = noDataLabel
                            }
                            self.offerTableView.reloadData()
                        }
                        
                    } else {
                        self.appendDataCollectionView(data: res)
                    }
                    if let meta = response.metaResponse{
                        self.meta = meta
                    }

                    
                }
                
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}

extension OfferScreen : SkeletonTableViewDataSource {
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "OfferCell"
    }
    
    func collectionSkeletonView(_ skeletonView: UITableView, numberOfRowsInSection section: Int) -> Int {
        2
    }
}
