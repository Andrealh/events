//
//  OnBoardingThirdScreen.swift
//  FitnessFactory
//
//  Created by iroid on 07/06/21.
//

import UIKit

class OnBoardingThirdScreen: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func onStart(_ sender: UIButton) {
        let controller = STORYBOARD.login.instantiateViewController(withIdentifier:"LogInScreen") as! LogInScreen
        navigationController?.pushViewController(controller, animated: true)
    }
}
