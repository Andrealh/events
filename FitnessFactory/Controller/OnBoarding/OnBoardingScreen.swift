//
//  OnBoardingScreen.swift
//  FitnessFactory
//
//  Created by iroid on 07/06/21.
//

import UIKit

class OnBoardingScreen: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func onSkip(_ sender: UIButton) {
        let controller = STORYBOARD.login.instantiateViewController(withIdentifier:"LogInScreen") as! LogInScreen
        navigationController?.pushViewController(controller, animated: true)
    }
    @IBAction func onNext(_ sender: UIButton) {
        let controller = STORYBOARD.onBoarding.instantiateViewController(withIdentifier:"OnBoardingSecondScreen") as! OnBoardingSecondScreen
        navigationController?.pushViewController(controller, animated: true)
    }
}
