//
//  TabBarScreen.swift
//  FitnessFactory
//
//  Created by iroid on 17/06/21.
//

import UIKit

class TabBarScreen: UITabBarController {

    var isFromLink : Bool = false
    var isFromSkip : Bool = false
    var gymDetail : GymLinkStatusResponse?
    let gradientlayer = CAGradientLayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTabBarControllers()
//        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        // Do any additional setup after loading the view.
//        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func setTabBarControllers() {
        
        self.tabBar.barTintColor = UIColor.black
//        self.navigationController?.isNavigationBarHidden = true
//
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor:UIColor(red: 0.804, green: 0.949, blue: 0.439, alpha: 1)], for: .selected)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor:UIColor(red: 0.804, green: 0.949, blue: 0.439, alpha: 0.5)], for: .normal)
        
        let HomeController = STORYBOARD.home.instantiateViewController(withIdentifier:"BreakfastScreen") as! BreakfastScreen
        HomeController.isFromSkip = isFromSkip
        HomeController.isFromLink = isFromLink
        HomeController.gymDetail = gymDetail
        let NutritionController = STORYBOARD.nutrition.instantiateViewController(withIdentifier:"NutritionScreen") as! NutritionScreen
        let CalenderController = STORYBOARD.calender.instantiateViewController(withIdentifier:"CalenderScreen") as! CalenderScreen
        let ExerciseController = STORYBOARD.exercise.instantiateViewController(withIdentifier:"ExerciseScreen") as! ExerciseScreen
        let OfferController = STORYBOARD.offer.instantiateViewController(withIdentifier:"OfferScreen") as! OfferScreen
        
        let HomeNavigationController = UINavigationController(rootViewController: HomeController)
        let NutritionNavigationController = UINavigationController(rootViewController: NutritionController )
        let CalenderNavigationController = UINavigationController(rootViewController: CalenderController)
        let ExerciseNavigationController = UINavigationController(rootViewController: ExerciseController )
        let OfferNavigationController = UINavigationController(rootViewController: OfferController)
        
        HomeNavigationController.navigationBar.isHidden = true
        NutritionNavigationController.navigationBar.isHidden = true
        CalenderNavigationController.navigationBar.isHidden = true
        ExerciseNavigationController.navigationBar.isHidden = true
        OfferNavigationController.navigationBar.isHidden = true
        
//        HomeNavigationController.interactivePopGestureRecognizer?.isEnabled = true
//        NutritionNavigationController.interactivePopGestureRecognizer?.isEnabled = true
//        CalenderNavigationController.interactivePopGestureRecognizer?.isEnabled = true
//        ExerciseNavigationController.interactivePopGestureRecognizer?.isEnabled = true
//        OfferNavigationController.interactivePopGestureRecognizer?.isEnabled = true
        
        HomeNavigationController.title = "Home"
        NutritionNavigationController.title = "Nutrition"
        CalenderNavigationController.title = "Calendar"
        ExerciseNavigationController.title = "Exercise"
        OfferNavigationController.title = "Offers"
        
        
        
        viewControllers = [HomeNavigationController,ExerciseNavigationController, NutritionNavigationController, CalenderNavigationController,OfferNavigationController]
        
        tabBar.items?[0].image = UIImage(named: "Home")
        tabBar.items?[1].image = UIImage(named: "Exercise")
        tabBar.items?[2].image = UIImage(named: "Nutrition")
        tabBar.items?[3].image = UIImage(named: "Calendar")
        tabBar.items?[4].image = UIImage(named: "Offer")
        let SelectedState:[String] = ["Home", "Exercise","Nutrition","cal","Offer"]
        let UnselectedState: [String] = ["home_un","Exe_un","nutr_un","cal_un","offer_un"]
        
        if let count = self.tabBar.items?.count {
            for i in 0...(count-1) {
                let SelectedState   = SelectedState[i]
               let UnselectedState = UnselectedState[i]
                self.tabBar.items?[i].selectedImage = UIImage(named: SelectedState)?.withRenderingMode(.alwaysOriginal)
                self.tabBar.items?[i].image = UIImage(named: UnselectedState)?.withRenderingMode(.alwaysOriginal)
            }
        }
        //selectedViewController = viewControllers?[0]
    }
}
