//
//  CalenderScreen.swift
//  FitnessFactory
//
//  Created by iroid on 28/06/21.
//

import UIKit
import FSCalendar
import SwipeMenuViewController

class CalenderScreen: UIViewController ,FSCalendarDelegate ,FSCalendarDataSource ,FSCalendarDelegateAppearance  {
    
    @IBOutlet weak var workoutTitleLabel: UILabel!
    @IBOutlet weak var workoutLineLabel: UILabel!
    @IBOutlet weak var nutritionListLabel: UILabel!
    @IBOutlet weak var nutritionListLineLabel: UILabel!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var eventsListLabel: UILabel!
    @IBOutlet weak var eventsListLineLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var ConstraintView: NSLayoutConstraint!
    @IBOutlet weak var swipeMenuView: SwipeMenuView!
    @IBOutlet weak var gymDayView: UIView!
    @IBOutlet weak var notGymDayView: UIView!
    @IBOutlet weak var workoutView: UIView!
    
    @IBOutlet weak var workoutCalender: FSCalendar!
    
    @IBOutlet weak var addMoreButton: UIButton!
    @IBOutlet weak var bottomView: NSLayoutConstraint!
    @IBOutlet weak var swipeViewHieghtConstraint: NSLayoutConstraint!

    var selectedBreakFast = 1
    var currentMonth = String()
    var selectDate = String()
    var currentDate = String()
    var swipeToGymDaydate = String()
    var eventList : [EventListResponse] = []
    var nutritionList : [NutritionListResponse] = []
    var workoutList : [WorkoutResponse] = []
    var calenderList : [WorkoutDayResponse] = []
    var holidayDayList = [String]()
    let dateFormatter = DateFormatter()
    var meta:Meta!
    var hasMorePage : Bool = false
    var page = 1
    var vc1 = ExerciseListScreen()
    var vc2 = NutritionListScreen()
    var vc3 = EventListScreen()
    var index = Int()
    var itemArray = [String]()
    var viewControllerArray: [UIViewController] = []
    var didChangeCalender = false
    var isHoliday = Bool()
    fileprivate let formatter: DateFormatter = {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initalizedDetails()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        self.workoutCalender.deselect(selectDateOfCalender)
//        workoutCalender.setCurrentPage(Date(), animated: true)
//        currentDate = Date().string(format: "yyyy-MM-dd")
//        calenderListAPI(selectDate: selectDate)
//        selectedBreakFast = 1
//        setUpSwipeMenu(selectDate: currentDate)
//        setMonthName()
//        setUpData()
//        selectedBreakFast = 1
//        setUpSelectedoption()
//        setUpSwipeMenu(selectDate: currentDate)
    }
    
    //MARK:- initalizedDetails Details
    func initalizedDetails(){
        workoutCalender.delegate = self
        workoutCalender.dataSource = self
        workoutCalender.headerHeight = 0
        workoutCalender.placeholderType = .none

        workoutCalender.customizeCalenderAppearance()
        selectDate = Date().string(format: "yyyy-MM")
        currentDate = Date().string(format: "yyyy-MM-dd")
        swipeToGymDaydate = currentDate
        dateFormatter.dateFormat = "yyyy-MM"
        self.workoutTitleLabel.textColor = UIColor.calendarSwipeMenuHighEmphasisText
        self.workoutLineLabel.backgroundColor = UIColor.calendarButtonTextStyle1
        workoutCalender.placeholderType = .none
        calenderListAPI(selectDate: selectDate)
        setUpSwipeMenu(selectDate: currentDate)
        setMonthName()
    }
    
    func setUpSelectedoption(){
        if selectedBreakFast == 1 {
            self.workoutTitleLabel.textColor = UIColor.calendarSwipeMenuHighEmphasisText
            self.workoutLineLabel.backgroundColor = UIColor.calendarButtonTextStyle1
            self.nutritionListLineLabel.backgroundColor = UIColor.clear
            self.nutritionListLabel.textColor = UIColor.calendarSwipeMenuMediumEmphasisText
            self.eventsListLineLabel.backgroundColor = UIColor.clear
            self.eventsListLabel.textColor = UIColor.calendarSwipeMenuMediumEmphasisText
        } else if selectedBreakFast == 2 {
            self.nutritionListLabel.textColor = UIColor.calendarSwipeMenuHighEmphasisText
            self.nutritionListLineLabel.backgroundColor = UIColor.calendarButtonTextStyle1
            self.workoutLineLabel.backgroundColor = UIColor.clear
            self.workoutTitleLabel.textColor = UIColor.calendarSwipeMenuMediumEmphasisText
            self.eventsListLineLabel.backgroundColor = UIColor.clear
            self.eventsListLabel.textColor = UIColor.calendarSwipeMenuMediumEmphasisText
        } else if selectedBreakFast == 3 {
            self.eventsListLabel.textColor = UIColor.calendarSwipeMenuHighEmphasisText
            self.eventsListLineLabel.backgroundColor = UIColor.calendarButtonTextStyle1
            self.workoutLineLabel.backgroundColor = UIColor.clear
            self.workoutTitleLabel.textColor = UIColor.calendarSwipeMenuMediumEmphasisText
            self.nutritionListLineLabel.backgroundColor = UIColor.clear
            self.nutritionListLabel.textColor = UIColor.calendarSwipeMenuMediumEmphasisText
        }
    }
    
    func setUpData() {
        workoutList = []
        nutritionList = []
        eventList = []
        //onGymDay(selectdate: currentDate)
        if holidayDayList.contains(currentDate) {
            isHoliday = true
            onHoliday(selectdate: currentDate)
        } else {
            isHoliday = false
            onGymDay(selectdate: currentDate)
        }
    }
    
    func setUpDataForSwipeGymDay() {
        workoutList = []
        nutritionList = []
        eventList = []
        //onGymDay(selectdate: currentDate)
        if holidayDayList.contains(swipeToGymDaydate) {
            isHoliday = true
            onHoliday(selectdate: swipeToGymDaydate)
        } else {
            isHoliday = false
            onGymDay(selectdate: swipeToGymDaydate)
        }
    }
    
    func swipeGymDay() {
        workoutList = []
        nutritionList = []
        eventList = []
        //onGymDay(selectdate: currentDate)
        if holidayDayList.contains(swipeToGymDaydate) {
            isHoliday = true
            onSwipeHoliday()
           // onHoliday(selectdate: swipeToGymDaydate)
        } else {
            isHoliday = false
            onSwipeGymDay()
            //onGymDay(selectdate: swipeToGymDaydate)
        }
    }
    @IBAction func calFormatChnage(){
          
          if(workoutCalender.scope == .week){
              
              self.workoutCalender.scope = .month
              workoutCalender.isHidden = true
              ConstraintView.constant = 270
              bottomView.constant = 0
              
              UIView.animate(withDuration: 0.5) {
                  
                  self.view.layoutIfNeeded()
                 
                  
              }
              let seconds = 0.45
              DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
                  // Put your code which should be executed with a delay here
                  self.workoutCalender.isHidden = false
              }
              
          }else{
              
              ConstraintView.constant = 120
              
              
              bottomView.constant = -50
              UIView.animate(withDuration: 0.5) {
                  self.view.layoutIfNeeded()
                  self.workoutCalender.scope = .week
              }
          }
    }
    //MARK:- setUpSwipeMenu
    func setUpSwipeMenu(selectDate:String){
        vc1 = STORYBOARD.home.instantiateViewController(withIdentifier: "ExerciseListScreen") as! ExerciseListScreen
        vc1.vc = self
        vc1.isFromHome = false
        vc1.selectDate = selectDate
       // vc1.delegate = self
        vc2 = STORYBOARD.home.instantiateViewController(withIdentifier: "NutritionListScreen") as! NutritionListScreen
        vc2.vc = self
        vc2.isFromHome = false
       // vc2.delegate = self
        vc2.selectDate = selectDate
        vc3 = STORYBOARD.home.instantiateViewController(withIdentifier: "EventListScreen") as! EventListScreen
        vc3.selectDate = selectDate
       // vc3.delegate = self
        //            vc3.isRideHistory = false
        swipeMenuView.dataSource = self
        swipeMenuView.delegate = self
       
//        if isHoliday{
//            viewControllerArray = [vc2,vc3]
//            itemArray = ["",""]
//        } else {
            viewControllerArray = [vc1,vc2,vc3]
            itemArray = ["","",""]
//        }
        
        var options: SwipeMenuViewOptions = .init()
        if addMoreButton.isHidden {
            options.tabView.height = 70
        } else {
            options.tabView.height = 100
        }
        
        options.tabView.itemView.selectedTextColor = #colorLiteral(red: 0.2745098039, green: 0.8980392157, blue: 0.8235294118, alpha: 1)
        options.tabView.itemView.textColor   =  #colorLiteral(red: 0.5568627451, green: 0.5568627451, blue: 0.5568627451, alpha: 1)
        options.tabView.additionView.backgroundColor  = .clear
        
        options.tabView.needsAdjustItemViewWidth = false
      //  if isHoliday{
       //     options.tabView.itemView.width = self.view.frame.width / 2
       // } else {
            options.tabView.itemView.width = self.view.frame.width / 3
    //    }
        
        // options.tabView.itemView.font = UIFont(name: "NunitoSans-Bold", size: 14)!
        swipeMenuView.reloadData(options: options)
        swipeMenuView.jump(to: selectedBreakFast - 1, animated: true)
    }
    
    func onSwipeHoliday() {
        notGymDayView.isHidden = false
        gymDayView.isHidden = true
    }
    
    func onHoliday(selectdate:String)  {
        notGymDayView.isHidden = false
        gymDayView.isHidden = true
        setUpSelectedoption()
        setUpSwipeMenu(selectDate: selectdate)
    }
    
    func onSwipeGymDay() {
        notGymDayView.isHidden = true
        gymDayView.isHidden = false
    }
    
    func onGymDay(selectdate:String)   {
        gymDayView.isHidden = false
        notGymDayView.isHidden = true
        workoutView.isHidden = false
        setUpSelectedoption()
        setUpSwipeMenu(selectDate: selectdate)
    }
    
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
            currentDate = formatter.string(from: date)
            swipeToGymDaydate = formatter.string(from: date)
            workoutList = []
            nutritionList = []
            eventList = []
            
       // onGymDay(selectdate: currentDate)
            if holidayDayList.contains(currentDate) {
                isHoliday = true
                onHoliday(selectdate: currentDate)
            } else {
                isHoliday = false
                onGymDay(selectdate: currentDate)
            }
            // DispatchQueue.main.asyncAfter(deadline: .now() + 0.1){
            //            self.workoutCalender.reloadData()
            guard #available(iOS 13.0, *) else {
                // Code for earlier iOS versions
                if let cell = calendar.cell(for: date, at: monthPosition) {
                    let index = calendar.collectionView.indexPath(for: cell as UICollectionViewCell)!
                    calendar.collectionView.reloadItems(at: [index])
                }
                    return
                }
        
        //           }
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let dateString = self.formatter.string(from: date)
        currentDate = Date().string(format: "yyyy-MM-dd")
        //Remove timeStamp from date
        if dateString == currentDate {
            return UIColor.calendarButtonTextStyle1
        } else if self.holidayDayList.contains(dateString) {
            return #colorLiteral(red: 0.9459537864, green: 0.4344328642, blue: 0.4142383933, alpha: 1)
        } else if date.removeTimeStamp!.compare(Date().removeTimeStamp!) == .orderedAscending {
            return UIColor(red: 0.027, green: 0.027, blue: 0.024, alpha: 0.3)
        } else if date.removeTimeStamp!.compare(Date().removeTimeStamp!) == .orderedDescending{
            return .black
        } else {
            return .black
        }
    }
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        print(Date())
        if calendar.currentPage.string(format: "yyyy-MM") ==  Date().string(format: "yyyy-MM"){
            didChangeCalender = false
        }else{
            didChangeCalender = true
        }
        selectDate = calendar.currentPage.string(format: "yyyy-MM")
        calenderListAPI(selectDate: selectDate)
        setMonthName()
    }
    
    func getNextMonth(date:Date)->Date {
        return  Calendar.current.date(byAdding: .month, value: 1, to:date)!
    }
    func getNextWeek(date:Date)->Date{
        return Calendar.current.date(byAdding: .weekOfYear, value: 1, to: date)!
    }
    
    func getPreviousMonth(date:Date)->Date {
        return  Calendar.current.date(byAdding: .month, value: -1, to:date)!
    }
    func getPreviousWeek(date:Date)->Date {
        return  Calendar.current.date(byAdding: .weekOfYear, value: -1, to:date)!
    }
    func setMonthName(){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM yyyy"
        monthLabel.text = dateFormatter.string(from: self.workoutCalender.currentPage)
    }
    
    @IBAction func onNext(_ sender: UIButton) {
        if(workoutCalender.scope == .week){
            let nextWeek  = getNextWeek(date: workoutCalender.currentPage)
            selectDate = dateFormatter.string(from: nextWeek)
            workoutCalender.setCurrentPage(nextWeek, animated: true)
            calenderListAPI(selectDate: selectDate)
            setMonthName()
        }else{
        let nextMonth  = getNextMonth(date: workoutCalender.currentPage)
        selectDate = dateFormatter.string(from: nextMonth)
        workoutCalender.setCurrentPage(nextMonth, animated: true)
        calenderListAPI(selectDate: selectDate)
        setMonthName()
        }
    }
    
    @IBAction func onPrevious(_ sender: UIButton) {
        if(workoutCalender.scope == .week){
            let nextWeek  = getPreviousWeek(date: workoutCalender.currentPage)
            selectDate = dateFormatter.string(from: nextWeek)
            workoutCalender.setCurrentPage(nextWeek, animated: true)
            calenderListAPI(selectDate: selectDate)
            setMonthName()
        }else{
                let nextMonth  = getPreviousMonth(date: workoutCalender.currentPage)
                selectDate = dateFormatter.string(from: nextMonth)
                workoutCalender.setCurrentPage(nextMonth, animated: true)
                calenderListAPI(selectDate: selectDate)
                setMonthName()
        }
    }
    
    
//    let nextMonth  = getPreviousMonth(date: workoutCalender.currentPage)
//    selectDate = dateFormatter.string(from: nextMonth)
//    workoutCalender.setCurrentPage(nextMonth, animated: true)
//    calenderListAPI(selectDate: selectDate)
//    setMonthName()
    @IBAction func onCurrentDate(_ sender: UIButton) {
        workoutCalender.setCurrentPage(Date(), animated: true)
    }
    
    @IBAction func onGymDay(_ sender: UIButton) {
        swipeToGymDay(selectDate: swipeToGymDaydate)
    }
    
    @IBAction func onNotGymDay(_ sender: UIButton) {
        swipeToGymDay(selectDate: swipeToGymDaydate)
    }
    
    @IBAction func onAddMore(_ sender: UIButton) {
        if selectedBreakFast == 1  {
            let controller = STORYBOARD.myWorkout.instantiateViewController(withIdentifier:"MyWorkoutScreen") as! MyWorkoutScreen
            controller.pickWorkoutId = 0
            controller.isFromAddMore = true
            self.navigationController?.pushViewController(controller, animated: true)
        } else {
            let controller = STORYBOARD.nutrition.instantiateViewController(withIdentifier:"MyMealScreen") as! MyMealScreen
            controller.isFromCalender = true
            controller.isFormAddMore = true
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    @IBAction func onProfile(_ sender: UIButton) {
        tabBarController?.tabBar.isHidden = true
        let storyBoard: UIStoryboard = UIStoryboard(name: "Profile", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ProfileScreen") as! ProfileScreen
        
        let navigationController = self.navigationController
        
        vc.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Close", style: .plain, target: vc, action: #selector(vc.closeView))
        vc.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: vc, action: nil)
        
        let transition = CATransition()
        transition.duration = 0.2
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.moveIn
        transition.subtype = CATransitionSubtype.fromLeft
        navigationController?.view.layer.add(transition, forKey: nil)
        navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func onHome(_ sender: UIButton) {
        //tabBarController?.tabBar.isHidden = false
        tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: true)
    }
}

extension CalenderScreen: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        var numOfSections: Int = 0
        
        if selectedBreakFast == BreakfastType.Excercises.rawValue {
            
            if workoutList.count > 0 {
                numOfSections = 1
                tableView.backgroundView = nil
            } else {
                let noDataLabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
                noDataLabel.text = "No Workouts today."
                noDataLabel.textColor = UIColor.darkGray
                noDataLabel.textAlignment = .center
                noDataLabel.numberOfLines = 1
                tableView.backgroundView = noDataLabel
            }
            
            return numOfSections
            
        } else  if selectedBreakFast == BreakfastType.Nutrition.rawValue {
            
            if nutritionList.count > 0 {
                numOfSections = 1
                tableView.backgroundView = nil
            } else {
                let noDataLabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
                noDataLabel.text = "No Meals today."
                noDataLabel.textColor = UIColor.darkGray
                noDataLabel.textAlignment = .center
                noDataLabel.numberOfLines = 1
                tableView.backgroundView = noDataLabel
            }
            
            return numOfSections
            
        } else {
            
            if eventList.count > 0 {
                numOfSections = 1
                tableView.backgroundView = nil
            } else {
                let noDataLabel = UILabel(frame: CGRect(x: 0, y: 100, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
                noDataLabel.text = "No Events today."
                noDataLabel.textColor = UIColor.darkGray
                noDataLabel.textAlignment = .center
                noDataLabel.numberOfLines = 1
                tableView.backgroundView = noDataLabel
            }
            return numOfSections
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectedBreakFast == BreakfastType.Excercises.rawValue {
            return workoutList.count
        } else  if selectedBreakFast == BreakfastType.Nutrition.rawValue {
            return nutritionList.count
        } else {
            return eventList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if selectedBreakFast == BreakfastType.Excercises.rawValue{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ExcercisesCell", for: indexPath) as! ExcercisesCell
            cell.itemWorkoutResponse = workoutList[indexPath.row]
            return cell
        } else if selectedBreakFast == BreakfastType.Nutrition.rawValue{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ExcercisesCell", for: indexPath) as! ExcercisesCell
            cell.nutritionList = nutritionList[indexPath.row]
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EventsTableViewCell", for: indexPath) as! EventsTableViewCell
            cell.item = eventList[indexPath.row]
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectedBreakFast == BreakfastType.Excercises.rawValue{
            //tabBarController?.tabBar.isHidden = true
            //            let controller = STORYBOARD.exercise.instantiateViewController(withIdentifier:"ExerciseNameListScreen") as! ExerciseNameListScreen
            //            controller.exerciseId = workoutList[indexPath.row].workout_id ?? 0
            //            controller.titleName = workoutList[indexPath.row].title ?? ""
            //            controller.isFromHome = true
            //            self.navigationController?.pushViewController(controller, animated: true)
            
            let controller = STORYBOARD.myWorkout.instantiateViewController(withIdentifier:"WorkoutDetailsScreen") as! WorkoutDetailsScreen
            controller.WorkoutId = workoutList[indexPath.row].workout_id ?? 0
            controller.isFromCalenderScreen = true
            //            controller.delegate = self
            self.navigationController?.pushViewController(controller, animated: true)
            
        }  else if selectedBreakFast == BreakfastType.Nutrition.rawValue {
            //tabBarController?.tabBar.isHidden = true
            let controller = STORYBOARD.nutrition.instantiateViewController(withIdentifier:"MealItelListScreen") as! MealItelListScreen
            controller.id = nutritionList[indexPath.row].nutrition_recipe__id ?? 0
            controller.titleName = nutritionList[indexPath.row].title ?? ""
            controller.isFromHome = true
            navigationController?.pushViewController(controller, animated: true)
        }
    }
}

extension FSCalendar {
    func customizeCalenderAppearance() {
        self.appearance.headerTitleFont      = UIFont.init(name: "Montserrat-Bold" , size: 18)
        self.appearance.weekdayFont          = UIFont.init(name: "Montserrat-Medium" , size: 13)
        self.appearance.titleFont            = UIFont.init(name: "Montserrat-Bold" , size: 16)
        self.appearance.headerMinimumDissolvedAlpha = 0.0 // Hide Left Right Month Name
        self.appearance.titlePlaceholderColor = .clear
        self.appearance.borderRadius = 0
        self.appearance.borderSelectionColor = UIColor.black
       
    }
}

extension Date {
    public var removeTimeStamp : Date? {
        guard let date = Calendar.current.date(from: Calendar.current.dateComponents([.year, .month, .day], from: self)) else {
            return nil
        }
        return date
    }
}

extension Date {
    func string(format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}

extension CalenderScreen {
    
    //MARK:- Login API
    func calenderListAPI(selectDate:String){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            let data = WorkoutListRequest(date: selectDate)
            CalenderService.shared.workoutDay(parameters: data.toJSON()) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response?.workoutDayResponse {
                    self?.calenderList = []
                    self?.holidayDayList = []
                    self?.calenderList.append(contentsOf: res)
                    if self?.calenderList.count ?? 0 > 0 {
                        for i in 0...(self?.calenderList.count ?? 0) - 1 {
                            self?.holidayDayList.append(self?.calenderList[i].date ?? "")
                            //holidayDayList.append(contentsOf: calenderList[i].date ?? "")
                        }
                        if !self!.didChangeCalender{
                            self?.setUpData()
                        }
                        
                        DispatchQueue.main.async
                        {
                            self?.workoutCalender.reloadData()
                        }
                    }
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:- Swipe to Gym Status API
    func swipeToGymDay(selectDate:String){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndecator()
            let data = WorkoutListRequest(date: selectDate)
            CalenderService.shared.swipeGymDay(parameters: data.toJSON()) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response?.message {
                    if let index = self?.holidayDayList.firstIndex(where: {$0 == self?.swipeToGymDaydate}){
                        self?.holidayDayList.remove(at: index)
                    } else {
                        self?.holidayDayList.append(self!.swipeToGymDaydate)
                    }
                    //self?.setUpDataForSwipeGymDay()
                    self?.swipeGymDay()
                    DispatchQueue.main.async
                    {
                        self?.workoutCalender.reloadData()
                    }
                    
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        } else {
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}
//MARK:- SwipeMenuViewDelegate
extension CalenderScreen : SwipeMenuViewDelegate,SwipeMenuViewDataSource{
    func swipeMenuView(_ swipeMenuView: SwipeMenuView, viewWillSetupAt currentIndex: Int) {
        print("viewWillSetupAt",currentIndex)
    }
    
    func swipeMenuView(_ swipeMenuView: SwipeMenuView, viewDidSetupAt currentIndex: Int) {
        //swipeMenuView.jump(to: selectedPage, animated: true)
        print("viewDidSetupAt",currentIndex)
    }
    
    func swipeMenuView(_ swipeMenuView: SwipeMenuView, willChangeIndexFrom fromIndex: Int, to toIndex: Int) {
        print(fromIndex)
            if toIndex == 0{
                DispatchQueue.main.async() {
                    self.addMoreButton.isHidden = false
                    self.workoutTitleLabel.textColor = UIColor.calendarSwipeMenuHighEmphasisText
                    self.workoutLineLabel.backgroundColor = UIColor.calendarButtonTextStyle1
                    self.nutritionListLineLabel.backgroundColor = UIColor.clear
                    self.nutritionListLabel.textColor = UIColor.calendarSwipeMenuMediumEmphasisText
                    self.eventsListLineLabel.backgroundColor = UIColor.clear
                    self.eventsListLabel.textColor = UIColor.calendarSwipeMenuMediumEmphasisText
                    self.selectedBreakFast = 1
                    self.swipeViewHieghtConstraint.constant = CGFloat(tableViewCellHeightExercise + 130)
                    self.view.layoutIfNeeded()
                }
            }
            else if toIndex == 1{
                DispatchQueue.main.async() {
                    self.addMoreButton.isHidden = false
                    self.workoutTitleLabel.textColor = UIColor.calendarSwipeMenuMediumEmphasisText
                    self.workoutLineLabel.backgroundColor = UIColor.clear
                    self.nutritionListLineLabel.backgroundColor = UIColor.calendarButtonTextStyle1
                    self.nutritionListLabel.textColor = UIColor.calendarSwipeMenuHighEmphasisText
                    self.eventsListLineLabel.backgroundColor = UIColor.clear
                    self.eventsListLabel.textColor = UIColor.calendarSwipeMenuMediumEmphasisText
                    self.selectedBreakFast = 2
                    self.swipeViewHieghtConstraint.constant = CGFloat(tableViewCellHeightNutrition + 130)
                    self.view.layoutIfNeeded()
                }
            } else {
                DispatchQueue.main.async() {
                    self.addMoreButton.isHidden = true
                    self.workoutTitleLabel.textColor = UIColor.calendarSwipeMenuMediumEmphasisText
                    self.workoutLineLabel.backgroundColor = UIColor.clear
                    self.nutritionListLineLabel.backgroundColor = UIColor.clear
                    self.nutritionListLabel.textColor = UIColor.calendarSwipeMenuMediumEmphasisText
                    self.eventsListLineLabel.backgroundColor = UIColor.calendarButtonTextStyle1
                    self.eventsListLabel.textColor = UIColor.calendarSwipeMenuHighEmphasisText
                    self.selectedBreakFast = 3
                    self.swipeViewHieghtConstraint.constant = CGFloat(tableViewCellHeightEvent + 130)
                    self.view.layoutIfNeeded()
                }
            }
       // }
        
        print(toIndex)
    }
    
    
    func numberOfPages(in swipeMenuView: SwipeMenuView) -> Int {
        return itemArray.count
    }
    
    func swipeMenuView(_ swipeMenuView: SwipeMenuView, titleForPageAt index: Int) -> String {
        return itemArray[index]
    }
    
    func swipeMenuView(_ swipeMenuView: SwipeMenuView, viewControllerForPageAt index: Int) -> UIViewController {
      //  if viewControllerArray.count == index{
       //     return viewControllerArray[0]
      //  }else{
            return viewControllerArray[index]
      //  }
    }
}

